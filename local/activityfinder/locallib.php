<?php
// This file is part of the Local welcome plugin
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package    local
 * @subpackage activityfinder
 * @copyright  2015 onwards Androgogic Pty Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

function local_activityfinder_get_results_where_cond($data, &$flextable, $total = false) 
{
    $params = array();
    $where = array();
    
    if (!$total) {
        list($twhere, $tparams) = $flextable->get_sql_where();
        if (!empty($twhere)) {
            $where[] = $twhere;
            $params = $tparams;
        }
    }

    if (empty($data)) {
        return array($where, $params);
    }
    
    if (!empty($data->moduleid)) {
        $where[] = " cm.module = :moduleid";
        $params['moduleid'] = $data->moduleid;
    }
    
    if (!empty($data->courseid)) {
        $where[] = " cm.course = :courseid";
        $params['courseid'] = $data->courseid;
    }
    
    return array($where, $params);
}

function local_activityfinder_get_results_count($data, &$flextable, $total = false) 
{
    global $DB;
    
    list($where, $params) = local_activityfinder_get_results_where_cond($data, $flextable, $total);

    $sql = "SELECT COUNT(*) FROM {course_modules} cm ";
    if (!empty($where)) {
        $sql .= ' WHERE '.implode(' AND ', $where);
    }
    
    return $DB->count_records_sql($sql, $params);
}

function local_activityfinder_get_results($data, &$flextable) 
{
    global $DB;
    
    list($where, $params) = local_activityfinder_get_results_where_cond($data, $flextable);

    $sql = "SELECT 
            cm.id,
            m.name modulename,
            c.id courseid,
            c.shortname coursename,
            cm.added
        FROM {course_modules} cm 
        LEFT JOIN {modules} m ON cm.module = m.id
        LEFT JOIN {course} c ON cm.course = c.id";
    if (!empty($where)) {
        $sql .= ' WHERE '.implode(' AND ', $where);
    }
    if ($flextable->get_sql_sort()) {
        $sql .= ' ORDER BY '.$flextable->get_sql_sort();
    }
    
    return $DB->get_records_sql($sql, $params, $flextable->get_page_start(), $flextable->get_page_size());
}