<?php
// This file is part of the Local welcome plugin
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package    local
 * @subpackage activityfinder
 * @copyright  2015 onwards Androgogic Pty Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once('../../config.php');
require_once($CFG->dirroot . '/local/activityfinder/locallib.php');
require_once($CFG->dirroot . '/local/activityfinder/search_form.php');

require_login();

$courseid = optional_param('courseid', null, PARAM_INT);
$moduleid = optional_param('moduleid', null, PARAM_INT);
$tsort = optional_param('tsort', null, PARAM_TEXT);
$page = optional_param('page', null, PARAM_TEXT);
$perpage = 30;

$context = context_system::instance();
$PAGE->set_context($context);
$PAGE->set_url('/local/activityfinder/index.php');
$PAGE->set_heading($SITE->fullname);
$PAGE->set_pagelayout('course');
$PAGE->set_title(get_string('pluginname', 'local_activityfinder'));
$PAGE->navbar->add(get_string('pluginname', 'local_activityfinder'));

require_capability('local/activityfinder:view', $context);

echo $OUTPUT->header();
echo $OUTPUT->heading(get_string('pluginname', 'local_activityfinder'), 3, 'main');

$mform = new activityfinder_search_form();
$mform->display();

$data = $mform->get_data();
if (empty($moduleid)) {
    echo $OUTPUT->footer();
    die;
}   
$data->moduleid = $moduleid;
$data->courseid = $courseid;

// We have to add the search parameters here, otherwise the pagination won't work.
$pageurl = new moodle_url($PAGE->url, array(
    'moduleid' => $moduleid, 
    'courseid' => $courseid,
    'tsort' => $tsort,
));

$table = new flexible_table('activities-list');
$table->set_attribute('class', 'activities-list');
$table->define_baseurl($pageurl);
$table->sortable(true, 'added', SORT_DESC);
$table->define_columns(array(
    'coursename',
    'id',
    'added',
));
$table->define_headers(array(
    get_string('course'),
    get_string('activity'),
    get_string('datewhenadded', 'local_activityfinder'),
));
$table->setup();
    
$totalcount = local_activityfinder_get_results_count($data, $table, true);
if (empty($totalcount)) 
{
    echo $OUTPUT->notification(get_string('nothingfound', 'local_activityfinder'));
    echo $OUTPUT->footer();
    die;
}
$matchcount = local_activityfinder_get_results_count($data, $table);

$table->initialbars($totalcount > $perpage);
$table->pagesize($perpage, $matchcount);

echo $OUTPUT->notification(get_string('resultsfound', 'local_activityfinder', $totalcount), 'notifysuccess');

$results = local_activityfinder_get_results($data, $table);
foreach ($results as $result) 
{
    $row = array();
    
    $courseurl = new moodle_url('/course/view.php', array('id' => $result->courseid));
    $row[] = '<a href="'.$courseurl.'" target="_blank">'.$result->coursename.'</a>';
    
    $modurl = new moodle_url('/mod/'.$result->modulename.'/view.php', array('id' => $result->id));
    $row[] = '<a href="'.$modurl.'" target="_blank">#'.$result->id.'</a>';
    
    $row[] = date('d/m/Y H:i:s', $result->added);
    
    $table->add_data($row);
}

$table->print_html();

echo $OUTPUT->footer();
