<?php
// This file is part of the Local welcome plugin
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package    local
 * @subpackage activityfinder
 * @copyright  2015 onwards Androgogic Pty Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.');    // It must be included from a Moodle page.
}

require_once($CFG->dirroot.'/lib/formslib.php');

class activityfinder_search_form extends moodleform 
{
    public function definition() 
    {
        global $DB;
        
        $mform =& $this->_form;
        
        $results = $DB->get_records('modules', null, '', 'id, name');
        $activities = array();
        foreach ($results as $result) {
            $activities[$result->id] = get_string('pluginname', 'mod_'.$result->name);
        }
        asort($activities);
        $mform->addElement('select', 'moduleid', get_string('activitytype', 'local_activityfinder'), $activities);
        
        $mform->addElement('text', 'courseid', get_string('courseid', 'local_activityfinder'), array('size' => 6));
        
        $this->add_action_buttons(false, get_string('search'));
    }
}