<?php

/**
 * Androgogic Sync
 *
 * @author      Keith Buss <kbuss@outlook.com>
 * @version     May 2015
 *
 **/
 
defined('MOODLE_INTERNAL') || die();

function androgogic_sync_load_csvfile($log, $source, $filename, $progressbar=null) {
    global $DB;

    assert(is_object($log), '$log must be an object');
    assert(is_object($source), '$log must be an object');
    assert(is_string($filename), '$filename must be a string');

    if (!is_readable($filename)) {
        throw new Exception("CSV file $filename is not readable");
    }

	ini_set('auto_detect_line_endings',TRUE);
    if (($handle = fopen($filename, 'r')) === FALSE) {
        throw new Exception('unable to read CSV file ' . $filename);
    }
    
	if ($progressbar<>null) {
		$message = 'loading '.basename($filename);
		$total = count(file($filename));
		$progressbar->update_full(0, $message);
	}
	
	try {
		$fieldmapping = androgogic_sync_get_field_mapping($source->id);

		$reccount = 0;
		$linecount = 0;
		while (($data = fgetcsv($handle, 0, $source->csvdelimiter, '"')) !== FALSE) {
			$linecount++;
		
			if ($progressbar<>null) {
				$progressbar->update($linecount, $total, $message);
			}
			// skip first line if CSV file has header row
			if ($linecount > 1 or ( $linecount == 1 and $source->csvheader == 0)) {
				$time = time();
				$new = array('timecreated' => $time, 'runid' => $log->runid, 'sourceid' => $source->id, 'processed' => 0);
				foreach ($fieldmapping->csvcolumns as $dbfieldname => $csvcolumnno) {
					if (isset($data[$csvcolumnno - 1])) {
						$value = androgogic_sync_encode_utf8(trim($data[$csvcolumnno - 1]));
						if ($dbfieldname == 'suspended' or $dbfieldname == 'deleted') {
							// transform other possible values, not case sensitive
							$value = strtolower($value);
							if ($value == 'yes' or $value == 'true') {
								$value = '1';
							} else if ($value == 'no' or $value == 'false') {
								$value = '0';
							} else if ($value <> '1' and $value <> '0') {
								throw new Exception("unable to load CSV file, $dbfieldname field at line $linecount contains invalid data");
							}
						}
						$new[$dbfieldname] = $value;
					}
				}
				$tablename = 'androgogic_sync_' . strtolower($source->element);
				$stagingid = $DB->insert_record($tablename, $new, true, true);
				$reccount++;
					
				if (count($fieldmapping->customfields) > 0) {
					$new = array('stagingid' => $stagingid, 'sourceid' => $source->id);
					foreach ($fieldmapping->customfields as $shortname => $csvcolumnno) {
						if (isset($data[$csvcolumnno - 1])) {
							$new['shortname'] = $shortname;
							$new['data'] = androgogic_sync_encode_utf8($data[$csvcolumnno - 1]);
							$DB->insert_record('androgogic_sync_user_custom', $new, true, true);
						}
					}
				}
			}
	    } 
    } catch (Exception $e) {
    	// ensure that fclose() is executed
	}
    fclose($handle);
	if ($progressbar<>null) {
		$progressbar->update_full(100, $message);
	}
	
    $log->add_to_log(SyncLog::TYPE_INFO, "loaded $reccount records from ".basename($filename));
    return $reccount;
}

function androgogic_sync_get_field_mapping($sourceid) {
    global $DB;

    assert(is_numeric($sourceid), '$sourceid must be numeric');

    // load field mapping
    $fieldmapping = new stdClass();
    $fieldmapping->csvcolumns = array();
    $fieldmapping->customfields = array();

    if ($fields = $DB->get_records('androgogic_sync_field', array('sourceid' => $sourceid))) {
        foreach ($fields as $field) {
            if (substr($field->dbfieldname, 0, 14) == 'profile_field_') {
                $shortname = substr($field->dbfieldname, 14);
                $fieldmapping->customfields[$shortname] = $field->csvcolumnno;
            } else {
                $fieldmapping->csvcolumns[$field->dbfieldname] = $field->csvcolumnno;
            }
        }
    }
    return $fieldmapping;
}

function androgogic_sync_encode_utf8($data) {
	// detect character encoding and convert to UTF8 to prevent MYSQL errors
	if ($data == '') {
		$retval = '';
	} else {
		$retval = iconv(mb_detect_encoding($data, mb_detect_order(), true), 'UTF-8', $data);
	}
	return $retval;
}
         
function androgogic_sync_archive_csvfile($filename) {
    assert(is_string($filename), '$filename must be a string');

	$archivedir = get_config('local_androgogic_sync', 'archive_dir');	
    if (empty($archivedir)) {
        throw new Exception('Archive directory setting is required');
    }

    if (!is_dir($archivedir)) {
        throw new Exception("Archive directory '$source->archivedir' is not found");
    }
    	
    // Move file to processed folder
    $archivefilename = $archivedir . '/' . basename($filename);
    if (!rename($filename, $archivefilename)) {
        throw new Exception("cannot rename sync file $filename to $archivefilename");
    }	
    return $archivefilename;
}
            
function androgogic_sync_validate_csvfile($log, $source, $filename) {

    assert(is_object($log), '$log must be an object');
    assert(is_object($source), '$source must be an object');
    assert(is_string($filename), '$filename must be a string');
    assert(!empty($filename), '$filename is required');

	$basename = basename($filename);
    $linecount = 0;
    $reccount = 0;
    $errorcount = 0;
    $fieldcount = 0;
	ini_set('auto_detect_line_endings',TRUE);

    if (($handle = fopen($filename, 'r')) === FALSE) {
        throw new Exception('unable to read CSV file ' . $filename);
    }

    while (($data = fgetcsv($handle, 0, $source->csvdelimiter, '"')) !== FALSE) {
        $linecount++;
        if ($linecount == 1) {
            $fieldcount = count($data);
            if ($source->csvheader == 0) {
                $reccount++;
            }
        } else {
            $reccount++;
            $num = count($data);
            if ($num <> $fieldcount) {
                $errorcount++;
                $log->add_to_log(SyncLog::TYPE_ERROR, "CSV file $basename contains formatting error: line $linecount has $num fields, expecting $fieldcount fields");
            }
        }
    }
    fclose($handle);
    if ($errorcount > 0) {
        $log->add_to_log(SyncLog::TYPE_WARNING, "CSV file $basename skipped, contains $errorcount errors");
	} else {
    	$log->add_to_log(SyncLog::TYPE_TRACE, "CSV file $basename passed validation, contains $reccount records");
    } 
    return ($errorcount==0);
}
