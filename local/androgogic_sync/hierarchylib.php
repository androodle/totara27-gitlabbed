<?php

/**
 * Androgogic Sync
 *
 * @author      Keith Buss <kbuss@outlook.com>
 * @version     May 2015
 *
 **/
 
defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot . '/totara/hierarchy/prefix/position/lib.php');
require_once($CFG->dirroot . '/totara/hierarchy/prefix/organisation/lib.php');

function androgogic_sync_process_hierarchy_source($log, $source, $hierarchy, $progressbar=null) {
    global $DB, $CFG;

    assert(is_object($log), '$log must be an object');
    assert(is_object($source), '$source must be an object');
	assert(is_object($hierarchy), '$hierarchy must be an object');
	if ($progressbar<>null) {
		$message = 'updating '.$hierarchy->prefix.'s';
		$count = 0;
		$total = $DB->count_records('androgogic_sync_'.$hierarchy->shortprefix, array('runid'=>$log->runid, 'sourceid'=>$source->id));
		$progressbar->update_full(0, $message);
	}
	
    $log->count_hierarchy_created = 0;
    $log->count_hierarchy_updated = 0;
    $log->count_hierarchy_deleted = 0;
    $log->count_hierarchy_skipped = 0;

    $frameworkid = 0;
    if ($hierarchy->shortprefix == 'org') {
        $frameworkid = $source->orgframeworkid;
    } else if ($hierarchy->shortprefix == 'pos') {
        $frameworkid = $source->posframeworkid;
    }
    if (empty($frameworkid)) {
        throw new Exception("$hierarchy->prefix framework setting is required");
    }

    // skip records that are missing required fields
 	androgogic_sync_hierarchy_required_field($log, $source, $hierarchy, 'idnumber');
 	androgogic_sync_hierarchy_required_field($log, $source, $hierarchy, 'fullname');

    // source contains all records
    if ($source->allrecords == 1) {
        // delete obsolete items with no matching staging record
        $rs = $DB->get_recordset_sql("SELECT h.id, h.idnumber, h.frameworkid FROM {{$hierarchy->shortprefix}} h
			 LEFT OUTER JOIN {androgogic_sync_{$hierarchy->shortprefix}} s ON LOWER(s.idnumber) = LOWER(h.idnumber) AND s.runid=$log->runid AND s.sourceid=$source->id AND s.processed=0 
			WHERE h.frameworkid = $frameworkid
			  AND s.id IS NULL");
        if ($rs->valid()) {
            foreach ($rs as $row) {
                $log->stagingid = NULL;                
                androgogic_sync_delete_hierarchy_item($log, $hierarchy, $frameworkid, $row->idnumber);
            }
        }
        $rs->close();
    }

    // create new items
    $rs = $DB->get_recordset_sql("SELECT s.*, s.id AS stagingid FROM {androgogic_sync_{$hierarchy->shortprefix}} s
		 LEFT OUTER JOIN {{$hierarchy->shortprefix}} h ON LOWER(s.idnumber) = LOWER(h.idnumber) AND h.frameworkid = $frameworkid
		WHERE s.runid=$log->runid AND s.sourceid=$source->id AND s.processed=0
		  AND h.id IS NULL");
    if ($rs->valid()) {
        // create new items without setting parent in case some new items are also parents
        // parent of newly created items will be updated next
        foreach ($rs as $staging) {

            $log->stagingid = $staging->id;
			$mappedfields = androgogic_sync_load_mapped_fields($source, $staging);
            $mappedfields->parentidnumber = '';
            androgogic_sync_create_hierarchy_item($log, $hierarchy, $frameworkid, $mappedfields);
        }   		
    }
   	$rs->close();
   	
   	// update items - including newly created items 
    $rs = $DB->get_recordset_sql("SELECT * FROM {androgogic_sync_{$hierarchy->shortprefix}} 
    	WHERE runid=$log->runid AND sourceid=$source->id AND processed=0");
    if ($rs->valid()) {
    	$count = 0;
        foreach ($rs as $staging) {
            $log->stagingid = $staging->id;
            
			if ($progressbar<>null) {
				$count++;
				$progressbar->update($count, $total, $message);
			}
			
			$mappedfields = androgogic_sync_load_mapped_fields($source, $staging);
            if (isset($mappedfields->deleted) and $mappedfields->deleted == 1) {

                androgogic_sync_delete_hierarchy_item($log, $hierarchy, $frameworkid, $mappedfields->idnumber);
            } else {

                androgogic_sync_update_hierarchy_item($log, $hierarchy, $frameworkid, $mappedfields);
            }
            $DB->execute("UPDATE {androgogic_sync_{$hierarchy->shortprefix}} SET processed=1 WHERE id=$staging->id");
        }
    }
    $rs->close();
	
	if ($progressbar<>null) {
		$progressbar->update_full(100, $message);
	}
	
    $log->stagingid = NULL;
    $log->add_to_log(SyncLog::TYPE_INFO, "$hierarchy->prefix totals: $log->count_hierarchy_created created, $log->count_hierarchy_updated updated, $log->count_hierarchy_skipped skipped");
}

function androgogic_sync_hierarchy_required_field($log, $source, $hierarchy, $fieldname) {
	global $DB;
	
	assert(is_object($log), '$log must be an object');
	assert(is_object($source), '$source must be an object');
	assert(is_object($hierarchy), '$hierarchy must be an object');
	assert(is_string($fieldname), '$fieldname must be a string');
	
	$rs = $DB->get_recordset_sql("SELECT * FROM {androgogic_sync_{$hierarchy->shortprefix}}  
		WHERE runid=$log->runid AND sourceid=$source->id AND processed=0 AND TRIM($fieldname) = ''");
	if ($rs->valid()) {
        foreach ($rs as $staging) {
        	$log->stagingid = $staging->id;
			$log->count_hierarchy_skipped++;
			$log->add_to_log(SyncLog::TYPE_WARNING, "$hierarchy->prefix skipped: $fieldname is required", "$staging->fullname, idnumber=$staging->idnumber");
    		$DB->execute("UPDATE {androgogic_sync_{$hierarchy->shortprefix}} SET processed=-1 WHERE id=$staging->id");
        }
    }
    $rs->close();
}

function androgogic_sync_create_hierarchy_item($log, $hierarchy, $frameworkid, $mappedfields) {
	global $DB;

	assert(is_object($log), '$log must be an object');
	assert(is_object($hierarchy), '$hierarchy must be an object');
	assert(is_numeric($frameworkid), '$frameworkid must be numeric');
	assert(is_object($mappedfields), '$mappedfields must be an object');
    	
	$item = new stdClass;
	$item->fullname = $mappedfields->fullname;
	$item->idnumber = $mappedfields->idnumber;
	$item->frameworkid = $frameworkid;
	$item->visible = 1;
	$item->usermodified = get_admin()->id;
	$item->timecreated = time();
	$item->timemodified = $item->timecreated;

	$item->parentid = 0;  // default to top level
	if (!empty($mappedfields->parentidnumber)) {
        $parentid = $DB->get_field_sql("SELECT id FROM {{$hierarchy->shortprefix}} WHERE LOWER(idnumber)=LOWER(:idnumber) AND frameworkid=:frameworkid", array('idnumber'=>$mappedfields->parentidnumber, 'frameworkid'=>$frameworkid), IGNORE_MULTIPLE);
		if (!$parentid) {
			$log->add_to_log(SyncLog::TYPE_WARNING, "cannot set $hierarchy->prefix parent, parent idnumber '$mappedfields->parentidnumber' not found in LMS", $mappedfields);
		} else {
			$item->parentid = $parentid;
		}
	}

	$item->typeid = 0;
	if (!empty($mappedfields->typeidnumber)) {
        $typeid = $DB->get_field_sql("SELECT id FROM {{$hierarchy->shortprefix}_type} WHERE LOWER(idnumber)=LOWER(:idnumber)", array('idnumber'=>$mappedfields->typeidnumber));
		if (!$typeid) {
			$log->add_to_log(SyncLog::TYPE_WARNING, "cannot set $hierarchy->prefix typeid, type idnumber '$mappedfields->typeidnumber' not found in LMS", $mappedfields);
		} else {
			$item->typeid = $typeid;
		}
	}
	
	if (!$item = $hierarchy->add_hierarchy_item($item, $item->parentid, $frameworkid, false, true, false)) {
		$log->add_to_log(SyncLog::TYPE_WARNING, "$hierarchy->prefix create failed: $item->fullname", "idnumber=$item->idnumber, frameworkid=$frameworkid");
	} else {
	    $log->count_hierarchy_created++;
        $log->add_to_log(SyncLog::TYPE_TRACE, "$hierarchy->prefix created", "$item->fullname, idnumber=$item->idnumber, {$hierarchy->shortprefix}id=$item->id");
	}
}

function androgogic_sync_delete_hierarchy_item($log, $hierarchy, $frameworkid, $idnumber) {
	global $DB;
	
	assert(is_object($log), '$log must be an object');
	assert(is_object($hierarchy), '$hierarchy must be an object');
	assert(is_numeric($frameworkid), '$frameworkid must be numeric');
	assert(is_string($idnumber), '$idnumber must be a string');
	
	
	if (!$item = $DB->get_record_sql("SELECT * FROM {{$hierarchy->shortprefix}} WHERE LOWER(idnumber)=LOWER(:idnumber) AND frameworkid=:frameworkid", array('idnumber'=>$idnumber, 'frameworkid'=>$frameworkid))) {

		$log->add_to_log(SyncLog::TYPE_ERROR, "$hierarchy->prefix hierarchy item not found: idnumber=$idnumber, frameworkid=$frameworkid");

	} else {
		if (!$hierarchy->delete_hierarchy_item($item->id)) {
			$log->add_to_log(SyncLog::TYPE_ERROR, "$hierarchy->prefix delete failed", "$item->fullname, idnumber=$item->idnumber, {$hierarchy->shortprefix}id=$item->id");
		} else {
			$log->count_hierarchy_deleted++;
			$log->add_to_log(SyncLog::TYPE_TRACE, "$hierarchy->prefix deleted", "$item->fullname, idnumber=$item->idnumber, {$hierarchy->shortprefix}id=$item->id");
		}
	}
}

function androgogic_sync_update_hierarchy_item($log, $hierarchy, $frameworkid, $mappedfields) {
	global $DB;
		
	assert(is_object($log), '$log must be an object');
	assert(is_object($hierarchy), '$hierarchy must be an object');
	assert(is_numeric($frameworkid), '$frameworkid must be numeric');
	assert(is_object($mappedfields), '$mappedfields must be an object');

	if (!$item = $DB->get_record_sql("SELECT * FROM {{$hierarchy->shortprefix}} WHERE LOWER(idnumber)=LOWER(:idnumber) AND frameworkid=:frameworkid", array('idnumber'=>$mappedfields->idnumber, 'frameworkid'=>$frameworkid))) {

		$log->add_to_log(SyncLog::TYPE_ERROR, "$hierarchy->prefix hierarchy item not found: idnumber=$mappedfields->idnumber, frameworkid=$frameworkid");

	} else {
		$mappedfields->parentid = 0;  // default to top level
		if (!empty($mappedfields->parentidnumber)) {
            $parentid = $DB->get_field_sql("SELECT id FROM {{$hierarchy->shortprefix}} WHERE LOWER(idnumber)=LOWER(:idnumber) AND frameworkid=:frameworkid", array('idnumber'=>$mappedfields->parentidnumber, 'frameworkid'=>$frameworkid));
			if (!$parentid) {
		
				$log->add_to_log(SyncLog::TYPE_WARNING, "cannot set $hierarchy->prefix parent, parent idnumber 'parentidnumber=$mappedfields->parentidnumber' not found in LMS", "$mappedfields->fullname, idnumber=$mappedfields->idnumber, {$hierarchy->shortprefix}id=$item->id");
			} else {
				$mappedfields->parentid = $parentid;
			}
		}
	
		$mappedfields->typeid = 0;
		if (!empty($mappedfields->typeidnumber)) {
            $typeid = $DB->get_field_sql("SELECT id FROM {{$hierarchy->shortprefix}_type} WHERE LOWER(idnumber)=LOWER(:idnumber)", array('idnumber'=>$mappedfields->typeidnumber));
			if (!$typeid) {
				$log->add_to_log(SyncLog::TYPE_WARNING, "cannot set $hierarchy->prefix typeid, type idnumber 'typeidnumber=$mappedfields->typeidnumber' not found in LMS", "$mappedfields->fullname, idnumber=$mappedfields->idnumber, {$hierarchy->shortprefix}id=$item->id");
			} else {
				$mappedfields->typeid = $typeid;
			}
		}
	
		if ($item->fullname <> $mappedfields->fullname or $item->parentid <> $mappedfields->parentid or $item->typeid <> $mappedfields->typeid) {
			$item->parentid = $mappedfields->parentid;
			$item->fullname = $mappedfields->fullname;
			$item->typeid = $mappedfields->typeid;
			$item->usermodified = get_admin()->id;
			$item->timemodified = time();
			
			if (!$hierarchy->update_hierarchy_item($item->id, $item, false, true, false)) {
				$log->add_to_log(SyncLog::TYPE_ERROR, "$hierarchy->prefix update failed: $item->fullname, idnumber=$item->idnumber, {$hierarchy->shortprefix}id=$item->id");
			} else {
				$log->count_hierarchy_updated++;	
				$log->add_to_log(SyncLog::TYPE_TRACE, "$hierarchy->prefix updated", "$item->fullname, idnumber=$item->idnumber, {$hierarchy->shortprefix}id=$item->id");
			}
		}
	}
}
