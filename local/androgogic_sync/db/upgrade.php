<?php
/**
 * Androgogic Sync
 *
 * @author      Keith Buss <kbuss@outlook.com>
 * @version     July 2015
 *
 **/
 
require_once($CFG->dirroot.'/local/androgogic_sync/lib.php');

function xmldb_local_androgogic_sync_upgrade($oldversion=0) {

    global $DB;
    
	$dbman = $DB->get_manager();

	if ($oldversion < 2015082100) {	
		$reportname = get_string('logreport', 'local_androgogic_sync');
		if (!$DB->get_record('report_builder', array('fullname'=>$reportname))) {
			// rename sync log report
			$DB->execute("UPDATE {report_builder} SET fullname='$reportname', shortname='report_androsync_log' WHERE fullname='Androgogic Sync Log'");
		}
		if ($row = $DB->get_record('capabilities', array('name'=>'local/androgogic_sync:synclog'))) {
			// rename capability
			$row->name = 'local/androgogic_sync:viewlog';
			$DB->update_record('capabilities', $row);	
		}
		if (!$DB->get_record('capabilities', array('name'=>'local/androgogic_sync:uploadsyncfile'))) {
			// create capability
			if ($row = $DB->get_record('capabilities', array('name'=>'local/androgogic_sync:managesources'))) {
				unset($row->id);
				$row->name = 'local/androgogic_sync:uploadsyncfile';
				$DB->insert_record('capabilities', $row);
			}
		}
	}
	
	if ($oldversion < 2015082800) {	
    	$table = new xmldb_table('androgogic_sync_user');
        $field = new xmldb_field('posstartdate');
        $field->set_attributes(XMLDB_TYPE_CHAR, '20');
        $dbman->change_field_precision($table, $field);
        $field = new xmldb_field('posenddate');
        $field->set_attributes(XMLDB_TYPE_CHAR, '20');
        $dbman->change_field_precision($table, $field);
        
        $table = new xmldb_table('androgogic_sync_comp');
        $field = new xmldb_field('completedate');
        $field->set_attributes(XMLDB_TYPE_CHAR, '20');
        $dbman->change_field_precision($table, $field);
    }
    
    if ($oldversion < 2015090100) {	
    	$table = new xmldb_table('androgogic_sync_comp');
    	$field = new xmldb_field('enroldate');
        $field->set_attributes(XMLDB_TYPE_CHAR, '20');
    	if (!$dbman->field_exists($table, $field)) {
        	$dbman->add_field($table, $field);
		}
	}    
	
	if ($oldversion < 2015090800) {	
    	$table = new xmldb_table('androgogic_sync_source');
    	$field = new xmldb_field('syncdir');
    	if ($dbman->field_exists($table, $field)) {
			$dbman->drop_field($table, $field);
		}
    	$field = new xmldb_field('archivedir');
    	if ($dbman->field_exists($table, $field)) {
			$dbman->drop_field($table, $field);
		}
	}

	if ($oldversion < 2015091500) {	
    	$table = new xmldb_table('androgogic_sync_source');
    	$field = new xmldb_field('description');
    	if ($dbman->field_exists($table, $field)) {
			$dbman->drop_field($table, $field);
		}
	
    	$table = new xmldb_table('androgogic_sync_user_custom');
    	$field = new xmldb_field('timecreated');
    	if ($dbman->field_exists($table, $field)) {
			$dbman->drop_field($table, $field);
		}
	}
	
	if ($oldversion < 2015091600) {	
    	$table = new xmldb_table('androgogic_sync_log');
    	$field = new xmldb_field('trace');
        $field->set_attributes(XMLDB_TYPE_TEXT);
    	if (!$dbman->field_exists($table, $field)) {
        	$dbman->add_field($table, $field);
		}
	}
	
    if ($oldversion < 2015100700) {	
    	$table = new xmldb_table('androgogic_sync_comp');
    	$field = new xmldb_field('startdate');
    	$field->set_attributes(XMLDB_TYPE_CHAR, '20');
    	if (!$dbman->field_exists($table, $field)) {
        	$dbman->add_field($table, $field);
		}
	}    
	
    if ($oldversion < 2015102200) {	
        // delete existing sync log report
	    $reportname = get_string('logreport', 'local_androgogic_sync');
	    if ($DB->get_record('report_builder', array('fullname'=>$reportname))) {
		    $DB->delete_records('report_builder', array('fullname'=>$reportname));
	    }
	    // create new sync log report
        androgogic_sync_create_log_report($reportname);
    }
    
    if ($oldversion < 2015102300) {	
    	$rows = $DB->get_records_sql("SELECT * FROM {androgogic_sync_field} WHERE LEFT(dbfieldname,13)='profilefield_'");
        if ($rows) {
            foreach ($rows as $row) {
                $row->dbfieldname = 'profile_field_'.substr($row->dbfieldname,13);
                $DB->update_record('androgogic_sync_field', $row);
            }
        }
    }
	return true;
}
