<?php
/**
 * Androgogic Sync
 *
 * @author      Keith Buss <kbuss@outlook.com>
 * @version     May 2015
 *
 **/

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/lib/formslib.php');
require_once($CFG->dirroot.'/local/androgogic_sync/classes/synclog.class.php');

class addsourceform extends moodleform {

    // Define the form
    function definition() {

        $mform =& $this->_form;
		
		$mform->addElement('header', 'addsource', get_string('addsource', 'local_androgogic_sync'));

		$options = array(
			SyncLog::ELEMENT_USER=>get_string(SyncLog::ELEMENT_USER, 'local_androgogic_sync'),
			SyncLog::ELEMENT_ORG=>get_string(SyncLog::ELEMENT_ORG, 'local_androgogic_sync'),
			SyncLog::ELEMENT_POS=>get_string(SyncLog::ELEMENT_POS, 'local_androgogic_sync'),
			SyncLog::ELEMENT_COMP=>get_string(SyncLog::ELEMENT_COMP, 'local_androgogic_sync')
		);		

		$mform->addElement('select', 'element', get_string('element', 'local_androgogic_sync'), $options);
        $mform->setType('element', PARAM_TEXT);
        
		$options = array(
			SyncLog::SOURCE_CSV=>get_string(SyncLog::SOURCE_CSV, 'local_androgogic_sync'),
			SyncLog::SOURCE_WEBSERVICE=>get_string(SyncLog::SOURCE_WEBSERVICE, 'local_androgogic_sync')
		);
		$mform->addElement('select', 'source', get_string('source', 'local_androgogic_sync'), $options);
        $mform->setType('source', PARAM_TEXT);
        
        $this->add_action_buttons(false, 'Add source');
    }
}
