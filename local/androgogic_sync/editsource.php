<?php
/**
 * Androgogic Sync
 *
 * @author      Keith Buss <kbuss@outlook.com>
 * @version     May 2015
 *
 **/

require_once(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once($CFG->libdir.'/adminlib.php');
require_once('classes/editsourceform.class.php');

require_login();

$context = context_system::instance();
require_capability('local/androgogic_sync:managesources', $context);

// Get params
$id = optional_param('id', 0, PARAM_INT);    // 0 if creating adding a new source 
$source = optional_param('source', '', PARAM_ALPHA);  
$element = optional_param('element', '', PARAM_ALPHA); 

$elementname = get_string($element, 'local_androgogic_sync');
$sourcename = get_string($source, 'local_androgogic_sync');
$heading = "$elementname $sourcename settings";

$url_params = array('id'=>$id, 'source'=>$source, 'element'=>$element);
admin_externalpage_setup('managesources', '', $url_params, new moodle_url('/local/androgogic_sync/editsource.php'));

// create form
$mform = new editsourceform(null, $url_params);
if ($id <> 0) {
    // Editing existing source
	if (!$row = $DB->get_record('androgogic_sync_source', array('id'=>$id))) {
		throw new Exception($DB->get_last_error());	
	}
	
	// load field mappings
	if ($fields = $DB->get_records('androgogic_sync_field', array('sourceid'=>$id))) {
		foreach ($fields as $field) {
			if (substr($field->dbfieldname, 0, 14) == 'profile_field_') {
				$elementname = $field->dbfieldname;
			} else {
				$elementname = 'field_'.$field->dbfieldname;
			}
			if ($source == SyncLog::SOURCE_CSV) {
				$row->$elementname = $field->csvcolumnno;
			} else {
				$row->$elementname = 1;
			}
		}
	}
	$mform->set_data($row);
}

// cancelled
if ($mform->is_cancelled()) {

    redirect('sources.php');

// Update data
} else if ($new = $mform->get_data()) {

	$transaction = $DB->start_delegated_transaction();
	try {    
		// Save source settings
		$new->timemodified = time();
		$new->usermodified = $USER->id;
		if ($new->id == 0) {
	
			// New record
			$new->timecreated = $new->timemodified;
			$new->visible = 1;

			$max = $DB->get_field_sql("SELECT MAX(sortorder) AS max FROM {androgogic_sync_source} WHERE deleted=0");
			$new->sortorder = $max + 1;
		
			unset($new->id);
			if (!$new->id = $DB->insert_record('androgogic_sync_source', $new)) {
				throw new Exception($DB->get_last_error());	
			}

			$notification = get_string('sourceadded', 'local_androgogic_sync');

		} else {
	
			// Existing record
			if (!$DB->update_record('androgogic_sync_source', $new)) {
				throw new Exception($DB->get_last_error());	
			}
			$DB->delete_records('androgogic_sync_field', array('sourceid'=>$new->id));

			$notification = get_string('sourceupdated', 'local_androgogic_sync');
		}
	
		// save field mapping
		foreach ($new as $name=>$value) {
			// save mapped fields
			if (!empty($value)) {
				$child = new stdClass();

				// identify field names
				$child->dbfieldname = '';
				if (substr($name, 0, 6) == 'field_') {
					$child->dbfieldname = substr($name, 6);
				} else if (substr($name, 0, 14) == 'profile_field_') {
					$child->dbfieldname = $name;
				}
			
				if (!empty($child->dbfieldname)) {
					$child->sourceid = $new->id;
					
					if ($source == SyncLog::SOURCE_CSV) {
						$child->csvcolumnno = $value;
					}
					if (!$DB->insert_record('androgogic_sync_field', $child)) {
						throw new Exception($DB->get_last_error());	
					}
				}
			}
		}
		$transaction->allow_commit();
		totara_set_notification($notification, 'sources.php', array('class'=>'notifysuccess'));

	} catch (Exception $e) {

		$transaction->rollback($e);
		throw $e;
	}
}

///
/// Display page
///

echo $OUTPUT->header();
echo $OUTPUT->heading($heading);

$mform->display();

echo $OUTPUT->footer();
