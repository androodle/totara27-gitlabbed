<?php

/**
 * Androgogic Sync
 *
 * @author      Keith Buss <kbuss@outlook.com>
 * @version     May 2015
 *
 **/
 
defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot . '/local/androgogic_sync/classes/usermatch.class.php');
require_once($CFG->dirroot . '/totara/hierarchy/prefix/position/lib.php');
require_once($CFG->dirroot . '/totara/hierarchy/prefix/organisation/lib.php');
require_once($CFG->dirroot . '/user/lib.php');
require_once($CFG->dirroot . '/user/profile/lib.php');

function androgogic_sync_process_user_source($log, $source, $progressbar = null) {
    global $DB;

    assert(is_object($log), '$log must be an object');
    assert(is_object($source), '$source must be an object');

    if (empty($source->auth)) {
        throw new Exception('Authentication setting is required');
    }

    if (empty($source->dateformat)) {
        throw new Exception('Date format setting is required');
    }

    if ($progressbar <> null) {
        $message = 'updating users';
        $count = 0;
        $total = $DB->count_records('androgogic_sync_user', array('runid' => $log->runid, 'sourceid' => $source->id));
        $progressbar->update_full(0, $message);
    }

    $log->count_user_created = 0;
    $log->count_user_updated = 0;
    $log->count_user_deleted = 0;
    $log->count_user_suspended = 0;
    $log->count_user_unsuspended = 0;
    $log->count_user_skipped = 0;

    // skip records that are missing required fields
    androgogic_sync_user_required_field($log, $source, 'idnumber');
    androgogic_sync_user_required_field($log, $source, 'firstname');
    androgogic_sync_user_required_field($log, $source, 'lastname');
    androgogic_sync_user_required_field($log, $source, 'email');

    // source contains all records 
    if ($source->allrecords == 1) {
        // suspend or delete obsolete users with no matching staging record
        $sql = "SELECT u.id FROM {user} u 
		   LEFT OUTER JOIN {androgogic_sync_user} s ON LOWER(s.idnumber) = LOWER(u.idnumber) AND s.runid=$log->runid AND s.sourceid=$source->id AND s.processed=0			
					 WHERE s.id IS NULL
					 AND TRIM(u.idnumber) <> '' AND u.deleted=0";
        if ($source->userdeletion == 0) {
            $sql .= ' AND u.suspended=0';
        }
        $rs = $DB->get_recordset_sql($sql);
        if ($rs->valid()) {
            $log->stagingid = NULL;
            foreach ($rs as $user) {
                if ($source->userdeletion == 0) {
                    androgogic_sync_suspend_user($log, $source, $user->id);
                } else {
                    androgogic_sync_delete_user($log, $source, $user->id);
                }
            }
        }
        $rs->close();
    }

    // delete users with deleted flag set in staging record
    $sql = "SELECT s.id AS stagingid, u.id AS userid FROM {androgogic_sync_user} s
			  JOIN {user} u ON LOWER(s.idnumber)=LOWER(u.idnumber) AND TRIM(u.idnumber) <> '' AND u.deleted=0			
			 WHERE s.runid=$log->runid AND s.sourceid=$source->id 
			   AND s.deleted=1 AND s.processed=0";
    $rs = $DB->get_recordset_sql($sql);
    if ($rs->valid()) {
        foreach ($rs as $row) {
            $log->stagingid = $row->stagingid;
            androgogic_sync_delete_user($log, $source, $row->userid);
            $DB->execute("UPDATE {androgogic_sync_user} SET processed=1 WHERE id=$row->stagingid");
        }
    }
    $rs->close();

    // add or update remaining users 
    $rs = $DB->get_recordset_sql("SELECT * FROM {androgogic_sync_user} 
    	WHERE runid=$log->runid AND sourceid=$source->id AND deleted=0 AND processed=0 ORDER BY id");
    if ($rs->valid()) {
        foreach ($rs as $staging) {
            $success = false;
            $log->stagingid = $staging->id;

            if ($progressbar <> null) {
                $count++;
                $progressbar->update($count, $total, $message);
            }

            $mappedfields = androgogic_sync_load_mapped_fields($source, $staging);
            $success = androgogic_sync_process_user($log, $source, $mappedfields);
            if ($success) {
                $processed = 1;
            } else {
                $processed = -1;
            }
            $DB->execute("UPDATE {androgogic_sync_user} SET processed=$processed WHERE id=$staging->id");
        }
    }
    $rs->close();

    if ($progressbar <> null) {
        $progressbar->update_full(100, $message);
    }

    $log->stagingid = NULL;
    $log->add_to_log(SyncLog::TYPE_INFO, "user totals: $log->count_user_created created, $log->count_user_updated updated, $log->count_user_deleted deleted, $log->count_user_suspended suspended, $log->count_user_unsuspended unsuspended, $log->count_user_skipped skipped");
}

function androgogic_sync_process_user($log, $source, $mappedfields) {
    global $DB;

    // Note this is also called from completionlib.php

    assert(is_object($log), '$log must be an object');
    assert(is_object($source), '$source must be an object');
    assert(is_object($mappedfields), '$mappedfields must be an object');

    if (!empty($mappedfields->username)) {
        $mappedfields->username = strtolower($mappedfields->username);  // Usernames always lowercase in moodle.
        if ($mappedfields->username != clean_param($mappedfields->username, PARAM_USERNAME)) {
            $log->count_user_skipped++;
            $log->add_to_log(SyncLog::TYPE_WARNING, "user skipped: username '$mappedfields->username' contains invalid characters", "$mappedfields->firstname $mappedfields->lastname, idnumber=$mappedfields->idnumber, email=$mappedfields->email");
            return false;
        }
    }

    // split first name and middle name to improve accuracy of user matching
    if (empty($mappedfields->middlename)) {
        $mappedfields->firstname = trim($mappedfields->firstname);
        $pos = strpos($mappedfields->firstname, ' ');
        if ($pos > 0) {
            $names = explode(' ', $mappedfields->firstname);
            $mappedfields->firstname = $names[0];
            $mappedfields->middlename = $names[1];
        }
    }

    // search for existing user
    $UserMatch = new UserMatch();
    $matchcount = $UserMatch->findMatch($mappedfields);
    $matchresult = $UserMatch->getMatchResult();
    if ($matchresult == UserMatch::RESULT_NOMATCH) {

        $log->add_to_log(SyncLog::TYPE_TRACE, 'user deduplication found no match', "$mappedfields->firstname $mappedfields->lastname, idnumber=$mappedfields->idnumber, email=$mappedfields->email");

        if (empty($mappedfields->username)) {

            $log->count_user_skipped++;
            $log->add_to_log(SyncLog::TYPE_WARNING, 'user skipped: cannot create user, missing username', "$mappedfields->firstname $mappedfields->lastname, idnumber=$mappedfields->idnumber, email=$mappedfields->email");
            return false;
        } else if ($DB->record_exists('user', array('username' => $mappedfields->username))) {

            $log->count_user_skipped++;
            $log->add_to_log(SyncLog::TYPE_WARNING, "user skipped: cannot create user, username '$mappedfields->username' already exists", "$mappedfields->firstname $mappedfields->lastname, idnumber=$mappedfields->idnumber, email=$mappedfields->email");
            return false;
        } else {

            //
            // create user
            //
			$success = androgogic_sync_create_user($log, $source, $mappedfields);
            if ($success) {
                androgogic_sync_pos_assignments($log, $source, $mappedfields);

                if ($source->senduseremail == 1) {
                    // send new account email to new user 
                    if (androgogic_sync_send_email($mappedfields->userid, 'newaccountemail')) {
                        $log->add_to_log(SyncLog::TYPE_TRACE, "new account email sent", "$mappedfields->firstname $mappedfields->lastname, idnumber=$mappedfields->idnumber, email=$mappedfields->email, userid=$mappedfields->userid");
                    } else {
                        $log->add_to_log(SyncLog::TYPE_ERROR, "new account email send failed", $mappedfields);
                    }
                }
            }
        }
    } elseif ($matchresult == UserMatch::RESULT_MATCHED) {

        if (!empty($mappedfields->username)) {
            $prevusername = $mappedfields->username;
        }
        $mappedfields->userid = $UserMatch->getMatchedUserid(1);
        $mappedfields->username = $UserMatch->getMatchedUsername(1);
        $log->add_to_log(SyncLog::TYPE_TRACE, 'user deduplication matched: ' . $UserMatch->getMatchedFields(1), "$mappedfields->firstname $mappedfields->lastname, idnumber=$mappedfields->idnumber, email=$mappedfields->email, username=$mappedfields->username, userid=$mappedfields->userid");

        //
        // update user
        //
		$success = androgogic_sync_update_user($log, $source, $mappedfields);
        if ($success) {
            if (!empty($prevusername) and $prevusername <> $mappedfields->username) {
                $log->add_to_log(SyncLog::TYPE_TRACE, "username changed from '$prevusername' to '$mappedfields->username'", "$mappedfields->firstname $mappedfields->lastname, idnumber=$mappedfields->idnumber, email=$mappedfields->email, userid=$mappedfields->userid");
				if ($source->senduseremail == 1) {
					// send new username email to user
					if (androgogic_sync_send_email($mappedfields->userid, 'newusernameemail')) {
						$log->add_to_log(SyncLog::TYPE_TRACE, "new username email sent", "$mappedfields->firstname $mappedfields->lastname, idnumber=$mappedfields->idnumber, email=$mappedfields->email, userid=$mappedfields->userid");
					} else {
						$log->add_to_log(SyncLog::TYPE_ERROR, "new username email send failed", $mappedfields);
					}
				}
            }
            androgogic_sync_pos_assignments($log, $source, $mappedfields);
        }
    } elseif ($matchresult == UserMatch::RESULT_REVIEW) {
        //
        // partial match
        //
		if ($matchcount == 1) {
            $logmessage = 'User skipped: unable to process partial match';
            $log->add_to_log(SyncLog::TYPE_REVIEW, 'user deduplication found partial match: ' . $UserMatch->getMatchedFields(1), 'username=' . $UserMatch->getMatchedUsername(1) . ', userid=' . $UserMatch->getMatchedUserid(1));
        } else {
            //
            // multiple matches 
            //
			$logmessage = 'User skipped: possible duplicate LMS accounts';
            for ($i = 1; $i <= $matchcount; $i++) {
                $log->add_to_log(SyncLog::TYPE_REVIEW, "user deduplication found $i of $matchcount matches: " . $UserMatch->getMatchedFields($i), 'username=' . $UserMatch->getMatchedUsername($i) . ', userid=' . $UserMatch->getMatchedUserid($i));
            }
        }
        $log->count_user_skipped++;
        $log->add_to_log(SyncLog::TYPE_REVIEW, $logmessage, "$mappedfields->firstname $mappedfields->lastname, username=$mappedfields->username, idnumber=$mappedfields->idnumber, email=$mappedfields->email");
        return false;
    }
    //if ($success) {
    //
		// successfully created or updated user
    //

		// check for duplicate email 
    //	if ($source->allowduplicatedemails == 0) {
    //		if ($DB->record_exists_sql("SELECT id FROM {user} WHERE email=:email AND deleted=0 AND id <> $mappedfields->userid"),array('email=>$mappedfields->email)) {
    //			$log->add_to_log(SyncLog::TYPE_WARNING, "duplicate email: $mappedfields->email", "$mappedfields->firstname $mappedfields->lastname, username=$mappedfields->username");
    //		}
    //	}
    //}
    return true;
}

function androgogic_sync_create_user($log, $source, $mappedfields) {
    global $CFG, $DB;

    assert(is_object($log), '$log must be an object');
    assert(is_object($source), '$source must be an object');
    assert(is_object($mappedfields), '$mappedfields must be an object');

    $success = false;
    $transaction = $DB->start_delegated_transaction();
    try {
        // set defaults
        $mappedfields->confirmed = 1;
        $mappedfields->mnethostid = $CFG->mnet_localhost_id;
        $mappedfields->lang = $CFG->lang;
        $mappedfields->htmleditor = 1;
        $mappedfields->policyagreed = $source->policyagreed;

        if (empty($mappedfields->timezone)) {
            $mappedfields->timezone = '99';
        }

        if (empty($mappedfields->auth)) {
            $mappedfields->auth = $source->auth;
        }

        if (empty($mappedfields->country)) {
            $mappedfields->country = $CFG->country;
        }

        //
        // Create user
        //
        // do not update password
        unset($mappedfields->password);
        $updatepassword = false;
        $mappedfields->userid = user_create_user($mappedfields, $updatepassword);

        //
        // Save user profile custom fields
        //
        $mappedfields->id = $mappedfields->userid;
        profile_save_data($mappedfields);
        unset($mappedfields->id);

        if ($source->forcepasswordchange == 1) {
            $auth = get_auth_plugin($mappedfields->auth);
            if ($auth->is_internal() and $auth->can_change_password()) {
                set_user_preference('auth_forcepasswordchange', 1, $mappedfields->userid);
            }
            unset($auth);
        }

        $log->add_to_log(SyncLog::TYPE_TRACE, 'user created', "$mappedfields->firstname $mappedfields->lastname, idnumber=$mappedfields->idnumber, email=$mappedfields->email, userid=$mappedfields->userid");
        $log->count_user_created++;

        if (isset($mappedfields->suspended) and $mappedfields->suspended == 1) {
            androgogic_sync_user_suspended_event($mappedfields);
            $log->add_to_log(SyncLog::TYPE_TRACE, 'user suspended', "$mappedfields->firstname $mappedfields->lastname, idnumber=$mappedfields->idnumber, email=$mappedfields->email, userid=$mappedfields->userid");
            $log->count_user_suspended++;
        }
        $transaction->allow_commit();
        $success = true;
    } catch (Exception $e) {

        $transaction->rollback($e);
        $log->add_to_log(SyncLog::TYPE_ERROR, 'user create failed', $mappedfields);
        throw $e;
    }
    return $success;
}

function androgogic_sync_delete_user($log, $source, $userid) {
    global $DB;

    assert(is_object($log), '$log must be an object');
    assert(is_object($source), '$source must be an object');
    assert(is_numeric($userid), '$userid must be numeric');

    $success = false;
    $transaction = $DB->start_delegated_transaction();
    try {
        $user = $DB->get_record('user', array('id' => $userid));
        if ($user === false) {
            throw new Exception("user ID '$userid' not found");
        }

        delete_user($user);
        $log->add_to_log(SyncLog::TYPE_TRACE, 'user deleted', "$user->firstname $user->lastname, username=$user->username, idnumber=$user->idnumber, email=$user->email, userid=$user->id");
        $log->count_user_deleted++;

        $transaction->allow_commit();
        $success = true;
    } catch (Exception $e) {

        $transaction->rollback($e);
        $log->add_to_log(SyncLog::TYPE_ERROR, 'user delete failed', $user);
        throw $e;
    }
    return $success;
}

function androgogic_sync_suspend_user($log, $source, $userid) {
    global $DB;

    assert(is_object($log), '$log must be an object');
    assert(is_object($source), '$source must be an object');
    assert(is_numeric($userid), '$userid must be numeric');

    $success = false;
    $transaction = $DB->start_delegated_transaction();
    try {
        $user = $DB->get_record('user', array('id' => $userid));
        if ($user === false) {
            throw new Exception("user ID '$userid' not found");
        }

        $user->suspended = 1;
        // do not update password
        unset($user->password);
        $updatepassword = false;
        user_update_user($user, $updatepassword);
        androgogic_sync_user_suspended_event($user);
        $log->add_to_log(SyncLog::TYPE_TRACE, 'user suspended', "$user->firstname $user->lastname, username=$user->username, idnumber=$user->idnumber, email=$user->email, userid=$user->id");
        $log->count_user_suspended++;

        $transaction->allow_commit();
        $success = true;
    } catch (Exception $e) {

        $transaction->rollback($e);
        $log->add_to_log(SyncLog::TYPE_ERROR, 'user update failed', $user);
        throw $e;
    }
    return $success;
}

function androgogic_sync_update_user($log, $source, $mappedfields) {
    global $DB;

    assert(is_object($log), '$log must be an object');
    assert(is_object($source), '$source must be an object');
    assert(is_object($mappedfields), '$mappedfields must be an object');

    $success = false;
    $transaction = $DB->start_delegated_transaction();
    try {
        if (!$user = $DB->get_record('user', array('id' => $mappedfields->userid))) {
            throw new Exception("user ID '$mappedfields->userid' not found");
        }
        $profile = androgogic_sync_get_user_profile($user->id);

        if ($user->deleted == 1) {
            // Revive previously deleted user.
            if (undelete_user($user)) {
                $user->deleted = 0;
                $log->add_to_log(SyncLog::TYPE_TRACE, 'user undeleted', "$user->firstname $user->lastname, username=$user->username, idnumber=$user->idnumber, email=$user->email, userid=$user->id");

                // Tag the revived user for new password generation (if applicable).
                $auth = get_auth_plugin($user->auth);
                if ($auth->is_internal() and $auth->can_change_password()) {
                    set_user_preference('auth_forcepasswordchange', 1, $user->id);
                    set_user_preference('create_password', 1, $user->id);
                }
                unset($auth);
            } else {
                $log->add_to_log(SyncLog::TYPE_ERROR, 'user undelete failed', $user);
            }
        }

        //
        // Update user
        //
		// do not update deleted users to avoid user_update_user() throwing an exception
        if ($user->deleted == 0) {
            $prevsuspended = $user->suspended;

        	// do not update password
        	unset($user->password);
            $usermodified = androgogic_sync_fields($user, $mappedfields);
            if ($usermodified) {
                $updatepassword = false;
                user_update_user($user, $updatepassword);
            }

            //
            // Update user profile fields
            //
            $profilemodified = androgogic_sync_fields($profile, $mappedfields);
            if ($profilemodified) {
                profile_save_data($profile);
            }

            if ($usermodified or $profilemodified) {
                $log->add_to_log(SyncLog::TYPE_TRACE, 'user updated', "$user->firstname $user->lastname, username=$user->username, idnumber=$user->idnumber, email=$user->email, userid=$user->id");
                $log->count_user_updated++;

                if ($prevsuspended <> $user->suspended) {
                    if ($user->suspended == 1) {
                        androgogic_sync_user_suspended_event($user);
                        $log->add_to_log(SyncLog::TYPE_TRACE, 'user suspended', "$user->firstname $user->lastname, username=$user->username, idnumber=$user->idnumber, email=$user->email, userid=$user->id");
                        $log->count_user_suspended++;
                    } else {
                        $log->add_to_log(SyncLog::TYPE_TRACE, 'user unsuspended', "$user->firstname $user->lastname, username=$user->username, idnumber=$user->idnumber, email=$user->email, userid=$user->id");
                        $log->count_user_unsuspended++;
                    }
                }
            }
        }
        $transaction->allow_commit();
        $success = true;
        
    } catch (Exception $e) {

        $transaction->rollback($e);
        $log->add_to_log(SyncLog::TYPE_ERROR, 'user update failed', $user);
        throw $e;
    }
    return $success;
}

function androgogic_sync_user_suspended_event($user) {
    //global $CFG;
    //if (substr($CFG->totara_version, 0, 3) == '2.5') {
    //    events_trigger('user_suspended', $user);
    //} else {
    $event = \totara_core\event\user_suspended::create(
                    array(
                        'objectid' => $user->id,
                        'context' => context_user::instance($user->id),
                        'other' => array(
                            'username' => $user->username,
                        )
                    )
    );
    $event->trigger();
    //}
}

function androgogic_sync_get_user_profile($userid) {
    global $DB;

    assert(is_numeric($userid), '$userid must be numeric');

    $profile = new stdClass;
    $profile->id = $userid;
    profile_load_data($profile);

    // convert date custom fields 
    if ($rows = $DB->get_records('user_info_field', array('datatype' => 'datetime'))) {
        foreach ($rows as $row) {
            $fieldname = 'profile_field_' . $row->shortname;
            if (!empty($profile->$fieldname)) {
                $profile->$fieldname = userdate($profile->$fieldname, '%Y-%m-%d', 99, false);
            }
        }
    }
    return $profile;
}

function androgogic_sync_get_user_custom_field($userid, $shortname) {
    global $DB;

    assert(is_numeric($userid), '$userid must be numeric');
    assert(is_string($shortname), '$shortname must be a string');

    $retval = '';

    $sql = "SELECT d.data, f.datatype FROM {user_info_data} d 
              JOIN {user_info_field} f ON d.fieldid = f.id
             WHERE d.userid = :userid
               AND f.shortname = :shortname";
    if ($row = $DB->get_record_sql($sql, array('userid'=>$userid,'shortname'=>$shortname))) {
        if ($row->datatype == 'datetime') {
            if (!empty($row->data)) {
                $retval = userdate($row->data, '%Y-%m-%d', 99, false);
            }
        } else {
            $retval = $row->data;
        }
    }
    return $retval;
}

function androgogic_sync_user_required_field($log, $source, $fieldname) {
    global $DB;

    assert(is_object($log), '$log must be an object');
    assert(is_object($source), '$source must be an object');
    assert(is_string($fieldname), '$fieldname must be a string');
    //
    // validate required fields
    //
    $rs = $DB->get_recordset_sql("SELECT * FROM {androgogic_sync_user} 
		WHERE runid=$log->runid AND sourceid=$source->id AND processed=0 AND TRIM($fieldname) = ''");
    if ($rs->valid()) {
        foreach ($rs as $staging) {
            $log->stagingid = $staging->id;
            $log->count_user_skipped++;
            $log->add_to_log(SyncLog::TYPE_WARNING, "user skipped: $fieldname is required", "$staging->firstname $staging->lastname, idnumber=$staging->idnumber, email=$staging->email");
            $DB->execute("UPDATE {androgogic_sync_user} SET processed=-1 WHERE id=$staging->id");
        }
    }
    $rs->close();
}

function androgogic_sync_pos_assignments($log, $source, $mappedfields) {
    global $DB;

    assert(is_object($log), '$log must be an object');
    assert(is_object($source), '$source must be an object');
    assert(is_object($mappedfields), '$mappedfields must be an object');

    $deleted = $DB->get_field('user', 'deleted', array('id' => $mappedfields->userid));
    if ($deleted == 1) {
        // do not update deleted users to avoid assign_user_position() throwing an exception
        return false;
    }

    $syncfields = new stdClass();

    if (!empty($mappedfields->posfullname)) {
        $syncfields->fullname = $mappedfields->posfullname;
    }

    // position framework is optional setting
    if (!empty($source->posframeworkid)) {
        if (!empty($mappedfields->posidnumber)) {
            //
            // search for position hierarchy item by idnumber
            //
			$row = $DB->get_record_sql('SELECT id,fullname FROM {pos} WHERE LOWER(idnumber)=LOWER(:idnumber) AND frameworkid=:frameworkid', array('idnumber'=>$mappedfields->posidnumber, 'frameworkid'=>$source->posframeworkid));
            if (!$row) {
                $log->add_to_log(SyncLog::TYPE_WARNING, "position idnumber '$mappedfields->posidnumber' not found in position framework", "$mappedfields->firstname $mappedfields->lastname, idnumber=$mappedfields->idnumber, email=$mappedfields->email, userid=$mappedfields->userid");
            } else {
                $syncfields->positionid = $row->id;
                if (empty($syncfields->fullname)) {
                    $syncfields->fullname = $row->fullname;
                }
            }
        }

        if (empty($syncfields->positionid) and ! empty($mappedfields->posfullname)) {
            //
            // search for position hierarchy item by name
            //
			$row = $DB->get_record_sql('SELECT id,fullname FROM {pos} WHERE LOWER(fullname)=LOWER(:fullname) AND frameworkid=:frameworkid', array('fullname'=>$mappedfields->posfullname, 'frameworkid'=>$source->posframeworkid), IGNORE_MULTIPLE);
            if (!$row) {
                $log->add_to_log(SyncLog::TYPE_WARNING, "position fullname '$mappedfields->posfullname' not found in position framework", "$mappedfields->firstname $mappedfields->lastname, idnumber=$mappedfields->idnumber, email=$mappedfields->email, userid=$mappedfields->userid");
            } else {
                $syncfields->positionid = $row->id;
            }
        }
    }


    // organisation framework is optional setting
    if (!empty($source->orgframeworkid)) {
        if (!empty($mappedfields->orgidnumber)) {
            //
            // search for organisation hierarchy item by idnumber
            //
        
            $id = $DB->get_field_sql('SELECT id FROM {org} WHERE LOWER(idnumber)=LOWER(:idnumber) AND frameworkid=:frameworkid', array('idnumber' => $mappedfields->orgidnumber, 'frameworkid' => $source->orgframeworkid));
            if (!$id) {
                $log->add_to_log(SyncLog::TYPE_WARNING, "organisation idnumber '$mappedfields->orgidnumber' not found in organisation framework", "$mappedfields->firstname $mappedfields->lastname, idnumber=$mappedfields->idnumber, email=$mappedfields->email, userid=$mappedfields->userid");
            } else {
                $syncfields->organisationid = $id;
            }
        }
        if (empty($syncfields->organisationid) and ! empty($mappedfields->orgfullname)) {
            //
            // search for organisation hierarchy item by name
            //
            $id = $DB->get_field_sql('SELECT id FROM {org} WHERE LOWER(fullname)=LOWER(:fullname) AND frameworkid=:frameworkid', array('fullname' => $mappedfields->orgfullname, 'frameworkid' => $source->orgframeworkid), IGNORE_MULTIPLE);
            if (!$id) {
                $log->add_to_log(SyncLog::TYPE_WARNING, "organisation fullname '$mappedfields->orgfullname' not found in organisation framework", "$mappedfields->firstname $mappedfields->lastname, idnumber=$mappedfields->idnumber, email=$mappedfields->email, userid=$mappedfields->userid");
            } else {
                $syncfields->organisationid = $id;
            }
        }
    }

    if (!empty($mappedfields->manageridnumber)) {
        //
        // search for manager 
        //
    
        $id = $DB->get_field_sql('SELECT id FROM {user} WHERE LOWER(idnumber)=LOWER(:idnumber) AND deleted=0', array('idnumber' => $mappedfields->manageridnumber));
        if (!$id) {
            $log->add_to_log(SyncLog::TYPE_WARNING, "manager idnumber '$mappedfields->manageridnumber' not found in LMS", "$mappedfields->firstname $mappedfields->lastname, idnumber=$mappedfields->idnumber, email=$mappedfields->email, userid=$mappedfields->userid");
        } else {
            $syncfields->managerid = $id;
        }
    }

    if (!empty($mappedfields->appraiseridnumber)) {
        //
        // search for appraiser 
        //
        $id = $DB->get_field_sql('SELECT id FROM {user} WHERE LOWER(idnumber)=LOWER(:idnumber) AND deleted=0', array('idnumber' => $mappedfields->appraiseridnumber));
        if (!$id) {
            $log->add_to_log(SyncLog::TYPE_WARNING, "appraiser idnumber '$mappedfields->appraiseridnumber' not found in LMS", "$mappedfields->firstname $mappedfields->lastname, idnumber=$mappedfields->idnumber, email=$mappedfields->email, userid=$mappedfields->userid");
        } else {
            $syncfields->appraiserid = $id;
        }
    }

    if (!empty($mappedfields->posstartdate)) {
        if (!empty($mappedfields->posstartdate)) {
            $time = totara_date_parse_from_format($source->dateformat, $mappedfields->posstartdate);
            if ($time == -1) {
                $log->add_to_log(SyncLog::TYPE_WARNING, "unable to set position start date, invalid posstartdate: $mappedfields->posstartdate", "$mappedfields->firstname $mappedfields->lastname, idnumber=$mappedfields->idnumber, email=$mappedfields->email, userid=$mappedfields->userid");
            } else {
                $syncfields->timevalidfrom = $time;
            }
        }
    }

    if (!empty($mappedfields->posenddate)) {
        if (!empty($mappedfields->posenddate)) {
            $time = totara_date_parse_from_format($source->dateformat, $mappedfields->posenddate);
            if ($time == -1) {
                $log->add_to_log(SyncLog::TYPE_WARNING, "unable to set position finish date, invalid posenddate: $mappedfields->posenddate", "$mappedfields->firstname $mappedfields->lastname, idnumber=$mappedfields->idnumber, email=$mappedfields->email, userid=$mappedfields->userid");
            } else {
                $syncfields->timevalidto = $time;
            }
        }
    }

    // Attempt to load position assignment
    $pa = new position_assignment(array('userid' => $mappedfields->userid, 'type' => POSITION_TYPE_PRIMARY));

    if (empty($pa->id)) {
        //
        // create position assignment
        //
		if (!empty($syncfields->positionid) or ! empty($syncfields->fullname) or ! empty($syncfields->organisationid) or ! empty($syncfields->managerid) or ! empty($syncfields->appraiserid)) {
            androgogic_sync_fields($pa, $syncfields);

            $pa->timecreated = time();
            $pa->timemodified = $pa->timecreated;
            $pa->usermodified = get_admin()->id;

            assign_user_position($pa);
            $log->add_to_log(SyncLog::TYPE_TRACE, 'primary position assignment created', "$pa->fullname, posassignmentid=$pa->id, $mappedfields->firstname $mappedfields->lastname, username=$mappedfields->username, idnumber=$mappedfields->idnumber, userid=$mappedfields->userid");
        }
    } else {
        //
        // Update position assignment
        //
		$modified = androgogic_sync_fields($pa, $syncfields);
        if ($modified) {
            $pa->timemodified = time();
            $pa->usermodified = get_admin()->id;

            assign_user_position($pa);
            $log->add_to_log(SyncLog::TYPE_TRACE, 'primary position assignment updated', "$pa->fullname, posassignmentid=$pa->id, $mappedfields->firstname $mappedfields->lastname, username=$mappedfields->username, idnumber=$mappedfields->idnumber, userid=$mappedfields->userid");
        }
    }
    return true;
}
