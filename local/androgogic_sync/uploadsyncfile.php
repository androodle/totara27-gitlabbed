<?php
/**
 * Androgogic Sync
 *
 * @author      Keith Buss <kbuss@outlook.com>
 * @version     August 2015
 *
 **/
 
require_once(dirname(dirname(dirname(__FILE__))).'/config.php');
require_once($CFG->libdir.'/adminlib.php');
require_once($CFG->libdir.'/formslib.php');
require_once('classes/uploadsyncfileform.class.php');

require_login();
 
$context = context_system::instance();
require_capability('local/androgogic_sync:uploadsyncfile', $context);

// make sure we really are on the https page
$PAGE->https_required();

// create form
admin_externalpage_setup('uploadsyncfile', '', null, new moodle_url($CFG->httpswwwroot.'/local/androgogic_sync/uploadsyncfile.php'));

$PAGE->verify_https_required();

$heading = get_string('uploadsyncfile', 'local_androgogic_sync');
echo $OUTPUT->header();
echo $OUTPUT->heading($heading);
 
$syncdir = get_config('local_androgogic_sync', 'sync_dir');	
if (empty($syncdir)) {
	echo $OUTPUT->notification('Sync directory setting is required');
    echo $OUTPUT->footer();
    exit();
}
if (!is_dir($syncdir)) {
	echo $OUTPUT->notification("Sync directory '$syncdir' is not found");
    echo $OUTPUT->footer();
    exit();
}

$mform = new uploadsyncfileform();

// cancelled
if (!$mform->is_cancelled() and $data = $mform->get_data()) {

	$filename = $mform->get_new_filename('syncfile');
	$path = $syncdir.'/'.$filename;
	if (file_exists($path)) {
		// create unique filename to avoid overwriting existing files
		$path_parts = pathinfo($path);
		$basename = basename($path, '.'.$path_parts['extension']);
		$count = 0;
		do {
			$count++;
			$path = $path_parts['dirname'].'/'.$basename.$count.'.'.$path_parts['extension'];
		} while (file_exists($path));
	}
    if (!$mform->save_file('syncfile', $path)) {
		totara_set_notification("Failed saving sync file to $path", 'uploadsyncfile.php', array('class'=>'notifyproblem'));
	} else {
		totara_set_notification("Sync file saved to $path", 'uploadsyncfile.php', array('class'=>'notifysuccess'));
	}
}

///
/// Display page
///

$mform->display();

echo $OUTPUT->footer();

