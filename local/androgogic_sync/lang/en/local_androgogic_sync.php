<?php
/** 
 * Androgogic Sync
 *
 * @author      Keith Buss <kbuss@outlook.com>
 * @version     May 2015
 *
 **/
require_once($CFG->dirroot.'/local/androgogic_sync/classes/synclog.class.php');

defined('MOODLE_INTERNAL') || die();

$string['pluginname'] = 'AndroSync';

///
/// Capabilities
///
$string['androgogic_sync:managesources'] = 'Manage sync sources';
$string['androgogic_sync:uploadsyncfile'] = 'Upload sync files';
$string['androgogic_sync:settings'] = 'General settings';
$string['androgogic_sync:viewlog'] = 'View log';
$string['androgogic_sync:runsync'] = 'Run sync';

///
/// Admin menu
///
$string['managesources'] = 'Manage sources';
$string['uploadsyncfile'] = 'Upload sync file';
$string['viewlog'] = 'View log';
$string['runsync'] = 'Run sync';
$string['generalsettings'] = 'General settings';

///
/// run sync
///
$string['alreadyrunning'] = 'An instance of AndroSync is currently running';

//
// Cron
// 
$string['syncstarted'] = 'AndroSync started';
$string['syncsuccess'] = 'AndroSync completed successfully';
$string['syncproblem'] = 'AndroSync terminated with error';

///
/// Sources
///
$string[SyncLog::SOURCE_CSV] = 'CSV file';
$string[SyncLog::SOURCE_WEBSERVICE] = 'Web service';

///
/// Elements
///
$string[SyncLog::ELEMENT_USER] = 'User';
$string[SyncLog::ELEMENT_ORG] = 'Organisation';
$string[SyncLog::ELEMENT_POS] = 'Position';
$string[SyncLog::ELEMENT_COMP] = 'Course completion';

///
/// Manage sources
///
$string['deletesource'] = 'Delete source';
$string['addsource'] = 'Add source';

$string['shortname'] = 'Name';
$string['dateformat'] = 'Date format';
$string['senduseremail'] = 'Send email to users';
$string['senduseremail_help'] = 'Notification of new account or username change';

$string['fieldmapping'] = 'Field mapping';
$string['fieldmapping_help'] = 'Map CSV file columns to LMS fields by selecting the CSV column number or Excel column letter.<BR>It is possible to map the same CSV column to multiple LMS fields.';
$string['customfields'] = 'User custom field mapping';
$string['idnumberuid'] = 'idnumber is required for UID';
$string['requiredfield'] = 'required field';

$string['obsoleteusers'] = 'Obsolete users';
$string['suspenduser'] = 'Suspend user account in LMS';
$string['deleteuser'] = 'Delete user account in LMS';

$string['coursenotfound'] = 'Course ID number not found in LMS';
$string['createcourses'] = 'Create new course in LMS';
$string['createlearningplan'] = 'Create new learning plan in LMS';

///
/// Source file settings
///
$string['filesettings'] = 'Source settings';
$string['csvfileprefix'] = 'File prefix';

$string['allrecords'] = 'Source contains all records';
$string['allrecords_help'] = 'Does the source provide all sync records, everytime OR are only records that need to be updated/deleted provided? If "No" (only records to be updated/deleted), then the source must use the "delete" flag.';

$string['csvheader'] = 'CSV File header';
$string['csvheader_help'] = 'Is first row of the CSV file a header record containing column names';

$string['syncdir'] = 'Sync directory';
$string['archivedir'] = 'Archive directory';

$string['sourceadded'] = 'Source added';
$string['sourceupdated'] = 'Source updated';
$string['sourcedeleted'] = 'Source deleted';

///
/// Source field defaults
///
$string['fielddefaults'] = 'Field defaults';

$string['auth'] = 'Authentication';
$string['auth_help'] = 'Authentication for new users when authentication is missing from source';

$string['policyagreed'] = 'Policy agreed';

$string['forcepasswordchange'] = 'Force password change';
$string['forcepasswordchange_help'] = 'Force new users to change password, not applicable to SSO authentication';

$string['allowduplicatedemails'] = 'Allow duplicate emails';

$string['orgframework'] = 'Organisation framework';
$string['posframework'] = 'Position framework';
$string['coursecategory'] = 'Course category';
$string['evidencetype'] = 'Evidence type';

///
/// CSV delimiters
///
$string['csvdelimiter'] = 'Field delimiter';
$string['comma'] = 'Comma (,)';
$string['semicolon'] = 'Semi-colon (;)';
$string['colon'] = 'Colon (:)';
$string['tab'] = 'Tab (\t)';
$string['pipe'] = 'Pipe (|)';

///
/// Upload source files
///
$string['syncfile'] = 'Sync file';

///
/// General settings
///
$string['cronhours'] = 'CRON hours';
$string['cronhours_help'] = 'A comma-separated list of hours in 24 hour format (0 - 23) when the CRON should execute';

$string['stagingretention'] = 'Staging data retention period';
$string['stagingretention_help'] = 'Number of days the staging data is kept (0 = retain all staging data)';

$string['notifications'] = 'Notifications';
$string['notifyemailto'] = 'Email Sync Log messages to';
$string['notifyemailto_help'] = 'A comma-separated list of email addresses to which notifications should be sent';
$string['notifyinfo'] = 'Send log info';
$string['notifywarning'] = 'Send log warning';
$string['notifyreview'] = 'Send log review';
$string['notifyerror'] = 'Send log error';

///
/// new account email
///
$string['newaccountemail'] = 'Dear {$a->firstname}

Your account has been created on the {$a->sitename} which is available at {$a->siteurl} . Your username is: {$a->username} 

Please set your password at: {$a->changepasswordurl}';

///
/// new username email
///
$string['newusernameemail'] = 'Dear {$a->firstname}

Your username has changed on the {$a->sitename} which is available at {$a->siteurl} . Your new username is: {$a->username} 

Please set your password at: {$a->changepasswordurl}';

///
/// Log notification email
///
$string['notifymessage'] = '{$a->time}  {$a->logtype}: {$a->message}';
$string['notifymessagestart'] = '{$a->count} new AndroSync log messages ({$a->logtypes})';
$string['notifysubject'] = '{$a} : AndroSync notification';
$string['syncnotifications'] = 'AndroSync notifications';
$string['viewloghere'] = 'For more information, view the AndroSync log at {$a}';

///
/// Log report
///
$string['viewstaging'] = 'Staging data';

$string['logreport'] = 'AndroSync Log';
$string['logid'] = 'Log ID';
$string['stagingid'] = 'Staging ID';
$string['logtype'] = 'Log type';
$string['source'] = 'Source';
$string['element'] = 'Element';
$string['action'] = 'Action';
$string['info'] = 'Info';
$string['runid'] = 'Run ID';
$string['datetime'] = 'Date/Time';
?>