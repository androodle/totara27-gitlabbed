<?php
// This file is part of the Local welcome plugin
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package    local
 * @subpackage activityfinder
 * @copyright  2015 onwards Androgogic Pty Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.');    // It must be included from a Moodle page.
}

require_once($CFG->dirroot.'/lib/formslib.php');

class logviewer_search_form extends moodleform 
{
    public function definition() 
    {
        global $DB;
        
        $mform =& $this->_form;
        
        $mform->addElement('date_time_selector', 'timestart', get_string('datetime', 'local_logviewer'), array('optional'=>true));
        $mform->setType('timestart', PARAM_INT);
        
        $mform->addElement('date_time_selector', 'timeend', '', array('optional'=>true));
        $mform->setType('timeend', PARAM_INT);
        
        $sql = "SELECT DISTINCT(action) action FROM {log} ORDER BY action";
        $results = $DB->get_records_sql($sql);
        $actions = array(0 => '');
        foreach ($results as $result) {
            $actions[$result->action] = $result->action;
        }
        asort($actions);
        $mform->addElement('select', 'action', get_string('action'), $actions);
        $mform->setType('action', PARAM_TEXT);
        
        $mform->addElement('text', 'module', get_string('module', 'local_logviewer'));
        $mform->setType('module', PARAM_TEXT);
        
        $mform->addElement('text', 'userid', get_string('userid', 'local_logviewer'), array('size' => 6));
        $mform->setType('userid', PARAM_TEXT);
        
        $mform->addElement('text', 'courseid', get_string('courseid', 'local_logviewer'), array('size' => 6));
        $mform->setType('courseid', PARAM_TEXT);
        
        $mform->addElement('text', 'cmid', get_string('cmid', 'local_logviewer'), array('size' => 6));
        $mform->setType('cmid', PARAM_TEXT);
        
        $this->add_action_buttons();
    }
    
    function add_action_buttons()
    {
        $mform =& $this->_form;
        $this_url = new moodle_url('/local/logviewer/');
        
        $buttonarray=array();
        $buttonarray[] = &$mform->createElement('submit', 'submitbutton', get_string('search'));
        $buttonarray[] = &$mform->createElement('button', 'resetbutton', get_string('reset'), array(
            'onclick' => 'javascript:location.href=\''.$this_url.'\';'
        ));
        $mform->addGroup($buttonarray, 'buttonar', '', array(' '), false);
        $mform->closeHeaderBefore('buttonar');
    }
}