<?php
// This file is part of the Local welcome plugin
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package    local
 * @subpackage logviewer
 * @copyright  2015 onwards Androgogic Pty Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

function local_logviewer_get_results_where_cond($data, $total = false) 
{
    $params = array();
    $where = array();
    
    if (!empty($data->timestart) && !empty($data->timeend)) {
        $where[] = " l.time BETWEEN :timestart AND :timeend";
        $params['timestart'] = $data->timestart;
        $params['timeend'] = $data->timeend;
    } elseif (!empty($data->timestart) && empty($data->timeend)) {
        $where[] = " l.time >= :timestart";
        $params['timestart'] = $data->timestart;
    } elseif (empty($data->timestart) && !empty($data->timeend)) {
        $where[] = " l.time <= :timeend";
        $params['timeend'] = $data->timeend;
    }
    
    if (!empty($data->action)) {
        $where[] = " l.action = :action";
        $params['action'] = $data->action;
    }
    
    if (!empty($data->module)) {
        $where[] = " l.module = :module";
        $params['module'] = $data->module;
    }
    
    if (!empty($data->userid)) {
        $where[] = " l.userid = :userid";
        $params['userid'] = $data->userid;
    }
    
    if (!empty($data->courseid)) {
        $where[] = " l.course = :courseid";
        $params['courseid'] = $data->courseid;
    }
    
    if (!empty($data->cmid)) {
        $where[] = " l.cmid = :cmid";
        $params['cmid'] = $data->cmid;
    }
    
    return array($where, $params);
}

function local_logviewer_get_results_count($data, $total = false) 
{
    global $DB;
    
    list($where, $params) = local_logviewer_get_results_where_cond($data, $total);

    $sql = "SELECT COUNT(*) FROM {log} l ";
    if (!empty($where)) {
        $sql .= ' WHERE '.implode(' AND ', $where);
    }
    
    return $DB->count_records_sql($sql, $params);
}

function local_logviewer_get_results($data, $start, $limit) 
{
    global $DB;
    
    list($where, $params) = local_logviewer_get_results_where_cond($data);

    $sql = "SELECT l.* FROM {log} l ";
    if (!empty($where)) {
        $sql .= ' WHERE '.implode(' AND ', $where);
    }
    
    $sql .= ' ORDER BY id DESC ';
    $sql .= ' LIMIT '.($start * $limit).', '.$limit;
    
    return $DB->get_records_sql($sql, $params);
}