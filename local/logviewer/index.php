<?php
// This file is part of the Local welcome plugin
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * @package    local
 * @subpackage logviewer
 * @copyright  2015 onwards Androgogic Pty Ltd
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once('../../config.php');
require_once($CFG->dirroot . '/local/logviewer/locallib.php');
require_once($CFG->dirroot . '/local/logviewer/search_form.php');

require_login();

$page = optional_param('page', -1, PARAM_INT);
$courseid = optional_param('courseid', null, PARAM_INT);
$userid = optional_param('userid', null, PARAM_INT);
$cmid = optional_param('cmid', null, PARAM_INT);
$module = optional_param('module', null, PARAM_TEXT);
$action = optional_param('action', null, PARAM_TEXT);
$timestart = optional_param('timestart', null, PARAM_INT);
$timeend = optional_param('timeend', null, PARAM_INT);
$limit = 50;

$context = context_system::instance();
$PAGE->set_context($context);
$PAGE->set_url('/local/logviewer/index.php');
$PAGE->set_heading($SITE->fullname);
$PAGE->set_title(get_string('pluginname', 'local_logviewer'));
$PAGE->navbar->add(get_string('pluginname', 'local_logviewer'));

require_capability('local/logviewer:view', $context);

echo $OUTPUT->header();
echo $OUTPUT->heading(get_string('pluginname', 'local_logviewer'), 3, 'main');

$mform = new logviewer_search_form();
$mform->display();

$data = $mform->get_data();
if (empty($data) && $page == -1) {
    echo $OUTPUT->footer();
    die;
}
$data->courseid = $courseid;
$data->userid = $userid;
$data->cmid = $cmid;
$data->module = $module;
$data->action = $action;
$data->timestart = $timestart;
$data->timeend = $timeend;

if ($page == -1) {
    $page = 0; // a clunky, but necessary evil
}

$totalcount = local_logviewer_get_results_count($data);
if (empty($totalcount)) {
    echo $OUTPUT->notification(get_string('nothingfound', 'local_logviewer'));
    echo $OUTPUT->footer();
    die;
}

$baseurl = new moodle_url('/local/logviewer/index.php', array(
    'courseid' => $courseid,
    'userid' => $userid,
    'cmid' => $cmid,
    'module' => $module,
    'action' => $action,
    'timestart' => $timestart,
    'timeend' => $timeend,
));
$pagingbar = new paging_bar($totalcount, $page, $limit, $baseurl);
$pagingbar->pagevar = 'page';
echo $OUTPUT->render($pagingbar);

echo '<table class="generaltable">';
echo '<tr>';
echo '<th>'.get_string('datetime', 'local_logviewer').'</th>';
echo '<th>'.get_string('user').'</th>';
echo '<th>'.get_string('courseid', 'local_logviewer').'</th>';
echo '<th>'.get_string('module', 'local_logviewer').'</th>';
echo '<th>'.get_string('cmid', 'local_logviewer').'</th>';
echo '<th>'.get_string('action').'</th>';
echo '<th>'.get_string('url').'</th>';
echo '<th>'.get_string('info').'</th>';
echo '<th>'.get_string('ip', 'local_logviewer').'</th>';
echo '</tr>';

$results = local_logviewer_get_results($data, $page, $limit);
foreach ($results as $result) {
    
    $userurl = new moodle_url('/user/view.php', array('id' => $result->userid));
    $courseurl = new moodle_url('/course/view.php', array('id' => $result->course));
    
    echo '<tr>';
    echo '<td style="white-space: nowrap;">'.date('Y-m-d H:i:s', $result->time).'</td>';
    echo '<td><a href="'.$userurl.'" target="_blank">'.$result->userid.'</a></td>';
    echo '<td><a href="'.$courseurl.'" target="_blank">'.$result->course.'</a></td>';
    echo '<td>'.$result->module.'</td>';
    echo '<td>'.$result->cmid.'</td>';
    echo '<td>'.$result->action.'</td>';
    echo '<td><a href="'.$CFG->wwwroot.'/'.$result->url.'" target="_blank">'.$result->url.'</a></td>';
    echo '<td>'.$result->info.'</td>';
    echo '<td>'.$result->ip.'</td>';
    echo '</tr>';
}

echo '</table>';

echo $OUTPUT->render($pagingbar);

echo $OUTPUT->footer();
