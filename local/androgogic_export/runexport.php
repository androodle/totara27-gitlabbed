<?php
/** 
 * Androgogic Export
 *
 * @author      Keith Buss <kbuss@outlook.com>
 * @version     July 2015
 *
 **/
 
require_once(dirname(dirname(dirname(__FILE__))).'/config.php');
require_once("{$CFG->libdir}/adminlib.php");
require_once($CFG->dirroot.'/local/androgogic_export/lib.php');

require_login();

$context = context_system::instance();
require_capability('local/androgogic_export:runexport', $context);

admin_externalpage_setup('runexport');

$heading = get_string('runexport', 'local_androgogic_export');
$execute = optional_param('execute', false, PARAM_BOOL);

echo $OUTPUT->header();
echo $OUTPUT->heading($heading);

$buttons = $OUTPUT->single_button(new moodle_url('/local/androgogic_export/runexport.php', array('execute'=>1)), get_string('runexport', 'local_androgogic_export'), 'post');
	
if ($execute) {
	require_sesskey();
	
	// Run the export
	if (androgogic_export_get_cronlock()) {
		androgogic_export_run(true);
	} else {
		echo $OUTPUT->notification(get_string('alreadyrunning', 'local_androgogic_sync'));
	}
	$spacer = $OUTPUT->spacer(array('height'=>15, 'width'=>15)); // should be done with CSS instead
	$buttons .= $spacer.$OUTPUT->single_button(new moodle_url('/local/androgogic_export/exportlog.php'), get_string('viewlog', 'local_androgogic_export'), 'post');

} 

echo $OUTPUT->container_start('buttons mdl-align');
echo $buttons;
echo $OUTPUT->container_end();

echo $OUTPUT->footer();

?>
