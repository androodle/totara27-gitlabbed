<?php
/**
 * Androgogic Export
 *
 * @author      Keith Buss <kbuss@outlook.com>
 * @version     July 2015
 *
 **/
 
require_once(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once($CFG->libdir.'/adminlib.php');

require_login();

$context = context_system::instance();
require_capability('local/androgogic_export:manageexports', $context);

// Get params
$id = required_param('id', PARAM_INT);

// Delete confirmation hash
$delete = optional_param('delete', '', PARAM_ALPHANUM);

if (!$row = $DB->get_record('androgogic_export_source', array('id'=>$id))) {
    throw new Exception($DB->get_last_error());	
}

$elementname = get_string($row->element, 'local_androgogic_export');
$formatname = get_string($row->format, 'local_androgogic_export');
$heading = get_string('deleteexport', 'local_androgogic_export');

$url_params = array('id'=>$id, 'delete'=>$delete);
admin_externalpage_setup('manageexports', '', $url_params, new moodle_url('/local/androgogic_export/deleteexport.php'));
    
///
/// Display page
///

if (!$delete) {
    echo $OUTPUT->header();

    echo $OUTPUT->heading($heading);

    echo $OUTPUT->confirm("Are you sure you want to delete the $row->shortname $elementname $formatname settings?" . html_writer::empty_tag('br') . html_writer::empty_tag('br'), "deleteexport.php?id={$id}&amp;delete=".md5($row->timemodified)."&amp;sesskey={$USER->sesskey}", "exports.php");

    echo $OUTPUT->footer();
    exit;
}

///
/// Delete source
///

if ($delete != md5($row->timemodified)) {
	throw new Exception("unable to delete, record has been modified by another");	
}
if (!confirm_sesskey()) {
	throw new Exception("invalid sesskey");	
}

$DB->execute("UPDATE {androgogic_export_source} SET deleted=1 WHERE id=$id");

// Log
//add_to_log(SITEID, $prefix, 'framework delete', "framework/index.php?prefix=$prefix", "$framework->fullname (ID $row->id)");
totara_set_notification(get_string('sourcedeleted', 'local_androgogic_export'), 'exports.php', array('class'=>'notifysuccess'));

echo $OUTPUT->footer();
