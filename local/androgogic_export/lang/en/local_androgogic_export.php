<?php
/** 
 * Androgogic Export
 *
 * @author      Keith Buss <kbuss@outlook.com>
 * @version     July 2015
 *
 **/
require_once($CFG->dirroot.'/local/androgogic_export/classes/exportlog.class.php');

defined('MOODLE_INTERNAL') || die();

$string['pluginname'] = 'AndroExport';

///
/// Capabilities
///
$string['androgogic_export:manageexports'] = 'Manage exports';
$string['androgogic_export:settings'] = 'General settings';
$string['androgogic_export:viewlog'] = 'View export log';
$string['androgogic_export:runexport'] = 'Run export';

///
/// Format
///
$string[ExportLog::FORMAT_CSV] = 'CSV file';

///
/// Elements
///
$string[ExportLog::ELEMENT_USER] = 'User';
$string[ExportLog::ELEMENT_COURSE] = 'Course';
$string[ExportLog::ELEMENT_COMP] = 'Course completion';

///
/// Admin menu
///
$string['manageexports'] = 'Manage exports';
$string['viewlog'] = 'View log';
$string['runexport'] = 'Run export';
$string['generalsettings'] = 'General settings';

//
// Cron
// 
$string['syncstarted'] = 'AndroExport started';
$string['syncsuccess'] = 'AndroExport completed successfully';
$string['syncproblem'] = 'AndroExport terminated with error';

///
/// Manage exports
///
$string['deleteexport'] = 'Delete export';
$string['addexport'] = 'Add export';

$string['shortname'] = 'Name';
$string['dateformat'] = 'Date format';
$string['format'] = 'Format';

$string['exportfields'] = 'Export fields';
$string['usercustomfields'] = 'User custom fields';
$string['coursecustomfields'] = 'Course custom fields';

$string['exportdir'] = 'Export directory';

$string['csvfile'] = 'CSV file';
$string['csvfileprefix'] = 'File prefix';

$string['csvheader'] = 'CSV File header';
$string['csvheader_help'] = 'Should the first row of the CSV file be a header record containing column names';

$string['allrecords'] = 'Include all records?';
$string['allrecords_help'] = 'Does the export include all records OR only records that have been created/updated since the last export?';

$string['completionstatus'] = 'Record filtering by completion status';
$string['statusnotyetstarted'] = 'Not yet started';
$string['statusinprogress'] = 'In progress';
$string['statuscomplete'] = 'Complete';
$string['statuscompleteviarpl'] = 'Complete via RPL';

$string['sourceadded'] = 'Export added';
$string['sourceupdated'] = 'Export updated';
$string['sourcedeleted'] = 'Export deleted';

///
/// CSV delimiters
///
$string['csvdelimiter'] = 'Field delimiter';
$string['comma'] = 'Comma (,)';
$string['semicolon'] = 'Semi-colon (;)';
$string['colon'] = 'Colon (:)';
$string['tab'] = 'Tab (\t)';
$string['pipe'] = 'Pipe (|)';

///
/// General settings
///
$string['settingssaved'] = 'Settings saved';

$string['cronhours'] = 'CRON hours';
$string['cronhours_help'] = 'A comma-separated list of hours in 24 hour format (0 - 23) when the CRON should execute';

$string['notifications'] = 'Notifications';
$string['notifyemailto'] = 'Email Export Log messages to';
$string['notifyemailto_help'] = 'A comma-separated list of email addresses to which notifications should be sent';
$string['notifyinfo'] = 'Send log info';
$string['notifywarning'] = 'Send log warnings';
$string['notifyerror'] = 'Send log errors';

///
/// New account email
///
$string['newaccountemail'] = 'Dear {$a->firstname}

Your account has been created on the {$a->sitename} which is available at {$a->siteurl} . Your username is: {$a->username} 

Please set your password at: {$a->changepasswordurl}';

///
/// Log notification email
///
$string['notifymessage'] = '{$a->time}  {$a->logtype}: {$a->message}';
$string['notifymessagestart'] = '{$a->count} new AndroExport log messages ({$a->logtypes})';
$string['notifysubject'] = '{$a} : AndroExport notification';
$string['syncnotifications'] = 'AndroExport notifications';
$string['viewloghere'] = 'For more information, view the AndroExport log at {$a}';

///
/// Log report
///
$string['logreport'] = 'AndroExport Log';
$string['logid'] = 'Log ID';
$string['logtype'] = 'Log type';
$string['export'] = 'Export';
$string['element'] = 'Element';
$string['action'] = 'Action';
$string['info'] = 'Info';
$string['runid'] = 'Run ID';
$string['datetime'] = 'Date/Time';
?>