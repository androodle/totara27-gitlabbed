<?php
/**
 * Androgogic Export
 *
 * @author      Keith Buss <kbuss@outlook.com>
 * @version     July 2015
 *
 **/
 
defined('MOODLE_INTERNAL') || die();

global $CFG;

require_once($CFG->dirroot.'/local/androgogic_export/classes/exportlog.class.php');

class rb_source_androgogic_export_log extends rb_base_source {
    public $base, $joinlist, $columnoptions, $filteroptions;
    public $contentoptions, $paramoptions, $defaultcolumns;
    public $defaultfilters, $requiredcolumns, $sourcetitle;
   
    function __construct() {
        global $CFG;
        $this->base = '{androgogic_export_log}';
        $this->joinlist = $this->define_joinlist();
        $this->columnoptions = $this->define_columnoptions();
        $this->filteroptions = $this->define_filteroptions();
        $this->contentoptions = $this->define_contentoptions();
        $this->paramoptions = $this->define_paramoptions();
        $this->defaultcolumns = $this->define_defaultcolumns();
        $this->defaultfilters = $this->define_defaultfilters();
        $this->requiredcolumns = $this->define_requiredcolumns();
        $this->sourcetitle = get_string('logreport', 'local_androgogic_export');
        parent::__construct();
    }

    //
    //
    // Methods for defining contents of source
    //
    //

    protected function define_joinlist() {
        $joinlist = array(
            new rb_join(
                'source',
                'LEFT',
                '{androgogic_export_source}',
                'source.id = base.sourceid',
                REPORT_BUILDER_RELATION_ONE_TO_ONE
            )
        );

        //$this->add_user_table_to_joinlist($joinlist, 'base', 'userid');
        //$this->add_position_tables_to_joinlist($joinlist, 'base', 'userid');
        return $joinlist;
    }

    protected function define_columnoptions() {
        
        $columnoptions = array(
            new rb_column_option(
                'androgogic_export_log',
                'id',
                get_string('logid', 'local_androgogic_export'),
                "base.id"
            ),
            new rb_column_option(
                'androgogic_export_log',
                'runid',
                get_string('runid', 'local_androgogic_export'),
                "base.runid"
            ),
            new rb_column_option(
                'androgogic_export_log',
                'time',
                get_string('datetime', 'local_androgogic_export'),
                "base.time",
                array('displayfunc'=>'nice_datetime_seconds')
            ),
            new rb_column_option(
                'androgogic_export_log',
                'logtype',
                get_string('logtype', 'local_androgogic_export'),
                "base.logtype",
                array('displayfunc'=>'logtype')
            ),
            new rb_column_option(
                'androgogic_export_log',
                'action',
                get_string('action', 'local_androgogic_export'),
                "base.action"
            ),
            new rb_column_option(
                'androgogic_export_log',
                'info',
                get_string('info', 'local_androgogic_export'),
                "base.info",
                array('displayfunc'=>'linkinfo')
            ),         
            new rb_column_option(
                'androgogic_export_log',
                'sourceid',
                get_string('export', 'local_androgogic_export'),
                "base.sourceid",
                array('joins'=>'source',
                	'displayfunc'=>'linksourceid',
                    'extrafields' => array(
                    'format' => 'source.format',
                    'element' => 'source.element',
                    'shortname' => 'source.shortname'
               ))
            ),                    
        );

        return $columnoptions;
    }

    protected function define_filteroptions() {
        $filteroptions = array(
            new rb_filter_option(
                'androgogic_export_log',         // type
                'runid',           // value
                get_string('runid', 'local_androgogic_export'), // label
                'number'     // filtertype
            ),
            new rb_filter_option(
                'androgogic_export_log',         // type
                'time',           // value
                get_string('datetime', 'local_androgogic_export'), // label
                'date',     // filtertype
                array(
                    'includetime'=>true,
                )
            ),
            new rb_filter_option(
                'androgogic_export_log',         // type
                'logtype',           // value
                get_string('logtype', 'local_androgogic_export'), // label
                'select',     // filtertype
                array(
                    'selectfunc'=>'logtypes',
                    'attributes'=>rb_filter_option::select_width_limiter(),
                )
            ),
            new rb_filter_option(
                'androgogic_export_log',         // type
                'action',           // value
                get_string('action', 'local_androgogic_export'), // label
                'text'     // filtertype
            ),
            new rb_filter_option(
                'androgogic_export_log',         // type
                'info',           // value
                get_string('info', 'local_androgogic_export'), // label
                'textarea'     // filtertype
            ),            
            new rb_filter_option(
                'androgogic_export_log',         // type
                'sourceid',           // value
                get_string('export', 'local_androgogic_export'), // label
                'select',     // filtertype
                array(
                    'selectfunc'=>'sources'
                )
            ),
        );

        return $filteroptions;
    }

    protected function define_contentoptions() {
        $contentoptions = array(

            new rb_content_option(
                'date',
                get_string('datetime', 'local_androgogic_export'),
                'base.time'
            ),
        );

        return $contentoptions;
    }

    protected function define_paramoptions() {
        return array();
    }

    protected function define_defaultcolumns() {
        $defaultcolumns = array(
            array(
                'type'=>'androgogic_export_log',
                'value'=>'id',
            ),
            array(
                'type'=>'androgogic_export_log',
                'value'=>'runid',
            ),
            array(
                'type'=>'androgogic_export_log',
                'value'=>'time',
            ),
            array(
                'type'=>'androgogic_export_log',
                'value'=>'logtype',
            ),
            array(
                'type'=>'androgogic_export_log',
                'value'=>'action',
            ),
            array(
                'type'=>'androgogic_export_log',
                'value'=>'info',
            ),                 
            array(
                'type'=>'androgogic_export_log',
                'value'=>'sourceid',
            ),        
        );

        return $defaultcolumns;
    }

    protected function define_defaultfilters() {
        $defaultfilters = array(
            array(
                'type'=>'androgogic_export_log',
                'value'=>'runid',
                'advanced'=>0,
            ),
            array(
                'type'=>'androgogic_export_log',
                'value'=>'time',
                'advanced'=>0,
            ),
            array(
                'type'=>'androgogic_export_log',
                'value'=>'logtype',
                'advanced'=>0,
            ),
            array(
                'type'=>'androgogic_export_log',
                'value'=>'action',
                'advanced'=>0,
            ),
            array(
                'type'=>'androgogic_export_log',
                'value'=>'info',
                'advanced'=>0,
            ), 

            array(
                'type'=>'androgogic_export_log',
                'value'=>'sourceid',
                'advanced'=>0,
            ),   
        );

        return $defaultfilters;
    }

    protected function define_requiredcolumns() {
        $requiredcolumns = array(
            /*
            // array of rb_column objects, e.g:
            new rb_column(
                '',         // type
                '',         // value
                '',         // heading
                '',         // field
                array()     // options
            )
            */
        );
        return $requiredcolumns;
    }


    //
    //
    // Source specific column display methods
    //
    //
    // add methods here with [name] matching column option displayfunc
    /*
    function rb_display_[name]($item, $row) {
        // variable $item refers to the current item
        // $row is an object containing the whole row
        // which will include any extrafields
        //
        // should return a string containing what should be displayed
    }
    */
    
    function rb_display_logtype($type, $row) {
        switch ($type) {
            case ExportLog::TYPE_ERROR:
                $class = 'notifyproblem';
                break;
            case ExportLog::TYPE_WARNING:
                $class = 'notifynotice';
                break;
            case ExportLog::TYPE_REVIEW:
                $class = 'notifynotice';
                break;
            default:
                $class = 'notifysuccess';
                break;
        }
        return html_writer::tag('span', $type, array('class'=>$class, 'title'=>$type));
    }
    
    function rb_display_linksourceid($sourceid, $row) {
		if (empty($sourceid)) {
			return '';
		}
		$elementname = get_string($row->element, 'local_androgogic_export');
		$formatname = get_string($row->format, 'local_androgogic_export');
        $url = new moodle_url('/local/androgogic_export/editexport.php', array('format'=>$row->format,'element'=>$row->element,'id'=>$sourceid));
        return html_writer::link($url, "$row->shortname, $elementname $formatname");
    }
    
    function rb_display_linkinfo($info, $row, $isexport = false) {
        if ($isexport) {
            return $info;
        }
        
		if (empty($info)) {
			return '';
		}
		
		$url = '';
		$pieces = explode(",", $info);
		foreach ($pieces as $piece) {
			$arr = explode("=", $piece);
			if (count($arr) == 2 and !empty($arr[1])) {
				$prefix = trim($arr[0]);
				$suffix = trim($arr[1]);
				if ($prefix == 'userid') {
					$url = new moodle_url('/user/view.php', array('id'=>$suffix));
					break;
				} else if ($prefix == 'orgid') {
					$url = new moodle_url('/totara/hierarchy/item/view.php', array('prefix'=>'organisation','id'=>$suffix));
					break;
				} else if ($prefix == 'posid') {
					$url = new moodle_url('/totara/hierarchy/item/view.php', array('prefix'=>'position','id'=>$suffix));
					break;
				} else if ($prefix == 'courseid') {
					$url = new moodle_url('/course/edit.php', array('id'=>$suffix));
					break;
				}
			}
		}
		if (empty($url)) {
			return $info;
		}
        return html_writer::link($url, $info);
    }

    //
    //
    // Source specific filter display methods
    //
    //

    function rb_filter_logtypes() {
        return array(
            ExportLog::TYPE_INFO=>ExportLog::TYPE_INFO,
            ExportLog::TYPE_REVIEW=>ExportLog::TYPE_REVIEW,
            ExportLog::TYPE_WARNING=>ExportLog::TYPE_WARNING,
            ExportLog::TYPE_TRACE=>ExportLog::TYPE_TRACE,
            ExportLog::TYPE_ERROR=>ExportLog::TYPE_ERROR
        );
    }
        
    function rb_filter_sources() {
		global $DB;

		$options = array();
		if ($rows = $DB->get_records('androgogic_export_source')) {
			foreach ($rows as $row) {
				$elementname = get_string($row->element, 'local_androgogic_export');
				$formatname = get_string($row->format, 'local_androgogic_export');
				$options[$row->id] = "$row->shortname, $elementname $formatname";
			}
		}
        return $options;
    }
} 

