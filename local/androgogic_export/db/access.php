<?php
/** 
 * Androgogic Export
 *
 * @author      Keith Buss <kbuss@outlook.com>
 * @version     July 2015
 *
 **/

defined('MOODLE_INTERNAL') || die();

$capabilities = array(
    'local/androgogic_export:settings' => array(
    	'riskbitmask'   => RISK_PERSONAL | RISK_DATALOSS | RISK_CONFIG,
        'captype' => 'write',
        'contextlevel' => CONTEXT_SYSTEM,
        'archetypes' => array(
            'manager' => CAP_ALLOW
        )
    ),
    'local/androgogic_export:manageexports' => array(
    	'riskbitmask'   => RISK_PERSONAL | RISK_DATALOSS | RISK_CONFIG,
        'captype' => 'write',
        'contextlevel' => CONTEXT_SYSTEM,
        'archetypes' => array(
            'manager' => CAP_ALLOW
        )
    ),
    'local/androgogic_export:runexport' => array(
    	'riskbitmask'   => RISK_PERSONAL | RISK_DATALOSS | RISK_CONFIG,
        'captype' => 'write',
        'contextlevel' => CONTEXT_SYSTEM,
        'archetypes' => array(
            'manager' => CAP_ALLOW
        )
    ),
   'local/androgogic_export:viewlog'=>array(
	    'captype'=>'read',
		'contextlevel'=>CONTEXT_SYSTEM,
        'archetypes' => array(
            'manager' => CAP_ALLOW
        ),
	),
);
