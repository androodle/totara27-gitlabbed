<?php
/**
 * Androgogic Export
 *
 * @author      Keith Buss <kbuss@outlook.com>
 * @version     July 2015
 *
 **/
 
require_once($CFG->dirroot . '/totara/reportbuilder/lib.php');

function xmldb_local_androgogic_export_install() {
    global $DB;

	// create Export Log in Report Builder
	// copied from /totara/reportbuilder/index.php
	// modified by Keith Buss
		
	$reportname = get_string('logreport', 'local_androgogic_export');
	if ($DB->get_record('report_builder', array('fullname'=>$reportname))) {
		return true;
	}
	
	$sourcename = 'androgogic_export_log';

	$todb = new stdClass();
	$todb->fullname = $reportname;
	$todb->shortname = reportbuilder::create_shortname($reportname);
	$todb->source = $sourcename;
	$todb->hidden = 0;
	$todb->recordsperpage = 40;
	$todb->contentmode = REPORT_BUILDER_CONTENT_MODE_NONE;
	$todb->accessmode = REPORT_BUILDER_ACCESS_MODE_ANY; // default to limited access
	$todb->embedded = 0;
	$todb->defaultsortcolumn = 'androgogic_export_log_id';
	$todb->defaultsortorder = 3;

	try {
		$transaction = $DB->start_delegated_transaction();

		$newid = $DB->insert_record('report_builder', $todb);
		//add_to_log(SITEID, 'reportbuilder', 'new report', 'report.php?id=' . $newid, $reportname . ' (ID=' . $newid . ')');

		// by default we'll require a role but not set any, which will restrict report access to
		// the site administrators only
		$todb = new stdClass();
		$todb->reportid = $newid;
		$todb->type = 'role_access';
		$todb->name = 'enable';
		$todb->value = 1;
		$DB->insert_record('report_builder_settings', $todb);

		// restrict access to new report to site managers (and implicitly admins)
		// (if role doesn't exist report will not be visible to anyone)
		if ($managerroleid = $DB->get_field('role', 'id', array('shortname'=>'manager'))) {
			$todb = new stdClass();
			$todb->reportid = $newid;
			$todb->type = 'role_access';
			$todb->name = 'activeroles';
			$todb->value = $managerroleid;
			$DB->insert_record('report_builder_settings', $todb);
		}

		// create columns for new report based on default columns
		$src = reportbuilder::get_source_object($sourcename);
		if (isset($src->defaultcolumns) && is_array($src->defaultcolumns)) {
			$defaultcolumns = $src->defaultcolumns;
			$so = 1;
			foreach ($defaultcolumns as $option) {
				$heading = isset($option['heading']) ? $option['heading'] :
					null;
				$hidden = isset($option['hidden']) ? $option['hidden'] : 0;
				$column = $src->new_column_from_option($option['type'],
					$option['value'], $heading, $hidden);
				$todb = new stdClass();
				$todb->reportid = $newid;
				$todb->type = $column->type;
				$todb->value = $column->value;
				$todb->heading = $column->heading;
				$todb->hidden = $column->hidden;
				$todb->sortorder = $so;
				$todb->customheading = 0; // initially no columns are customised
				$DB->insert_record('report_builder_columns', $todb);
				$so++;
			}
		}
		// create filters for new report based on default filters
		if (isset($src->defaultfilters) && is_array($src->defaultfilters)) {
			$defaultfilters = $src->defaultfilters;
			$so = 1;
			foreach ($defaultfilters as $option) {
				$todb = new stdClass();
				$todb->reportid = $newid;
				$todb->type = $option['type'];
				$todb->value = $option['value'];
				$todb->advanced = isset($option['advanced']) ? $option['advanced'] : 0;
				$todb->sortorder = $so;
				$todb->region = isset($option['region']) ? $option['region'] : rb_filter_type::RB_FILTER_REGION_STANDARD;
				$DB->insert_record('report_builder_filters', $todb);
				$so++;
			}
		}
		// Create toolbar search columns for new report based on default toolbar search columns.
		if (isset($src->defaulttoolbarsearchcolumns) && is_array($src->defaulttoolbarsearchcolumns)) {
			foreach ($src->defaulttoolbarsearchcolumns as $option) {
				$todb = new stdClass();
				$todb->reportid = $newid;
				$todb->type = $option['type'];
				$todb->value = $option['value'];
				$DB->insert_record('report_builder_search_cols', $todb);
			}
		}
		$transaction->allow_commit();
		$result = true;

	} catch (Exception $e) {
		$transaction->rollback($e);
		$result = false;
	}

    return $result;
}
