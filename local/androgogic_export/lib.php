<?php

/**
 * Androgogic Export
 *
 * @author      Keith Buss <kbuss@outlook.com>
 * @version     July 2015
 *
 * */
defined('MOODLE_INTERNAL') || die();

require_once('classes/exportlog.class.php');
require_once($CFG->libdir . '/gradelib.php');
require_once($CFG->dirroot . '/grade/querylib.php');
require_once($CFG->dirroot . '/completion/completion_completion.php');

function local_androgogic_export_cron() {

    $starthour = date('G');
    $cronhours = get_config('local_androgogic_export', 'cron_hours');
    if (!empty($cronhours) && in_array($starthour, explode(',', $cronhours))) {
        if (androgogic_export_get_cronlock()) {
            mtrace("AndroExport cron started.");
            androgogic_export_run();
            mtrace("AndroExport cron ended.");
        } else {
            mtrace("AndroExport cron is already running.");
        }
    }
}

function androgogic_export_get_cronlock() {
    global $CFG;

    $lockfile = $CFG->dataroot . '/' . $CFG->dbname . '_androgogic_export.lock';
    if (file_exists($lockfile)) {
        // Check if file modification time is more than a day old. If so, something 
        // has probably gone wrong, and the lock file is not valid anymore.
        if (filemtime($lockfile) < time() - 86400) {
            unlink($lockfile);
        } else {
            return false;
        }
    }
    if (!touch($lockfile)) {
        return false;
    }
    register_shutdown_function('androgogic_export_release_cronlock');
    return true;
}

function androgogic_export_release_cronlock() {
    global $CFG;

    // make sure the lockfile is deleted
    // before the script is complete.

    $lockfile = $CFG->dataroot . '/' . $CFG->dbname . '_androgogic_export.lock';
    unlink($lockfile);
}

function androgogic_export_run($runfrombrowser = false) {
    global $DB, $USER, $CFG;

    $config = get_config('local_androgogic_export');
    if (empty($config->export_dir)) {
        throw new Exception('Export directory setting is required');
    }
    if (!is_dir($config->export_dir)) {
        throw new Exception("Export directory '$config->export_dir' is not found");
    }

    if ($runfrombrowser) {
        $progressbar = new progress_bar('syncprogress', 500, true);
    } else {
        $progressbar = null;
    }

    $incfile = $CFG->dirroot . '/local/androgogic_export/hooks.php';
    if (file_exists($incfile)) {
        require_once($incfile);
    }

    try {
        raise_memory_limit(MEMORY_EXTRA);
        // Stop time outs, this might take a while
        set_time_limit(0);

        $log = new ExportLog();
        $message = get_string('syncstarted', 'local_androgogic_export');
        $log->add_to_log(ExportLog::TYPE_INFO, $message, 'run by ' . ($runfrombrowser ? "$USER->firstname $USER->lastname, username=$USER->username" : 'CRON'));
        $timestart = microtime(true);
        if ($runfrombrowser) {
            $progressbar->update_full(0, $message);
        }

        if ($exports = $DB->get_records_sql("SELECT * FROM {androgogic_export_source} WHERE deleted=0 AND visible=1 ORDER BY sortorder ASC")) {
            foreach ($exports as $export) {
                $log->sourceid = $export->id;
                $selectcount = 0;
                $elementname = strtolower(get_string($export->element, 'local_androgogic_export'));

                if ($progressbar <> null) {
                    $progresscount = 0;
                    $message = "selecting $elementname records";
                    $progressbar->update_full(0, $message);
                }

                $fields = $DB->get_records('androgogic_export_field', array('sourceid' => $export->id));
                if ($fields === false) {
                    throw new Exception("no export fields selected for $export->shortname, sourceid=$export->id");
                }
                if ($export->element == ExportLog::ELEMENT_COURSE) {

                    if ($progressbar <> null) {
                        $total = $DB->count_records_sql("SELECT COUNT(id) FROM {course}");
                    }

                    $sql = "SELECT shortname, id AS courseid, idnumber FROM {course}";
                    $rs = $DB->get_recordset_sql($sql);
                    if ($rs->valid()) {
                        foreach ($rs as $row) {
                            if ($progressbar <> null) {
                                $progresscount++;
                                $progressbar->update($progresscount, $total, $message);
                            }
                            $mappedfields = androgogic_export_get_mappedfields($export, null, $row->courseid);
                            $staging = androgogic_export_get_staging($export, null, $row->courseid);
                            if ($staging === false) {
                                $selected = true;
							} else {
								$mappedfields->stagingid = $staging->stagingid;
                            	if ($export->allrecords == 1) {
                                	$selected = true;
                            	} else {
                                    $selected = androgogic_export_modified($mappedfields, $staging);
                                }
                            }
                            if ($selected) {
                                if (function_exists('androgogic_export_hook_course_selected')) {
                                     $selected = androgogic_export_hook_course_selected($log, $export, $row->courseid, $mappedfields);
                            	}
                                if ($selected) {
                                    $selectcount++;
                                    $log->stagingid = androgogic_export_save_staging($log, $export, $mappedfields);
                                    $log->add_to_log(ExportLog::TYPE_TRACE, 'selected course', "$row->shortname, courseid=$row->courseid, idnumber=$row->idnumber");
                                }
                            }
                        }
                    }
                    $rs->close();
                } else if ($export->element == ExportLog::ELEMENT_COMP) {

                    if ($progressbar <> null) {
                        $total = $DB->count_records_sql("SELECT COUNT(cc.id) FROM {course_completions} cc INNER JOIN {user} u ON u.id = cc.userid WHERE u.idnumber<>'' AND u.deleted=0 AND u.suspended=0");
                    }

                    $sql = "SELECT u.firstname, u.lastname, u.username, u.id AS userid, c.id AS courseid, c.shortname, cc.id AS ccid, cc.status
                            FROM {course_completions} cc
                            INNER JOIN {course} c ON c.id = cc.course
                            INNER JOIN {user} u ON u.id = cc.userid
                            WHERE u.idnumber<>'' AND u.deleted=0 AND u.suspended=0";
                    $completionstatus = array();
                    if ($export->statusnotyetstarted == 1) {
                        $completionstatus[] = COMPLETION_STATUS_NOTYETSTARTED;
                    }
                    if ($export->statusinprogress == 1) {
                        $completionstatus[] = COMPLETION_STATUS_INPROGRESS;
                    }
                    if ($export->statuscomplete == 1) {
                        $completionstatus[] = COMPLETION_STATUS_COMPLETE;
                    }
                    if ($export->statuscompleteviarpl == 1) {
                        $completionstatus[] = COMPLETION_STATUS_COMPLETEVIARPL;
                    }
                    if (count($completionstatus) > 0) {
                        list($sqlin, $params) = $DB->get_in_or_equal($completionstatus);
                        $sql .= " AND cc.status {$sqlin}";
                    } else {
                        $params = null;
                    }
                    $rs = $DB->get_recordset_sql($sql, $params);
                    if ($rs->valid()) {
                        foreach ($rs as $row) {
                            if ($progressbar <> null) {
                                $progresscount++;
                                $progressbar->update($progresscount, $total, $message);
                            }
                            $mappedfields = androgogic_export_get_mappedfields($export, $row->userid, $row->courseid, $row->ccid);
                            $staging = androgogic_export_get_staging($export, $row->userid, $row->courseid);
                            if ($staging === false) {
                                $selected = true;
							} else {
								$mappedfields->stagingid = $staging->stagingid;
                            	if ($export->allrecords == 1) {
                                	$selected = true;
                            	} else {
                                    $selected = androgogic_export_modified($mappedfields, $staging);
                                }
                            }
                            if ($selected) {
                                if (function_exists('androgogic_export_hook_comp_selected')) {
                                     $selected = androgogic_export_hook_comp_selected($log, $export, $row->userid, $row->courseid, $row->ccid, $mappedfields);
                            	}
                                if ($selected) {
                                    $selectcount++;
                                    $log->stagingid = androgogic_export_save_staging($log, $export, $mappedfields);
                                    $log->add_to_log(ExportLog::TYPE_TRACE, 'selected course completion', "$row->firstname $row->lastname, username=$row->username, userid=$row->userid, $row->shortname courseid=$row->courseid, status=$row->status");
                                }
                            }
                        }
                    }
                    $rs->close();
                } else if ($export->element == ExportLog::ELEMENT_USER) {

                    if ($progressbar <> null) {
                        $total = $DB->count_records_sql("SELECT COUNT(id) FROM {user} WHERE idnumber<>'' AND deleted=0 AND suspended=0");
                    }

                    $sql = "SELECT firstname, lastname, username, u.id AS userid, idnumber
                            FROM {user} u
                            WHERE u.idnumber<>'' AND u.deleted=0 AND u.suspended=0";
                    $rs = $DB->get_recordset_sql($sql);
                    if ($rs->valid()) {
                        foreach ($rs as $row) {
                            if ($progressbar <> null) {
                                $progresscount++;
                                $progressbar->update($progresscount, $total, $message);
                            }
                            $mappedfields = androgogic_export_get_mappedfields($export, $row->userid);
                            $staging = androgogic_export_get_staging($export, $row->userid);
                            if ($staging === false) {
                                $selected = true;
							} else {
								$mappedfields->stagingid = $staging->stagingid;
                            	if ($export->allrecords == 1) {
                                	$selected = true;
                            	} else {
                                    $selected = androgogic_export_modified($mappedfields, $staging);
                                }
                            }
                            if ($selected) {
                                if (function_exists('androgogic_export_hook_user_selected')) {
                                     $selected = androgogic_export_hook_user_selected($log, $export, $row->userid, $mappedfields);
                                }
                                if ($selected) {
                                    $selectcount++;
                                    $log->stagingid = androgogic_export_save_staging($log, $export, $mappedfields);
                                    $log->add_to_log(ExportLog::TYPE_TRACE, 'selected user', "$row->firstname $row->lastname, username=$row->username, userid=$row->userid, idnumber=$row->idnumber");
                                }
                            }
                        }
                    }
                    $rs->close();
                }
                
				$log->stagingid = NULL;
                $log->add_to_log(ExportLog::TYPE_INFO, "selected $selectcount $elementname records");
                if ($selectcount > 0) {
                    androgogic_export_write_csv($log, $export, $progressbar);
                }
            }
        }

    } catch (Exception $e) {
        $log->error_to_log($e);
    }
    
    $log->stagingid = NULL;
    $log->sourceid = NULL;   
	$warnings = $DB->count_records('androgogic_export_log', array('runid' => $log->runid, 'logtype' => ExportLog::TYPE_WARNING));
	$errors = $DB->count_records('androgogic_export_log', array('runid' => $log->runid, 'logtype' => ExportLog::TYPE_ERROR));
    $message = get_string(($errors==0 ? 'syncsuccess' : 'syncproblem'), 'local_androgogic_export');
    $timeend = microtime(true);
	$log->add_to_log(($errors==0 ? ExportLog::TYPE_INFO : ExportLog::TYPE_ERROR), "$message, $warnings warnings, $errors errors", 'execution time ' . number_format($timeend - $timestart, 2) . 's');

    if ($runfrombrowser) {
        $progressbar->update_full(100, $message);
    }

    $notifyemailto = get_config('local_androgogic_export', 'notify_emailto');
    if (!empty($notifyemailto)) {
        $notifyinfo = get_config('local_androgogic_export', 'notify_info');
        $notifywarning = get_config('local_androgogic_export', 'notify_warning');
        $notifyerror = get_config('local_androgogic_export', 'notify_error');
        $logtypes = array();
        if (!empty($notifyinfo)) {
            $logtypes[] = ExportLog::TYPE_INFO;
        }
        if (!empty($notifywarning)) {
            $logtypes[] = ExportLog::TYPE_WARNING;
        }
        if (!empty($notifyerror)) {
            $logtypes[] = ExportLog::TYPE_ERROR;
        }
        if (count($logtypes) > 0) {
            androgogic_export_notify($log->runid, $notifyemailto, $logtypes);
        }
    }
    return;
}

function androgogic_export_modified($dbfields, $stagingfields) {
    foreach ($dbfields as $key => $value) {
        if (isset($stagingfields->$key)) {
            if ($value != $stagingfields->$key) {
                return true;
            }
        }
    }
    return false;
}

function androgogic_export_write_csv($log, $export, $progressbar = null) {
    global $DB;

    assert(is_object($log), '$log must be an object');
    assert(is_object($export), '$export must be an object');

    $reccount = 0;
    $config = get_config('local_androgogic_export');
    if (empty($config->export_dir)) {
        throw new Exception('Export directory setting is required');
    }

    if (!is_dir($config->export_dir)) {
        throw new Exception("$config->export_dir is not a valid directory");
    }

    if (empty($export->dateformat)) {
        throw new Exception('Date format setting is required');
    }

    $csvfilename = $config->export_dir . '/' . $export->csvfileprefix . date("YmdHis") . '.csv';

    if ($export->element == ExportLog::ELEMENT_USER) {
        $stagingtable = 'androgogic_export_user';
    } else if ($export->element == ExportLog::ELEMENT_COURSE) {
        $stagingtable = 'androgogic_export_course';
    } else if ($export->element == ExportLog::ELEMENT_COMP) {
        $stagingtable = 'androgogic_export_comp';
    } else {
        throw new Exception("element '$export->element' is invalid");
    }

    if ($progressbar <> null) {
        $message = 'writing CSV export file ' . $csvfilename;
        $count = 0;
        $total = $DB->count_records($stagingtable, array('runid' => $log->runid, 'sourceid' => $export->id));
        $progressbar->update_full(0, $message);
    }

    $rs = $DB->get_recordset_sql("SELECT * FROM {{$stagingtable}} WHERE runid=$log->runid AND sourceid=$export->id");
    if ($rs->valid()) {
        $fp = @fopen($csvfilename, 'w');
        if ($fp === false) {
            throw new Exception("$csvfilename is not a writable");
        }

        ///
        /// load mappedfield names
        ///
        $mappedfieldnames = array();
        $rows = $DB->get_records('androgogic_export_field', array('sourceid' => $export->id));
        foreach ($rows as $row) {
            $mappedfieldnames[] = $row->dbfieldname;
        }

        if ($export->csvheader == 1) {
            // get export field names for CSV header
            $retval = fputcsv($fp, $mappedfieldnames, $export->csvdelimiter);
            if ($retval === false) {
                throw new Exception("failed writing CSV export file $csvfilename");
            }
        }

        foreach ($rs as $staging) {
            ///
            /// load mappedfield data from staging tables
            ///
            $mappedfields = array();
            foreach ($mappedfieldnames as $mappedfieldname) {
                $mappedfields[$mappedfieldname] = isset($staging->$mappedfieldname) ? $staging->$mappedfieldname : '';
            }
            $rows = $DB->get_records('androgogic_export_custom', array('sourceid' => $export->id, 'stagingid' => $staging->id));
            if (!$rows === false) {
                foreach ($rows as $row) {
                    if (isset($mappedfields[$row->mappedfieldname])) {
                        $mappedfields[$row->mappedfieldname] = $row->data;
                    }
                }
            }
            unset($staging->id);
            unset($staging->runid);
            unset($staging->sourceid);
			if (function_exists('androgogic_export_hook_pre_write_csv')) {
				 $mappedfields = androgogic_export_hook_pre_write_csv($log, $export, $mappedfields);
			}
            $retval = fputcsv($fp, $mappedfields, $export->csvdelimiter);
            if ($retval === false) {
                throw new Exception("failed writing CSV export file $csvfilename");
            }
            $reccount++;
            if ($progressbar <> null) {
                $count++;
                $progressbar->update($reccount, $total, $message);
            }
        }
        fclose($fp);
    }
    $rs->close();

    if ($progressbar <> null) {
        $progressbar->update_full(100, $message);
    }

    $log->add_to_log(ExportLog::TYPE_INFO, "$reccount records written to CSV export file $csvfilename");
    return $reccount;
}

function androgogic_export_save_staging($log, $export, $mappedfields) {
    global $DB;

    assert(is_object($log), '$log must be an object');
    assert(is_object($export), '$export must be an object');
    assert(is_object($mappedfields), '$mappedfields must be an object');

    if ($export->element == ExportLog::ELEMENT_USER) {
        $stagingtable = 'androgogic_export_user';
    } else if ($export->element == ExportLog::ELEMENT_COURSE) {
        $stagingtable = 'androgogic_export_course';
    } else if ($export->element == ExportLog::ELEMENT_COMP) {
        $stagingtable = 'androgogic_export_comp';
    } else {
        throw new Exception("element '$export->element' is invalid");
    }

    $mappedfields->sourceid = $export->id;
    $mappedfields->runid = $log->runid;
    if (empty($mappedfields->stagingid)) {
        $mappedfields->stagingid = $DB->insert_record($stagingtable, $mappedfields);
    } else {
    	$mappedfields->id = $mappedfields->stagingid;
        $DB->update_record($stagingtable, $mappedfields);
    }

    $row = new stdClass();
    $row->sourceid = $export->id;
    $row->stagingid = $mappedfields->stagingid;
    foreach ($mappedfields as $key => $value) {
        if ($value <> '' and ( substr($key, 0, 14) == 'profile_field_' or substr($key, 0, 12) == 'customfield_')) {
            $row->mappedfieldname = $key;
            $row->data = $value;
            $DB->insert_record('androgogic_export_custom', $row);
        }
    }
    return $mappedfields->stagingid;
}

function androgogic_export_get_mappedfields($export, $userid, $courseid = null, $ccid = null) {
    global $DB, $COMPLETION_STATUS;

    assert(is_object($export), '$export must be an object');

    $mappedfields = new stdClass();

    // get export field names
    $fields = $DB->get_records('androgogic_export_field', array('sourceid' => $export->id));
    if ($fields === false) {
        return $mappedfields;
    }

    if (!empty($userid)) {
        if (!$user = $DB->get_record('user', array('id' => $userid))) {
            throw new Exception("user ID $userid not found");
        }
        $usercustom = androgogic_export_get_user_customfields($user->id, $export->dateformat);
    }

    if (!empty($courseid)) {
        if (!$course = get_course($courseid)) {
            throw new Exception("course ID $courseid not found");
        }
        $coursecustom = androgogic_export_get_course_customfields($courseid, $export->dateformat);
    }

    if (!empty($ccid)) {
        if (!$completion = $DB->get_record('course_completions', array('id' => $ccid))) {
            throw new Exception("course completions ID $ccid not found");
        }
    }

    //
    // only return export fields
    //
    foreach ($fields as $field) {
        $data = '';
        if (substr($field->dbfieldname, 0, 14) == 'profile_field_') {
            $data = empty($usercustom->{$field->dbfieldname}) ? '' : $usercustom->{$field->dbfieldname};
        } else if (substr($field->dbfieldname, 0, 12) == 'customfield_') {
            $data = empty($coursecustom->{$field->dbfieldname}) ? '' : $coursecustom->{$field->dbfieldname};
        } else if ($field->dbfieldname == 'user_lmsid') {
            $data = $user->id;
        } else if (substr($field->dbfieldname, 0, 5) == 'user_') {
            $fieldname = substr($field->dbfieldname, 5);
            $data = empty($user->$fieldname) ? '' : $user->$fieldname;
        } else if ($field->dbfieldname == 'course_lmsid') {
            $data = $course->id;
        } else if (substr($field->dbfieldname, 0, 7) == 'course_') {
            $fieldname = substr($field->dbfieldname, 7);
            $data = empty($course->$fieldname) ? '' : $course->$fieldname;
        } else if ($field->dbfieldname == 'completion_datestarted') {
            $data = empty($completion->timestarted) ? '' : date($export->dateformat, $completion->timestarted);
        } else if ($field->dbfieldname == 'completion_dateenrolled') {
            $data = empty($completion->timeenrolled) ? '' : date($export->dateformat, $completion->timeenrolled);
        } else if ($field->dbfieldname == 'completion_datecompleted') {
            $data = empty($completion->timecompleted) ? '' : date($export->dateformat, $completion->timecompleted);
        } else if ($field->dbfieldname == 'completion_status') {
            $data = empty($completion->status) ? '' : (isset($COMPLETION_STATUS[$completion->status]) ? $COMPLETION_STATUS[$completion->status] : $completion->status);
        } else if ($field->dbfieldname == 'completion_grade') {
            if (!empty($completion->userid) and ! empty($completion->course)) {
                $grade = grade_get_course_grade($completion->userid, $completion->course);
                $data = $grade->grade;
            }
        }
        $mappedfields->{$field->dbfieldname} = $data;
    }
    return $mappedfields;
}

function androgogic_export_get_staging($export, $userid, $courseid = null) {
    global $DB;

    assert(is_object($export), '$export must be an object');

    ///
    /// staging tables contain copy of last exported data
    ///

    $stagingfields = false;
    $staging = false;

    if ($export->element == ExportLog::ELEMENT_USER and ! empty($userid)) {
        $staging = $DB->get_record('androgogic_export_user', array('sourceid' => $export->id, 'user_lmsid' => $userid));
    } else if ($export->element == ExportLog::ELEMENT_COMP and ! empty($userid) and ! empty($courseid)) {
        $staging = $DB->get_record('androgogic_export_comp', array('sourceid' => $export->id, 'user_lmsid' => $userid, 'course_lmsid' => $courseid));
    } else if ($export->element == ExportLog::ELEMENT_COURSE and ! empty($courseid)) {
        $staging = $DB->get_record('androgogic_export_course', array('sourceid' => $export->id, 'course_lmsid' => $courseid));
    }
    if (!$staging === false) {
        $stagingfields = new stdClass();
        $stagingfields->stagingid = $staging->id;
        unset($staging->id);
        foreach ($staging as $key => $value) {
            $stagingfields->$key = $value;
        }
        // get customfields
        $rows = $DB->get_records('androgogic_export_custom', array('sourceid' => $export->id, 'stagingid' => $stagingfields->stagingid));
        if (!$rows === false) {
            foreach ($rows as $row) {
                $stagingfields->{$row->mappedfieldname} = $row->data;
            }
        }
    }
    return $stagingfields;
}

function androgogic_export_get_user_customfields($userid, $dateformat) {
    global $DB;

    assert(is_numeric($userid), '$userid must be numeric');
    assert(is_string($dateformat), '$dateformat must be a string');

    $customfields = new stdClass;
    $sql = "SELECT f.shortname, d.data, f.datatype
            FROM {user_info_data} d 
            JOIN {user_info_field} f ON d.fieldid = f.id
            WHERE d.userid = $userid";
    $rows = $DB->get_records_sql($sql);
    if (!$rows === false) {
        foreach ($rows as $row) {
            if ($row->datatype == 'datetime' and ! empty($row->data)) {
                // convert date custom field 
        		$value = date($dateformat, $row->data);
            } else {
                $value = $row->data;
            }
            $fieldname = 'profile_field_' . $row->shortname;
            $customfields->$fieldname = $value;
        }
    }
    return $customfields;
}

function androgogic_export_get_course_customfields($courseid, $dateformat) {
    global $DB;

    assert(is_numeric($courseid), '$courseid must be numeric');
    assert(is_string($dateformat), '$dateformat must be a string');

    $customfields = new stdClass;
    $sql = "SELECT f.shortname, d.data, f.datatype
            FROM {course_info_data} d 
            JOIN {course_info_field} f ON d.fieldid = f.id
            WHERE d.courseid = $courseid";
    $rows = $DB->get_records_sql($sql);
    if (!$rows === false) {
        foreach ($rows as $row) {
            if ($row->datatype == 'datetime' and !empty($row->data)) {
                // convert date custom field 
        		$value = date($dateformat, $row->data);
            } else {
                $value = $row->data;
            }
            $fieldname = 'customfield_' . $row->shortname;
            $customfields->$fieldname = $value;
        }
    }
    return $customfields;
}

function androgogic_export_notify($runid, $notifyemailto, $notifytypes) {
    global $CFG, $DB;

    assert(is_numeric($runid), '$runid must be numeric');
    assert(is_string($notifyemailto), '$notifyemailto must be a string');
    assert(is_array($notifytypes), '$notifytypes must be an array');

    // Get log messages for runid
    list($sqlin, $params) = $DB->get_in_or_equal($notifytypes);
    $sql = "SELECT l.*, s.shortname, s.element, s.source FROM {androgogic_export_log} l
   LEFT OUTER JOIN {androgogic_export_source} s ON s.id = l.sourceid
    		 WHERE logtype {$sqlin} AND runid = $runid
             ORDER BY id DESC LIMIT 200";
    $rows = $DB->get_records_sql($sql, $params);
    if (!$rows) {
        // Nothing to report.
        return true;
    }

    // Build email message
    $dateformat = get_string('strftimedateseconds', 'langconfig');
    $logcount = count($rows);
    $sitename = get_site();
    $sitename = format_string($sitename->fullname);
    $subject = get_string('notifysubject', 'local_androgogic_export', $sitename);

    $a = new stdClass();
    $a->logtypes = implode(', ', $notifytypes);
    $a->count = $logcount;
    $message = get_string('notifymessagestart', 'local_androgogic_export', $a);
    $message .= "\n\n";
    foreach ($rows as $row) {
        $a = new stdClass();
        $a->time = date_format_string($row->time, $dateformat);
        $a->message = $row->action;
        $a->logtype = $row->logtype;
        if (!empty($row->info)) {
            $a->message .= ", $row->info";
        }
        if (!empty($row->source)) {
            $elementname = get_string($row->element, 'local_androgogic_export');
            $formatname = get_string($row->format, 'local_androgogic_export');
            $a->message .= ", Source: $row->shortname $elementname $formatname";
        }
        $message .= get_string('notifymessage', 'local_androgogic_export', $a) . "\n\n";
    }
    $message .= "\n" . get_string('viewloghere', 'local_androgogic_export', $CFG->wwwroot . '/local/androgogic_export/exportlog.php');

    // Send email
    if (strpos($notifyemailto, ',') === false) {
        $notifyemailtos = array($notifyemailto);
    } else {
        $notifyemailtos = explode(',', $notifyemailto);
    }
    $supportuser = core_user::get_support_user();
    foreach ($notifyemailtos as $emailaddress) {
        $userto = \totara_core\totara_user::get_external_user(trim($emailaddress));
        email_to_user($userto, $supportuser, $subject, $message);
    }
    return true;
}
