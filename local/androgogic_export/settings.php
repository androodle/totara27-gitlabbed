<?php
/** 
 * Androgogic Export
 *
 * @author      Keith Buss <kbuss@outlook.com>
 * @version     July 2015
 *
 **/

defined('MOODLE_INTERNAL') || die(); 

if ($hassiteconfig) { // needs this condition or there is error on login page   

	$ADMIN->add('root', new admin_category('androgogic_export', get_string('pluginname', 'local_androgogic_export')));

	$ADMIN->add('androgogic_export', new admin_externalpage('manageexports', 
			get_string('manageexports', 'local_androgogic_export'), 
			new moodle_url('/local/androgogic_export/exports.php'),
			'local/androgogic_export:manageexports'));

	$ADMIN->add('androgogic_export', new admin_externalpage('runexport',
			get_string('runexport', 'local_androgogic_export'),
			new moodle_url('/local/androgogic_export/runexport.php'),
			'local/androgogic_export:runexport'));

	$ADMIN->add('androgogic_export', new admin_externalpage('exportlog',
			get_string('viewlog', 'local_androgogic_export'),
			new moodle_url('/local/androgogic_export/exportlog.php'),
			'local/androgogic_export:viewlog'));
	
	if (has_capability('local/androgogic_export:settings', context_system::instance())) {	
		$settings = new admin_settingpage('local_androgogic_export', get_string('generalsettings', 'local_androgogic_export'), 'local/androgogic_export:settings');
	
		$settings->add(new admin_setting_configtext('local_androgogic_export/export_dir', get_string('exportdir', 'local_androgogic_export'), 
			'', "/deployments/$CFG->dbname/var/integrations/transfer/to_client", PARAM_PATH, 60));

		$settings->add(new admin_setting_configtext('local_androgogic_export/cron_hours', get_string('cronhours', 'local_androgogic_export'), 
			get_string('cronhours_help', 'local_androgogic_export'), '', PARAM_RAW));

		// notifications         
		$settings->add(new admin_setting_heading('notifications', get_string('notifications', 'local_androgogic_export'),''));
				  
		$settings->add(new admin_setting_configtext('local_androgogic_export/notify_emailto', get_string('notifyemailto', 'local_androgogic_export'), 
			get_string('notifyemailto_help', 'local_androgogic_export'), '', PARAM_RAW, 60));
		
		$settings->add(new admin_setting_configcheckbox('local_androgogic_export/notify_info', get_string('notifyinfo', 'local_androgogic_export'),
			'', 0));    
		$settings->add(new admin_setting_configcheckbox('local_androgogic_export/notify_warning', get_string('notifywarning', 'local_androgogic_export'),
			'', 0));
		$settings->add(new admin_setting_configcheckbox('local_androgogic_export/notify_error', get_string('notifyerror', 'local_androgogic_export'),
			'', 0));    
		 
		$ADMIN->add('androgogic_export', $settings);
	}
}
