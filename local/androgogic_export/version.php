<?php
/** 
 * Androgogic Export
 *
 * @author      Keith Buss <kbuss@outlook.com>
 * @version     July 2015
 *
 **/

$plugin->version = 2015092200;		// The current plugin version (Date: YYYYMMDDXX).
$plugin->requires = 2013111800; 	// Requires this Moodle version - requires Totara 2.6 or above
$plugin->component = 'local_androgogic_export';	// Full name of the plugin (used for diagnostics)
