<?php
/**
 * Androgogic Export
 *
 * @author      Keith Buss <kbuss@outlook.com>
 * @version     July 2015
 *
 **/

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot.'/lib/formslib.php');
require_once($CFG->dirroot.'/local/androgogic_export/classes/exportlog.class.php');

class addexportform extends moodleform {

    // Define the form
    function definition() {

        $mform =& $this->_form;
		
		$mform->addElement('header', 'addexport', get_string('addexport', 'local_androgogic_export'));

		$options = array(
			ExportLog::ELEMENT_USER=>get_string(ExportLog::ELEMENT_USER, 'local_androgogic_export'),
			ExportLog::ELEMENT_COURSE=>get_string(ExportLog::ELEMENT_COURSE, 'local_androgogic_export'),
			ExportLog::ELEMENT_COMP=>get_string(ExportLog::ELEMENT_COMP, 'local_androgogic_export')
		);		

		$mform->addElement('select', 'element', get_string('element', 'local_androgogic_export'), $options);
        $mform->setType('element', PARAM_TEXT);
        
		$options = array(
			ExportLog::FORMAT_CSV=>get_string(ExportLog::FORMAT_CSV, 'local_androgogic_export')
		);
		$mform->addElement('select', 'format', get_string('format', 'local_androgogic_export'), $options);
        $mform->setType('format', PARAM_TEXT);
        
        $this->add_action_buttons(false, 'Add export');
    }
}
