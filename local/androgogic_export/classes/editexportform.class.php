<?php
/**
 * Androgogic Export
 *
 * @author      Keith Buss <kbuss@outlook.com>
 * @version     July 2015
 *
 **/

defined('MOODLE_INTERNAL') || die();

require_once($CFG->dirroot . '/lib/formslib.php');
require_once($CFG->dirroot.'/local/androgogic_export/classes/exportlog.class.php');

class editexportform extends moodleform {

    // Define the form
    function definition() {
        global $DB;

        $mform =& $this->_form;
        $format = $this->_customdata['format'];
        $element = $this->_customdata['element'];
        
        $mform =& $this->_form;
        
        $mform->addElement('text', 'shortname', get_string('shortname', 'local_androgogic_export'), array("size"=>60));
        $mform->setType('shortname', PARAM_TEXT);
        
		$mform->addElement('header', 'csvfile', get_string('csvfile', 'local_androgogic_export'));
		$mform->setExpanded('csvfile');
	
		$mform->addElement('selectyesno', 'allrecords', get_string('allrecords', 'local_androgogic_export'));
		$mform->addHelpButton('allrecords', 'allrecords', 'local_androgogic_export');
		$mform->setType('allrecords', PARAM_INT);
		
       	$mform->addElement('text', 'csvfileprefix', get_string('csvfileprefix', 'local_androgogic_export'));
		$mform->setType('csvfileprefix', PARAM_TEXT);
		$mform->addRule('csvfileprefix', null, 'required', null, 'client');
	
		$mform->addElement('selectyesno', 'csvheader', get_string('csvheader', 'local_androgogic_export'));
		$mform->addHelpButton('csvheader', 'csvheader', 'local_androgogic_export');
		$mform->setType('csvheader', PARAM_INT);
		$mform->setDefault('csvheader', 1);
			
		$options = array(
			','=>get_string('comma', 'local_androgogic_export'),
			';'=>get_string('semicolon', 'local_androgogic_export'),
			':'=>get_string('colon', 'local_androgogic_export'),
			'\t'=>get_string('tab', 'local_androgogic_export'),
			'|'=>get_string('pipe', 'local_androgogic_export')
		);
		$mform->addElement('select', 'csvdelimiter', get_string('csvdelimiter', 'local_androgogic_export'), $options);
		$mform->setType('csvdelimiter', PARAM_TEXT);

		$dateformats = array(
			'Y-m-d',
			'Y/m/d',
			'Y M d',
			'Y-M-d',
			'd-m-Y',
			'd/m/Y',
			'd.m.Y',
			'd/m/y',
			'd M Y',
			'd-M-Y',
			'm/d/Y'
		);				
		
		$date = new DateTime('now');
		$options = array();
		foreach ($dateformats as $dateformat) {
			$dateformatoptions[$dateformat] = $dateformat.'   ('.$date->format($dateformat).')';
		}
		$mform->addElement('select', 'dateformat', get_string('dateformat', 'local_androgogic_export'), $dateformatoptions);
		$mform->setType('dateformat', PARAM_TEXT);
 
		if ($element == ExportLog::ELEMENT_COMP) {
			//
			// completion status
			//
			$mform->addElement('header', 'completionstatus', get_string('completionstatus', 'local_androgogic_export'));
			$mform->setExpanded('completionstatus');
			
			$mform->addElement('checkbox', 'statusnotyetstarted', get_string('statusnotyetstarted', 'local_androgogic_export'));
			$mform->addElement('checkbox', 'statusinprogress', get_string('statusinprogress', 'local_androgogic_export'));
			$mform->addElement('checkbox', 'statuscomplete', get_string('statuscomplete', 'local_androgogic_export'));
			$mform->addElement('checkbox', 'statuscompleteviarpl', get_string('statuscompleteviarpl', 'local_androgogic_export'));
			$mform->setDefault('statuscomplete', 1);
			$mform->setDefault('statuscompleteviarpl', 1);
		}
		
		$mform->addElement('header', 'exportfields', get_string('exportfields', 'local_androgogic_export'));
		$mform->setExpanded('exportfields');
		if ($element == ExportLog::ELEMENT_USER or $element == ExportLog::ELEMENT_COMP) {
			//
			// user fields
			//

			$fieldnames = array(
				'user_idnumber',
				'user_lmsid',
				'user_username',
				'user_firstname',
				'user_lastname',
				'user_alternatename',
				'user_middlename',
				'user_email',
				'user_address',
				'user_city',
				'user_phone1',
				'user_phone2');
			foreach ($fieldnames as $fieldname) {
				$elementname = 'field_'.$fieldname;
				$mform->addElement('checkbox', $elementname, $fieldname);
			}
		}
		
		if ($element == ExportLog::ELEMENT_COURSE or $element == ExportLog::ELEMENT_COMP) {
			//
			// course fields
			//
			$fieldnames = array(
				'course_idnumber',
				'course_lmsid',
				'course_shortname',
				'course_fullname');
			foreach ($fieldnames as $fieldname) {
				$elementname = 'field_'.$fieldname;
				$mform->addElement('checkbox', $elementname, $fieldname);
			}
		}
		
		if ($element == ExportLog::ELEMENT_COMP) {
			//
			// course completion fields
			//
			$fieldnames = array(
				'completion_dateenrolled',
				'completion_datestarted',
				'completion_datecompleted',
				'completion_status',
				'completion_grade');
			foreach ($fieldnames as $fieldname) {
				$elementname = 'field_'.$fieldname;
				$mform->addElement('checkbox', $elementname, $fieldname);
			}
		}		

		if ($element == ExportLog::ELEMENT_USER or $element == ExportLog::ELEMENT_COMP) {
			//
			// user custom fields
			//
			$rows = $DB->get_records('user_info_field');
			if (count($rows) > 0) {
				$mform->addElement('header', 'usercustomfields', get_string('usercustomfields', 'local_androgogic_export'));
				//$mform->setExpanded('usercustomfields');
				foreach ($rows as $row) {
					$elementname = 'profile_field_'.$row->shortname;
					$mform->addElement('checkbox', $elementname, "$row->name<br>($elementname)");
				}
			} 
		}

		if ($element == ExportLog::ELEMENT_COURSE or $element == ExportLog::ELEMENT_COMP) {
			//
			// course custom fields
			//
			$rows = $DB->get_records('course_info_field');
			if (count($rows) > 0) {
				$mform->addElement('header', 'coursecustomfields', get_string('coursecustomfields', 'local_androgogic_export'));
				//$mform->setExpanded('coursecustomfields');

				foreach ($rows as $row) {
					$elementname = 'customfield_'.$row->shortname;
					$mform->addElement('checkbox', $elementname, "$row->fullname<br>($elementname)");
				}
			} 
		}
		
        /// hidden fields
        $mform->addElement('hidden', 'id');
        $mform->setType('id', PARAM_INT);
        
        $mform->addElement('hidden', 'visible');
        $mform->setType('visible', PARAM_INT);
		$mform->setDefault('visible', 1);
		
       	$mform->addElement('hidden', 'format');
       	$mform->setType('format', PARAM_ALPHA);
		$mform->setDefault('format', $format);

       	$mform->addElement('hidden', 'element');
       	$mform->setType('element', PARAM_ALPHA);
		$mform->setDefault('element', $element);
		
        $this->add_action_buttons();
    }
    
    function validation($data, $files) {
        $errors = array();
        $data = (object)$data;

        if ($data->element == SyncLog::ELEMENT_COMP) {
    		if (empty($data->statusnotyetstarted) and empty($data->statusinprogress) and empty($data->statuscomplete) and empty($data->statuscompleteviarpl)) {
                $errors['statusnotyetstarted'] = 'At least one completion status must be selected';
            }
    	}
        return $errors;
    }
}

