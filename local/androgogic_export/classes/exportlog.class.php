<?php
/**
 * Androgogic Export
 *
 * @author      Keith Buss <kbuss@outlook.com>
 * @version     July 2015
 *
 **/

defined('MOODLE_INTERNAL') || die();

class ExportLog {

	const TYPE_ERROR		= 'ERROR';
	const TYPE_WARNING		= 'WARNING';
	const TYPE_INFO			= 'INFO';
	const TYPE_TRACE		= 'TRACE';
	const TYPE_REVIEW		= 'REVIEW';
	
	const FORMAT_CSV		= 'CSV';
	
	const ELEMENT_COMP		= 'COMP';
	const ELEMENT_USER		= 'USER';
	const ELEMENT_COURSE	= 'COURSE';

	public $runid = NULL;
	public $sourceid = NULL;
	public $stagingid = NULL;
	
	protected $_tablename = 'androgogic_export_log';

	public function __construct() {				

		if (empty($this->_tablename)) {
			throw new Exception('log tablename is missing');
		}
		$this->runid = $this->get_last_runid() + 1;
	}

	/**
	 * Returns the run id of the last sync run
	 *
	 * @return int latest runid
	 */
	
	public function get_last_runid() {
		global $DB;

		$runid = $DB->get_field_sql("SELECT MAX(runid) FROM {{$this->_tablename}}");

		if (empty($runid)) {
			return 0;
		} else {
			return $runid;
		}
	}
	
	public function error_to_log($e) {
		$this->add_to_log(self::TYPE_ERROR, $e->getMessage(), '', $e->getTraceAsString());
	}
	
	/**
	 * Method for adding export log messages
	 * 
	 * @param string $type the log message type
	 * @param string $action the action which caused the log message
	 * @param string $info the log message
	 */
	public function add_to_log($type, $action, $info='', $trace='') {
		global $DB, $OUTPUT;
	
		$log = new stdClass;
		$log->logtype = $type;
		$log->time = time();
		$log->runid = $this->runid;
		$log->action = substr($action, 0, 255);
		$log->info = $info;
		$log->trace = $trace;

		if (!empty($this->sourceid)) {
			$log->sourceid = $this->sourceid;
		}				
		if (!empty($this->stagingid)) {
			$log->stagingid = $this->stagingid;
		}
		
		$DB->insert_record($this->_tablename, $log);
		return;
	}
}
