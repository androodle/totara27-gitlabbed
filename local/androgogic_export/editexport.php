<?php
/**
 * Androgogic Export
 *
 * @author      Keith Buss <kbuss@outlook.com>
 * @version     July 2015
 *
 **/
 
require_once(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once($CFG->libdir.'/adminlib.php');
require_once('classes/editexportform.class.php');

require_login();

$context = context_system::instance();
require_capability('local/androgogic_export:manageexports', $context);

// Get params
$id = optional_param('id', 0, PARAM_INT);    // 0 if creating adding a new source 
$format = optional_param('format', '', PARAM_ALPHA);  
$element = optional_param('element', '', PARAM_ALPHA); 

$elementname = get_string($element, 'local_androgogic_export');
$formatname = get_string($format, 'local_androgogic_export');
$heading = "$elementname $formatname settings";

$url_params = array('id'=>$id, 'format'=>$format, 'element'=>$element);
admin_externalpage_setup('manageexports', '', $url_params, new moodle_url('/local/androgogic_export/editexport.php'));

// create form
$mform = new editexportform(null, $url_params);
if ($id <> 0) {
    // Editing existing source
	if (!$row = $DB->get_record('androgogic_export_source', array('id'=>$id))) {
		throw new Exception($DB->get_last_error());	
	}
	
	// load field mappings
	if ($fields = $DB->get_records('androgogic_export_field', array('sourceid'=>$id))) {
		foreach ($fields as $field) {
			if (substr($field->dbfieldname, 0, 14) == 'profile_field_' or substr($field->dbfieldname, 0, 12) == 'customfield_'){
				$elementname = $field->dbfieldname;
			} else {
				$elementname = 'field_'.$field->dbfieldname;
			}
			$row->$elementname = 1;
		}
	}
	$mform->set_data($row);
}

// cancelled
if ($mform->is_cancelled()) {

    redirect('exports.php');

// Update data
} else if ($new = $mform->get_data()) {

	$transaction = $DB->start_delegated_transaction();
	try {    
		// Save source settings
		$new->timemodified = time();
		$new->usermodified = $USER->id;
		if ($new->id == 0) {
	
			// New record
			$new->timecreated = $new->timemodified;
			$new->visible = 1;

			$max = $DB->get_field_sql("SELECT MAX(sortorder) AS max FROM {androgogic_export_source} WHERE deleted=0");
			$new->sortorder = $max + 1;
		
			unset($new->id);
			if (!$new->id = $DB->insert_record('androgogic_export_source', $new)) {
				throw new Exception($DB->get_last_error());	
			}

			$notification = get_string('sourceadded', 'local_androgogic_export');

		} else {
		
			// update unselected checkboxes
			if (!isset($new->statusnotyetstarted)) {
				$new->statusnotyetstarted = 0;
			}
			if (!isset($new->statusinprogress)) {
				$new->statusinprogress = 0;
			}			
			if (!isset($new->statuscomplete)) {
				$new->statuscomplete = 0;
			}			
			if (!isset($new->statuscompleteviarpl)) {
				$new->statuscompleteviarpl = 0;
			}

			// Existing record
			if (!$DB->update_record('androgogic_export_source', $new)) {
				throw new Exception($DB->get_last_error());	
			}
			$DB->delete_records('androgogic_export_field', array('sourceid'=>$new->id));
			$notification = get_string('sourceupdated', 'local_androgogic_export');
		}
	
		// save field mapping
		foreach ($new as $name=>$value) {	
			// save mapped fields
			if (!empty($value)) {
				$child = new stdClass();
								
				// identify field names
				$child->dbfieldname = '';
				if (substr($name, 0, 6) == 'field_') {
					$child->dbfieldname = substr($name, 6);
				} else if (substr($name, 0, 14) == 'profile_field_' or substr($name, 0, 12) == 'customfield_'){
					$child->dbfieldname = $name;
				}
			
				if (!empty($child->dbfieldname)) {
					$child->sourceid = $new->id;

					if (!$DB->insert_record('androgogic_export_field', $child)) {
						throw new Exception($DB->get_last_error());	
					}
				}
			}
		}
		$transaction->allow_commit();
		totara_set_notification($notification, 'exports.php', array('class'=>'notifysuccess'));

	} catch (Exception $e) {

		$transaction->rollback($e);
		throw $e;
	}
}

///
/// Display page
///

echo $OUTPUT->header();
echo $OUTPUT->heading($heading);

$mform->display();

echo $OUTPUT->footer();
