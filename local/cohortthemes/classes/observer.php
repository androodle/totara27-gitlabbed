<?php
/**
 * Event observers used in local_cohortthemes.
 *
 * @package     local_cohortthemes
 * @author      Kirill Astashov <kirill.astashov@gmail.com>
 * @copyright   2015+ Androgogic Pty Ltd <http://www.androgogic.com>
 */

defined('MOODLE_INTERNAL') || die();

class local_cohortthemes_observer {

    /**
     * Update user theme, set session theme upon login
     *
     * @param \core\event\user_loggedin $event
     */

    public static function user_loggedin(\core\event\user_loggedin $event) {
        global $CFG, $DB, $SESSION;

        // Get cohort membership.
        // Assuming the user is enrolled in only 1 cohort that is assigned a custom theme.
        $sql = "
            SELECT c.id AS id, theme
            FROM {cohort} c
                JOIN {cohort_members} cm ON c.id = cm.cohortid AND cm.userid = :userid
                JOIN {local_cohortthemes} lct ON cm.cohortid = lct.cohortid
            WHERE c.active=1
            ";
        if (($row = $DB->get_record_sql($sql, array('userid' => $event->userid), IGNORE_MULTIPLE)) !== false) {
            $theme = $row->theme;
            $SESSION->theme = $theme;
        }
        return;
    }
}
