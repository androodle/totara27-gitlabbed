<?php
/**
 * Cohort (audience) based theme assignment
 *
 * @package     local_cohortthemes
 * @author      Kirill Astashov <kirill.astashov@gmail.com>
 * @copyright   2015+ Androgogic Pty Ltd <http://www.androgogic.com>
 * 
 **/

$string['pluginname'] = 'Audience based user themes';
$string['settings'] = 'Audience theme settings';
$string['changessaved'] = 'Audience theme settings have been saved.<br />
                           Changes, if any, will be applied to enrolled users by a scheduled cron task soon.';
$string['assignthemes'] = 'Assign themes to audiences';
$string['formnotice'] = 'Here you can link audiences to themes.<br />
    <strong>NOTE:</strong> Please assign only one theme per audience.<br />
    <br />
    Users will get their theme when they next log in to Totara.
    ';
$string['cohortassignedto2themes'] = 'Audience "{$a}" linked to more than one theme';
$string['updateexistingusers'] = 'Update existing users';




