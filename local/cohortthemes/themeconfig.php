<?php
/**
 * Cohort (audience) based theme assignment
 *
 * @package     local_cohortthemes
 * @author      Kirill Astashov <kirill.astashov@gmail.com>
 * @copyright   2015+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 **/

require_once(dirname(dirname(dirname(__file__))) . '/config.php');
require_once(dirname(__file__). '/locallib.php');
require_once(dirname(__file__). '/themeconfig_form.php');

$syscontext = context_system::instance();
require_capability('moodle/site:config', $syscontext);

$url = new moodle_url('/local/cohortthemes/themeconfig.php');
$PAGE->set_url($url);
$PAGE->set_context($syscontext);
$PAGE->set_pagelayout('admin');
$pagetitle = get_string('settings', 'local_cohortthemes');
$PAGE->set_title(format_string($SITE->fullname) . ": $pagetitle");
$PAGE->set_heading(format_string($SITE->fullname) . ": $pagetitle");
$PAGE->requires->css('/local/cohortthemes/multi-select.css');

// get themes
$themes = array();
$themeplugins = core_component::get_plugin_list('theme');
foreach ($themeplugins as $themename => $themedir) {
    // Load the theme config.
    try {
        $theme = theme_config::load($themename);
    } catch (Exception $e) {
        // Bad theme, just skip it for now.
        continue;
    }
    if ($themename !== $theme->name) {
        // Obsoleted or broken theme, just skip for now.
        continue;
    }
    //Hide the standard moodle theme from users, they should be using standardTotara.
    //Do Not! remove the code though, the totara themes require it.
if ($themename == 'standard' || $themename == 'base' || $themename == 'bootstrapbase') {
        continue;
    }
    //Hide the standard mymobile theme from users, they should be using mymobiletotara.
    //Do Not! remove the code though, the totara themes require it.
    if ($themename == 'mymobile') {
        continue;
    }

    $tmp = new stdclass();
    $tmp->strthemename = get_string('pluginname', 'theme_' . $theme->name);
    $tmp->name = $theme->name;
    $themes[] = $tmp;
}

// get cohorts
$cohorts = $DB->get_records('cohort', null, 'name');

$mform = new local_cohortthemes_themeconfigform($url, array('themes' => $themes, 'cohorts' => $cohorts));
if ($mform->is_cancelled()) {
    redirect($CFG->wwwroot);
}

// output form or update settings

echo $OUTPUT->header();

if ($data = $mform->get_data()) {
    // Update settings
    $DB->delete_records('local_cohortthemes');
    foreach ($data as $themename => $themecohorts) {
        if (is_array($themecohorts) && !empty($themecohorts)) {
            foreach ($themecohorts as $cohortid) {
                $row = new stdclass();
                $row->cohortid = $cohortid;
                $row->theme = $themename;
                $DB->insert_record('local_cohortthemes', $row);
            }
        }
    }

    echo $OUTPUT->box(get_string('changessaved', 'local_cohortthemes'), 'center');
    echo $OUTPUT->continue_button($url);
} else {
    // get existing settings from DB and send to form
    $cohortthemes = $DB->get_records('local_cohortthemes');
    $data = array();
    foreach ($cohortthemes as $row) {
        if (!isset($data[$row->theme])) {
            $data[$row->theme] = array();
        }
        $data[$row->theme][] = $row->cohortid;
    }
    $mform->set_data($data);

    // Require jquery and plugin(s)
    local_js();
    $PAGE->requires->js('/local/cohortthemes/jquery.multi-select.js');
    $PAGE->requires->js('/local/cohortthemes/multi-select.js');

    // show form
    $mform->display();
}

echo $OUTPUT->footer();
