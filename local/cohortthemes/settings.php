<?php
/**
 * Cohort (audience) based theme assignment
 *
 * @package     local_cohortthemes
 * @author      Kirill Astashov <kirill.astashov@gmail.com>
 * @copyright   2015+ Androgogic Pty Ltd <http://www.androgogic.com>
 **/

defined('MOODLE_INTERNAL') || die;

if($ADMIN->locate('localplugins')) {
    $temp = new admin_externalpage('cohortthemes', get_string('pluginname', 'local_cohortthemes'), new moodle_url("/local/cohortthemes/themeconfig.php"));
    $ADMIN->add('localplugins', $temp);
}
