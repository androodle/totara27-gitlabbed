<?php
/**
 * Cohort (audience) based theme assignment config form
 *
 * @package     local_cohortthemes
 * @author      Kirill Astashov <kirill.astashov@gmail.com>
 * @copyright   2015+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 **/


defined('MOODLE_INTERNAL') || die();

require_once $CFG->libdir.'/formslib.php';

class local_cohortthemes_themeconfigform extends moodleform {
    function definition () {
        global $USER;
        $data = $this->_customdata;
        $mform = $this->_form;

        $mform->addElement('header', 'assignthemes', get_string('assignthemes', 'local_cohortthemes'));
        $mform->addElement('html', html_writer::tag('div', get_string('formnotice', 'local_cohortthemes'), array('style' => 'padding: 10px 0 20px 0;')));

        $cohortlist = $this->build_cohort_list($data['cohorts']);
        foreach ($data['themes'] as $theme) {
            $mform->addElement('select', $theme->name, $theme->strthemename, $cohortlist, array('multiple' => true, 'class' => 'multiselect'));
        }

        $this->add_action_buttons(true, get_string('savechanges'));
    }

    /**
     * Validation of input fields beyond capabilities of filters and rules.
     *
     * @param $data
     * @param $files
     * @return array
     */
    public function validation($data, $files) {
        global $CFG, $DB, $USER;
        $errors = parent::validation($data, $files);

        foreach ($this->_customdata['cohorts'] as $cohort) {
            $found = false;
            foreach ($data as $themename => $themecohorts) {
                if (is_array($themecohorts) && !empty($themecohorts)) {
                    foreach ($themecohorts as $cohortid) {
                        if ($cohort->id == $cohortid) {
                            if ($found) {
                                $errors[$themename] = get_string('cohortassignedto2themes', 'local_cohortthemes', $cohort->name);
                            } else {
                                $found = true;
                            }
                        }
                    }
                }
            }
        }
        return $errors;
    }

    private function build_cohort_list($cohorts) {
        $ret = array();
        foreach ($cohorts as $cohort) {
            $ret[$cohort->id] = $cohort->name;
        }
        return $ret;
    }

}