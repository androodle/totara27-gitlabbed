<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/** 
 * @package     block_androgogic_catalogue
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 **/

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir . '/formslib.php');

class location_search_form extends moodleform {
    
    function definition() {
        
        $mform =& $this->_form;
        
        $mform->addElement(
            'text', 
            'search', 
            get_string('location_name', 'block_androgogic_catalogue'),
            array('size' => 100)
        );
        $mform->setType('search', PARAM_TEXT);
        
        $mform->addElement('hidden','tab');
        $mform->setType('tab', PARAM_TEXT);
        $mform->addElement('hidden','sort');
        $mform->setType('sort', PARAM_TEXT);
        $mform->addElement('hidden','dir');
        $mform->setType('dir', PARAM_TEXT);
        $mform->addElement('hidden','perpage');
        $mform->setType('perpage', PARAM_INT);

        $this->add_action_buttons();
    }

    function add_action_buttons ($cancel = true, $submitlabel=null) {
        
        $mform =& $this->_form;
        
        $cancel_url = new moodle_url('/blocks/androgogic_catalogue/index.php', array(
            'tab' => 'location_search',
        ));
        
        $buttonarray=array();
        $buttonarray[] = &$mform->createElement('submit', 'submitbutton', get_string('search'));
        $buttonarray[] = &$mform->createElement('button', 'cancelbutton', get_string('cancel'),  ' onclick="location.href=\''.$cancel_url.'\'";');
        $mform->addGroup($buttonarray, 'buttonar', '', array(' '), false);
        $mform->closeHeaderBefore('buttonar');
    }
    
}
