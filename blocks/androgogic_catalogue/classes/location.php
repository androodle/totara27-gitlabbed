<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/** 
 * @package     block_androgogic_catalogue
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 **/

namespace block_androgogic_catalogue;

class location
{
    public $id;
    
    public function __construct($data = NULL)
    {
        $this->id = (!empty($data->id)) ? $data->id : NULL;
    }
    
    public function load($id) 
    {
        global $DB;

        $entry = $DB->get_record('androgogic_catalogue_locations', array('id' => $id));
        if (!$entry) {
            throw new Exception('locationnotfound', 'block_androgogic_catalogue');
        }
        
        $this->id = $entry->id;
        $this->name = $entry->name;
    }
    
    public function delete() 
    {
        global $DB;
        
        if (empty($this->id)) {
            throw new \Exception(get_string('idwrong', 'block_androgogic_catalogue'));
        }
        
        // TODO some safety checks to see if entries use this location
        
        $DB->delete_records('androgogic_catalogue_locations',array('id' => $this->id));
    }
    
    public function save(&$data) {

        global $USER, $DB;

        $data->modified_by = $USER->id;
        $data->datemodified = time();
        
        if (!empty($data->id)) {
            $DB->update_record('androgogic_catalogue_locations', $data);
        } else {
            $data->created_by = $USER->id;
            $data->datecreated = time();
            $data->id = $DB->insert_record('androgogic_catalogue_locations', $data);
        }
        return $data->id;
        
    }
}