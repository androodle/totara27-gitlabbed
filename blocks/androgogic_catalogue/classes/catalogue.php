<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/** 
 * @package     block_androgogic_catalogue
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 **/

namespace block_androgogic_catalogue;

class catalogue
{
    public static function get_view_mode()
    {
        global $CFG, $SESSION;
        
        $view = optional_param('view', '', PARAM_TEXT);
        
        // Remember the way we want to view the calendar.
        // If nothing chosen, set the default mode according to the plugin settings.
        if (!isset($SESSION->catalogue_view_mode)) {
            if (isset($CFG->catalogue_view_mode)) {
                $SESSION->catalogue_view_mode = $CFG->catalogue_view_mode;
            } else {
                $SESSION->catalogue_view_mode = 'standard';
            }
        }
        if (!empty($view)) {
            $SESSION->catalogue_view_mode = $view;
        }
        
        return $SESSION->catalogue_view_mode;
    }
    
    protected function build_search_joins() 
    {
        // TODO make something more flexible out of this.
        return "FROM {androgogic_catalogue_entries} a 
            LEFT JOIN {androgogic_catalogue_entry_locations} cel ON a.id = cel.catalogue_entry_id
            LEFT JOIN {androgogic_catalogue_locations} cl ON cel.location_id = cl.id
            LEFT JOIN {androgogic_catalogue_entry_courses} cec ON a.id = cec.catalogue_entry_id
            LEFT JOIN {course} c ON cec.course_id = c.id
            LEFT JOIN {androgogic_catalogue_entry_programs} cep ON a.id = cep.catalogue_entry_id
            LEFT JOIN {prog} p ON cep.program_id = p.id
            LEFT JOIN {androgogic_catalogue_entry_organisations} ceo ON a.id = ceo.catalogue_entry_id
            LEFT JOIN {org} o ON ceo.organisation_id = o.id
            LEFT JOIN {androgogic_catalogue_entry_positions} cepos ON a.id = cepos.catalogue_entry_id
            LEFT JOIN {pos} pos ON cepos.position_id = pos.id
            LEFT JOIN {androgogic_catalogue_entry_competencies} cecomp ON a.id = cecomp.catalogue_entry_id
            LEFT JOIN {comp} comp ON cecomp.competency_id = comp.id
            LEFT JOIN {androgogic_catalogue_entry_cohorts} cecoh ON a.id = cecoh.catalogue_entry_id
            LEFT JOIN {cohort} coh ON cecoh.cohort_id = coh.id";
    }

    protected function build_search_where(&$data) 
    {
        global $USER, $DB, $CFG;
        
        $where = array();
        $params = array();
        
        if (!empty($CFG->catalogue_comp_framework)) {
            // Limit results only to a selected framework.
            $where[] = "comp.frameworkid = :frameworkid";
            $params['frameworkid'] = $CFG->catalogue_comp_framework;
        }
        
        if (!empty($data->q)) {
            $where[] = $DB->sql_like($DB->sql_concat('a.name', 'a.description'), ':search', FALSE);
            $params['search'] = '%'.$data->q.'%';
        }
        
        if (!empty($data->l)) {
            $where[] = "cl.id = :locationid";
            $params['locationid'] = $data->l;
        }
        
        if (!empty($data->c)) {
            $where[] = "comp.id = :compid";
            $params['compid'] = $data->c;
        }
        
        if (isset($data->ct) && $data->ct != -1) {
            $where[] = "c.coursetype = :coursetype";
            $params['coursetype'] = $data->ct;
        }
        
        if (!empty($data->ds)) {
            $where[] = "c.startdate >= :startdate1 or p.availablefrom >= :startdate2";
            $params['startdate1'] = $data->ds;
            $params['startdate2'] = $data->ds;
        }
        
        if (!empty($data->de)) {
            $where[] = "p.availablefrom <= :enddate";
            $params['enddate'] = $data->de;
        }

        // if they are not logged in, they don't get to see non-public entries, 
        // and they can't have the pos or org logic applied, so don't show them these
        if (empty($USER->id)) {
            $where[] = "(a.public = 1 
                AND o.id IS NULL 
                AND pos.id IS NULL 
                AND coh.id IS NULL)";
        }
        
        return array($where, $params);
    }
    
    public function search_count(&$data) 
    {
        global $DB;
        
        list($where, $params) = $this->build_search_where($data);
        
        $sql = "SELECT COUNT(DISTINCT a.id)
            ".$this->build_search_joins()."";
        if (!empty($where)) {
            $sql .= ' WHERE '.implode(' AND ', $where);
        }
        
        return $DB->get_field_sql($sql, $params);
    }
    
    public function one($id)
    {
        $entry = new entry();
        if (!empty($id)) {
            try {
                $entry->load($id);
            } catch (Exception $ex) {
                return false;
            }
        }
        
        return array($entry);
    }

    public function search(&$data, $from = 0, $limit = 20) 
    {
        global $DB;
        
        list($where, $params) = $this->build_search_where($data);
        
        $sql = "SELECT DISTINCT a.*  
            ".$this->build_search_joins();
        if (!empty($where)) {
            $sql .= ' WHERE '.implode(' AND ', $where);
        }
        $sql .= " ORDER BY a.name DESC";
        
        return $DB->get_records_sql($sql, $params, $from, $limit);
    }
 
    public static function get_comp_frameworks()
    {
        global $DB;
        return $DB->get_records_menu('comp_framework', NULL, NULL, 'id, fullname');
    }
}
