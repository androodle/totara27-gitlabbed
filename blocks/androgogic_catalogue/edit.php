<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/** 
 * @package     block_androgogic_catalogue
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 **/


require_once('../../config.php');
require_once('lib.php');
require_once("$CFG->dirroot/totara/core/js/lib/setup.php");
require_once('forms/catalogue_entry_edit_form.php');

$id = optional_param('id', null, PARAM_INT);
$returnurl = new moodle_url('/blocks/androgogic_catalogue/index.php');

$context = context_system::instance();
$pageurl = new moodle_url('/blocks/androgogic_catalogue/edit.php');

require_capability('block/androgogic_catalogue:edit', $context);

$entry = new \block_androgogic_catalogue\entry();
if (!empty($id)) {
    try {
        $entry->load($id);
    } catch (Exception $ex) {
        throw $ex;
    }
}

$PAGE->set_context($context);
$PAGE->set_url($pageurl);
$PAGE->set_pagelayout('standard');
$PAGE->set_title(get_string('catalogue_entry_search', 'block_androgogic_catalogue'));
$PAGE->requires->css('/blocks/androgogic_catalogue/css/multi-select.css');
$PAGE->set_heading(get_string('plugintitle', 'block_androgogic_catalogue'));
$PAGE->navbar->add(get_string('catalogue_entry_search','block_androgogic_catalogue'), $pageurl);

local_js();
$PAGE->requires->js('/blocks/androgogic_catalogue/js/jquery.multi-select.js');
$PAGE->requires->js('/blocks/androgogic_catalogue/js/jquery.quicksearch.js');
$PAGE->requires->js('/blocks/androgogic_catalogue/js/multi-select.js');

$editoroptions = array(
    'maxfiles' => EDITOR_UNLIMITED_FILES, 
    'maxbytes' => $CFG->maxbytes, 
    'forcehttps' => false,
    'subdirs' => true,
    'context' => $context,
);
$mform = new catalogue_entry_edit_form(null, array('editoroptions' => $editoroptions));
if (!empty($id)) {
    $entry = file_prepare_standard_editor(
        $entry, 
        'description', 
        $editoroptions, 
        $context, 
        'block_androgogic_catalogue', 
        'catalogue_entry_description', 
        $id
    );
}

if ($mform->is_cancelled()) {
    redirect($returnurl);
}

$mform->set_data($entry);

$data = $mform->get_data();
if ($data) {
    $entry = new \block_androgogic_catalogue\entry($data);
    $id = $entry->save($data);
    
    $data = file_postupdate_standard_editor(
        $data, 
        'description', 
        $editoroptions, 
        $context, 
        'block_androgogic_catalogue', 
        'catalogue_entry_description', 
        $id
    );
    $DB->set_field('androgogic_catalogue_entries', 'description', $data->description, array('id' => $id));
    redirect($returnurl, get_string('datasubmitted', 'block_androgogic_catalogue'));
}

include_once('tabs.php');

echo $OUTPUT->header();
if (has_capability('block/androgogic_catalogue:edit', $context)) {
    // No point in showing only one tab if user can't edit locations.
    echo $OUTPUT->tabtree($tabs, 'index');
} else {
    echo html_writer::tag('h2', get_string('catalogue_entry_search', 'block_androgogic_catalogue'));
}
echo $OUTPUT->heading(get_string('catalogue_entry_edit', 'block_androgogic_catalogue'));

$mform->display();

echo $OUTPUT->footer();
