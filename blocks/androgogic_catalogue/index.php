<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/** 
 * Available search params:
 * - q (search by the entry name)
 * - l (location)
 * - c (competency)
 * - ct (course type)
 * 
 * @package     block_androgogic_catalogue
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 **/

require_once('../../config.php');
require_once('lib.php');
require_once('forms/catalogue_entry_search_form.php');

$id = optional_param('id', null, PARAM_INT);
$page = optional_param('page', 0, PARAM_INT);
$delete = optional_param('delete', null, PARAM_INT);

// Search params.
$q = optional_param('q', '', PARAM_TEXT);
$l = optional_param('l', 0, PARAM_INT);
$c = optional_param('c', 0, PARAM_INT);
$ct = optional_param('ct', -1, PARAM_INT);

// Handle legacy search parameters.
$search = optional_param('search', '', PARAM_TEXT);
$androgogic_catalogue_locations_id = optional_param('androgogic_catalogue_locations_id', 0, PARAM_INT);
$comp_id = optional_param('comp_id', 0, PARAM_INT);
$coursetype = optional_param('coursetype', -1, PARAM_INT);
if (!empty($search) && empty($q)) {
    $q = $search;
}
if (!empty($androgogic_catalogue_locations_id) && empty($l)) {
    $l = $androgogic_catalogue_locations_id;
}
if (!empty($comp_id) && empty($c)) {
    $c = $comp_id;
}
if (!empty($coursetype) && empty($ct)) {
    $ct = $coursetype;
}

// Check if login is required to view the catalogue.
if (!$CFG->catalogue_allow_guest_access) {
    require_login();
}

// Choose the view mode, calendar or list (standard).
$view = \block_androgogic_catalogue\catalogue::get_view_mode();
if (!empty($id)) {
    // We're only viewing one entry. Calendar view not applicable.
    $view = 'standard';
}

$context = context_system::instance();
$pageurl = new moodle_url('/blocks/androgogic_catalogue/index.php');

if ($delete && has_capability('block/androgogic_catalogue:delete', $context)) {
    $data = new stdClass();
    $data->id = $delete;
    $entry = new \block_androgogic_catalogue\entry($data);
    try {
        $entry->delete();
    } catch (Exception $ex) {
        echo $OUTPUT->notification($ex->getMessage());
    }
    redirect($pageurl, get_string('itemdeleted','block_androgogic_catalogue'));
}

$PAGE->set_context($context);
$PAGE->set_url($pageurl, compact('q', 'l', 'c', 'ct'));
$PAGE->set_pagelayout('standard');
$PAGE->set_blocks_editing_capability('moodle/site:manageblocks');
$PAGE->set_title(get_string('catalogue_entry_search', 'block_androgogic_catalogue'));
$PAGE->requires->css('/blocks/androgogic_catalogue/css/multi-select.css');
$PAGE->set_heading(get_string('plugintitle', 'block_androgogic_catalogue'));
$PAGE->navbar->ignore_active();
$PAGE->navbar->add(get_string('catalogue_entry_search','block_androgogic_catalogue'), $pageurl);
if ($SESSION->catalogue_view_mode == 'calendar') {
    $PAGE->requires->css('/blocks/androgogic_catalogue/css/fullcalendar.min.css');
    $PAGE->requires->jquery();
    $PAGE->requires->js('/blocks/androgogic_catalogue/js/moment.js');
    $PAGE->requires->js('/blocks/androgogic_catalogue/js/fullcalendar.min.js');
}

include_once('tabs.php');

echo $OUTPUT->header();
if (has_capability('block/androgogic_catalogue:edit', $context)) {
    // No point in showing only one tab if user can't edit locations.
    echo $OUTPUT->tabtree($tabs, 'index');
} else {
    echo html_writer::tag('h2', get_string('catalogue_entry_search', 'block_androgogic_catalogue'));
}

$mform = new catalogue_entry_search_form();
$show_results_count = FALSE;
$data = $mform->get_data();
if (!$data) {
    // Pass some GET values into the form.
    $data = new stdClass();
    $data->q = $q;
    $data->l = $l;
    $data->c = $c;
    $data->ct = $ct;
} else {
    $show_results_count = TRUE;
}
$mform->set_data($data);

echo html_writer::start_tag('div', array('class' => 'catalogue-links'));
if ($SESSION->catalogue_view_mode == 'standard' && empty($CFG->catalogue_disable_calendar_view) ) {
    echo html_writer::tag('a', 
        get_string('viewascalendar', 'block_androgogic_catalogue'),
        array(
            'href' => new moodle_url('index.php', array('view' => 'calendar')),
            'class' => 'catalogue-button-link link-as-button',
        )
    );
} elseif ($SESSION->catalogue_view_mode == 'calendar') {
    echo html_writer::tag('a', 
        get_string('viewaslist', 'block_androgogic_catalogue'),
        array(
            'href' => new moodle_url('index.php', array('view' => 'standard')),
            'class' => 'catalogue-button-link link-as-button',
        )
    );
}
if (has_capability('block/androgogic_catalogue:edit', $context)) {
    echo html_writer::tag('a', 
        get_string('catalogue_entry_new', 'block_androgogic_catalogue'),
        array(
            'href' => new moodle_url('edit.php'),
            'class' => 'catalogue-button-link link-as-button',
        )
    );
}
echo html_writer::end_tag('div');

if (!$CFG->catalogue_hide_search_form) {
    $mform->display();
}

$catalogue = new \block_androgogic_catalogue\catalogue();

$result_count = $catalogue->search_count($data);
if (!$result_count) {
    echo $OUTPUT->notification(get_string('noresults', 'block_androgogic_catalogue'));
    echo $OUTPUT->footer();
    die;
}

if ($show_results_count) {
    echo $OUTPUT->notification(get_string('entriesfound', 'block_androgogic_catalogue', $result_count), 'notifysuccess');
}

$renderer = $PAGE->get_renderer('block_androgogic_catalogue');

if (!empty($id)) {
    // Only one entry needed.
    $results = $catalogue->one($id);
} else {
    // Search in all entries.
    $results = $catalogue->search($data, $page * 20, 20);
}

echo $renderer->show_results($view, $results, $result_count, $page);

echo $OUTPUT->footer();
