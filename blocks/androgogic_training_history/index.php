<?php
/** 
 * Androgogic Training History Block: Index
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     17/06/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 *
 **/

require_once('../../config.php');
require_once('lib.php');
require_once($CFG->dirroot .'/lib/tablelib.php');
require_once($CFG->dirroot .'/totara/hierarchy/lib.php');
global $CFG,$USER,$DB,$PAGE, $OUTPUT;
//expected params
$currenttab = optional_param('tab', 'training_history_search', PARAM_FILE);
$download = optional_param('download','',PARAM_RAW);
$report_name_lang = optional_param('report_name_lang','',PARAM_RAW);
$debug = optional_param('debug', 0, PARAM_INT);
$context = context_system::instance();
$PAGE->set_context($context);
$PAGE->set_url("$CFG->wwwroot/blocks/androgogic_training_history/index.php", array('tab'=>$currenttab));
$PAGE->set_title(get_string($currenttab, 'block_androgogic_training_history'));
$PAGE->set_heading(get_string('plugintitle', 'block_androgogic_training_history'));
$PAGE->navbar->add(get_string('training_history_search','block_androgogic_training_history'),"$CFG->wwwroot/blocks/androgogic_training_history/index.php",'misc');

$config = get_config('block_androgogic_training_history');

require_login();
if($download == ''){
    echo $OUTPUT->header();
    //show tabs
    include('tabs.php');
    
}
// include current page
include $currenttab.'.php';

if($download == ''){
    echo $OUTPUT->footer();
}

// End of blocks/androgogic_training_history/index.php