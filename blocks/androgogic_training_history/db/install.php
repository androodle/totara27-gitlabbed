<?php

/**
 * Androgogic Training History Block: Install DB scripts
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     03/07/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 *
 * */
function xmldb_block_androgogic_training_history_install() {
global $DB;
$dbman = $DB->get_manager();
$result = true;
// create tables for the block
if (!$dbman->table_exists('androgogic_activities')) {
$table = new xmldb_table('androgogic_activities');
$table->add_field('id', XMLDB_TYPE_INTEGER , '11', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
$table->add_field('name', XMLDB_TYPE_CHAR , '50', null, XMLDB_NOTNULL, null, null);
$table->add_field('created_by', XMLDB_TYPE_INTEGER , '10', null, XMLDB_NOTNULL, null, null);
$table->add_field('date_created', XMLDB_TYPE_DATETIME , 'null', null, XMLDB_NOTNULL, null, null);
$table->add_field('modified_by', XMLDB_TYPE_INTEGER , '10', null, null, null, null);
$table->add_field('date_modified', XMLDB_TYPE_DATETIME , 'null', null, null, null, null);
$table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
$dbman->create_table($table);
}
if (!$dbman->table_exists('androgogic_course_dimensions')) {
$table = new xmldb_table('androgogic_course_dimensions');
$table->add_field('id', XMLDB_TYPE_INTEGER , '11', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
$table->add_field('course_id', XMLDB_TYPE_INTEGER , '11', null, XMLDB_NOTNULL, null, null);
$table->add_field('dimension_id', XMLDB_TYPE_INTEGER , '11', null, XMLDB_NOTNULL, null, null);
$table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
$table->add_index('ix_course_id', XMLDB_INDEX_NOTUNIQUE, array('course_id'));
$table->add_index('ix_dimension_id', XMLDB_INDEX_NOTUNIQUE, array('dimension_id'));
$dbman->create_table($table);
}
if (!$dbman->table_exists('androgogic_dimensions')) {
$table = new xmldb_table('androgogic_dimensions');
$table->add_field('id', XMLDB_TYPE_INTEGER , '11', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
$table->add_field('name', XMLDB_TYPE_CHAR , '50', null, XMLDB_NOTNULL, null, null);
$table->add_field('created_by', XMLDB_TYPE_INTEGER , '10', null, XMLDB_NOTNULL, null, null);
$table->add_field('date_created', XMLDB_TYPE_DATETIME , 'null', null, XMLDB_NOTNULL, null, null);
$table->add_field('modified_by', XMLDB_TYPE_INTEGER , '10', null, null, null, null);
$table->add_field('date_modified', XMLDB_TYPE_DATETIME , 'null', null, null, null, null);
$table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
$dbman->create_table($table);
}
if (!$dbman->table_exists('androgogic_org_pos_periods')) {
$table = new xmldb_table('androgogic_org_pos_periods');
$table->add_field('id', XMLDB_TYPE_INTEGER , '11', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
$table->add_field('org_id', XMLDB_TYPE_INTEGER , '11', null, XMLDB_NOTNULL, null, null);
$table->add_field('pos_id', XMLDB_TYPE_INTEGER , '11', null, XMLDB_NOTNULL, null, null);
$table->add_field('period', XMLDB_TYPE_CHAR , '10', null, XMLDB_NOTNULL, null, null);
$table->add_field('created_by', XMLDB_TYPE_INTEGER , '10', null, XMLDB_NOTNULL, null, null);
$table->add_field('date_created', XMLDB_TYPE_DATETIME , 'null', null, XMLDB_NOTNULL, null, null);
$table->add_field('modified_by', XMLDB_TYPE_INTEGER , '10', null, null, null, null);
$table->add_field('date_modified', XMLDB_TYPE_DATETIME , 'null', null, null, null, null);
$table->add_field('soa_course_id', XMLDB_TYPE_INTEGER , '11', null, XMLDB_NOTNULL, null, null);
$table->add_field('soa_comp_id', XMLDB_TYPE_INTEGER , '11', null, XMLDB_NOTNULL, null, null);
$table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
$table->add_index('ix_org_id', XMLDB_INDEX_NOTUNIQUE, array('org_id'));
$table->add_index('ix_pos_id', XMLDB_INDEX_NOTUNIQUE, array('pos_id'));
$table->add_index('ix_soa_course_id', XMLDB_INDEX_NOTUNIQUE, array('soa_course_id'));
$table->add_index('ix_soa_comp_id', XMLDB_INDEX_NOTUNIQUE, array('soa_comp_id'));
$dbman->create_table($table);
}
if (!$dbman->table_exists('androgogic_training_history')) {
$table = new xmldb_table('androgogic_training_history');
$table->add_field('id', XMLDB_TYPE_INTEGER , '11', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
$table->add_field('title_of_training', XMLDB_TYPE_CHAR , '255', null, null, null, null);
$table->add_field('activity_id', XMLDB_TYPE_INTEGER , '10', null, null, null, null);
$table->add_field('date_issued', XMLDB_TYPE_INTEGER , '20', null, null, null, null);
$table->add_field('file_id', XMLDB_TYPE_INTEGER , '10', null, null, null, null);
$table->add_field('user_id', XMLDB_TYPE_INTEGER , '10', null, XMLDB_NOTNULL, null, null);
$table->add_field('created_by', XMLDB_TYPE_INTEGER , '10', null, XMLDB_NOTNULL, null, null);
$table->add_field('date_created', XMLDB_TYPE_DATETIME , 'null', null, XMLDB_NOTNULL, null, null);
$table->add_field('modified_by', XMLDB_TYPE_INTEGER , '10', null, null, null, null);
$table->add_field('date_modified', XMLDB_TYPE_DATETIME , 'null', null, null, null, null);
$table->add_field('assessed', XMLDB_TYPE_INTEGER , '4', null, null, null, null);
$table->add_field('assessment_code', XMLDB_TYPE_CHAR , '128', null, null, null, null);
$table->add_field('approved', XMLDB_TYPE_INTEGER , '4', null, null, null, null);
$table->add_field('provider_id', XMLDB_TYPE_INTEGER , '10', null, null, null, null);
$table->add_field('qualification_id', XMLDB_TYPE_INTEGER , '10', null, null, null, null);
$table->add_field('membership_id', XMLDB_TYPE_INTEGER , '10', null, null, null, null);
$table->add_field('professional_designation_id', XMLDB_TYPE_INTEGER , '10', null, null, null, null);
$table->add_field('date_expiry', XMLDB_TYPE_INTEGER , '20', null, null, null, null);
$table->add_field('date_expiry_processed', XMLDB_TYPE_INTEGER , '11', null, XMLDB_NOTNULL, null, null);
$table->add_field('date_sent_to_sf', XMLDB_TYPE_INTEGER , '11', null, XMLDB_NOTNULL, null, null);
$table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
$table->add_index('ix_activity_id', XMLDB_INDEX_NOTUNIQUE, array('activity_id'));
$table->add_index('ix_file_id', XMLDB_INDEX_NOTUNIQUE, array('file_id'));
$table->add_index('ix_user_id', XMLDB_INDEX_NOTUNIQUE, array('user_id'));
$table->add_index('ix_provider_id', XMLDB_INDEX_NOTUNIQUE, array('provider_id'));
$table->add_index('ix_qualification_id', XMLDB_INDEX_NOTUNIQUE, array('qualification_id'));
$table->add_index('ix_membership_id', XMLDB_INDEX_NOTUNIQUE, array('membership_id'));
$table->add_index('ix_professional_designation_id', XMLDB_INDEX_NOTUNIQUE, array('professional_designation_id'));
$dbman->create_table($table);
}
if (!$dbman->table_exists('androgogic_training_history_competencies_cpd_points')) {
$table = new xmldb_table('androgogic_training_history_competencies_cpd_points');
$table->add_field('id', XMLDB_TYPE_INTEGER , '11', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
$table->add_field('training_history_id', XMLDB_TYPE_INTEGER , '11', null, XMLDB_NOTNULL, null, null);
$table->add_field('competency_id', XMLDB_TYPE_INTEGER , '11', null, XMLDB_NOTNULL, null, null);
$table->add_field('cpd_points', XMLDB_TYPE_NUMBER , '5,2', null, XMLDB_NOTNULL, null, null);
$table->add_field('date_modified', XMLDB_TYPE_DATETIME , 'null', null, null, null, null);
$table->add_field('deleted', XMLDB_TYPE_INTEGER , '1', null, null, null, null);
$table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
$table->add_index('ix_training_history_id', XMLDB_INDEX_NOTUNIQUE, array('training_history_id'));
$table->add_index('ix_competency_id', XMLDB_INDEX_NOTUNIQUE, array('competency_id'));
$dbman->create_table($table);
}
if (!$dbman->table_exists('androgogic_training_history_competencies_cpe_hours')) {
$table = new xmldb_table('androgogic_training_history_competencies_cpe_hours');
$table->add_field('id', XMLDB_TYPE_INTEGER , '11', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
$table->add_field('training_history_id', XMLDB_TYPE_INTEGER , '11', null, XMLDB_NOTNULL, null, null);
$table->add_field('competency_id', XMLDB_TYPE_INTEGER , '11', null, XMLDB_NOTNULL, null, null);
$table->add_field('cpe_hours', XMLDB_TYPE_NUMBER , '5,2', null, XMLDB_NOTNULL, null, null);
$table->add_field('date_modified', XMLDB_TYPE_DATETIME , 'null', null, null, null, null);
$table->add_field('deleted', XMLDB_TYPE_INTEGER , '1', null, null, null, null);
$table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
$table->add_index('ix_training_history_id', XMLDB_INDEX_NOTUNIQUE, array('training_history_id'));
$table->add_index('ix_competency_id', XMLDB_INDEX_NOTUNIQUE, array('competency_id'));
$dbman->create_table($table);
}
if (!$dbman->table_exists('androgogic_training_history_course_competencies_cpd_points')) {
$table = new xmldb_table('androgogic_training_history_course_competencies_cpd_points');
$table->add_field('id', XMLDB_TYPE_INTEGER , '11', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
$table->add_field('course_id', XMLDB_TYPE_INTEGER , '11', null, XMLDB_NOTNULL, null, null);
$table->add_field('competency_id', XMLDB_TYPE_INTEGER , '11', null, XMLDB_NOTNULL, null, null);
$table->add_field('cpd_points', XMLDB_TYPE_NUMBER , '5,2', null, XMLDB_NOTNULL, null, null);
$table->add_field('date_modified', XMLDB_TYPE_DATETIME , 'null', null, null, null, null);
$table->add_field('deleted', XMLDB_TYPE_INTEGER , '1', null, null, null, null);
$table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
$table->add_index('ix_course_id', XMLDB_INDEX_NOTUNIQUE, array('course_id'));
$table->add_index('ix_competency_id', XMLDB_INDEX_NOTUNIQUE, array('competency_id'));
$dbman->create_table($table);
}
if (!$dbman->table_exists('androgogic_training_history_course_competencies_cpe_hours')) {
$table = new xmldb_table('androgogic_training_history_course_competencies_cpe_hours');
$table->add_field('id', XMLDB_TYPE_INTEGER , '11', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
$table->add_field('course_id', XMLDB_TYPE_INTEGER , '11', null, XMLDB_NOTNULL, null, null);
$table->add_field('competency_id', XMLDB_TYPE_INTEGER , '11', null, XMLDB_NOTNULL, null, null);
$table->add_field('cpe_hours', XMLDB_TYPE_NUMBER , '5,2', null, XMLDB_NOTNULL, null, null);
$table->add_field('date_modified', XMLDB_TYPE_DATETIME , 'null', null, null, null, null);
$table->add_field('deleted', XMLDB_TYPE_INTEGER , '1', null, null, null, null);
$table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
$table->add_index('ix_course_id', XMLDB_INDEX_NOTUNIQUE, array('course_id'));
$table->add_index('ix_competency_id', XMLDB_INDEX_NOTUNIQUE, array('competency_id'));
$dbman->create_table($table);
}
if (!$dbman->table_exists('androgogic_training_history_cpd_targets')) {
$table = new xmldb_table('androgogic_training_history_cpd_targets');
$table->add_field('id', XMLDB_TYPE_INTEGER , '11', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
$table->add_field('competency_id', XMLDB_TYPE_INTEGER , '11', null, XMLDB_NOTNULL, null, null);
$table->add_field('org_pos_period_id', XMLDB_TYPE_INTEGER , '11', null, XMLDB_NOTNULL, null, null);
$table->add_field('cpd_target', XMLDB_TYPE_NUMBER , '5,2', null, XMLDB_NOTNULL, null, null);
$table->add_field('created_by', XMLDB_TYPE_INTEGER , '10', null, XMLDB_NOTNULL, null, null);
$table->add_field('date_created', XMLDB_TYPE_DATETIME , 'null', null, XMLDB_NOTNULL, null, null);
$table->add_field('modified_by', XMLDB_TYPE_INTEGER , '10', null, null, null, null);
$table->add_field('date_modified', XMLDB_TYPE_DATETIME , 'null', null, null, null, null);
$table->add_field('deleted', XMLDB_TYPE_INTEGER , '1', null, null, null, null);
$table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
$table->add_index('ix_competency_id', XMLDB_INDEX_NOTUNIQUE, array('competency_id'));
$table->add_index('ix_org_pos_period_id', XMLDB_INDEX_NOTUNIQUE, array('org_pos_period_id'));
$dbman->create_table($table);
}
if (!$dbman->table_exists('androgogic_training_history_cpe_targets')) {
$table = new xmldb_table('androgogic_training_history_cpe_targets');
$table->add_field('id', XMLDB_TYPE_INTEGER , '11', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
$table->add_field('competency_id', XMLDB_TYPE_INTEGER , '11', null, XMLDB_NOTNULL, null, null);
$table->add_field('org_pos_period_id', XMLDB_TYPE_INTEGER , '11', null, XMLDB_NOTNULL, null, null);
$table->add_field('cpe_target', XMLDB_TYPE_NUMBER , '5,2', null, XMLDB_NOTNULL, null, null);
$table->add_field('created_by', XMLDB_TYPE_INTEGER , '10', null, XMLDB_NOTNULL, null, null);
$table->add_field('date_created', XMLDB_TYPE_DATETIME , 'null', null, XMLDB_NOTNULL, null, null);
$table->add_field('modified_by', XMLDB_TYPE_INTEGER , '10', null, null, null, null);
$table->add_field('date_modified', XMLDB_TYPE_DATETIME , 'null', null, null, null, null);
$table->add_field('deleted', XMLDB_TYPE_INTEGER , '1', null, null, null, null);
$table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
$table->add_index('ix_competency_id', XMLDB_INDEX_NOTUNIQUE, array('competency_id'));
$table->add_index('ix_org_pos_period_id', XMLDB_INDEX_NOTUNIQUE, array('org_pos_period_id'));
$dbman->create_table($table);
}
if (!$dbman->table_exists('androgogic_training_history_dimensions')) {
$table = new xmldb_table('androgogic_training_history_dimensions');
$table->add_field('id', XMLDB_TYPE_INTEGER , '11', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
$table->add_field('training_history_id', XMLDB_TYPE_INTEGER , '11', null, XMLDB_NOTNULL, null, null);
$table->add_field('dimension_id', XMLDB_TYPE_INTEGER , '11', null, XMLDB_NOTNULL, null, null);
$table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
$table->add_index('ix_training_history_id', XMLDB_INDEX_NOTUNIQUE, array('training_history_id'));
$table->add_index('ix_dimension_id', XMLDB_INDEX_NOTUNIQUE, array('dimension_id'));
$dbman->create_table($table);
}
if (!$dbman->table_exists('androgogic_training_history_email_log')) {
$table = new xmldb_table('androgogic_training_history_email_log');
$table->add_field('id', XMLDB_TYPE_INTEGER , '11', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
$table->add_field('membership_id', XMLDB_TYPE_INTEGER , '11', null, XMLDB_NOTNULL, null, null);
$table->add_field('user_id', XMLDB_TYPE_INTEGER , '11', null, XMLDB_NOTNULL, null, null);
$table->add_field('period', XMLDB_TYPE_CHAR , '20', null, XMLDB_NOTNULL, null, null);
$table->add_field('date_sent', XMLDB_TYPE_INTEGER , '11', null, XMLDB_NOTNULL, null, null);
$table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
$table->add_index('ix_membership_id', XMLDB_INDEX_NOTUNIQUE, array('membership_id'));
$table->add_index('ix_user_id', XMLDB_INDEX_NOTUNIQUE, array('user_id'));
$dbman->create_table($table);
}
if (!$dbman->table_exists('androgogic_training_history_membership')) {
$table = new xmldb_table('androgogic_training_history_membership');
$table->add_field('id', XMLDB_TYPE_INTEGER , '11', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
$table->add_field('name', XMLDB_TYPE_CHAR , '255', null, XMLDB_NOTNULL, null, null);
$table->add_field('course_id', XMLDB_TYPE_INTEGER , '11', null, XMLDB_NOTNULL, null, null);
$table->add_field('created_by', XMLDB_TYPE_INTEGER , '10', null, XMLDB_NOTNULL, null, null);
$table->add_field('date_created', XMLDB_TYPE_DATETIME , 'null', null, XMLDB_NOTNULL, null, null);
$table->add_field('modified_by', XMLDB_TYPE_INTEGER , '10', null, null, null, null);
$table->add_field('date_modified', XMLDB_TYPE_DATETIME , 'null', null, null, null, null);
$table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
$table->add_index('ix_course_id', XMLDB_INDEX_NOTUNIQUE, array('course_id'));
$dbman->create_table($table);
}
if (!$dbman->table_exists('androgogic_training_history_periods')) {
$table = new xmldb_table('androgogic_training_history_periods');
$table->add_field('id', XMLDB_TYPE_INTEGER , '11', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
$table->add_field('type', XMLDB_TYPE_CHAR , '20', null, XMLDB_NOTNULL, null, null);
$table->add_field('start_year', XMLDB_TYPE_INTEGER , '4', null, XMLDB_NOTNULL, null, null);
$table->add_field('end_year', XMLDB_TYPE_INTEGER , '4', null, XMLDB_NOTNULL, null, null);
$table->add_field('is_current', XMLDB_TYPE_INTEGER , '1', null, XMLDB_NOTNULL, null, null);
$table->add_field('created_by', XMLDB_TYPE_INTEGER , '10', null, XMLDB_NOTNULL, null, null);
$table->add_field('date_created', XMLDB_TYPE_DATETIME , 'null', null, XMLDB_NOTNULL, null, null);
$table->add_field('modified_by', XMLDB_TYPE_INTEGER , '10', null, null, null, null);
$table->add_field('date_modified', XMLDB_TYPE_DATETIME , 'null', null, null, null, null);
$table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
$dbman->create_table($table);
}
if (!$dbman->table_exists('androgogic_training_history_professional_designations')) {
$table = new xmldb_table('androgogic_training_history_professional_designations');
$table->add_field('id', XMLDB_TYPE_INTEGER , '11', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
$table->add_field('name', XMLDB_TYPE_CHAR , '255', null, XMLDB_NOTNULL, null, null);
$table->add_field('membership_id', XMLDB_TYPE_INTEGER , '11', null, XMLDB_NOTNULL, null, null);
$table->add_field('created_by', XMLDB_TYPE_INTEGER , '10', null, XMLDB_NOTNULL, null, null);
$table->add_field('date_created', XMLDB_TYPE_DATETIME , 'null', null, XMLDB_NOTNULL, null, null);
$table->add_field('modified_by', XMLDB_TYPE_INTEGER , '10', null, null, null, null);
$table->add_field('date_modified', XMLDB_TYPE_DATETIME , 'null', null, null, null, null);
$table->add_field('course_id', XMLDB_TYPE_INTEGER , '11', null, null, null, null);
$table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
$table->add_index('ix_membership_id', XMLDB_INDEX_NOTUNIQUE, array('membership_id'));
$table->add_index('ix_course_id', XMLDB_INDEX_NOTUNIQUE, array('course_id'));
$dbman->create_table($table);
}
if (!$dbman->table_exists('androgogic_training_history_providers')) {
$table = new xmldb_table('androgogic_training_history_providers');
$table->add_field('id', XMLDB_TYPE_INTEGER , '11', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
$table->add_field('name', XMLDB_TYPE_CHAR , '255', null, XMLDB_NOTNULL, null, null);
$table->add_field('created_by', XMLDB_TYPE_INTEGER , '10', null, XMLDB_NOTNULL, null, null);
$table->add_field('date_created', XMLDB_TYPE_DATETIME , 'null', null, XMLDB_NOTNULL, null, null);
$table->add_field('modified_by', XMLDB_TYPE_INTEGER , '10', null, null, null, null);
$table->add_field('date_modified', XMLDB_TYPE_DATETIME , 'null', null, null, null, null);
$table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
$dbman->create_table($table);
}
if (!$dbman->table_exists('androgogic_training_history_qualifications')) {
$table = new xmldb_table('androgogic_training_history_qualifications');
$table->add_field('id', XMLDB_TYPE_INTEGER , '11', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
$table->add_field('name', XMLDB_TYPE_CHAR , '256', null, XMLDB_NOTNULL, null, null);
$table->add_field('created_by', XMLDB_TYPE_INTEGER , '10', null, XMLDB_NOTNULL, null, null);
$table->add_field('date_created', XMLDB_TYPE_DATETIME , 'null', null, XMLDB_NOTNULL, null, null);
$table->add_field('modified_by', XMLDB_TYPE_INTEGER , '10', null, null, null, null);
$table->add_field('date_modified', XMLDB_TYPE_DATETIME , 'null', null, null, null, null);
$table->add_field('course_id', XMLDB_TYPE_INTEGER , '11', null, null, null, null);
$table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
$table->add_index('ix_course_id', XMLDB_INDEX_NOTUNIQUE, array('course_id'));
$dbman->create_table($table);
}
if (!$dbman->table_exists('androgogic_user_hours_override')) {
$table = new xmldb_table('androgogic_user_hours_override');
$table->add_field('id', XMLDB_TYPE_INTEGER , '11', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
$table->add_field('user_id', XMLDB_TYPE_INTEGER , '11', null, XMLDB_NOTNULL, null, null);
$table->add_field('competency_id', XMLDB_TYPE_INTEGER , '11', null, XMLDB_NOTNULL, null, null);
$table->add_field('hours', XMLDB_TYPE_INTEGER , '11', null, XMLDB_NOTNULL, null, null);
$table->add_field('created_by', XMLDB_TYPE_INTEGER , '10', null, XMLDB_NOTNULL, null, null);
$table->add_field('date_created', XMLDB_TYPE_DATETIME , 'null', null, XMLDB_NOTNULL, null, null);
$table->add_field('modified_by', XMLDB_TYPE_INTEGER , '10', null, null, null, null);
$table->add_field('date_modified', XMLDB_TYPE_DATETIME , 'null', null, null, null, null);
$table->add_field('deleted', XMLDB_TYPE_INTEGER , '1', null, null, null, null);
$table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
$table->add_index('ix_user_id', XMLDB_INDEX_NOTUNIQUE, array('user_id'));
$table->add_index('ix_competency_id', XMLDB_INDEX_NOTUNIQUE, array('competency_id'));
$dbman->create_table($table);
}
if (!$dbman->table_exists('androgogic_user_period')) {
$table = new xmldb_table('androgogic_user_period');
$table->add_field('id', XMLDB_TYPE_INTEGER , '11', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
$table->add_field('startdate', XMLDB_TYPE_INTEGER , '20', null, XMLDB_NOTNULL, null, null);
$table->add_field('enddate', XMLDB_TYPE_INTEGER , '20', null, XMLDB_NOTNULL, null, null);
$table->add_field('user_id', XMLDB_TYPE_INTEGER , '11', null, XMLDB_NOTNULL, null, null);
$table->add_field('created_by', XMLDB_TYPE_INTEGER , '10', null, XMLDB_NOTNULL, null, null);
$table->add_field('date_created', XMLDB_TYPE_DATETIME , 'null', null, XMLDB_NOTNULL, null, null);
$table->add_field('modified_by', XMLDB_TYPE_INTEGER , '10', null, null, null, null);
$table->add_field('date_modified', XMLDB_TYPE_DATETIME , 'null', null, null, null, null);
$table->add_field('period_id', XMLDB_TYPE_INTEGER , '11', null, null, null, null);
$table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
$table->add_index('ix_user_id', XMLDB_INDEX_NOTUNIQUE, array('user_id'));
$table->add_index('ix_period_id', XMLDB_INDEX_NOTUNIQUE, array('period_id'));
$dbman->create_table($table);
}
if (!$dbman->table_exists('androgogic_user_points_override')) {
$table = new xmldb_table('androgogic_user_points_override');
$table->add_field('id', XMLDB_TYPE_INTEGER , '11', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
$table->add_field('user_id', XMLDB_TYPE_INTEGER , '11', null, XMLDB_NOTNULL, null, null);
$table->add_field('competency_id', XMLDB_TYPE_INTEGER , '11', null, XMLDB_NOTNULL, null, null);
$table->add_field('points', XMLDB_TYPE_INTEGER , '11', null, XMLDB_NOTNULL, null, null);
$table->add_field('created_by', XMLDB_TYPE_INTEGER , '10', null, XMLDB_NOTNULL, null, null);
$table->add_field('date_created', XMLDB_TYPE_DATETIME , 'null', null, XMLDB_NOTNULL, null, null);
$table->add_field('modified_by', XMLDB_TYPE_INTEGER , '10', null, null, null, null);
$table->add_field('date_modified', XMLDB_TYPE_DATETIME , 'null', null, null, null, null);
$table->add_field('deleted', XMLDB_TYPE_INTEGER , '1', null, null, null, null);
$table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
$table->add_index('ix_user_id', XMLDB_INDEX_NOTUNIQUE, array('user_id'));
$table->add_index('ix_competency_id', XMLDB_INDEX_NOTUNIQUE, array('competency_id'));
$dbman->create_table($table);
}
return $result;
} 
