<?php

/** 
 * Androgogic Training History Block: Delete object 
 * 
 * @author      Daniel Morphett <dan@androgogic.com> 
 * @version     07/11/2014 
 * @copyright   2014+ Androgogic Pty Ltd <http://www.androgogic.com> 
 * 
 * Delete one of the providers
 *  
 **/

require_capability('block/androgogic_training_history:admin', $context);
$id = required_param('id', PARAM_INT);
$DB->delete_records('androgogic_training_history_providers',array('id'=>$id));
echo $OUTPUT->notification(get_string('itemdeleted','block_androgogic_training_history'), 'notifysuccess');

