<?php

/** 
 * Androgogic Training History Block: Edit form 
 * 
 * @author      Daniel Morphett <dan@androgogic.com> 
 * @version     06/11/2014 
 * @copyright   2014+ Androgogic Pty Ltd <http://www.androgogic.com> 
 * 
 * Provides edit form for the object.
 * This is used by both new and edit pages
 *  
 **/

if (!defined('MOODLE_INTERNAL')) {
die('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}
require_once($CFG->libdir . '/formslib.php');
class qualification_edit_form extends moodleform {
protected $qualification;
function definition() {
global $USER,$courseid,$DB,$PAGE;
$mform =& $this->_form;
$context = context_system::instance();
if(isset($_REQUEST['id'])){
$q = "select DISTINCT a.* , mdl_course.fullname as course 
from mdl_androgogic_training_history_qualifications a 
LEFT JOIN mdl_course  on a.course_id = mdl_course.id
where a.id = {$_REQUEST['id']} ";
$qualification = $DB->get_record_sql($q);
}
else{
$qualification = $this->_customdata['$qualification']; // this contains the data of this form
}
$tab = 'qualification_new'; // from whence we were called
if (!empty($qualification->id)) {
$tab = 'qualification_edit';
}
$mform->addElement('html','<div>');

//name
$mform->addElement('text', 'name', get_string('name','block_androgogic_training_history'), array('size'=>255));
$mform->addRule('name', get_string('required'), 'required', null, 'server');
$mform->addRule('name', 'Maximum 255 characters', 'maxlength', 255, 'client');

//course_id
$options = $DB->get_records_menu('course',null,'fullname','id,fullname');
$mform->addElement('select', 'course_id', get_string('course_id','block_androgogic_training_history'), $options);
//set values if we are in edit mode
if (!empty($qualification->id) && isset($_GET['id'])) {
$mform->setConstant('name', $qualification->name);
$mform->setConstant('course_id', $qualification->course_id);
}
//hiddens
$mform->addElement('hidden','tab',$tab);
if(isset($_REQUEST['id'])){
$mform->addElement('hidden','id',$_REQUEST['id']);
}
elseif(isset($id)){
$mform->addElement('hidden', 'id', $id);
}
$this->add_action_buttons(false);
$mform->addElement('html','</div>');
}
}
