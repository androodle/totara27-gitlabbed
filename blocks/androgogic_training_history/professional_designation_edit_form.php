<?php

/** 
 * Androgogic Training History Block: Edit form 
 * 
 * @author      Daniel Morphett <dan@androgogic.com> 
 * @version     06/11/2014 
 * @copyright   2014+ Androgogic Pty Ltd <http://www.androgogic.com> 
 * 
 * Provides edit form for the object.
 * This is used by both new and edit pages
 *  
 **/

if (!defined('MOODLE_INTERNAL')) {
die('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}
require_once($CFG->libdir . '/formslib.php');
class professional_designation_edit_form extends moodleform {
protected $professional_designation;
function definition() {
global $USER,$courseid,$DB,$PAGE;
$mform =& $this->_form;
$context = context_system::instance();
if(isset($_REQUEST['id'])){
$q = "select DISTINCT a.* , mdl_androgogic_training_history_membership.name as membership, mdl_course.fullname as course 
from mdl_androgogic_training_history_professional_designations a 
LEFT JOIN mdl_androgogic_training_history_membership  on a.membership_id = mdl_androgogic_training_history_membership.id
LEFT JOIN mdl_course  on a.course_id = mdl_course.id
where a.id = {$_REQUEST['id']} ";
$professional_designation = $DB->get_record_sql($q);
}
else{
$professional_designation = $this->_customdata['$professional_designation']; // this contains the data of this form
}
$tab = 'professional_designation_new'; // from whence we were called
if (!empty($professional_designation->id)) {
$tab = 'professional_designation_edit';
}
$mform->addElement('html','<div>');

//name
$mform->addElement('text', 'name', get_string('name','block_androgogic_training_history'), array('size'=>255));
$mform->addRule('name', get_string('required'), 'required', null, 'server');
$mform->addRule('name', 'Maximum 255 characters', 'maxlength', 255, 'client');

//membership_id
$options = $DB->get_records_menu('androgogic_training_history_membership',null,'name','id,name'); //sometimes this needs a manual tweak, if the 2nd col is mainly nulls
$mform->addElement('select', 'membership_id', get_string('membership_id','block_androgogic_training_history'), $options);
$mform->addRule('membership_id', get_string('required'), 'required', null, 'server');
//course_id
$options = $DB->get_records_menu('course',null,'fullname','id,fullname'); //sometimes this needs a manual tweak, if the 2nd col is mainly nulls
$mform->addElement('select', 'course_id', get_string('course_id','block_androgogic_training_history'), $options);
//set values if we are in edit mode
if (!empty($professional_designation->id) && isset($_GET['id'])) {
$mform->setConstant('name', $professional_designation->name);
$mform->setConstant('membership_id', $professional_designation->membership_id);
$mform->setConstant('course_id', $professional_designation->course_id);
}
//hiddens
$mform->addElement('hidden','tab',$tab);
if(isset($_REQUEST['id'])){
$mform->addElement('hidden','id',$_REQUEST['id']);
}
elseif(isset($id)){
$mform->addElement('hidden', 'id', $id);
}
$this->add_action_buttons(false);
$mform->addElement('html','</div>');
}
}
