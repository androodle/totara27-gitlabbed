<?php

/**
 * Androgogic Training History Block: Search
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     17/06/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 * Search training_histories
 * Also provides access to edit and delete functions if user has sufficient permissions
 *
 * */
//params
$sort = optional_param('sort', 'date_issued', PARAM_RAW);
$dir = optional_param('dir', 'DESC', PARAM_ALPHA);
$page = optional_param('page', 0, PARAM_INT);
$perpage = optional_param('perpage', 50, PARAM_INT);
$search = optional_param('search', '', PARAM_TEXT);
$tab = optional_param('tab', 'training_history_search', PARAM_FILE);
$androgogic_activities_id = optional_param('androgogic_activities_id', 0, PARAM_INT);
$user_id = optional_param('user_id', 0, PARAM_INT);
$user_search = optional_param('user_search', '', PARAM_TEXT);
$androgogic_dimensions_id = optional_param('androgogic_dimensions_id', 0, PARAM_INT);
$training_history_type = optional_param('training_history_type', '', PARAM_TEXT);
$membership_id = optional_param('membership_id', 0, PARAM_INT);
$qualification_id = optional_param('qualification_id', 0, PARAM_INT);
$professional_designation_id = optional_param('professional_designation_id', 0, PARAM_INT);
$startdate = optional_param('startdate', 0, PARAM_INT); //this one is the array that comes in from the date filter. This throws a debugging message, but works as we want it to
$startdate_int = optional_param('startdate_int', '0', PARAM_INT);
$enddate = optional_param('enddate', 0, PARAM_INT); //ditto
$enddate_int = optional_param('enddate_int', '0', PARAM_INT);

// this report can be long winded...
set_time_limit(30*60*60);
raise_memory_limit('4G');

$get_user_period = true;
if (isset($startdate['enabled'])) {
    //make it into a unix time
    $startdate = mktime(0, 0, 0, $startdate['month'], $startdate['day'], $startdate['year']);
    $startdate_int = $startdate;
} else if ($startdate_int > 0) {
    //might be coming in on the url via paging
    $startdate = $startdate_int;
}
if (isset($enddate['enabled'])) {
    //make it into a unix time
    $enddate = mktime(0, 0, 0, $enddate['month'], $enddate['day'], $enddate['year']) + 1;
    $enddate_int = $enddate;
} else if ($enddate_int > 0) {
    //might be coming in on the url via paging
    $enddate = $enddate_int;
}
// prepare url for paging bar
$PAGE->set_url($PAGE->url, compact('sort', 'dir', 'page', 'perpage', 'search', 'tab', 'androgogic_activities_id', 
        'user_id', 'user_search', 'startdate_int', 'enddate_int','training_history_type','membership_id','qualification_id',
        'professional_designation_id'));
// prepare columns for results table
$columns = array(
    "title_of_training",
    "activity",
    "qualification",
    "membership",
    "professional_designation",
    "date_issued",
    "date_expiry",
    "provider",
    "file",
    "user",
    "username",
    "assessed",
    "assessment_code",
    "approved"
);
if ($user_search != '') {
    //reset user id 
    $user_id = 0;
}
if ($user_id == 0) {
    $columns[] = "user";
    $columns[] = "username";
}
foreach ($columns as $column) {
    $string[$column] = get_string("$column", 'block_androgogic_training_history');
    if ($sort != $column) {
        $columnicon = '';
        $columndir = 'ASC';
    } else {
        $columndir = $dir == 'ASC' ? 'DESC' : 'ASC';
        $columnicon = $dir == 'ASC' ? 'down' : 'up';
    }
    if ($column != 'details') {
        $$column = "<a href='$PAGE->url&amp;dir=$columndir&amp;sort=$column'>$string[$column]</a>";
    } else {
        $$column = $string[$column];
    }
}
//figure out the and clause from what has been submitted
$and = '';
$and2 = '';
if ($search != '') {
    $search = str_ireplace("'", "\'", $search);
    $and .= " and concat( ifnull(a.title_of_training,''), ifnull(p.name,''), ifnull(a.assessment_code,'') )  like '%$search%'";
}
//are we filtering on androgogic_activities?
if ($androgogic_activities_id > 0) {
    $and .= " and mdl_androgogic_activities.id = $androgogic_activities_id ";
}
if ($training_history_type == 'activity') {
    $and .= " and a.activity_id is not null";
}
if ($training_history_type == 'membership') {
    $and .= " and a.membership_id is not null";
}
if ($training_history_type == 'qualification') {
    $and .= " and a.qualification_id is not null";
}
if ($membership_id > 0) {
    $and .= " and a.membership_id = $membership_id ";
}
if ($qualification_id > 0) {
    $and .= " and a.qualification_id = $qualification_id ";
}
if ($professional_designation_id > 0) {
    $and .= " and pd.id = $professional_designation_id ";
}

//is the user a practice viewer?
$is_practice_viewer = has_capability('block/androgogic_training_history:see_members_of_own_practice',$context);

//are we filtering on user?
if (!has_capability('block/androgogic_training_history:admin', $context)) {
    if ($is_practice_viewer) {
        //get the practice (org)
        $org = $DB->get_record('pos_assignment',array('userid'=>$USER->id,'type'=>1));
        if($org){
           $and .= " and ( mdl_user.id in (select userid from mdl_pos_assignment where organisationid = $org->organisationid) OR mdl_user.id = $USER->id )"; 
        }
        else {
            $user_id = $USER->id;
        }
    } else {
        $user_id = $USER->id;
    }
}
if ($user_id > 0) {
    $and .= " and mdl_user.id = $user_id ";
} else if (empty($_POST) and ! isset($_GET['page'])) {
    $and .= " and 1=0 ";
}
//are we filtering on androgogic_dimensions?
if ($androgogic_dimensions_id > 0) {
    $and .= " and mdl_androgogic_dimensions.id = $androgogic_dimensions_id ";
}

if (isset($startdate) and is_integer($startdate) and $startdate > 0) {
    $filter_startdate = $startdate - (60 * 60 * 24);
    $and .= " and (a.date_issued > $filter_startdate)";
}
if (isset($enddate) and is_integer($enddate) and $enddate > 0) {
    $and .= " and (a.date_issued < $enddate)";
}
if ($user_search != '') {
    $user_search = str_ireplace("'", "\'", $user_search);
    $and .= " and concat(firstname, ' ', lastname, ' ', username) like '%$user_search%' ";
    $and2 .= " and concat(firstname, ' ', lastname, ' ', username) like '%$user_search%' ";
}
$q = "select DISTINCT a.* , mdl_androgogic_activities.name as activity, p.name as provider, q.name as qualification, m.name as membership,
    CONCAT(mdl_user.firstname,' ',mdl_user.lastname) as user, mdl_user.username, pd.name as professional_designation  
from mdl_androgogic_training_history a 
LEFT JOIN mdl_androgogic_activities  on a.activity_id = mdl_androgogic_activities.id
LEFT JOIN mdl_androgogic_training_history_providers p on a.provider_id = p.id
LEFT JOIN mdl_androgogic_training_history_qualifications q on a.qualification_id = q.id
LEFT JOIN mdl_androgogic_training_history_membership m on a.membership_id = m.id
LEFT JOIN mdl_androgogic_training_history_professional_designations pd on a.professional_designation_id = pd.id 
LEFT JOIN mdl_user  on a.user_id = mdl_user.id
LEFT JOIN mdl_androgogic_training_history_dimensions on a.id = mdl_androgogic_training_history_dimensions.training_history_id
LEFT JOIN mdl_androgogic_dimensions on mdl_androgogic_training_history_dimensions.dimension_id = mdl_androgogic_dimensions.id
where 1 = 1 
$and 
order by $sort $dir";
if ($debug == 1) {
    echo '$query : ' . $q . '';
}
if ($download == '') {
    //get a page worth of records
    $results = $DB->get_records_sql($q, array(), $page * $perpage, $perpage);
} else {
    $results = $DB->get_records_sql($q);
}

if ($download == '') {
    //also get the total number we have of these
    $q = "SELECT COUNT(DISTINCT a.id)
from mdl_androgogic_training_history a 
LEFT JOIN mdl_androgogic_activities  on a.activity_id = mdl_androgogic_activities.id
LEFT JOIN mdl_androgogic_training_history_providers p on a.provider_id = p.id
LEFT JOIN mdl_androgogic_training_history_qualifications q on a.qualification_id = q.id
LEFT JOIN mdl_androgogic_training_history_membership m on a.membership_id = m.id
LEFT JOIN mdl_androgogic_training_history_professional_designations pd on a.professional_designation_id = pd.id 
LEFT JOIN mdl_user  on a.user_id = mdl_user.id
LEFT JOIN mdl_androgogic_training_history_dimensions on a.id = mdl_androgogic_training_history_dimensions.training_history_id
LEFT JOIN mdl_androgogic_dimensions on mdl_androgogic_training_history_dimensions.dimension_id = mdl_androgogic_dimensions.id
 where 1 = 1  $and";

    if ($debug == 1) {
        echo '$query : ' . $q . '<br>';
    }

    $result_count = $DB->get_field_sql($q);
    require_once('training_history_search_form.php');
    $mform = new training_history_search_form(null, array(
        'sort' => $sort, 'dir' => $dir, 'perpage' => $perpage, 'search' => $search, 'tab' => $currenttab, 
        'androgogic_activities_id' => $androgogic_activities_id, 'androgogic_dimensions_id' => $androgogic_dimensions_id, 
        'user_id' => $user_id, 'context' => $context, 'debug' => $debug, 'is_practice_viewer' => $is_practice_viewer, 
        'training_history_type' => $training_history_type,'membership_id' => $membership_id,
        'qualification_id' => $qualification_id,'professional_designation_id' => $qualification_id));
    $mform->display();
    echo '<table width="100%"><tr><td width="50%">';
    if (!empty($_POST)) {
        echo $result_count . ' ' . get_string('training_history_plural', 'block_androgogic_training_history') . " found" . '<br>';
    }
    echo '</td><td style="text-align:right;">';
    $base_url = 'index.php?tab=training_history_new&type=';
    $url_act = $base_url . 'activity';
    $url_qual = $base_url . 'qualification';
    $url_mem = $base_url . 'membership';
    echo get_string('training_history_new', 'block_androgogic_training_history') . ' <select onchange="this.options[this.selectedIndex].value && (window.location = this.options[this.selectedIndex].value);">
        <option value="">Choose type</option>
        <option value="' . $url_act . '">Activity</option>
        <option value="' . $url_mem . '">Membership</option>
        <option value="' . $url_qual . '">Qualification</option>
    </select>';
    //echo "<a href='index.php?tab=training_history_new'>" . get_string('training_history_new', 'block_androgogic_training_history') . "</a>";

    echo '</td></tr></table>';
}

//RESULTS
if (!$results) {
    if (!empty($_POST) and $download == '') {
        echo $OUTPUT->heading(get_string('noresults', 'block_androgogic_training_history', $search));
    }
} else {
    $table = new html_table();
    $table->head = array(
        $title_of_training,
        $activity,
        $qualification,
        $membership,
        $professional_designation,
        $date_issued,
        $date_expiry,
        $provider,
        $user,
        $username,
        $assessed,
        $assessment_code,
        $approved
    );
    if ($download == '') {
        $table->head[] = 'Action';
    }
    $table->width = "95%";
    foreach ($results as $result) {

//link for file
        $file_link = '';
        if (isset($result->filename)) {
            $file_link = new moodle_url("/pluginfile.php/{$result->contextid}/{$result->component}/{$result->filearea}/" . $result->itemid . '/' . $result->filename);
        }
        $user_link = new moodle_url("/user/profile.php?id=" . $result->user_id);
        $table_row = array(
            $result->title_of_training,
            $result->activity,
            $result->qualification,
            $result->membership,
            $result->professional_designation,
            $result->date_issued == 0 ? '' : date('d-m-Y', $result->date_issued),
            $result->date_expiry == 0 ? '' : date('d-m-Y', $result->date_expiry),
            $result->provider,
            html_writer::link($user_link, $result->user),
            $result->username,
            $result->assessed == 0 ? 'No' : 'Yes',
            $result->assessment_code,
            $result->approved == 0 ? 'No' : 'Yes'
        );
        if ($download == '') {
            $delete_link = "";
            $edit_link = "<a href='index.php?tab=training_history_edit&id=$result->id'>Edit</a> ";
            if (has_capability('block/androgogic_training_history:admin', $context)) {
                // make a real delete link
                $delete_link = "<a href='index.php?tab=training_history_delete&id=$result->id' onclick='return confirm(\"Are you sure you want to delete this item?\")'>Delete</a> ";
            }
            $table_row[] = $edit_link . $delete_link;
        }
        $table->data[] = $table_row;
    }
}
if (!empty($table)) {
    if ($download != '') {
        //export the table to whatever they asked for
        $report_name_lang = get_string('training_history_report', 'block_androgogic_training_history');
        block_androgogic_training_history_export_data($download, $table, $tab,$report_name_lang);
    } else {
        echo html_writer::table($table);
        $pagingbar = new paging_bar($result_count, $page, $perpage, $PAGE->url);
        $pagingbar->pagevar = 'page';
        echo $OUTPUT->render($pagingbar);
        block_androgogic_training_history_output_download_links($PAGE->url, 'cpd_report_heading');
    }
}
?>
