<?php

/**
 * Androgogic Training History Block: Create object
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     17/06/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 * Create new training_history
 *
 * */
global $OUTPUT;
//require_capability('block/androgogic_training_history:admin', $context);
require_once('training_history_edit_form.php');
$mform = new training_history_edit_form();
if ($data = $mform->get_data()) {
    $data->created_by = $USER->id;
    $data->date_created = date('Y-m-d H:i:s');
    if (!isset($data->assessed)) {
        $data->assessed = 0;
    } else if (block_androgogic_training_history_is_manager() && $data->user_id == $USER->id) {
        //can't approve your own th!
        $data->assessed = 0;
        echo $OUTPUT->notification(get_string('cantapproveowntraininghistory', 'block_androgogic_training_history'), 'notifyfailure');
    }
    if (block_androgogic_training_history_membership_qualification_is_unique($data)){
        $newid = $DB->insert_record('androgogic_training_history', $data);
        $data->id = $newid;
        block_androgogic_training_history_fix_phantom_file_id($data);
    }
    else{
        echo $OUTPUT->notification(get_string('existing_membership', 'block_androgogic_training_history'), 'notifyfailure');
        die;
    }

    // if it's a qual or a membership then advise admin user
    if(isset($data->membership_id) or isset($data->qualification_id)){
        block_androgogic_training_history_advise_admin($data);        
    }
        
    if ($config->approval_workflow && has_capability('block/androgogic_training_history:admin', $context)) {
        if ($data->approved == '1') {
            block_androgogic_training_history_advise_user($data);
            block_androgogic_training_history_complete_related_courses($data);
        }
    }
    block_androgogic_training_history_cpd_points_update($data);
    block_androgogic_training_history_dimensions_update($data);

    if ($config->do_cpe_hours) {
        block_androgogic_training_history_cpe_hours_update($data);
    }
    echo $OUTPUT->notification(get_string('datasubmitted', 'block_androgogic_training_history'), 'notifysuccess');
    echo $OUTPUT->action_link($PAGE->url, 'Create another item');
} else {
    echo $OUTPUT->heading(get_string('training_history_new', 'block_androgogic_training_history'));
    $mform->display();
}
?>
