<?php

/** 
 * Androgogic Training History Block: Create object 
 * 
 * @author      Daniel Morphett <dan@androgogic.com> 
 * @version     06/11/2014 
 * @copyright   2014+ Androgogic Pty Ltd <http://www.androgogic.com> 
 * 
 * Create new professional_designation
 *  
 **/

global $OUTPUT;
require_capability('block/androgogic_training_history:admin', $context);
require_once('professional_designation_edit_form.php');
$mform = new professional_designation_edit_form();
if ($data = $mform->get_data()){
$data->created_by = $USER->id;
$data->date_created = date('Y-m-d H:i:s');
$newid = $DB->insert_record('androgogic_training_history_professional_designations',$data);
echo $OUTPUT->notification(get_string('datasubmitted','block_androgogic_training_history'), 'notifysuccess');
echo $OUTPUT->action_link($PAGE->url, 'Create another item');
}
else{
echo $OUTPUT->heading(get_string('professional_designation_new', 'block_androgogic_training_history'));
$mform->display();
}

