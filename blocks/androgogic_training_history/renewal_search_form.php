<?php

/**
 * Androgogic Training History Block: Search form
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     17/06/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 * Provides search form for the object.
 * This is used by search page
 *
 * */
if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}
require_once($CFG->libdir . '/formslib.php');

class renewal_search_form extends moodleform {

    function definition() {
        global $DB,$USER;
        $mform = & $this->_form;
        foreach ($this->_customdata as $custom_key => $custom_value) {
            $$custom_key = $custom_value;
        }
        $mform->addElement('html', '<div>');
//search controls
        //$mform->addElement('text', 'search', get_string('training_history_search_instructions', 'block_androgogic_training_history'));
        // if admin they can search across users
        if (has_capability('block/androgogic_training_history:admin', $context) or $is_manager) {            
            $mform->addElement('text','user_search',get_string('cpd_report_user_search_instructions', 'block_androgogic_training_history'));
        }
        
        //types
//        $options = array(''=>'Any', 'activity'=>'Activity','membership'=>'Membership','qualification'=>'Qualification');
//        $select = $mform->addElement('select', 'training_history_type', get_string('training_history_type', 'block_androgogic_training_history'), $options);
        //activities
//        $dboptions = $DB->get_records_menu('androgogic_activities', array(), 'name', 'id,name');
//        unset($options);
//        $options[0] = 'Any';
//        foreach ($dboptions as $key => $value) {
//            $options[$key] = $value;
//        }
//        $select = $mform->addElement('select', 'androgogic_activities_id', get_string('activity', 'block_androgogic_training_history'), $options);
        //quals
//        $dboptions = $DB->get_records_menu('androgogic_training_history_qualifications', array(), 'name', 'id,name');
//        unset($options);
//        $options[0] = 'Any';
//        foreach ($dboptions as $key => $value) {
//            $options[$key] = $value;
//        }
//        $select = $mform->addElement('select', 'qualification_id', get_string('qualification', 'block_androgogic_training_history'), $options);
        //memberships
        $dboptions = $DB->get_records_menu('androgogic_training_history_membership', array(), 'name', 'id,name');
        unset($options);
        $options[0] = 'Any';
        foreach ($dboptions as $key => $value) {
            $options[$key] = $value;
        }
        $select = $mform->addElement('select', 'membership_id', get_string('membership', 'block_androgogic_training_history'), $options);
        
        //professional designations
        $dboptions = $DB->get_records_menu('androgogic_training_history_professional_designations', array(), 'name', 'id,name');
        unset($options);
        $options[0] = 'Any';
        foreach ($dboptions as $key => $value) {
            $options[$key] = $value;
        }
        $mform->addElement('select', 'professional_designation_id', get_string('professional_designation', 'block_androgogic_training_history'), $options);
        
//        $dboptions = $DB->get_records_menu('androgogic_dimensions', array(), 'name', 'id,name');
//        unset($options);
//        $options[0] = 'Any';
//        foreach ($dboptions as $key => $value) {
//            $options[$key] = $value;
//        }
//        $select = $mform->addElement('select', 'androgogic_dimensions_id', get_string('dimension', 'block_androgogic_training_history'), $options);

        $mform->addElement('date_selector', 'startdate', get_string('startdate', 'block_androgogic_training_history'), array('optional' => true));
        $mform->addElement('date_selector', 'enddate', get_string('enddate', 'block_androgogic_training_history'), array('optional' => true));
        
        //if the user is looking at their own records then default to their period            
        $is_manager = block_androgogic_training_history_is_manager();
        if (!has_capability('block/androgogic_training_history:admin', $context) and !$is_manager) {
            $user_org_pos = block_androgogic_training_history_get_user_org_pos($USER);
            if($user_org_pos){
                $org_pos_period = block_androgogic_training_history_get_org_pos_period(
                                    $user_org_pos->org_framework_id,$user_org_pos->pos_framework_id,
                                    $user_org_pos->pos_sortthread,$user_org_pos->org_sortthread,$USER->id);
                if($org_pos_period){
                    list($period_start_date,$period_end_date) = block_androgogic_training_history_get_cpd_period($USER, $org_pos_period);
                    $mform->setDefault('startdate', $period_start_date);
                    $mform->setDefault('enddate', $period_end_date);
                }
            }
        }
//hiddens
        $mform->addElement('hidden', 'tab', $tab);
        $mform->addElement('hidden', 'sort', $sort);
        $mform->addElement('hidden', 'dir', $dir);
        $mform->addElement('hidden', 'perpage', $perpage);
        $mform->addElement('hidden', 'debug', $debug);
//button
        $mform->addElement('submit', 'submit', 'Search');
        $mform->addElement('html', '</div>');
        
    }
}   