<?php

/** 
 * Androgogic Training History Block: Edit form
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     05/08/2014
 * @copyright   2014+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 * Provides edit form for the object.
 * This is used by both new and edit pages
 *
 **/

if (!defined('MOODLE_INTERNAL')) {
die('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}
require_once($CFG->libdir . '/formslib.php');
class org_pos_period_edit_form extends moodleform {
protected $org_pos_period;
function definition() {
global $USER,$courseid,$DB,$PAGE;
$mform =& $this->_form;
$context = context_system::instance();
if(isset($_REQUEST['id'])){
$q = "select DISTINCT a.* , mdl_org.fullname as org, mdl_pos.fullname as pos, mdl_course.fullname as scope_of_authority 
from mdl_androgogic_org_pos_periods a 
LEFT JOIN mdl_org  on a.org_id = mdl_org.id
LEFT JOIN mdl_pos  on a.pos_id = mdl_pos.id
LEFT JOIN mdl_course  on a.soa_course_id = mdl_course.id
where a.id = {$_REQUEST['id']} ";
$org_pos_period = $DB->get_record_sql($q);
}
else{
$org_pos_period = $this->_customdata['$org_pos_period']; // this contains the data of this form
}
$tab = 'org_pos_period_new'; // from whence we were called
if (!empty($org_pos_period->id)) {
$tab = 'org_pos_period_edit';
}
$mform->addElement('html','<div>');

//org_id
$options = $DB->get_records_menu('org',null,'fullname','id,fullname'); //sometimes this needs a manual tweak, if the 2nd col is mainly nulls
$mform->addElement('select', 'org_id', get_string('org','block_androgogic_training_history'), $options);
$mform->addRule('org_id', get_string('required'), 'required', null, 'server');

//pos_id
$options = $DB->get_records_menu('pos',null,'fullname','id,fullname'); //sometimes this needs a manual tweak, if the 2nd col is mainly nulls
$mform->addElement('select', 'pos_id', get_string('pos','block_androgogic_training_history'), $options);
$mform->addRule('pos_id', get_string('required'), 'required', null, 'server');

//soa_course_id
$config = get_config('block_androgogic_training_history');
if(isset($config->scope_of_authority_category_id)){
    $dboptions = $DB->get_records_menu('course',array('category'=>$config->scope_of_authority_category_id),'fullname','id,fullname');
    unset($options);
    $options[0] = 'Any';
    foreach($dboptions as $key=>$value){
    $options[$key] = $value;
    }
    $mform->addElement('select', 'soa_course_id', get_string('scope_of_authority','block_androgogic_training_history'), $options);
    $mform->addRule('soa_course_id', get_string('required'), 'required', null, 'server');
}
else{
    echo "Cannot display the scope of authority dropdown - please configure in 'block_androgogic_training_history' settings";
}



//period
$options = array('calendar'=>'Calendar year','financial'=>'Financial year'); 
$mform->addElement('select', 'period', get_string('period','block_androgogic_training_history'), $options);
$mform->addRule('period', get_string('required'), 'required', null, 'server');

// include the kas and points etc
block_androgogic_training_history_course_edit_form($mform);

//set values if we are in edit mode
if (!empty($org_pos_period->id) && isset($_GET['id'])) {
$mform->setConstant('org_id', $org_pos_period->org_id);
$mform->setConstant('pos_id', $org_pos_period->pos_id);
$mform->setConstant('period', $org_pos_period->period);
$mform->setConstant('soa_course_id', $org_pos_period->soa_course_id);
block_androgogic_training_history_load_target_data($mform,$org_pos_period);
}
//hiddens
$mform->addElement('hidden','tab',$tab);
if(isset($_REQUEST['id'])){
$mform->addElement('hidden','id',$_REQUEST['id']);
}
elseif(isset($id)){
$mform->addElement('hidden', 'id', $id);
}
$this->add_action_buttons(false);
$mform->addElement('html','</div>');
}
}
