<?php

/** 
 * Androgogic Training History Block: Edit form 
 * 
 * @author      Daniel Morphett <dan@androgogic.com> 
 * @version     22/09/2014 
 * @copyright   2014+ Androgogic Pty Ltd <http://www.androgogic.com> 
 * 
 * Provides edit form for the object.
 * This is used by both new and edit pages
 *  
 **/

if (!defined('MOODLE_INTERNAL')) {
die('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}
require_once($CFG->libdir . '/formslib.php');
class period_edit_form extends moodleform {
protected $period;
function definition() {
global $USER,$courseid,$DB,$PAGE;
$mform =& $this->_form;
$context = get_context_instance(CONTEXT_SYSTEM);
if(isset($_REQUEST['id'])){
$q = "select DISTINCT a.*  
from mdl_androgogic_training_history_periods a 
where a.id = {$_REQUEST['id']} ";
$period = $DB->get_record_sql($q);
}
else{
$period = $this->_customdata['$period']; // this contains the data of this form
}
$tab = 'period_new'; // from whence we were called
if (!empty($period->id)) {
$tab = 'period_edit';
}
$mform->addElement('html','<div>');

//type
$options = array('calendar'=>'Calendar year','financial'=>'Financial year'); 
$mform->addElement('select', 'type', get_string('period_type','block_androgogic_training_history'), $options);
$mform->addRule('type', get_string('required'), 'required', null, 'server');

$years = array();
for ($index = 2013; $index < 2045; $index++) {
   $years[$index] = $index; 
}
//start_year
$mform->addElement('select', 'start_year', get_string('start_year','block_androgogic_training_history'), $years);
$mform->addRule('start_year', get_string('required'), 'required', null, 'server');

//end_year
$mform->addElement('select', 'end_year', get_string('end_year','block_androgogic_training_history'), $years);
$mform->addRule('end_year', get_string('required'), 'required', null, 'server');

//is_current
$mform->addElement('selectyesno', 'is_current', get_string('is_current','block_androgogic_training_history'));
$mform->addRule('is_current', get_string('required'), 'required', null, 'server');
//set values if we are in edit mode
if (!empty($period->id) && isset($_GET['id'])) {
$mform->setConstant('type', $period->type);
$mform->setConstant('start_year', $period->start_year);
$mform->setConstant('end_year', $period->end_year);
$mform->setConstant('is_current', $period->is_current);
}
//hiddens
$mform->addElement('hidden','tab',$tab);
if(isset($_REQUEST['id'])){
$mform->addElement('hidden','id',$_REQUEST['id']);
}
elseif(isset($id)){
$mform->addElement('hidden', 'id', $id);
}
$this->add_action_buttons(false);
$mform->addElement('html','</div>');
}
}
