<?php

/**
 * Androgogic Training History Block: Lib
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     17/06/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 *
 * */
function block_androgogic_training_history_cron($source = 'cron') {
    global $CFG, $DB;
    $config = get_config('block_androgogic_training_history');
    $day = date('j');
    $month = date('n');
    $hour = date('G');
    mtrace('<pre>the hour:');
    var_dump($hour);
    mtrace('the androgogic_training_history run cron hours: ');
    var_dump($config->run_cron_hours);
    mtrace('</pre>');
    $run_cron = false;
    if($source == 'test cron'){
        $run_cron = true;
    }
    else{
        if (isset($config->run_cron_hours) && in_array($hour, explode(';', $config->run_cron_hours))) {
            $run_cron = true;
            //special case for change expired periods
            if($day == 1){
                if($month == 1 or $month == 7){
                    mtrace( '<br>before block_androgogic_training_history_change_expired_periods:<br>');
                    flush();
                    block_androgogic_training_history_change_expired_periods();        
                }
            }
        }
    }
    
    if ($run_cron) {
        mtrace("\n\n");
        mtrace("Running cron for androgogic_training_history" . "<br><br>");
        $time = date("d-m-Y h-i-s", time());
        echo $time . '<br/>';
        echo '<br>before fix_files:<br>';
        block_androgogic_training_history_fix_files();
        echo '<br>before notify_expiring_memberships:<br>';
        flush();
        if($config->send_membership_emails){
            $periods = array('month','week','day');
            foreach ($periods as $period){
                block_androgogic_training_history_notify_expiring_memberships($period);
            }
        }
        echo '<br>before process_expired_memberships:<br>';
        block_androgogic_training_history_process_expired_memberships();
        echo '<br>before detect_ata_revoke_victims:<br>';
        block_androgogic_training_history_detect_ata_revoke_victims();
        $time = date("d-m-Y h-i-s", time());
        echo 'Finish time' . $time . '<br/>';
    } else {
        mtrace("\n\n");
        mtrace("not one of the run cron hours for androgogic_training_history");
    }
}
// change any current period that has expired and make a new one
function block_androgogic_training_history_change_expired_periods() {
    global $DB;
    $sql = "SELECT * FROM mdl_androgogic_training_history_periods where is_current = 1";
    $results = $DB->get_records_sql($sql);    
    foreach($results as $period){
        $do_change = false;
        if($period->type == 'financial' and $period->end_year == date('Y')){
            $do_change = true;
        }
        elseif($period->type == 'calendar' and $period->end_year < date('Y')){
            $do_change = true;
        }
        if($do_change){
            $period->is_current = 0;
            $DB->update_record('androgogic_training_history_periods',$period);
            $new_period = stdClass();
            $new_period->start_year = $period->start_year+1;
            $new_period->end_year = $period->end_year+1;
            $new_period->is_current = 1;
            $new_period->type = $period->type;
            $new_period->date_modified = time();
            $DB->insert_record('androgogic_training_history_periods',$new_period);
        }
    }
}
// when a membership expires, we have to do some revoking.
// the related course has to be marked incomplete, as does any course related to a professional designation 
// that is related to the membership
function block_androgogic_training_history_process_expired_memberships() {
    global $DB,$CFG;
    require_once($CFG->dirroot . '/completion/completion_completion.php');
    $config = get_config('block_androgogic_training_history');
    $ata_cat_id = $config->ata_category_id;
    
    $now = time();
    $sql = "select th.*, m.name as membership_name, m.course_id from mdl_androgogic_training_history th
        inner join mdl_androgogic_training_history_membership m on th.membership_id = m.id
        where date_expiry < $now
        and th.date_expiry_processed = 0";
    $results = $DB->get_records_sql($sql);
    foreach ($results as $result) {
        //do what we have to do
        //have we got an existing completion for them?
        $sql = "SELECT * FROM mdl_course_completions WHERE userid = $result->user_id AND course = $result->course_id and status IN(" . COMPLETION_STATUS_COMPLETE . "," . COMPLETION_STATUS_COMPLETEVIARPL . ")";
        $existing_completion = $DB->get_record_sql($sql);
        if($existing_completion){
            //if so mark them as not yet started
            $existing_completion->status = COMPLETION_STATUS_NOTYETSTARTED;
            $DB->update_record('course_completions',$existing_completion);
        }
        
        //does the membership have a professional designation that has a related course? if so then revoke any course completion on this
        $sql = "SELECT cc.* FROM mdl_androgogic_training_history_professional_designations pd
            inner join mdl_androgogic_training_history_membership m on pd.membership_id = m.id
            inner join mdl_course_completions cc on pd.course_id = cc.course
            where membership_id = $result->membership_id and cc.userid = $result->user_id ";

        $pd_courses = $DB->get_records_sql($sql);
        $pd_course_ids = array();
        foreach ($pd_courses as $pd_course) {
            //mark them as not yet started
            $pd_course->status = COMPLETION_STATUS_NOTYETSTARTED;
            $DB->update_record('course_completions',$pd_course);
            $pd_course_ids[] = $pd_course->course;
        }
        
        //is the course related to competencies that have been scaled to the level of 'Competent'? If so, mark them down
        $sql = "select cr.*,v.scaleid 
            from mdl_dp_plan p
            inner join mdl_dp_plan_course_assign c on p.id = c.planid
            inner join mdl_dp_plan_competency_assign ca on p.id = ca.planid
            inner join mdl_comp_record cr on ca.competencyid = cr.competencyid and p.userid = cr.userid
            inner join mdl_comp_scale_values v on cr.proficiency = v.id
            where p.userid = $result->user_id and courseid = $result->course_id
            and v.name = 'Competent'";

        $comp_records = $DB->get_records_sql($sql);
        if($comp_records){
            foreach ($comp_record as $comp_record) {
                //get the bottom proficiency from the same scale 
                $sql = "select name 
                    from mdl_comp_scale_values 
                    where scaleid = $comp_record->scaleid
                    and sortorder = (
                        select max(sortorder) 
                        from mdl_comp_scale_values 
                        where scaleid = $comp_record->scaleid
                    )";
                $comp_record->proficiency = $DB->get_field_sql($sql);
                $DB->update_record('comp_record',$comp_record);

            }
        }
        // we might also need to change completion status for user in any progs that house the course, so get these
        $expired_courses = $pd_course_ids;
        $expired_courses[] = $result->course_id;
        $expired_course_list = implode(',',$expired_courses);
        $sql = "select distinct p.* from mdl_prog p
            inner join mdl_prog_courseset cs on p.id = cs.programid
            inner join mdl_prog_courseset_course csc on cs.id = csc.coursesetid 
            WHERE csc.courseid IN ($expired_course_list)";
         
        $progs = $DB->get_records_sql($sql);
        
        foreach ($progs as $prog) {
            // get the ATA courses in the prog
            $sql = "SELECT distinct c.* 
                    from mdl_course c
                    inner join mdl_course_categories cc on c.category = cc.id
                    inner join mdl_prog_courseset_course pcs on c.id = pcs.courseid
                    inner join mdl_prog_courseset pc on pcs.coursesetid = pc.id
                    inner join mdl_prog p on pc.programid = p.id
                    WHERE p.id = $prog->id AND (cc.path LIKE '%/$ata_cat_id' OR cc.path LIKE '%/$ata_cat_id/%')"; // 
                     

            $courses = $DB->get_records_sql($sql);

            foreach ($courses as $course) {
                block_androgogic_training_history_revoke_ata_course($result->user_id,$course->id);
                //get the comp record/s so as to insert it into the history
                $sql = "SELECT cr.*, v.scaleid  
                FROM mdl_comp_record cr
                inner join mdl_comp_scale_values v on cr.proficiency = v.id
                WHERE userid = $result->user_id
                AND competencyid IN (SELECT c.id
                FROM mdl_comp_criteria cc 
                INNER JOIN mdl_comp c ON cc.competencyid = c.id 
                WHERE cc.iteminstance = $course->id)";
                $comp_records = $DB->get_records_sql($sql);
                foreach ($comp_records as $comp_record){
                    $sql = "SELECT id from mdl_comp_scale_values where name = 'Not yet competent' and scaleid = $comp_record->scaleid";
                    $not_yet_competent = $DB->get_field_sql($sql);    
    
                    //update the proficiency on the record
                    $comp_record->proficiency = $not_yet_competent; 
                    $comp_record->timemodified = time();                    
                    $DB->update_record('comp_record',$comp_record);
                    //insert it into history table
                    unset($comp_record->id);
                    $comp_record->usermodified = 2;
                    $DB->insert_record('comp_record_history',$comp_record);
                }
            }
            //has the user got a prog complete record? if so, change its status to incomplete. The totara prog cron will check it later and change it back if need be
            $sql = "SELECT * FROM mdl_prog_completion WHERE programid = $prog->id AND userid = $result->user_id and status = " . STATUS_PROGRAM_COMPLETE;
            $prog_completion = $DB->get_record_sql($sql);
            if($prog_completion){
                $prog_completion->status = STATUS_PROGRAM_INCOMPLETE;
                $DB->update_record('prog_completion',$prog_completion);
            }
        }                    
        
        //update processed status
        $result->date_expiry_processed = time();
        $DB->update_record('androgogic_training_history',$result);
    }
}
//The rule for ATA courses is that they can only be attempted and then completed, 
//if all of their dependencies have first been completed
//Here we want to find any ata course completion that is no longer supported by its dependent courses.
//Then for each of these, revoke their completion, change the competency scale value, unenrol them 
//and remove their program completion if that exists
function block_androgogic_training_history_detect_ata_revoke_victims() {
    global $DB;
    $config = get_config('block_androgogic_training_history');
    $ata_cat_id = $config->ata_category_id;
    //get prog completions
    $sql = "select c.* from mdl_prog_completion c inner join mdl_prog p on c.programid = p.id where status = 1";

    $program_completions = $DB->get_records_sql($sql);

    foreach ($program_completions as $program_completion) {
        $program = new program($program_completion->programid);
        $userid = $program_completion->userid;
        // Get the program content.
        $program_content = $program->get_content();

        if ($program->certifid) {
            // If this is a certification program get course sets for groups on the path the user is on.
            $certificationpath = get_certification_path_user($program->certifid, $userid);
            $courseset_groups = $program_content->get_courseset_groups($certificationpath);
        } else {
            // If standard program get the courseset groups (just one path).
            $courseset_groups = $program_content->get_courseset_groups(CERTIFPATH_STD);
        }

        // First check if the program is already marked as incomplete for this user and mark it so, then move on.
        if (!$program->is_program_complete($userid)) {
            $program_completion->status = 0;
            $DB->update_record('prog_completion',$program_completion);
            continue;
        }

        $courseset_group_completed = false;
        $previous_courseset_group_completed = false;

        // Go through the course set groups to determine the user's completion status.
        foreach ($courseset_groups as $courseset_group) {

            $courseset_group_completed = false;

            // Check if the user has completed any of the course sets in the group - this constitutes completion of the group.
            foreach ($courseset_group as $courseset) {

                // Carry out a check to see if the course set should be marked as complete and mark it as complete if so.
                if ($courseset->check_courseset_complete($userid)) {
                    $courseset_group_completed = true;
                    $previous_courseset_group_completed = true;                    
                }
                else{
                    $courseset_group_completed = false;
                    break;
                }
            }

            // If the user has not completed the course group the program is not complete.
            if (!$courseset_group_completed) {
                $completionsettings = array(
                    'status'        => STATUS_PROGRAM_INCOMPLETE,
                    'timecompleted' => 0
                );
                $program->update_program_complete($userid, $completionsettings);
                //now get the ATA course, and give it the revoke treatment
                $sql = "select c.* from mdl_prog_courseset_course cc 
                    inner join mdl_prog_courseset pcs on cc.coursesetid = pcs.id 
                    inner join mdl_course c on cc.courseid = c.id 
                    inner join mdl_course_categories cat on c.category = cat.id 
                    where (cat.path LIKE '%/$ata_cat_id' OR cat.path LIKE '%/$ata_cat_id/%') 
                    and programid = $program->id";

                $ata_course = $DB->get_record_sql($sql);

                if ($ata_course) {
                    block_androgogic_training_history_revoke_ata_course($userid,$ata_course->id);
                }
            }
        }
    }
}

function block_androgogic_training_history_revoke_ata_course($userid,$courseid) {
    global $DB;
    //unenrol the user from the course    
    if (!$instance = $DB->get_record('enrol', array('enrol'=>'manual', 'courseid'=>$courseid, 'status'=>ENROL_INSTANCE_ENABLED))) {
            throw new Exception('manual enrolment is not enabled');
    }

    // check if already enrolled
    $enrolment = $DB->get_record('user_enrolments', array('enrolid'=>$instance->id, 'userid'=>$userid));	
    if ($enrolment) {
        // we use only manual enrol plugin here
        if (!$plugin = enrol_get_plugin('manual')) {
                throw new Exception('failed to get manual enrolment plugin');
        }
        // unenrol user from the course.
        $plugin->unenrol_user($instance, $userid);
    }	
    
    //set the course to be incomplete for the user
    block_androgogic_training_history_set_course_incomplete($userid,$courseid);
    
    //mark down the competency for the user in the course
    block_androgogic_training_history_set_competency_to_lowest($userid,$courseid);
    
    return;
}
function block_androgogic_training_history_set_course_incomplete($userid,$courseid) {
    global $DB;
    //have we got an existing completion for them?
    $sql = "SELECT * FROM mdl_course_completions WHERE userid = $userid AND course = $courseid";
    $existing_completion = $DB->get_record_sql($sql);
    if($existing_completion){
        //if so mark them as not yet started
        $existing_completion->status = COMPLETION_STATUS_NOTYETSTARTED;
        $DB->update_record('course_completions',$existing_completion);

    }    
}
function block_androgogic_training_history_set_competency_to_lowest($userid,$courseid) {
    global $DB;
    //mark down their scale value
    $sql = "select competencyid FROM
            mdl_comp_criteria cc
        INNER JOIN
            mdl_comp c
         ON cc.competencyid = c.id
        INNER JOIN
            mdl_comp_framework  f
         ON f.id = c.frameworkid
        LEFT JOIN
            mdl_modules m
         ON cc.itemtype = 'activitycompletion'
        AND m.name = cc.itemmodule
        LEFT JOIN
            mdl_course_modules cm
         ON cc.itemtype = 'activitycompletion'
        AND cm.instance = cc.iteminstance
        AND cm.module = m.id
        WHERE
        (
                cc.itemtype <> 'activitycompletion'
            AND cc.iteminstance = $courseid
        )
        OR
        (
                cc.itemtype = 'activitycompletion'
            AND cm.course = $courseid
        )
    ";

    $competency_id = $DB->get_field_sql($sql);

    $sql = "SELECT * FROM mdl_comp_record WHERE competencyid = $competency_id AND userid = $userid";

    $comp_record = $DB->get_record_sql($sql);

    if($comp_record){
        //change the proficiency to the lowest one on the scale
        $sql = "
        select csv.id from mdl_comp_scale_values csv 
        inner join mdl_comp_scale cs on csv.scaleid = cs.id
        where cs.name like 'ATA comp%'
        order by sortorder desc
        limit 1";

        $proficiency_id = $DB->get_field_sql($sql);                    

        $comp_record->proficiency = $proficiency_id;
        $DB->update_record('comp_record',$comp_record);
    }
}
function block_androgogic_training_history_notify_expiring_memberships($period){
    global $DB;    
    $startdate = strtotime( "-1 $period", time() );
    $enddate = strtotime( "-1 day", $startdate );
    $sql = "select th.*, m.name as membership_name from mdl_androgogic_training_history th
        inner join mdl_androgogic_training_history_membership m on th.membership_id = m.id
        left join mdl_androgogic_training_history_email_log l on th.user_id = l.user_id and th.membership_id = l.membership_id 
            and l.period = '$period'                           
        where date_expiry between $startdate and $enddate
        and l.id is null";

    $results = $DB->get_records_sql($sql);
    foreach ($results as $result) {
        $user = $DB->get_record('user',array('id'=>$result->user_id));
        $from = get_admin();
        $subject = get_string('membership_expiry_reminder_subject','block_androgogic_training_history');
        $a = new stdClass();
        $a->expiry_date = date('d m Y',$result->date_expiry);
        $a->fullname = fullname($user);
        $a->firstname = $user->firstname;
        $a->membership_name = $result->membership_name;
        $messagetext = get_string('membership_expiry_reminder_body','block_androgogic_training_history',$a);
        //send reminder
        email_to_user($user, $from, $subject, $messagetext);
        //add a row to the log
        $log_entry = new stdClass();
        $log_entry->user_id = $result->user_id;
        $log_entry->period = $period;
        $log_entry->membership_id = $result->membership_id;
        $log_entry->date_sent = time();
        $DB->insert_record('androgogic_training_history_email_log',$log_entry);
    }
}
function block_androgogic_training_history_create_user_period($data) {
    global $DB, $USER;
    $data->created_by = $USER->id;
    $data->date_created = date('Y-m-d H:i:s');
    $period_id = block_androgogic_training_history_get_period_id($data);
    if($period_id  > 0){
        $data->period_id = $period_id;
        $newid = $DB->insert_record('androgogic_user_period',$data);
        return $newid;
    }
    else{
        return false;
    }
}
// get the current period that matches the data
function block_androgogic_training_history_get_period_id($data) {
    global $DB;
    //enddate is immutable
    $end_year = date('Y',$data->enddate);
    $end_month = date('n',$data->enddate);
    if ($end_month == 6){
        $period_type = 'financial';
    }
    else{
        $period_type = 'calendar';
    }
    $sql = "SELECT * FROM mdl_androgogic_training_history_periods where is_current = 1 and type = '$period_type' and end_year = $end_year";
    $result = $DB->get_record_sql($sql);
    if($result){
        return $result->id;
    }
    else{
        return 0;
    }
}
function block_androgogic_training_history_get_prorata($period_start_date,$period_end_date) {
    // plus one day because we are going from e.g. 1 Jan 00:00:00 to 31 Dec 00:00:00, which ends up being 364 days
    $day = 60*60*24;
    $prorata_days = ($period_end_date - $period_start_date + $day)/$day; 
    $prorata = $prorata_days/365;
    return $prorata;
}
// this function wraps up the complexity of the getting of the cpd points and cpe hours, for a bit more happiness on the consuming end
function block_androgogic_training_history_get_points_and_hours($user_id,$ands,$period_start_date,$period_end_date) {
    global $DB;
    $result = new stdClass();
    $ands_elements = array('and_training_history', 'and_training_history_date','and_course_completion_date');
    foreach($ands_elements as $ands_element){
        if(isset($ands->$ands_element)){
            $$ands_element = $ands->$ands_element;
            //var_dump($$ands_element);
        }
        else{
            $$ands_element = "";
        }
    } 
    if($and_training_history_date == ""){
        $and_training_history_date = " AND th.date_issued between '$period_start_date' AND '$period_end_date'";
    }
    if($and_course_completion_date == ""){
        $and_course_completion_date = " AND cc.timecompleted between '$period_start_date' AND '$period_end_date'";
    }
    //any KAs for this one?
    //moodle insists on treating first column as unique id, hence the rownum stuff here:
    $sql = "SELECT @rownum:=@rownum+1 id, competency_id, fullname, assessed, cpd_points FROM(
        SELECT competency_id, c.fullname, assessed,  sum(cpd_points) as cpd_points
        from mdl_androgogic_training_history_competencies_cpd_points cpd 
        inner join mdl_comp c on cpd.competency_id = c.id 
        inner join mdl_androgogic_training_history th on cpd.training_history_id = th.id
        where cpd.deleted = 0 AND user_id = $user_id
        $and_training_history
        $and_training_history_date
        group by assessed, competency_id, c.fullname
        union
        SELECT competency_id, c.fullname, 1, sum(cpd_points)
        FROM mdl_course_completions cc
        INNER JOIN mdl_course crs ON cc.course = crs.id
        INNER JOIN mdl_androgogic_training_history_course_competencies_cpd_points cpd on crs.id = cpd.course_id
        inner join mdl_comp c on cpd.competency_id = c.id
        INNER JOIN mdl_user ON cc.userid = mdl_user.id
        WHERE cpd.deleted = 0 AND STATUS >= 50
        and cc.userid = $user_id
        $and_course_completion_date
        group by competency_id, c.fullname
        ) b , (SELECT @rownum:=0) r "; //TODO: add the filters in 

    $result->cpd_knowledge_areas = $DB->get_records_sql($sql);

    //also we want to get the cpe knowledge areas and their cpe hours
    $sql = "SELECT @rownum:=@rownum+1 id, competency_id, fullname, assessed, cpe_hours FROM(
        SELECT competency_id, c.fullname, assessed,  sum(cpe_hours) as cpe_hours
        from mdl_androgogic_training_history_competencies_cpe_hours cpe 
        inner join mdl_comp c on cpe.competency_id = c.id 
        inner join mdl_androgogic_training_history th on cpe.training_history_id = th.id
        where cpe.deleted = 0 AND user_id = $user_id
        $and_training_history
        $and_training_history_date
        group by assessed, competency_id, c.fullname
        union
        SELECT competency_id, c.fullname, 1, sum(cpe_hours)
        FROM mdl_course_completions cc
        INNER JOIN mdl_course crs ON cc.course = crs.id
        INNER JOIN mdl_androgogic_training_history_course_competencies_cpe_hours cpe on crs.id = cpe.course_id
        inner join mdl_comp c on cpe.competency_id = c.id
        INNER JOIN mdl_user ON cc.userid = mdl_user.id
        WHERE cpe.deleted = 0 AND STATUS >= 50
        $and_course_completion_date
        and cc.userid = $user_id
        group by competency_id, c.fullname
        ) b , (SELECT @rownum:=0) r "; 

    $result->cpe_knowledge_areas = $DB->get_records_sql($sql);
    
    //REQUESTED BY JARRAD TO DISCONTINUE THIS:
//    //let's see if we have to discount some of their unassessed points - rule is 50% unassessed only
//    $total_assessed_points = 0;
//    $total_unassessed_points = 0;    
//    foreach($result->cpd_knowledge_areas as $points_earned){
//        if($points_earned->assessed == 0){                            
//            $total_unassessed_points += $points_earned->cpd_points;                            
//        }
//        else{
//            $total_assessed_points += $points_earned->cpd_points;
//        }
//    }
//    $total_completed_points = $total_unassessed_points + $total_assessed_points;
//    if($total_unassessed_points > $total_assessed_points){
//        // then we are discounting...
//        $discount = $total_assessed_points/$total_unassessed_points;
//        //set unassessed to be the same as assessed
//        $total_unassessed_points = $total_assessed_points;
//        //discount the individual unassessed ones
//        for ($index = 1; $index <= count($result->cpd_knowledge_areas); $index++) {
//            if($result->cpd_knowledge_areas[$index]->assessed == 0){ 
//                $result->cpd_knowledge_areas[$index]->cpd_points = $discount * $result->cpd_knowledge_areas[$index]->cpd_points;
//            }
//        }
//    }
    
    //now let's get some totals
    $total_completed_hours = 0;
    //reset $total_completed_points, because it may have changed
    $total_completed_points = 0;
    foreach($result->cpd_knowledge_areas as $user_knowledge_area){
        $total_completed_points += $user_knowledge_area->cpd_points; 
    }
    $result->total_completed_points = $total_completed_points;
    foreach($result->cpe_knowledge_areas as $user_knowledge_area){
        if(isset($user_knowledge_area->cpe_hours)){
            $total_completed_hours += $user_knowledge_area->cpe_hours;
        }
    } 
    $result->total_completed_hours = $total_completed_hours;
    return $result;
}
// this function wraps up the complexity of the getting of the targets, for a bit more happiness on the consuming end
function block_androgogic_training_history_get_targets($user,$org_pos_period,$and_clause,$period_start_date,$period_end_date) {
    global $DB;
    $return = new stdClass();
    
    //points targets and overrides
    $sql = "select ct.*, c.fullname, ifnull(o.points,0) as override_points
    from mdl_androgogic_training_history_cpd_targets ct
    inner join mdl_comp c on ct.competency_id = c.id
    left join mdl_androgogic_user_points_override o on ct.competency_id = o.competency_id and o.user_id = $user->id and o.deleted = 0
    where ct.deleted = 0 AND org_pos_period_id = $org_pos_period->id "
            . " $and_clause";
    $cpd_targets = $DB->get_records_sql($sql);     
    
    //hours targets and overrides
    $sql = "select ct.*, c.fullname, ifnull(o.hours,0) as override_hours 
    from mdl_androgogic_training_history_cpe_targets ct
    inner join mdl_comp c on ct.competency_id = c.id
    left join mdl_androgogic_user_hours_override o on ct.competency_id = o.competency_id and o.user_id = $user->id  and o.deleted = 0
    where ct.deleted = 0 AND org_pos_period_id = $org_pos_period->id "
            . " $and_clause";
    $cpe_targets = $DB->get_records_sql($sql);
    
    //unallocated points override
    $sql = "SELECT * FROM mdl_androgogic_user_points_override WHERE user_id = $user->id AND competency_id = 0  and deleted = 0";
    $unallocated_points_override = $DB->get_record_sql($sql);
    
    //unallocated points for each cpd target
    $unallocated_portion = 0;
    if($unallocated_points_override and count($cpd_targets)){
        $number_cpd_targets = count($cpd_targets);
        $unallocated_portion = $unallocated_points_override->points/$number_cpd_targets;
    }
    //get prorata
    $prorata = block_androgogic_training_history_get_prorata($period_start_date,$period_end_date);
    
    //apply points overrides
    $total_cpd_target = 0;
    foreach($cpd_targets as $cpd_target){
        $override_result = $cpd_target->cpd_target + $cpd_target->override_points;
        // if the override result is less than zero then it is an illegal one: protect against this
        if($override_result < 0){
            $override_result = 0;
        } 
        $cpd_target->cpd_target = round((($override_result + $unallocated_portion) * $prorata),2);
        $total_cpd_target += $cpd_target->cpd_target;
    }
    // apply hours overrides
    $total_cpe_target = 0;
    foreach($cpe_targets as $cpe_target){
        $override_result = $cpe_target->cpe_target + $cpe_target->override_hours;
        // if the override result is less than zero then it is an illegal one: protect against this
        if($override_result < 0){
            $override_result = 0;
        } 
        $cpe_target->cpe_target = round(($override_result * $prorata),2);
        $total_cpe_target += $cpe_target->cpe_target;        
    }
    
    //bundle it all up and send it back
    $return->cpd_targets = $cpd_targets;
    $return->cpe_targets = $cpe_targets;
    $return->total_cpd_target = $total_cpd_target;  
    $return->total_cpe_target = $total_cpe_target;  
    return $return;
}
function block_androgogic_training_history_save_upload_over_existing($form_data, $block_name, $table_name, $foreign_key_name) {
    global $DB;
    $form_elements = $form_data->_form->_elements;
    //reformat for ease of use
    $data = new stdClass();
    foreach ($form_elements as $element) {
//       echo '<pre> the $element:';
//       var_dump($element);
//       echo '</pre>';
        if (isset($element->_attributes['name']) && isset($element->_attributes['value'])) {
            $data->{$element->_attributes['name']} = $element->_attributes['value'];
        } else {
            //echo 'missing attribute for this element: ' . print_r($element);
        }
    }
//   echo '<pre> the $data:';
//   var_dump($data);
//   echo '</pre>';
    //change the reference to the itemid in the block table
    $sql = "update mdl_$table_name set $foreign_key_name = " . $data->itemid . " where id = " . $data->objectid;
    $DB->execute($sql);
    if(isset($data->itemid) and $data->itemid > 0){
        //change the component and filearea of the file entry
        $sql = "UPDATE mdl_files
            SET filearea = 'content',component='$block_name'
            WHERE itemid = '$data->itemid'";
        $DB->execute($sql);
    }
}

function block_androgogic_training_history_pluginfile($course, $birecord, $context, $filearea, $args, $forcedownload) {
    global $DB;
    $sql = 'select * from mdl_files 
            where itemid = ' . $args[0] . ' 
                and filename = \'' . $args[1] . '\'
                and contextid = ' . $context->id . ' 
                and filearea = \'' . $filearea . '\'';
    $file = $DB->get_record_sql($sql);
    if (!$file) {
        send_file_not_found();
    } else {
        $fs = get_file_storage();
        $stored_file = $fs->get_file_by_hash($file->pathnamehash);
    }
    $forcedownload = true;

    session_get_instance()->write_close();
    send_stored_file($stored_file, 60 * 60, 0, $forcedownload);
}

/**
 * the user (or an admin) has put in a new training history record. let their manager know this by email
 * @param object $training_history_record
 */
function block_androgogic_training_history_advise_admin($training_history_record) {
    global $DB, $CFG, $OUTPUT;
    $th_user = $DB->get_record('user', array('id' => $training_history_record->user_id));
    $config = get_config('block_androgogic_training_history');
    if(isset($config->approval_email)){
        $admin = get_admin();
        $admin->email = $config->approval_email;
        $subject = get_string('email_to_manager_subject', 'block_androgogic_training_history');
        $a = new stdClass();
        $a->url = $CFG->wwwroot . '/blocks/androgogic_training_history/index.php?tab=training_history_edit&id=' . $training_history_record->id;
        $body = get_string('email_to_manager_body', 'block_androgogic_training_history', $a);
        $message = "Sending email to admin to advise re edited training history. Admin email: $admin->email TH user: $th_user->firstname $th_user->lastname Email body: $body";
        error_log($message);
        email_to_user($admin, $th_user, $subject, $body, $body);
        $message = "After email send";
        error_log($message);
    }
    else{
        echo $OUTPUT->notification(get_string('no_approval_email','block_androgogic_training_history'),'notifyfailure');
    }
}
// a user can have only one record for a given membership or qual
function block_androgogic_training_history_membership_qualification_is_unique($data){
    global $DB;
    if(isset($data->activity_id)){
        return true;
    }
    else if (isset($data->membership_id)){
        $sql = "SELECT * from mdl_androgogic_training_history where user_id = $data->user_id and membership_id = $data->membership_id ";
        if(isset($data->professional_designation_id)){
            $sql .= " and professional_designation_id = $data->professional_designation_id";
        }
    }
    else{
        $sql = "SELECT * from mdl_androgogic_training_history where user_id = $data->user_id and qualification_id = $data->qualification_id ";
    }
    if(isset($data->id)){
        $sql .= " AND id != $data->id";
    }
    if($DB->get_record_sql($sql)) {
        return false;
    }
    return true;
}
function block_androgogic_training_history_record_unique($record,$type) {
    global $DB;
    if($type == 'qualification'){
        $sql = "SELECT * from mdl_androgogic_training_history_{$type}s where name = '$record->name' and course_id = $record->course_id";
    }
    else{
        $sql = "SELECT * from mdl_androgogic_training_history_{$type}s where name = '$record->name' ";
    }
    if(isset($record->id)){
        //means it is an update, so exclude the id
        $sql .= " and id != $record->id";
    }
    if($DB->get_record_sql($sql)) {
        return false;
    }
    return true;
}

/**
 * the admin has approved the training history record. let the user know this by email
 * @param object $training_history_record
 */
function block_androgogic_training_history_advise_user($training_history_record) {
    global $DB, $CFG, $OUTPUT;
    $config = get_config('block_androgogic_training_history');
    $th_user = $DB->get_record('user', array('id' => $training_history_record->user_id));
    $admin = get_admin();
    $admin->email = 'learning@amp.com.au';
    $subject = get_string('email_to_user_subject', 'block_androgogic_training_history');
    $a = new stdClass();
    $a->firstname = $th_user->firstname;
    $a->url = $CFG->wwwroot . '/blocks/androgogic_training_history/index.php?tab=training_history_edit&id=' . $training_history_record->id;
    $body = get_string('email_to_user_body', 'block_androgogic_training_history', $a);
    email_to_user($th_user, $admin, $subject, $body, $body);
     
}
function block_androgogic_training_history_complete_related_courses($training_history_record) {
    global $DB, $CFG, $OUTPUT;
    //get any related courses
    if($training_history_record->qualification_id){
        $q = "SELECT c.* FROM mdl_course c "
                . "INNER JOIN mdl_androgogic_training_history_qualifications q ON c.id = q.course_id "
                . "WHERE q.id = $training_history_record->qualification_id ";
    }
    else if($training_history_record->membership_id){
        $q = "SELECT c.* FROM mdl_course c "
                . "INNER JOIN mdl_androgogic_training_history_membership q ON c.id = q.course_id "
                . "WHERE q.id = $training_history_record->membership_id ";
    }
    if(isset($q)){
        $courses = $DB->get_records_sql($q);
        if ($courses) {
            foreach($courses as $course){
                $completion = new completion_completion(array('userid' => $training_history_record->user_id, 'course' => $course->id));
                $completion->mark_complete();
            }
        } 
    }
}
/**
 * is the user a manager of others?
 */
function block_androgogic_training_history_is_manager() {
    global $USER, $DB;
    $is_manager = false;
    if ($DB->get_records('pos_assignment', array('managerid' => $USER->id))) {
        $is_manager = true;
    }
    return $is_manager;
}
function block_androgogic_training_history_cpd_points_update($data) {
    global $DB;
    // did we get any cpd points?
    //out with the old
    $DB->delete_records('androgogic_training_history_competencies_cpd_points', array('training_history_id' => $data->id));
    //in with the new   
    foreach ($data as $key => $value) {
        if (substr($key, 0, 10) == 'cpd_points') {
            $th_comp_cpd = new stdClass();
            $th_comp_cpd->competency_id = substr($key, 11);
            $th_comp_cpd->cpd_points = $value;
            $th_comp_cpd->training_history_id = $data->id;
            $th_comp_cpd->date_modified = date('Y-m-d H:i:s');
            if ($value > 0) {
                $DB->insert_record('androgogic_training_history_competencies_cpd_points', $th_comp_cpd);
            }
        }
    }
}

function block_androgogic_training_history_dimensions_update($data) {
    global $DB;
    $DB->delete_records('androgogic_training_history_dimensions',array('training_history_id'=>$data->id));
    //many to many relationship: androgogic_training_history_dimensions
    if(isset($data->dimension_id)){
        foreach($data->dimension_id as $dimension_id){
            $insert = new stdClass();
            $insert->training_history_id = $data->id;
            $insert->dimension_id = $dimension_id;
            $DB->insert_record('androgogic_training_history_dimensions', $insert);
        }
    }
}

function block_androgogic_training_history_cpe_hours_update($data) {
    global $DB;
    //any cpe hours items?
    //out with the old
    $DB->delete_records('androgogic_training_history_competencies_cpe_hours', array('training_history_id' => $data->id));
    //in with the new   
    foreach ($data as $key => $value) {
        if (substr($key, 0, 9) == 'cpe_hours') {
            $th_comp_cpe = new stdClass();
            $th_comp_cpe->competency_id = substr($key, 10);
            $th_comp_cpe->cpe_hours = $value;
            $th_comp_cpe->training_history_id = $data->id;
            $th_comp_cpe->date_modified = date('Y-m-d H:i:s');
            if ($value > 0) {
                $DB->insert_record('androgogic_training_history_competencies_cpe_hours', $th_comp_cpe);
            }
        }
    }
}

// this function does update for course level cpd points
function block_androgogic_training_history_course_cpd_points_update($data) {
    global $DB;
    // did we get any cpd points?
    //out with the old
    $DB->delete_records('androgogic_training_history_course_competencies_cpd_points', array('course_id' => $data->id));
    //in with the new   
    foreach ($data as $key => $value) {
        if (substr($key, 0, 10) == 'cpd_points') {
            $th_comp_cpd = new stdClass();
            $th_comp_cpd->competency_id = substr($key, 11);
            $th_comp_cpd->cpd_points = $value;
            $th_comp_cpd->course_id = $data->id;
            $th_comp_cpd->date_modified = date('Y-m-d H:i:s');
            if ($value > 0) {
                $DB->insert_record('androgogic_training_history_course_competencies_cpd_points', $th_comp_cpd);
            }
        }
    }
}

// this function does update for course level cpe hours
function block_androgogic_training_history_course_cpe_hours_update($data) {
    global $DB;
    //any cpe hours items?
    //out with the old
    $DB->delete_records('androgogic_training_history_course_competencies_cpe_hours', array('course_id' => $data->id));
    //in with the new   
    foreach ($data as $key => $value) {
        if (substr($key, 0, 9) == 'cpe_hours') {
            $th_comp_cpe = new stdClass();
            $th_comp_cpe->competency_id = substr($key, 10);
            $th_comp_cpe->cpe_hours = $value;
            $th_comp_cpe->course_id = $data->id;
            $th_comp_cpe->date_modified = date('Y-m-d H:i:s');
            if ($value > 0) {
                $DB->insert_record('androgogic_training_history_course_competencies_cpe_hours', $th_comp_cpe);
            }
        }
    }
}
function block_androgogic_training_history_course_dimensions_update($data) {
    global $DB;
    $DB->delete_records('androgogic_course_dimensions',array('course_id'=>$data->id));
    //many to many relationship: androgogic_course_dimensions
    if(isset($data->dimension_id) and is_array($data->dimension_id)){
        foreach($data->dimension_id as $dimension_id){
            $insert = new stdClass();
            $insert->course_id = $data->id;
            $insert->dimension_id = $dimension_id;
            $DB->insert_record('androgogic_course_dimensions', $insert);
        }
    }
}
// this function does update for cpd targets
function block_androgogic_training_history_cpd_targets_update($data) {
    global $DB, $USER;
    // did we get any cpd points?
    //out with the old
    $DB->delete_records('androgogic_training_history_cpd_targets', array('org_pos_period_id' => $data->id));
    //in with the new   
    foreach ($data as $key => $value) {
        if (substr($key, 0, 10) == 'cpd_points') {
            $th_comp_cpd = new stdClass();
            $th_comp_cpd->competency_id = substr($key, 11);
            $th_comp_cpd->cpd_target = $value;
            $th_comp_cpd->org_pos_period_id = $data->id;
            $th_comp_cpd->created_by = $USER->id;
            $th_comp_cpd->date_created = date('Y-m-d H:i:s');
            if ($value > 0) {
                $DB->insert_record('androgogic_training_history_cpd_targets', $th_comp_cpd);
            }
        }
    }
}

// this function does update for cpe targets
function block_androgogic_training_history_cpe_targets_update($data) {
    global $DB, $USER;
    //any cpe hours items?
    //out with the old
    $DB->delete_records('androgogic_training_history_cpe_targets', array('org_pos_period_id' => $data->id));
    //in with the new   
    foreach ($data as $key => $value) {
        if (substr($key, 0, 9) == 'cpe_hours') {
            $th_comp_cpe = new stdClass();
            $th_comp_cpe->competency_id = substr($key, 10);
            $th_comp_cpe->cpe_target = $value;
            $th_comp_cpe->org_pos_period_id = $data->id;
            $th_comp_cpe->created_by = $USER->id;
            $th_comp_cpe->date_created = date('Y-m-d H:i:s');
            if ($value > 0) {
                $DB->insert_record('androgogic_training_history_cpe_targets', $th_comp_cpe);
            }
        }
    }
}

// this function does update for cpd overrides
function block_androgogic_training_history_cpd_overrides_update($data) {
    global $DB, $USER;
    // did we get any cpd points?
    //out with the old
    $DB->delete_records('androgogic_user_points_override', array('user_id' => $data->user_id));
    //in with the new   
    foreach ($data as $key => $value) {
        if (substr($key, 0, 10) == 'cpd_points') {
            $th_comp_cpd = new stdClass();
            $th_comp_cpd->competency_id = substr($key, 11);
            $th_comp_cpd->points = $value;
            $th_comp_cpd->user_id = $data->user_id;
            $th_comp_cpd->created_by = $USER->id;
            $th_comp_cpd->date_created = date('Y-m-d H:i:s');
            if ($value != 0) {
                $DB->insert_record('androgogic_user_points_override', $th_comp_cpd);
            }
        }
    }
}

// this function does update for cpe overrides
function block_androgogic_training_history_cpe_overrides_update($data) {
    global $DB, $USER;
    //any cpe hours items?
    //out with the old
    $DB->delete_records('androgogic_user_hours_override', array('user_id' => $data->user_id));
    //in with the new   
    foreach ($data as $key => $value) {
        if (substr($key, 0, 9) == 'cpe_hours') {
            $th_comp_cpe = new stdClass();
            $th_comp_cpe->competency_id = substr($key, 10);
            $th_comp_cpe->hours = $value;
            $th_comp_cpe->user_id = $data->user_id;
            $th_comp_cpe->created_by = $USER->id;
            $th_comp_cpe->date_created = date('Y-m-d H:i:s');
            if ($value != 0) {
                $DB->insert_record('androgogic_user_hours_override', $th_comp_cpe);
            }
        }
    }
}
function block_androgogic_training_history_load_target_data($mform, $org_pos_period) {
    global $DB;
    $config = get_config('block_androgogic_training_history');
    $sql = "SELECT * FROM mdl_androgogic_training_history_cpd_targets cp
                where cp.deleted = 0 AND org_pos_period_id = $org_pos_period->id";
    $cpd_items = $DB->get_records_sql($sql);
    foreach ($cpd_items as $cpd_item) {
        if (isset($mform->_elementIndex['cpd_points_' . $cpd_item->competency_id])) {
            $mform->setConstant('cpd_points_' . $cpd_item->competency_id, $cpd_item->cpd_target);
        }
    }
    if ($config->do_cpe_hours) {
        $sql = "SELECT * FROM mdl_androgogic_training_history_cpe_targets cp
                where cp.deleted = 0 AND org_pos_period_id = $org_pos_period->id";
        $cpe_items = $DB->get_records_sql($sql);
        foreach ($cpe_items as $cpe_item) {
            if (isset($mform->_elementIndex['cpe_hours_' . $cpe_item->competency_id])) {
                $mform->setConstant('cpe_hours_' . $cpe_item->competency_id, $cpe_item->cpe_target);
            }
        }
    }
}
function block_androgogic_training_history_load_overrides_data($mform, $user_id) {
    global $DB;
    $config = get_config('block_androgogic_training_history');
    $sql = "SELECT * FROM mdl_androgogic_user_points_override cp
                where user_id = $user_id  and cp.deleted = 0";
    $cpd_items = $DB->get_records_sql($sql);
    foreach ($cpd_items as $cpd_item) {
        if (isset($mform->_elementIndex['cpd_points_' . $cpd_item->competency_id])) {
            $mform->setConstant('cpd_points_' . $cpd_item->competency_id, $cpd_item->points);
        }
    }
    if ($config->do_cpe_hours) {
        $sql = "SELECT * FROM mdl_androgogic_user_hours_override cp
                where user_id = $user_id  and cp.deleted = 0";
        $cpe_items = $DB->get_records_sql($sql);
        foreach ($cpe_items as $cpe_item) {
            if (isset($mform->_elementIndex['cpe_hours_' . $cpe_item->competency_id])) {
                $mform->setConstant('cpe_hours_' . $cpe_item->competency_id, $cpe_item->hours);
            }
        }
    }
}

function block_androgogic_training_history_course_edit_form($mform) {
    global $DB;
    $config = get_config('block_androgogic_training_history');
    // we use this in a number of different ways: course edit form, org_pos_period_edit_form and new/edit training history record
    if ($mform->_formName == 'course_edit_form') {
        $section_header_text = get_string('pluginname', 'block_androgogic_training_history');
        $cpd_header = get_string('cpd_points', 'block_androgogic_training_history');
        $cpe_header = get_string('cpe_hours', 'block_androgogic_training_history');
    } else if ($mform->_formName == 'org_pos_period_edit_form') {
        //$section_header_text = get_string('competency_plural', 'block_androgogic_training_history');
        $cpd_header = get_string('cpd_target', 'block_androgogic_training_history');
        $cpe_header = get_string('cpe_target', 'block_androgogic_training_history');
    } else if ($mform->_formName == 'user_points_override_edit_form') {
        //$section_header_text = get_string('competency_plural', 'block_androgogic_training_history');
        $cpd_header = get_string('cpd_points_override', 'block_androgogic_training_history');
        $cpe_header = get_string('cpe_hours_override', 'block_androgogic_training_history');
    } else {
        //$section_header_text = get_string('competency_plural', 'block_androgogic_training_history');
        $cpd_header = get_string('cpd_points', 'block_androgogic_training_history');
        $cpe_header = get_string('cpe_hours', 'block_androgogic_training_history');
    }
    
    if(in_array($mform->_formName, array('course_edit_form'))){
        $mform->addElement('header', 'androgogic_training_history_header', $section_header_text);
    }
    //from the configured competency framework, get all of the comptencies and present them        
    $cpd_framework_id = $config->cpd_framework_id;
    $sql = "SELECT * FROM mdl_comp c where frameworkid = $cpd_framework_id";
    if (isset($config->comp_type_id)) {
        $sql .= " and typeid = $config->comp_type_id";
    }
    $competencies = $DB->get_records_sql($sql);

    $mform->addElement('html', html_writer::start_tag('table'));
    $html = html_writer::start_tag('tr') . html_writer::tag('th', get_string('competency', 'block_androgogic_training_history'));
    $html .= html_writer::tag('th', $cpd_header);
    $html .= html_writer::end_tag('tr');
    $mform->addElement('html', $html);
    
//    if($mform->_formName == 'user_points_override_edit_form'){
//        //add a fake competency
//        $fake_competency = new stdClass();
//        $fake_competency->id = 0;
//        $fake_competency->fullname = get_string('unallocated_points','block_androgogic_training_history');
//        array_unshift($competencies,$fake_competency);
//        
//    }

    foreach ($competencies as $competency) {
        $html = html_writer::start_tag('tr');
        $html .= html_writer::tag('td', $competency->fullname) . html_writer::start_tag('td');
        $mform->addElement('html', $html);

        $mform->addElement('text', 'cpd_points_' . $competency->id, '', array('size' => 10));
        $mform->addRule('cpd_points_' . $competency->id, 'Points have to be full numbers with up to 2 decimal points', 'numeric', '', 'client');

        $html = html_writer::end_tag('td') . html_writer::end_tag('tr');
        $mform->addElement('html', $html);
    }
    //cpe hours
    if ($config->do_cpe_hours) {
        $html = html_writer::start_tag('tr') . html_writer::tag('th', get_string('competency', 'block_androgogic_training_history'));
        $html .= html_writer::tag('th', $cpe_header);
        $html .= html_writer::end_tag('tr');
        $mform->addElement('html', $html);
        $sql = "SELECT * FROM mdl_comp where id in ($config->cpe_competencies)";
        $cpe_competencies = $DB->get_records_sql($sql);
        foreach ($cpe_competencies as $cpe_competency) {
            $html = html_writer::start_tag('tr');
            $html .= html_writer::tag('td', $cpe_competency->fullname) . html_writer::start_tag('td');
            $mform->addElement('html', $html);

            $mform->addElement('text', 'cpe_hours_' . $cpe_competency->id, '', array('size' => 10));
            $mform->addRule('cpe_hours_' . $cpe_competency->id, 'Hours have to be full numbers with up to 2 decimal points', 'numeric', '', 'client');

            $html = html_writer::end_tag('td') . html_writer::end_tag('tr');
            $mform->addElement('html', $html);
        }
    }
    $mform->addElement('html', html_writer::end_tag('table'));
    
    if (!in_array($mform->_formName, array('org_pos_period_edit_form','user_points_override_edit_form'))){
        //make me into cb array?
        $options = $DB->get_records_menu('androgogic_dimensions',array(),'name','id,name');
        $select = $mform->addElement('select', 'dimension_id', get_string('dimension','block_androgogic_training_history'), $options,array('size'=>10));
        $select->setMultiple(true);
    }
}

function block_androgogic_training_history_save_course_data($data) {
    block_androgogic_training_history_course_cpd_points_update($data);
    block_androgogic_training_history_course_cpe_hours_update($data);
    block_androgogic_training_history_course_dimensions_update($data);
}

//2 modes : either we load from db or get from POST
function block_androgogic_training_history_load_course_data($mform, $course) {
    global $DB;
    $config = get_config('block_androgogic_training_history');
    if(empty($_POST)){
        $sql = "SELECT * FROM mdl_androgogic_training_history_course_competencies_cpd_points cp
                    where cp.deleted = 0 AND course_id = $course->id";
        $cpd_items = $DB->get_records_sql($sql);
        foreach ($cpd_items as $cpd_item) {
            $mform->setConstant('cpd_points_' . $cpd_item->competency_id, $cpd_item->cpd_points);
        }
        if ($config->do_cpe_hours) {
            $sql = "SELECT * FROM mdl_androgogic_training_history_course_competencies_cpe_hours cp
                    where cp.deleted = 0 AND course_id = $course->id";
            $cpe_items = $DB->get_records_sql($sql);
            foreach ($cpe_items as $cpe_item) {
                $mform->setConstant('cpe_hours_' . $cpe_item->competency_id, $cpe_item->cpe_hours);
            }
        }

        //dimensions
        $sql = "SELECT * FROM mdl_androgogic_course_dimensions 
                    where course_id = $course->id";
        $result = $DB->get_records_sql($sql);
        $dimension_ids = array();
        foreach ($result as $row) {
            $dimension_ids[] = $row->dimension_id;
        }
        if (count($dimension_ids) > 0) {
            $mform->setConstant('dimension_id', $dimension_ids);
        }
    }
    else{
        foreach($_POST as $key=>$value){
            if(stristr($key,'cpd_points_') or stristr($key,'cpe_hours_') or stristr($key,'dimension_id')){
                if(isset($mform->_elementIndex[$key])){
                    $mform->setConstant($key,$value);
                }
            }
        }
    }
}

function block_androgogic_training_history_org_pos_unique($data) {
    global $DB;
    $sql = "SELECT * FROM mdl_androgogic_org_pos_periods where org_id = $data->org_id and pos_id = $data->pos_id and soa_course_id = $data->soa_course_id";
    // if it is a new one, this won't be set, if an update it ought to be
    if (isset($data->id)) {
        $sql .= " and id != $data->id";
    }
    if (!$DB->get_records_sql($sql)) {
        // if no records then it is unique
        return true;
    } else {
        return false;
    }
}

function block_androgogic_training_history_get_user_org_pos($user) {
    global $DB;
    //get the user's org and pos from their primary pos
    $sql = "SELECT pa.*, p.frameworkid as pos_framework_id, p.path as pos_path, 
            o.frameworkid as org_framework_id, o.path as org_path, p.sortthread as pos_sortthread, 
            o.sortthread as org_sortthread
        FROM mdl_pos_assignment pa
        inner join mdl_pos p on pa.positionid = p.id
        inner join mdl_org o on pa.organisationid = o.id
        where pa.userid = $user->id and pa.type = 1";
    return $DB->get_record_sql($sql);
}

function block_androgogic_training_history_get_org_pos_period($org_framework_id, $pos_framework_id, $position_sortthread,$organisation_sortthread,$user_id) {
    global $DB;
    //get the org pos period from the 2 framework ids that is equal or greater to the given pos sortthread 
    // (note this is lowest hanging relation based on pos)
    // also the user needs to have a completion for the related course
    $sql = "select pp.* 
        from mdl_androgogic_org_pos_periods pp
        inner join mdl_org o on pp.org_id = o.id
        inner join mdl_pos p on pp.pos_id = p.id
        inner join mdl_course c on pp.soa_course_id = c.id
        inner join mdl_course_completions cc on c.id = cc.course
        where o.frameworkid = $org_framework_id 
        and p.frameworkid = $pos_framework_id "
            . "AND p.visible = 1 "
            . "AND o.visible = 1 "
            . "and p.sortthread <= '$position_sortthread' "
            . "and o.sortthread <= '$organisation_sortthread' "
            . "and cc.userid = $user_id and cc.status >= 50 "
            . "ORDER BY p.sortthread DESC LIMIT 1";

    return $DB->get_record_sql($sql);
}
function block_androgogic_training_history_format_default_period($org_pos_period){
    $period_type = $org_pos_period->period;

    $this_year = date('Y');
    $last_year = $this_year - 1;
    $next_year = $this_year + 1;
    $this_month = date('m');
    if ($period_type == 'financial') {
        if ($this_month > 6) {
            $start_year = $this_year;
            $end_year = $next_year;
        } else {
            $start_year = $last_year;
            $end_year = $this_year;
        }
        $period_start_date = strtotime("07/01/$start_year");
        $period_end_date = strtotime("06/30/$end_year");
    } else {
        //calendar                        
        $period_start_date = strtotime("01/01/$this_year");
        $period_end_date = strtotime("12/31/$this_year");
    }
    return array($period_start_date,$period_end_date);
}
/**
 * get the period for a user - override if any, if not then their default
 * @param stdClass $user
 * @param stdClass $org_pos_period
 * @return array
 */
function block_androgogic_training_history_get_cpd_period($user, $org_pos_period) {
    global $DB;
    //get their current period override record if any
    $sql = "select u.* from mdl_androgogic_user_period u
        inner join mdl_androgogic_training_history_periods p on u.period_id = p.id
        where user_id = $user->id and p.is_current = 1";
    $user_period_user_record = $DB->get_record_sql($sql);

    if (!$user_period_user_record) {
        list($period_start_date,$period_end_date) = block_androgogic_training_history_format_default_period($org_pos_period);
    } else {
        $period_start_date = $user_period_user_record->startdate;
        $period_end_date = $user_period_user_record->enddate;
    }
    return array($period_start_date, $period_end_date);
}
/**
 * get the default period for a user (if you want a possibly overridden one use block_androgogic_training_history_get_cpd_period)
 * @param stdClass $user
 * @return boolean or array
 */
function block_androgogic_training_history_get_default_period_for_user($user){
    $user_org_pos = block_androgogic_training_history_get_user_org_pos($user);
    if(!$user_org_pos){
        // then it's all over
        return false;
    }
    $org_pos_period = block_androgogic_training_history_get_org_pos_period(
            $user_org_pos->org_framework_id,$user_org_pos->pos_framework_id,
            $user_org_pos->pos_sortthread,$user_org_pos->org_sortthread, $user->id);
    if(!$org_pos_period){
        // then it's all over
        return false;
    }

    list($period_start_date,$period_end_date) = block_androgogic_training_history_format_default_period($org_pos_period);
    return array($period_start_date,$period_end_date);
}
function block_androgogic_training_history_export_data($download_format, $table, $filename,$report_name_lang='Training history report',$table2=null) {
    $stripped_table = block_androgogic_training_history_strip_tags_from_table($table);
    $function = "block_androgogic_training_history_export_data_" . $download_format;
    if($table2){
        $stripped_table2 = block_androgogic_training_history_strip_tags_from_table($table2);
        $function($stripped_table, $filename,$report_name_lang,$stripped_table2);
    }
    else{
        $function($stripped_table, $filename,$report_name_lang);
    }
}

function block_androgogic_training_history_export_data_excel($table, $filename,$report_name_lang,$table2=null) {
    global $CFG;

    require_once("$CFG->libdir/excellib.class.php");

    $filename = clean_filename($filename) . '.xls';

    $workbook = new MoodleExcelWorkbook('-');
    $workbook->send($filename);

    $worksheet = array();

    $worksheet[0] = $workbook->add_worksheet('');
    $col = 0;
    foreach ($table->head as $cell) {
        $worksheet[0]->write(0, $col, $cell);
        $col++;
    }

    $row = 1;
    foreach ($table->data as $row_data) {
        $col = 0;
        foreach ($row_data as $cell) {
            $worksheet[0]->write($row, $col, $cell);
            $col++;
        }
        $row++;
    }
    if($table2){
        //add empty row
        $worksheet[0]->write($row, 0, 0);
        $row++;
        $col = 0;
        foreach ($table2->head as $cell) {
            $worksheet[0]->write($row, $col, $cell);
            $col++;
        }
        $row++;
        foreach ($table2->data as $row_data) {
            $col = 0;
            foreach ($row_data as $cell) {
                $worksheet[0]->write($row, $col, $cell);
                $col++;
            }
            $row++;
        }
    }

    $workbook->close();
    die;
}

function block_androgogic_training_history_export_data_csv($table, $filename,$report_name_lang,$table2=null) {
    global $CFG;

    require_once($CFG->libdir . '/csvlib.class.php');

    $filename = clean_filename($filename) . '.csv';

    $csvexport = new csv_export_writer();
    $csvexport->set_filename($filename);
    $csvexport->add_data($table->head);

    foreach ($table->data as $row_data) {
        $csvexport->add_data($row_data);
    }
    if($table2){
        $empty_row = array();
        $csvexport->add_data($empty_row);
        $csvexport->add_data($table2->head);

        foreach ($table2->data as $row_data) {
            $csvexport->add_data($row_data);
        }
    }
    $csvexport->download_file();
    die;
}

function block_androgogic_training_history_strip_tags_from_table($table) {
    //strip any html out of the table head and rows
    $stripped_table = new stdClass();
    foreach ($table->head as $table_head_cell) {
        $stripped_table->head[] = strip_tags($table_head_cell);
    }
    foreach ($table->data as $row_data) {
        $stripped_row_data = array();
        foreach ($row_data as $cell) {
            $stripped_row_data[] = strip_tags($cell);
        }
        $stripped_table->data[] = $stripped_row_data;
    }
    return $stripped_table;
}

function block_androgogic_training_history_export_data_pdf_portrait($table, $filename, $report_name_lang_entry,$table2=null) {
    block_androgogic_training_history_export_data_pdf($table, $filename, $report_name_lang_entry, true,$table2);
}

function block_androgogic_training_history_export_data_pdf_landscape($table, $filename, $report_name_lang_entry,$table2=null) {
    block_androgogic_training_history_export_data_pdf($table, $filename, $report_name_lang_entry, false,$table2);
}

function block_androgogic_training_history_export_data_pdf($table, $filename, $report_name_lang_entry, $portrait = true,$table2=null) {
    global $CFG;
    //set up some values - these might go to settings page at some stage
    $margin_footer = 10;
    $margin_bottom = 20;
    $font_size_title = 20;
    $font_size_record = 14;
    $font_size_data = 10;
    $memory_limit = 1024;

    require_once $CFG->libdir . '/pdflib.php';

    // Increasing the execution time to no limit.
    set_time_limit(0);
    $filename = clean_filename($filename . '.pdf');

    // Table.
    $html = '';
    $numfields = count($table->head);

    // Layout options.
    if ($portrait) {
        $pdf = new PDF('P', 'mm', 'A4', true, 'UTF-8');
    } else {
        $pdf = new PDF('L', 'mm', 'A4', true, 'UTF-8');
    }

    $pdf->setTitle($filename);
    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(true);
    $pdf->SetFooterMargin($margin_footer);
    $pdf->SetAutoPageBreak(true, $margin_bottom);
    $pdf->AddPage();

    // Get current language to set the font properly.
    $language = current_language();
    if (in_array($language, array('zh_cn', 'ja'))) {
        $font = 'droidsansfallback';
    } else if ($language == 'th') {
        $font = 'cordiaupc';
    } else {
        $font = 'dejavusans';
    }
    // Check if language is RTL.
    if (right_to_left()) {
        $pdf->setRTL(true);
    }
    if($report_name_lang_entry != ''){
        $report_header = get_string($report_name_lang_entry, 'block_androgogic_training_history');
        $pdf->SetFont($font, 'B', $font_size_title);
        $pdf->Write(0, format_string($report_header), '', 0, 'L', true, 0, false, false, 0);    
    }
    
    $count = count($table->data);
    $resultstr = $count == 1 ? 'record' : 'records';
    $recordscount = get_string('x' . $resultstr, 'totara_reportbuilder', $count);
    $pdf->SetFont($font, 'B', $font_size_record);
    if(!$table2){
        $pdf->Write(0, $recordscount, '', 0, 'L', true, 0, false, false, 0);
    }
    $pdf->SetFont($font, '', $font_size_data);

    if (is_array($restrictions) && count($restrictions) > 0) {
        $pdf->Write(0, get_string('reportcontents', 'totara_reportbuilder'), '', 0, 'L', true, 0, false, false, 0);
        foreach ($restrictions as $restriction) {
            $pdf->Write(0, $restriction, '', 0, 'L', true, 0, false, false, 0);
        }
    }

    // Add report caching data.
    if ($cache) {
        $usertz = totara_get_clean_timezone();
        $a = userdate($cache['lastreport'], '', $usertz);
        $lastcache = get_string('report:cachelast', 'totara_reportbuilder', $a);
        $pdf->Write(0, $lastcache, '', 0, 'L', true, 0, false, false, 0);
    }
    //make a space between the header and the table
    $html .= '<p>&nbsp;</p>';
    $html .= '<table border="1" cellpadding="2" cellspacing="0">
                        <thead>
                            <tr style="background-color: #CCC;">';
    foreach ($table->head as $field) {
        $html .= '<th>' . $field . '</th>';
    }
    $html .= '</tr></thead><tbody>';

    foreach ($table->data as $record_data) {
        $html .= '<tr>';
        for ($j = 0; $j < $numfields; $j++) {
            if (isset($record_data[$j])) {
                $cellcontent = html_entity_decode($record_data[$j], ENT_COMPAT, 'UTF-8');
            } else {
                $cellcontent = '';
            }
            $html .= '<td>' . $cellcontent . '</td>';
        }
        $html .= '</tr>';

        // Check memory limit.
        $mramuse = ceil(((memory_get_usage(true) / 1024) / 1024));
        if ($memory_limit <= $mramuse) {
            // Releasing resources.
            $records->close();
            // Notice message.
            print_error('exportpdf_mramlimitexceeded', 'totara_reportbuilder', '', $memory_limit);
        }
    }
    $html .= '</tbody></table>';
    
    if($table2){
        //reset the numfields
        $numfields = count($table2->head);
        //make a space between the tables
        $html .= '<p>&nbsp;</p>';
        //make some more html for table2
        $html .= '<table border="1" cellpadding="2" cellspacing="0">
                    <thead>
                        <tr style="background-color: #CCC;">';
        foreach ($table2->head as $field) {
            $html .= '<th>' . $field . '</th>';
        }
        $html .= '</tr></thead><tbody>';

        foreach ($table2->data as $record_data) {
            $html .= '<tr>';
            for ($j = 0; $j < $numfields; $j++) {
                if (isset($record_data[$j])) {
                    $cellcontent = html_entity_decode($record_data[$j], ENT_COMPAT, 'UTF-8');
                } else {
                    $cellcontent = '';
                }
                $html .= '<td>' . $cellcontent . '</td>';
            }
            $html .= '</tr>';

            // Check memory limit.
            $mramuse = ceil(((memory_get_usage(true) / 1024) / 1024));
            if ($memory_limit <= $mramuse) {
                // Releasing resources.
                $records->close();
                // Notice message.
                print_error('exportpdf_mramlimitexceeded', 'totara_reportbuilder', '', $memory_limit);
            }
        }
        $html .= '</tbody></table>';
    }

    // Closing the pdf.
    $pdf->WriteHTML($html, true, false, false, false, '');

    // Returning the complete pdf.
    if (!$file) {
        $pdf->Output($filename, 'D');
    } else {
        $pdf->Output($file, 'F');
    }
}

function block_androgogic_training_history_output_download_links($moodle_url, $report_name_lang_entry='') {
    $url = $moodle_url->out(false);
    $report_name_param = "&report_name_lang=$report_name_lang_entry";
    echo 'Download: ';
    $download_report_url = $url . "&download=csv";
    echo html_writer::link($download_report_url, 'CSV');
    echo "&nbsp;";
    $download_report_url = $url . "&download=excel";
    echo html_writer::link($download_report_url, 'Excel');
    echo "&nbsp;";
    $download_report_url = $url . "&download=pdf_portrait" . $report_name_param;
    echo html_writer::link($download_report_url, 'PDF Portrait');
    echo "&nbsp;";
    $download_report_url = $url . "&download=pdf_landscape" . $report_name_param;
    echo html_writer::link($download_report_url, 'PDF Landscape');
}
function block_androgogic_training_history_fix_phantom_file_id($data) {
    global $DB;
    if(isset($data->file_id) and $data->file_id > 0){
        //have we got a phantom file id?
        $sql = "SELECT * FROM mdl_files
        WHERE itemid = '$data->file_id'";
        $file_exists = $DB->get_records_sql($sql);
        if ($file_exists) {
            //fix file entries
            block_androgogic_training_history_fix_file($data->file_id);
        } else {
            //get rid of the phantom out of the th table
            $sql = "UPDATE mdl_androgogic_training_history
            SET file_id = NULL
            where id = '$data->id'";
            $DB->execute($sql);
        }
    }
}
function block_androgogic_training_history_fix_files() {
    global $DB;
    $sql = "select f.* from mdl_androgogic_training_history th
        inner join mdl_files f on th.file_id = f.itemid
        where f.component = 'user' and f.filearea = 'draft'";

    $results = $DB->get_records_sql($sql);

    foreach ($results as $row) {
        block_androgogic_training_history_fix_file($row->itemid);
    }
    
}
function block_androgogic_training_history_fix_file($item_id) {
    global $DB;
    if($item_id > 0){
    $sql = "UPDATE mdl_files
            SET filearea = 'content',component='block_androgogic_training_history'
            WHERE itemid = '$item_id'";
            $DB->execute($sql);
    }
}
class androgogic_training_history_observer {
    
    /**
     * Event that is triggered when a comp is deleted.
     *
     * Soft delete and change date modded on any related records in training history tables, tables to do this on are
     *  mdl_androgogic_training_history_competencies_cpd_points
        mdl_androgogic_training_history_competencies_cpe_hours
        mdl_androgogic_training_history_course_competencies_cpd_points
        mdl_androgogic_training_history_course_competencies_cpe_hours
        mdl_androgogic_training_history_cpd_targets
        mdl_androgogic_training_history_cpe_targets
        mdl_androgogic_user_points_override
        mdl_androgogic_user_hours_override
     *
     * @param \totara_hierarchy\event\competency_deleted $event
     *
     */
    public static function competency_deleted(\totara_hierarchy\event\competency_deleted $event) {
        global $DB;
        $comp_id = $event->objectid;
        $where = " WHERE competency_id = $comp_id";
        $tables = array('mdl_androgogic_training_history_competencies_cpd_points',
            'mdl_androgogic_training_history_competencies_cpe_hours','mdl_androgogic_training_history_course_competencies_cpd_points',
            'mdl_androgogic_training_history_course_competencies_cpe_hours','mdl_androgogic_training_history_cpd_targets',
            'mdl_androgogic_training_history_cpe_targets','mdl_androgogic_user_points_override','mdl_androgogic_user_hours_override');
        //$transaction = $DB->start_delegated_transaction();

        foreach ($tables as $table) {
            $sql = "UPDATE $table set deleted = 1, date_modified = '" . date('Y-m-d h:i:s') . "' " . $where;
            $DB->execute($sql);
        }

        //$transaction->allow_commit();
    }
}

