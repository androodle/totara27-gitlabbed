<?php

/** 
 * Androgogic Training History Block: Search form
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     05/08/2014
 * @copyright   2014+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 * Provides search form for the object.
 * This is used by search page
 *
 **/

if (!defined('MOODLE_INTERNAL')) {
die('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}
require_once($CFG->libdir . '/formslib.php');
class org_pos_period_search_form extends moodleform {
function definition() {
global $DB;
$mform =& $this->_form;
foreach($this->_customdata as $custom_key=>$custom_value){
$$custom_key = $custom_value;
}
$mform->addElement('html','<div>');
//search controls
$mform->addElement('text','search',get_string('org_pos_period_search_instructions', 'block_androgogic_training_history'));
//org_id
$dboptions = $DB->get_records_menu('org',array(),'fullname','id,fullname');
unset($options);
$options[0] = 'Any';
foreach($dboptions as $key=>$value){
$options[$key] = $value;
}
$select = $mform->addElement('select', 'org_id', get_string('org','block_androgogic_training_history'), $options);
//pos_id
$dboptions = $DB->get_records_menu('pos',array(),'fullname','id,fullname');
unset($options);
$options[0] = 'Any';
foreach($dboptions as $key=>$value){
$options[$key] = $value;
}
$select = $mform->addElement('select', 'pos_id', get_string('pos','block_androgogic_training_history'), $options);
//soa_course_id
$config = get_config('block_androgogic_training_history');
if(isset($config->scope_of_authority_category_id)){
    $dboptions = $DB->get_records_menu('course',array('category'=>$config->scope_of_authority_category_id),'fullname','id,fullname');
    unset($options);
    $options[0] = 'Any';
    foreach($dboptions as $key=>$value){
    $options[$key] = $value;
    }
    $select = $mform->addElement('select', 'soa_course_id', get_string('scope_of_authority','block_androgogic_training_history'), $options);
}
else{
    echo "Cannot display the scope of authority dropdown - please configure in 'block_androgogic_training_history' settings";
}
//hiddens
$mform->addElement('hidden','tab',$tab);
$mform->addElement('hidden','sort',$sort);
$mform->addElement('hidden','dir',$dir);
$mform->addElement('hidden','perpage',$perpage);
$mform->addElement('hidden', 'debug', $debug);
//button
$mform->addElement('submit','submit','Search');
$mform->addElement('html','</div>');
}
}
