<?php

/** 
 * Androgogic Training History Block: Edit form 
 * 
 * @author      Daniel Morphett <dan@androgogic.com> 
 * @version     07/11/2014 
 * @copyright   2014+ Androgogic Pty Ltd <http://www.androgogic.com> 
 * 
 * Provides edit form for the object.
 * This is used by both new and edit pages
 *  
 **/

if (!defined('MOODLE_INTERNAL')) {
die('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}
require_once($CFG->libdir . '/formslib.php');
class provider_edit_form extends moodleform {
protected $provider;
function definition() {
global $USER,$courseid,$DB,$PAGE;
$mform =& $this->_form;
$context = context_system::instance();
if(isset($_REQUEST['id'])){
$q = "select DISTINCT a.*  
from mdl_androgogic_training_history_providers a 
where a.id = {$_REQUEST['id']} ";
$provider = $DB->get_record_sql($q);
}
else{
$provider = $this->_customdata['$provider']; // this contains the data of this form
}
$tab = 'provider_new'; // from whence we were called
if (!empty($provider->id)) {
$tab = 'provider_edit';
}
$mform->addElement('html','<div>');

//name
$mform->addElement('text', 'name', get_string('name','block_androgogic_training_history'), array('size'=>255));
$mform->addRule('name', get_string('required'), 'required', null, 'server');
$mform->addRule('name', 'Maximum 255 characters', 'maxlength', 255, 'client');
//set values if we are in edit mode
if (!empty($provider->id) && isset($_GET['id'])) {
$mform->setConstant('name', $provider->name);
}
//hiddens
$mform->addElement('hidden','tab',$tab);
if(isset($_REQUEST['id'])){
$mform->addElement('hidden','id',$_REQUEST['id']);
}
elseif(isset($id)){
$mform->addElement('hidden', 'id', $id);
}
$this->add_action_buttons(false);
$mform->addElement('html','</div>');
}
}
