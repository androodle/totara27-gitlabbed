<?php

/**
 * Androgogic Training History Block: Edit object
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     17/06/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 * Edit one of the training_histories
 *
 * */
global $OUTPUT;
//require_capability('block/androgogic_training_history:admin', $context);
require_once('training_history_edit_form.php');
$id = required_param('id', PARAM_INT);
//need to have a snapshot of the th record for comparison
$q = "select DISTINCT a.* , mdl_androgogic_activities.name as activity, mdl_files.contextid, mdl_files.component, mdl_files.filearea, mdl_files.filename, mdl_files.itemid, mdl_files.id as fileid, CONCAT(mdl_user.firstname,' ',mdl_user.lastname) as user 
from mdl_androgogic_training_history a 
LEFT JOIN mdl_androgogic_activities  on a.activity_id = mdl_androgogic_activities.id
LEFT JOIN mdl_files  on a.file_id = mdl_files.itemid and mdl_files.itemid != 0 and mdl_files.filename != '.'
LEFT JOIN mdl_user  on a.user_id = mdl_user.id
where a.id = $id ";
$training_history = $DB->get_record_sql($q);
$mform = new training_history_edit_form();
if ($data = $mform->get_data()) {
    //var_dump($data);
    
    $data->id = $id;
    $data->modified_by = $USER->id;
    $data->date_modified = date('Y-m-d H:i:s');
    if (!isset($data->approved)) {
        $data->approved = 0;
    } else if (block_androgogic_training_history_is_manager() && $data->user_id == $USER->id) {
        //can't approve your own th!
        $data->approved = 0;
        echo $OUTPUT->notification(get_string('cantapproveowntraininghistory', 'block_androgogic_training_history'), 'notifyfailure');
    }
    if (block_androgogic_training_history_membership_qualification_is_unique($data)){
        $DB->update_record('androgogic_training_history', $data);
        if(!isset($data->file_id)){
            //it might be in the db, but not in the submission, so go and get it if it's there
            $data->file_id = $DB->get_field('androgogic_training_history','file_id',array('id'=>$id));
        }
        if(isset($data->file_id)){
            block_androgogic_training_history_fix_phantom_file_id($data);
        }
    }
    else{
        echo $OUTPUT->notification(get_string('existing_membership', 'block_androgogic_training_history'), 'notifyfailure');
        die;
    }
    block_androgogic_training_history_cpd_points_update($data);
    block_androgogic_training_history_dimensions_update($data);
    if ($config->approval_workflow) {
        if(has_capability('block/androgogic_training_history:admin', $context)){
            // if the user is an admin, and they just changed the approved value to 1 then advise user
            $old_approved_value = $training_history->approved;
            $new_approved_value = $data->approved;

            if ($new_approved_value == '1' && $old_approved_value != '1') {
                block_androgogic_training_history_advise_user($data);
                block_androgogic_training_history_complete_related_courses($data);
            }
        }
        else{
            // if it's a qual or a membership then advise admin user
            if(isset($data->membership_id) or isset($data->qualification_id)){
                block_androgogic_training_history_advise_admin($data);        
            }
        }
    }
    
    
    if($config->do_cpe_hours){
        block_androgogic_training_history_cpe_hours_update($data);
    }
    
    echo $OUTPUT->notification(get_string('datasubmitted', 'block_androgogic_training_history'), 'notifysuccess');
} else {
    echo $OUTPUT->heading(get_string('training_history_edit', 'block_androgogic_training_history'));
    $mform->display();
}
?>
