<?php

/**
 * Androgogic Training History Block: Edit form
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     17/06/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 * Provides edit form for the object.
 * This is used by both new and edit pages
 *
 * */
if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}
require_once($CFG->libdir . '/formslib.php');

class training_history_edit_form extends moodleform {

    protected $training_history;

    function definition() {
        global $USER, $courseid, $DB, $PAGE, $config;
        $mform = & $this->_form;
        $context = context_system::instance();
        $membership_id = optional_param('membership_id', 0, PARAM_INT);
        $debug = optional_param('debug', 0, PARAM_INT);
        $type = optional_param('type', 'activity', PARAM_TEXT);
        if (isset($_REQUEST['id'])) {
            $id = $_REQUEST['id'];
            $q = "select DISTINCT a.* , mdl_androgogic_activities.name as activity, mdl_files.contextid, mdl_files.component, mdl_files.filearea, mdl_files.filename, mdl_files.itemid, mdl_files.id as fileid, CONCAT(mdl_user.firstname,' ',mdl_user.lastname) as user 
                from mdl_androgogic_training_history a 
                LEFT JOIN mdl_androgogic_activities  on a.activity_id = mdl_androgogic_activities.id
                LEFT JOIN mdl_files  on a.file_id = mdl_files.itemid and mdl_files.itemid != 0 and mdl_files.filename != '.'
                LEFT JOIN mdl_user  on a.user_id = mdl_user.id
                where a.id = $id ";

            $training_history = $DB->get_record_sql($q);
            if ($training_history->qualification_id) {
                $type = 'qualification';
            } elseif ($training_history->membership_id) {
                $type = 'membership';
            }
        } else {
            $training_history = $this->_customdata['$training_history']; // this contains the data of this form
        }
        $tab = 'training_history_new'; // from whence we were called
        if (!empty($training_history->id)) {
            $tab = 'training_history_edit';
        }
        $mform->addElement('html', '<div>');

//title_of_training
        if ($type == 'activity') {
            $mform->addElement('text', 'title_of_training', get_string('title_of_training', 'block_androgogic_training_history'), array('size' => 255));
            $mform->addRule('title_of_training', get_string('required'), 'required', null, 'server');
            $mform->addRule('title_of_training', 'Maximum 255 characters', 'maxlength', 255, 'client');
        }
//activity_id
        if ($type == 'activity') {
            $options = $DB->get_records_menu('androgogic_activities', null, 'name', 'id,name'); //sometimes this needs a manual tweak, if the 2nd col is mainly nulls
            $mform->addElement('select', 'activity_id', get_string('activity', 'block_androgogic_training_history'), $options);
        } else if ($type == 'qualification') {
            $options = $DB->get_records_menu('androgogic_training_history_qualifications', null, 'name', 'id,name');
            $mform->addElement('select', 'qualification_id', get_string('qualification', 'block_androgogic_training_history'), $options);
        } else if ($type == 'membership') {
            $url = "index.php?tab=$tab&type=$type";
            if (isset($id)) {
                $url .= "&id=$id";
            }
            $url .= "&membership_id=";
            $attributes = array("onchange" => "this.options[this.selectedIndex].value && (window.location = '$url' + this.options[this.selectedIndex].value);");
            $options = $DB->get_records_menu('androgogic_training_history_membership', null, 'name', 'id,name');
            $mform->addElement('select', 'membership_id', get_string('membership', 'block_androgogic_training_history'), $options, $attributes);
            // designations
            if ($membership_id != 0) {
                $conditions = array('membership_id' => $membership_id);
            } else {
                $conditions = array();
            }
            $empty_option = array(0=>'');
            $options = $DB->get_records_menu('androgogic_training_history_professional_designations', $conditions, 'name', 'id,name');
            $menu_options = $empty_option+$options;
            $mform->addElement('select', 'professional_designation_id', get_string('professional_designation', 'block_androgogic_training_history'), $menu_options);
        }
        if ($type == 'activity' or $type == 'qualification') {
            //date_issued
            $mform->addElement('date_selector', 'date_issued', get_string('date_issued', 'block_androgogic_training_history'));
            $mform->addRule('date_issued', get_string('required'), 'required', null, 'server');
        } elseif ($type == 'membership') {
            $mform->addElement('date_selector', 'date_expiry', get_string('date_expiry', 'block_androgogic_training_history'));
            $mform->addRule('date_expiry', get_string('required'), 'required', null, 'server');
        }
//file_id
        if (empty($training_history->file_id)) {
            $mform->addElement('filepicker', 'file_id', get_string('file', 'block_androgogic_training_history'));
            //$mform->addRule('file_id', get_string('required'), 'required', null, 'server');
        } else {
            $file_link = new moodle_url("/pluginfile.php/{$training_history->contextid}/{$training_history->component}/{$training_history->filearea}/" . $training_history->itemid . '/' . $training_history->filename);

            $mform->addElement('static', 'file_id', get_string('file', 'block_androgogic_training_history'), '');
            $mform->addElement('button', 'editthisfile', get_string('editthisfile', 'block_androgogic_training_history'), array('onclick' => 'location.href="index.php?tab=upload&fileid=' . $training_history->fileid . '&object=training_history&objectid=' . $training_history->id . '&block_name=block_androgogic_training_history&table_name=androgogic_training_history&foreign_key_name=file_id"'));
        }

        if (has_capability('block/androgogic_training_history:admin', $context) || block_androgogic_training_history_is_manager()) {
//user_id
            $and = '';
            if (block_androgogic_training_history_is_manager() and !has_capability('block/androgogic_training_history:admin', $context)) {
                $and .= " and ( mdl_user.id in (select userid from mdl_pos_assignment where managerid = $USER->id) OR mdl_user.id = $USER->id )";
            }
            $sql = "select id, CONCAT(firstname,' ',lastname) as fullname from mdl_user where deleted = 0 $and ORDER BY firstname, lastname";
            $results = $DB->get_records_sql_menu($sql);
            $mform->addElement('select', 'user_id', get_string('user', 'block_androgogic_training_history'), $results);
            $mform->addRule('user_id', get_string('required'), 'required', null, 'server');
            if ($config->approval_workflow and $type != 'activity' && has_capability('block/androgogic_training_history:admin', $context)) {
                //approved
                $mform->addElement('selectyesno', 'approved', get_string('approved', 'block_androgogic_training_history'));
            }
        } else {
            $mform->addElement('hidden', 'user_id', $USER->id);
        }
        if ($config->approval_workflow) {
            if ($type == 'activity') {
                //assessed
                $mform->addElement('selectyesno', 'assessed', get_string('assessed', 'block_androgogic_training_history'));
                //assessment_code
                $mform->addElement('text', 'assessment_code', get_string('assessment_code', 'block_androgogic_training_history'), array('size' => 50));
                $mform->addRule('assessment_code', 'Maximum 50 characters', 'maxlength', 50, 'client');
                $mform->disabledIf('assessment_code', 'assessed', 'eq', '0');
            }
            if ($type != 'membership') {
                //provider_id
                $sql = "select id, name from mdl_androgogic_training_history_providers ORDER BY name";
                $results = $DB->get_records_sql_menu($sql);
                $mform->addElement('select', 'provider_id', get_string('provider', 'block_androgogic_training_history'), $results);
                if ($type == 'activity') {
                    $mform->disabledIf('provider_id', 'assessed', 'eq', '0');
                }
            }
        }

        if ($type == 'activity') {
            block_androgogic_training_history_course_edit_form($mform);
        }
        if ($membership_id != 0) {
            $mform->setConstant('membership_id', $membership_id);
        }
        //set values if we are in edit mode
        if (!empty($training_history->id) && isset($_GET['id'])) {
            $fields_to_check = array('title_of_training','activity_id','date_issued','date_expiry','provider_id',
                'professional_designation_id','membership_id','qualification_id','approved'
                );
            foreach ($fields_to_check as $field){
                if (isset($mform->_elementIndex[$field])) {
                    $mform->setConstant($field, $training_history->$field);
                }
            }
            
            if (isset($file_link)) {
                $mform->setConstant('file_id', html_writer::link($file_link, $training_history->filename));
            }
            $mform->setConstant('user_id', $training_history->user_id);
            if (has_capability('block/androgogic_training_history:admin', $context)) {
                if (isset($mform->_elementIndex['assessed'])) {
                    $mform->setConstant('assessed', $training_history->assessed);
                }
            }
            if (isset($mform->_elementIndex['assessment_code'])) {
                $mform->setConstant('assessment_code', $training_history->assessment_code);
            }
            if ($type == 'activity') {
                $sql = "SELECT * FROM mdl_androgogic_training_history_competencies_cpd_points cp
                    where cp.deleted = 0 AND training_history_id = $training_history->id";
                $cpd_items = $DB->get_records_sql($sql);
                foreach ($cpd_items as $cpd_item) {
                    $form_element = 'cpd_points_' . $cpd_item->competency_id;
                    if (isset($mform->_elementIndex[$form_element])) {
                        $mform->setConstant($form_element, $cpd_item->cpd_points);
                    }
                }
                if ($config->do_cpe_hours) {
                    $sql = "SELECT * FROM mdl_androgogic_training_history_competencies_cpe_hours cp
                    where cp.deleted = 0 AND training_history_id = $training_history->id";
                    $cpe_items = $DB->get_records_sql($sql);
                    foreach ($cpe_items as $cpe_item) {
                        $mform->setConstant('cpe_hours_' . $cpe_item->competency_id, $cpe_item->cpe_hours);
                    }
                }
                //many to many columns
                $result = $DB->get_records('androgogic_training_history_dimensions', array('training_history_id' => $training_history->id));
                $dimension_ids = array();
                foreach ($result as $row) {
                    $dimension_ids[] = $row->dimension_id;
                }
                if (count($dimension_ids) > 0) {
                    $mform->setConstant('dimension_id', $dimension_ids);
                }
            }
        }

//hiddens
        $mform->addElement('hidden', 'tab', $tab);
        $mform->addElement('hidden', 'type', $type);
        $mform->addElement('hidden', 'debug', $debug);
        if (isset($id)) {
            $mform->addElement('hidden', 'id', $id);
        }
        $this->add_action_buttons(false);
        $mform->addElement('html', '</div>');
    }

}
