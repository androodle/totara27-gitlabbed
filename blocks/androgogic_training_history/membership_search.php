<?php

/** 
 * Androgogic Training History Block: Search 
 * 
 * @author      Daniel Morphett <dan@androgogic.com> 
 * @version     06/11/2014 
 * @copyright   2014+ Androgogic Pty Ltd <http://www.androgogic.com> 
 * 
 * Search memberships
 * Also provides access to edit and delete functions if user has sufficient permissions
 *  
 **/

//params
$sort   = optional_param('sort', 'name', PARAM_RAW);
$dir    = optional_param('dir', 'ASC', PARAM_ALPHA);
$page   = optional_param('page', 0, PARAM_INT);
$perpage= optional_param('perpage', 20, PARAM_INT); 
$search = optional_param('search', '', PARAM_TEXT);
$tab    = optional_param('tab', 'membership_search', PARAM_FILE);
$course_id = optional_param('course_id', 0, PARAM_INT); 

// prepare url for paging bar
$PAGE->set_url($PAGE->url, compact('sort', 'dir', 'page','perpage','search','tab')); 
// prepare columns for results table
$columns = array(
"name",
"course",

);
foreach ($columns as $column) {
$string[$column] = get_string("$column",'block_androgogic_training_history');
if ($sort != $column) {
$columnicon = '';
$columndir = 'ASC';
} else {
$columndir = $dir == 'ASC' ? 'DESC':'ASC';
$columnicon = $dir == 'ASC' ? 'down':'up';
}
if($column != 'details'){
$$column = "<a href='$PAGE->url&amp;dir=$columndir&amp;sort=$column'>$string[$column]</a>";
}
else{
 $$column = $string[$column];
}
}
//figure out the and clause from what has been submitted
$and = '';
if($search != ''){
$and .= " and a.name like '%$search%'";
} 
//are we filtering on course?
if($course_id > 0){ 
$and .= " and mdl_course.id = $course_id ";
} 
$q = "select DISTINCT a.* , mdl_course.fullname as course 
from mdl_androgogic_training_history_membership a 
LEFT JOIN mdl_course  on a.course_id = mdl_course.id
where 1 = 1 
$and 
order by $sort $dir";
if($debug==1){echo '$query : ' . $q . ''   ;}
if($download == ''){
    //get a page worth of records
    $results = $DB->get_records_sql($q, array(), $page * $perpage, $perpage);
}
else{
    $results = $DB->get_records_sql($q);
}
//also get the total number we have of these
$q = "SELECT COUNT(DISTINCT a.id)
from mdl_androgogic_training_history_membership a 
LEFT JOIN mdl_course  on a.course_id = mdl_course.id
 where 1 = 1  $and";

if($debug==1){echo '$query : ' . $q . '<br>'   ;}

$result_count = $DB->get_field_sql($q);
if($download == ''){

require_once('membership_search_form.php');
$mform = new membership_search_form(null, array('sort' => $sort,'dir'=>$dir,'perpage'=>$perpage,'search'=>$search,'tab'=>$currenttab ,'course_id'=>$course_id, 'debug'=>$debug));
$mform->display();
echo '<table width="100%"><tr><td width="50%">';
echo $result_count . ' ' . get_string('membership_plural','block_androgogic_training_history') . " found" . '<br>';
echo '</td><td style="text-align:right;">';
if(has_capability('block/androgogic_training_history:admin', $context)){
echo "<a href='index.php?tab=membership_new'>" . get_string('membership_new','block_androgogic_training_history') . "</a>";
}
echo '</td></tr></table>';

}

//RESULTS
if (!$results) {
if($download == ''){
echo $OUTPUT->heading(get_string('noresults','block_androgogic_training_history',$search));
}
} else {
$table = new html_table();
$table->head = array (
$name,
$course,

'Action'
);
$table->align = array ("left","left","left","left","left","left","left","left",);
$table->width = "95%";
$table->size = array("13%","13%","13%","13%","13%","13%","13%","13%",);
foreach ($results as $result) {
$view_link = '';//enable if wanted: "<a href='index.php?tab=membership_view&id=$result->id'>View</a> "; 
$edit_link = "";
$delete_link = "";
if(has_capability('block/androgogic_training_history:admin', $context)){
// then we show the edit link
$edit_link = "<a href='index.php?tab=membership_edit&id=$result->id'>Edit</a> ";
}
if(has_capability('block/androgogic_training_history:admin', $context)){
// then we show the delete link
$delete_link = "<a href='index.php?tab=membership_delete&id=$result->id' onclick='return confirm(\"Are you sure you want to delete this item?\")'>Delete</a> ";
}
$table->data[] = array (
"$result->name",
html_writer::link(new moodle_url('/course/view.php',array('id'=>$result->course_id)),$result->course,array('target'=>'_blank')),
$view_link . $edit_link . $delete_link
);
}
}
if(!empty($table)){
if ($download != '') {
//export the table to whatever they asked for
block_androgogic_training_history_export_data($download,$table,$tab);
}
else {
echo html_writer::table($table);
$pagingbar = new paging_bar($result_count, $page, $perpage,$PAGE->url);
$pagingbar->pagevar = 'page';
echo $OUTPUT->render($pagingbar);
echo 'Download: ';    
$download_report_url = $PAGE->url . "&download=csv";
echo html_writer::link($download_report_url, 'csv');
echo "&nbsp;";
$download_report_url = $PAGE->url . "&download=excel";
echo html_writer::link($download_report_url, 'excel');
}
}

