<?php

/**
 * Androgogic Training History Block: CPD Report
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     17/06/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 * Report on user's CPD points
 *
 * */
//params
$sort = optional_param('sort', 'date_issued', PARAM_RAW);
$dir = optional_param('dir', 'DESC', PARAM_ALPHA);
$page = optional_param('page', 0, PARAM_INT);
$perpage = optional_param('perpage', 50, PARAM_INT);
$tab = optional_param('tab', 'cpd_report', PARAM_TEXT);
$user_search = optional_param('user_search', '', PARAM_TEXT);
$assessed_only = optional_param('assessed_only', false, PARAM_BOOL);
$androgogic_activities_id = optional_param('androgogic_activities_id', 0, PARAM_INT);
$dimension_id = optional_param('dimension_id', 0, PARAM_INT);
$competency_id = optional_param('competency_id', 0, PARAM_INT);
$user_id = optional_param('user_id', 0, PARAM_INT);
$startdate = optional_param('startdate', 0, PARAM_INT); //this one is the array that comes in from the date filter. This throws a debugging message, but works as we want it to
$startdate_int = optional_param('startdate_int', '0', PARAM_INT);
$enddate = optional_param('enddate', 0, PARAM_INT); //ditto
$enddate_int = optional_param('enddate_int', '0', PARAM_INT);
$org_id = optional_param('org_id', 0, PARAM_INT);
$pos_id = optional_param('pos_id', 0, PARAM_INT);
$show_suspended_users = optional_param('show_suspended_users', 0, PARAM_INT);

// this report can be long winded...
set_time_limit(30000);
raise_memory_limit('4G');

if (isset($startdate['enabled'])) {
    //make it into a unix time
    $startdate = mktime(0, 0, 0, $startdate['month'], $startdate['day'], $startdate['year']);
    $startdate_int = $startdate;
} 
else if($startdate_int > 0){
    //might be coming in on the url via paging
    $startdate = $startdate_int;
}
if (isset($enddate['enabled'])) {
    //make it into a unix time
    $enddate = mktime(0, 0, 0, $enddate['month'], $enddate['day'], $enddate['year']) + 1;
    $enddate_int = $enddate;
}
else if($enddate_int > 0){
    //might be coming in on the url via paging
    $enddate = $enddate_int;
}
// prepare url for paging bar
$PAGE->set_url($PAGE->url, compact('sort', 'dir', 'page', 'perpage', 'tab','androgogic_activities_id','user_id','user_search','dimension_id','competency_id','assessed_only','startdate_int','enddate_int'));

//figure out the 'and' clause from what has been submitted
$and_training_history = '';
$and_course_completion = '';
$and_ka = '';

$show_total = false;

//is the user a practice viewer?
$is_practice_viewer = has_capability('block/androgogic_training_history:see_members_of_own_practice',$context);

//are we filtering on user?
if (!has_capability('block/androgogic_training_history:admin', $context)) {
    if ($is_practice_viewer) {
    	//get the practice (org)
        $org = $DB->get_record('pos_assignment',array('userid'=>$USER->id,'type'=>1));
        if($org){
           $and_training_history .= " and ( u.id in (select userid from mdl_pos_assignment where organisationid = $org->organisationid) OR u.id = $USER->id )";
           $and_course_completion = $and_training_history;
        }
        else {
            $user_id = $USER->id;
        }
        
    } else {
        $user_id = $USER->id; 
        $user_org_pos = block_androgogic_training_history_get_user_org_pos($USER);
        if($user_org_pos and $download == ''){
            $org_pos_period = block_androgogic_training_history_get_org_pos_period(
                $user_org_pos->org_framework_id,$user_org_pos->pos_framework_id,
                $user_org_pos->pos_sortthread,$user_org_pos->org_sortthread, $USER->id);
            if($org_pos_period){
                list($period_start_date,$period_end_date) = block_androgogic_training_history_get_cpd_period($USER, $org_pos_period);
                if($startdate == 0){
                    $startdate_int = $startdate = $period_start_date;
                }
                if($enddate == 0){
                    $enddate_int = $enddate = $period_end_date;
                }
            }   
        }
    }
}

// prepare columns for results table
$columns = array(
    "title_of_training",
    "activity",
    "provider",
    "date_issued",
    "total_cpd_points",
    "assessed", 
    "assessment_code", 
    
);
if ($user_search != '') {
    //reset user id 
    $user_id = 0;
}

$columns[] = "user";
$columns[] = "username";

foreach ($columns as $column) {
    $string[$column] = get_string("$column", 'block_androgogic_training_history');
    if ($sort != $column) {
        $columnicon = '';
        $columndir = 'ASC';
    } else {
        $columndir = $dir == 'ASC' ? 'DESC' : 'ASC';
        $columnicon = $dir == 'ASC' ? 'down' : 'up';
    }
    if ($column != 'details') {
        $$column = "<a href='$PAGE->url&amp;dir=$columndir&amp;sort=$column'>$string[$column]</a>";
    } else {
        $$column = $string[$column];
    }
}


//are we filtering on androgogic_activities?
if ($androgogic_activities_id > 0) {
    $and_training_history .= " and mdl_androgogic_activities.id = $androgogic_activities_id ";
    //let's see which one they picked
    $chosen_activity = $DB->get_record('androgogic_activities', array('id' => $androgogic_activities_id));
    if ($chosen_activity->name != 'LMS course') {
        $and_course_completion .= " and 1 = 0 ";
    }
}
if ($dimension_id > 0) {
    $and_training_history .= " and a.id IN ( select training_history_id from mdl_androgogic_training_history_dimensions where dimension_id = $dimension_id )";
    $and_course_completion .= " and c.id IN ( select course_id from mdl_androgogic_course_dimensions where dimension_id = $dimension_id )";
    $and_ka .= " and c.id IN ( select course_id from mdl_androgogic_course_dimensions where dimension_id = $dimension_id )";
}

if ($competency_id > 0) {
    $and_training_history .= " and cpd.competency_id = $competency_id ";    
    $and_course_completion .= " and cpd.competency_id = $competency_id "; 
    $and_ka .= " and c.id = $competency_id "; 
}

if ($user_id > 0) {
    $and_training_history .= " and u.id = $user_id ";
    $and_course_completion .= " and u.id = $user_id ";
    $perpage = 1000;
    $show_total = true;
}
else if(empty($_POST) and !isset($_GET['page'])){
    $and_training_history .= " and 1=0 ";
    $and_course_completion .= " and 1=0 ";
}
if ($user_search != '') {
    $user_search = str_ireplace("'", "\'", $user_search);
    $and_training_history .= " and concat(firstname, ' ', lastname, ' ', username) like '%$user_search%' ";
    $and_course_completion .= " and concat(firstname, ' ', lastname, ' ', username) like '%$user_search%' ";
}
if (isset($startdate) and is_integer($startdate) and $startdate > 0) {
    $filter_startdate = $startdate - (60*60*24);
    $and_training_history .= " and (a.date_issued > $filter_startdate)";
    $and_course_completion .= " and (cc.timecompleted > $filter_startdate)";
}
if (isset($enddate) and is_integer($enddate) and $enddate > 0) {
    $and_training_history .= " and (a.date_issued < $enddate)";
    $and_course_completion .= " and (cc.timecompleted < $enddate)";
}
if($assessed_only){
    $and_training_history .= " and a.assessed = 1";
}
if ($pos_id > 0) {
    $and_training_history .= " AND u.id IN (SELECT userid FROM mdl_pos_assignment WHERE positionid = $pos_id )";
    $and_course_completion .= " AND u.id IN (SELECT userid FROM mdl_pos_assignment WHERE positionid = $pos_id )";   
}
if ($org_id > 0) {
    $and_training_history .= " AND u.id IN (SELECT userid FROM mdl_pos_assignment pa INNER JOIN mdl_org o ON pa.organisationid = o.id WHERE o.parentid = $org_id OR o.id = $org_id )";
    $and_course_completion .= " AND u.id IN (SELECT userid FROM mdl_pos_assignment pa INNER JOIN mdl_org o ON pa.organisationid = o.id WHERE o.parentid = $org_id OR o.id = $org_id )";    
}
$filters_chosen = true;
if($and_training_history == '' and $and_course_completion == ''){
    $and_training_history = " AND 1=0 ";
    $and_course_completion = " AND 1=0 ";
    $filters_chosen = false;
}
// this filter has to come after the check for filters chosen, because it is not enough of a filter to stop the bad performance
if ($show_suspended_users == 0) {
    $and_training_history .= " and u.suspended = 0 ";
    $and_course_completion .= " and u.suspended = 0 ";    
}
//need this fancy sequencing to allow both the join to work and to cater for moodle's insistence on the first col being id and values being unique in it
$q = "SELECT @rownum:=@rownum+1 id, title_of_training, provider, date_issued, total_cpd_points, assessed, assessment_code,activity, user, username, training_history_id, user_id, course_id
    FROM(
SELECT DISTINCT  a.title_of_training, p.name as provider, a.date_issued, sum(cpd_points) as total_cpd_points, a.assessed, a.assessment_code,  
mdl_androgogic_activities.name AS activity, CONCAT(u.firstname,' ',u.lastname) AS user, username, a.id as training_history_id, a.user_id, 0 as course_id
FROM mdl_androgogic_training_history a 
INNER JOIN mdl_androgogic_training_history_competencies_cpd_points cpd ON a.id = cpd.training_history_id
LEFT JOIN mdl_androgogic_training_history_providers p on a.provider_id = p.id
LEFT JOIN mdl_androgogic_activities  ON a.activity_id = mdl_androgogic_activities.id
LEFT JOIN mdl_user u ON a.user_id = u.id
WHERE cpd.deleted = 0 and u.deleted = 0
$and_training_history
group by a.title_of_training, p.name, a.date_issued, a.assessed, a.assessment_code,  
mdl_androgogic_activities.name, u.firstname,u.lastname, username, a.id, a.user_id
UNION
SELECT c.fullname AS title_of_training, '{$SITE->shortname}' AS provider, cc.timecompleted AS date_issued, sum(cpd_points) as total_cpd_points, '1', '',
'LMS course' AS activity, CONCAT(u.firstname,' ',u.lastname) AS user, username, 0, u.id as user_id, cc.course as course_id
FROM mdl_course_completions cc
INNER JOIN mdl_course c ON cc.course = c.id
INNER JOIN mdl_androgogic_training_history_course_competencies_cpd_points cpd on c.id = cpd.course_id
LEFT JOIN mdl_user u ON cc.userid = u.id
WHERE cpd.deleted = 0 AND STATUS >= 50 and u.deleted = 0
$and_course_completion
group by c.fullname, cc.timecompleted, u.firstname,u.lastname, username,u.id
) b , (SELECT @rownum:=0) r  
ORDER BY $sort $dir";
if ($debug==1) {
    echo '$query : ' . $q . '';
}
if($download == ''){
    //get a page worth of records
    $results = $DB->get_records_sql($q, array(), $page * $perpage, $perpage);
}
else{
    $results = $DB->get_records_sql($q);
}
if ($user_id > 0) {
    $subject_user = $DB->get_record('user', array('id' => $user_id));
}       
if($download == ''){
    //also get the total number we have of these
    $results_for_count = $DB->get_records_sql($q);
    $result_count = count($results_for_count);
    echo $OUTPUT->heading(get_string('cpd_report_heading', 'block_androgogic_training_history'));

    if ($user_id > 0) {
        echo $OUTPUT->heading(fullname($subject_user));
        echo $OUTPUT->heading(fullname($subject_user->idnumber));
    }

    require_once('cpd_report_form.php');
    $mform = new cpd_report_form(null, array('sort' => $sort, 'dir' => $dir, 'perpage' => $perpage, 'tab' => $currenttab, 
        'androgogic_activities_id' => $androgogic_activities_id, 'user_id' => $user_id, 'context' => $context, 
	'debug'=>$debug, 'startdate'=>$startdate_int, 'enddate'=>$enddate_int, 'is_practice_viewer'=>$is_practice_viewer));
    $mform->display();
    if ($results){
        echo '<table width="100%"><tr><td width="50%">';
        echo $result_count . ' ' . get_string('training_history_plural', 'block_androgogic_training_history') . " found" . '<br>';
        echo '</td><td style="text-align:right;">';
        echo '</td></tr></table>';
    }
}

//RESULTS
if (!$results) { 
    if($download == '' and !empty($_POST) and $filters_chosen){
        echo $OUTPUT->heading(get_string('noresults', 'block_androgogic_training_history'));
    }
} else {
    $kas = array();
    foreach ($results as $result) {
        //any KAs for this one?
        if($result->training_history_id != 0){
            $sql = "SELECT cpd.*, c.fullname "
                    . "from mdl_androgogic_training_history_competencies_cpd_points cpd "
                    . "inner join mdl_comp c on cpd.competency_id = c.id "
                    . "where cpd.deleted = 0 AND cpd.training_history_id = " . $result->training_history_id . " $and_ka";
            $these_kas = $DB->get_records_sql($sql);
        }
        else if($result->course_id != 0){
            $sql = "SELECT cpd.*, c.fullname "
                    . "from mdl_androgogic_training_history_course_competencies_cpd_points cpd "
                    . "inner join mdl_comp c on cpd.competency_id = c.id "
                    . "where cpd.deleted = 0 AND cpd.course_id = " . $result->course_id . " $and_ka";
            $these_kas = $DB->get_records_sql($sql);
            //get assessment code if any
            $sql = "select d.data from mdl_course_info_field cf
                inner join mdl_course_info_data d on cf.id = d.fieldid
                where cf.shortname = 'assessmentcode'
                and courseid = $result->course_id";
            $this_assessment_code = $DB->get_field_sql($sql);
            if($this_assessment_code){
                $result->assessment_code = $this_assessment_code;
            }
        }
        
        $kas = array_merge($kas,$these_kas);
        
    }
    //need to get a unique set of ka fullnames from the set now
    $ka_fullnames = array();
    foreach($kas as $ka){
        if(!in_array($ka->fullname, $ka_fullnames)){
            $ka_fullnames[] = $ka->fullname;
        }
    }
    $table = new html_table();
    $table->head = array();
    if($download == ""){
        
        $table->head[] = $user;
        $table->head[] = $username;
        
        $table->head[] = $title_of_training;
        $table->head[] = $activity;
        $table->head[] = $provider;
        $table->head[] = $date_issued;    
        $table->head[] = $assessed; 
        $table->head[] = $assessment_code;
    }
    else{
        
        $table->head[] = get_string('user','block_androgogic_training_history');
        $table->head[] = get_string('username','block_androgogic_training_history');
        
        $table->head[] = get_string('title_of_training','block_androgogic_training_history');
        $table->head[] = get_string('activity','block_androgogic_training_history');
        $table->head[] = get_string('provider','block_androgogic_training_history');
        $table->head[] = get_string('date_issued','block_androgogic_training_history');    
        $table->head[] = get_string('assessed','block_androgogic_training_history'); 
        $table->head[] = get_string('assessment_code','block_androgogic_training_history');
    }
    $table->head[] = $total_cpd_points;
    foreach($ka_fullnames as $ka_fullname){
        $table->head[] = $ka_fullname;
    }
    
    
    $total_points = 0;
    foreach ($results as $result) {
        $td_array = array();
        if ($user_id == 0) {
            $td_array[] = html_writer::link($PAGE->url . '&user_id=' . $result->user_id, $result->user);
            $td_array[] = $result->username;
        }
        else{
            $td_array[] = fullname($subject_user);
            $td_array[] = $subject_user->username;
        }
        
        $td_array[] = $result->title_of_training;
        $td_array[] = $result->activity;
        $td_array[] = $result->provider;
        $td_array[] = date('d-m-Y', $result->date_issued);
        $td_array[] = $result->assessed;
        $td_array[] = $result->assessment_code;            
        $td_array[] = $result->total_cpd_points; 
        //can we match up the record to its kas?
        foreach($kas as $ka){
            if(isset($ka->training_history_id)){
                if($ka->training_history_id == $result->training_history_id){
                    //run through the table columns and figure out which one we want to assign the points to
                    $col_index = 0;
                    foreach($table->head as $column_header){
                        if($ka->fullname == $column_header){
                            $td_array[$col_index] = $ka->cpd_points;
                        }
                        elseif (!isset($td_array[$col_index])){
                            $td_array[$col_index] = '';
                        }
                        $col_index++;
                    }
                }
            }
            if(isset($ka->course_id)){
                if($ka->course_id == $result->course_id){
                    //run through the table columns and figure out which one we want to assign the points to
                    $col_index = 0;
                    foreach($table->head as $column_header){
                        if($ka->fullname == $column_header){
                            $td_array[$col_index] = $ka->cpd_points;
                        }
                        elseif (!isset($td_array[$col_index])){
                            $td_array[$col_index] = '';
                        }
                        $col_index++;
                    }
                }
            }
        }
        //any missing array cells? if so, assign empty string to them
        for ($index1 = 0; $index1 < count($table->head); $index1++) {
            if(!isset($td_array[$index1])){
                $td_array[$index1] = '';
            }
        }
        $table->data[] = $td_array;
        $total_points += $result->total_cpd_points;
    }
    if ($show_total) {
        //total row
        $total_row = array();
        $total_row[] = "<strong>Total</strong>";
        for ($index = 1; $index < $col_index-1; $index++) {
            $total_row[] = "";
        }
        $total_row[$col_index] = "<strong>$total_points</strong>";
        $table->data[] = $total_row;
    }
}
if($download != ''){
    //export the table to whatever they asked for
    block_androgogic_training_history_export_data($download,$table,$tab,$report_name_lang);
}
elseif (!empty($table)) {
    echo html_writer::table($table);
    $pagingbar = new paging_bar($result_count, $page, $perpage, $PAGE->url);
    $pagingbar->pagevar = 'page';
    echo $OUTPUT->render($pagingbar);
    block_androgogic_training_history_output_download_links($PAGE->url,'cpd_report_heading');
}
if(!$filters_chosen){
    echo $OUTPUT->notification("Please choose a filter");
}
?>
