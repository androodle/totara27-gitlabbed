<?php

/** 
 * Androgogic Training History Block: Edit object 
 * 
 * @author      Daniel Morphett <dan@androgogic.com> 
 * @version     06/11/2014 
 * @copyright   2014+ Androgogic Pty Ltd <http://www.androgogic.com> 
 * 
 * Edit one of the qualifications
 *  
 **/

global $OUTPUT;
require_capability('block/androgogic_training_history:admin', $context);
require_once('qualification_edit_form.php');
$id = required_param('id', PARAM_INT);
$q = "select DISTINCT a.* , mdl_course.fullname as course 
from mdl_androgogic_training_history_qualifications a 
LEFT JOIN mdl_course  on a.course_id = mdl_course.id
where a.id = $id ";
$qualification = $DB->get_record_sql($q);
$mform = new qualification_edit_form();
if ($data = $mform->get_data()){
$data->id = $id;
$data->modified_by = $USER->id;
$data->date_modified = date('Y-m-d H:i:s');
//check for unique
    $is_unique = block_androgogic_training_history_record_unique($data,'qualification');
    if (!$is_unique) {
        echo $OUTPUT->notification(get_string('qualificationnotunique', 'block_androgogic_training_history'), 'notifyfailure');
    } else {
        $DB->update_record('androgogic_training_history_qualifications',$data);
        echo $OUTPUT->notification(get_string('datasubmitted','block_androgogic_training_history'), 'notifysuccess');
    }
}
else{
echo $OUTPUT->heading(get_string('qualification_edit', 'block_androgogic_training_history'));
$mform->display();
}

