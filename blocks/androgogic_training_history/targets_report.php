<?php

/**
 * Androgogic Training History Block: CPD/CPE targets Report
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     17/06/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 * Report on user's CPD/CPE points and targets
 *
 * */
//params
$sort = optional_param('sort', 'user_id', PARAM_RAW);
$dir = optional_param('dir', 'ASC', PARAM_ALPHA);
$page = optional_param('page', 0, PARAM_INT);
$perpage = optional_param('perpage', 50, PARAM_INT);
$tab = optional_param('tab', 'targets_report', PARAM_TEXT);
$user_search = optional_param('user_search', '', PARAM_TEXT);
$org_id = optional_param('org_id', 0, PARAM_INT);
$dimension_id = optional_param('dimension_id', 0, PARAM_INT);
$competency_id = optional_param('competency_id', 0, PARAM_INT);
$user_id = optional_param('user_id', 0, PARAM_INT);
$startdate = optional_param('startdate', 0, PARAM_INT); //this one is the array that comes in from the date filter. This throws a debugging message, but works as we want it to
$startdate_int = optional_param('startdate_int', '0', PARAM_INT);
$enddate = optional_param('enddate', 0, PARAM_INT); //ditto
$enddate_int = optional_param('enddate_int', '0', PARAM_INT);
$show_suspended_users = optional_param('show_suspended_users', 0, PARAM_INT);

// this report can be long winded...
set_time_limit(30000);
raise_memory_limit('4G');

if (isset($startdate['enabled'])) {
    //make it into a unix time
    $startdate = mktime(0, 0, 0, $startdate['month'], $startdate['day'], $startdate['year']);
    $startdate_int = $startdate;
} 
else if($startdate_int > 0){
    //might be coming in on the url via paging
    $startdate = $startdate_int;
}
if (isset($enddate['enabled'])) {
    //make it into a unix time
    $enddate = mktime(0, 0, 0, $enddate['month'], $enddate['day'], $enddate['year']) + 1;
    $enddate_int = $enddate;
}
else if($enddate_int > 0){
    //might be coming in on the url via paging
    $enddate = $enddate_int;
}
// prepare url for paging bar
$PAGE->set_url($PAGE->url, compact('sort', 'dir', 'page', 'perpage', 'tab','user_id','user_search','dimension_id','competency_id','org_id','startdate_int','enddate_int'));


//is the user a practice viewer?
$is_practice_viewer = has_capability('block/androgogic_training_history:see_members_of_own_practice',$context);
//are we filtering on user?
$and_user = '';

if (!has_capability('block/androgogic_training_history:admin', $context)) {
    if ($is_practice_viewer) {
    	//get the practice (org)
        $org = $DB->get_record('pos_assignment',array('userid'=>$USER->id,'type'=>1));
        if($org){
           $and_user .= " and ( u.id in (select userid from mdl_pos_assignment where organisationid = $org->organisationid) OR u.id = $USER->id )";
           
        }
        else {
            $user_id = $USER->id;
        }
        
    } else {
        $user_id = $USER->id; 
        $user_org_pos = block_androgogic_training_history_get_user_org_pos($USER);
        if($user_org_pos){
            $org_pos_period = block_androgogic_training_history_get_org_pos_period(
                $user_org_pos->org_framework_id,$user_org_pos->pos_framework_id,
                $user_org_pos->pos_sortthread,$user_org_pos->org_sortthread, $USER->id);
            if($org_pos_period){
                list($period_start_date,$period_end_date) = block_androgogic_training_history_get_cpd_period($USER, $org_pos_period);
                if($startdate == 0){
                    $startdate_int = $startdate = $period_start_date;
                }
                if($enddate == 0){
                    $enddate_int = $enddate = $period_end_date;
                }
            }   
        }
    }
}
if ($user_search != '') {
    //reset any existing userid - user is searching
    $user_id = 0;
}
// prepare columns for results table
if ($user_id == 0) {
    $columns = array('user','username','email','user_start_date','position','org_path','user_state');    
}
else{
   $columns = array(); 
}
foreach ($columns as $column) {
    $string[$column] = get_string("$column", 'block_androgogic_training_history');
    if ($sort != $column) {
        $columnicon = '';
        $columndir = 'ASC';
    } else {
        $columndir = $dir == 'ASC' ? 'DESC' : 'ASC';
        $columnicon = $dir == 'ASC' ? 'down' : 'up';
    }
    if ($column != 'details') {
        $$column = "<a href='$PAGE->url&amp;dir=$columndir&amp;sort=$column'>$string[$column]</a>";
    } else {
        $$column = $string[$column];
    }
}
//figure out the and clauses from what has been submitted

$and_training_history_date = '';
$and_course_completion_date = '';
$and_training_history = ""; 

//are we filtering on org?
if ($org_id > 0) {
    $and_user .= " and (o.id = $org_id or o.path like '/$org_id/%' or o.path = '/$org_id') ";    
}

if ($user_search != '') {
    $and_user .= " and concat(firstname, ' ', lastname, ' ', username) like '%$user_search%' ";
}
else if ($user_id > 0) {
    $and_user .= " and u.id = $user_id ";
}
else if(empty($_POST) and !isset($_GET['page'])){
    $and_training_history .= " and 1=0 ";
    $and_user .= " and 1=0 ";
}
if (isset($startdate) and is_integer($startdate) and $startdate > 0) {
    $filter_startdate = $startdate - (60*60*24);
    $and_training_history_date .= " and (th.date_issued > $filter_startdate)";
    $and_course_completion_date .= " and (cc.timecompleted > $filter_startdate)";
}
if (isset($enddate) and is_integer($enddate) and $enddate > 0) {
    $and_training_history_date .= " and (th.date_issued < $enddate)";
    $and_course_completion_date .= " and (cc.timecompleted < $enddate)";
}
       
if ($dimension_id > 0) {
    $and_training_history .= " and c.id IN ( SELECT id FROM mdl_comp WHERE parentid = $dimension_id )";            
}

if ($competency_id > 0) {
    $and_training_history .= " and c.id = $competency_id ";
}
if ($show_suspended_users == 0) {
    $and_user .= " and u.suspended = 0 ";
}

//package up the filters for the points and hours function later on
$ands = new stdClass();
$ands->and_training_history = $and_training_history;
$ands->and_training_history_date = $and_training_history_date;
$ands->and_course_completion_date = $and_course_completion_date;
        
//get any users with a primary pos and org (which then means they might have a cpd/cpe target)
$q = "SELECT DISTINCT pa.userid as user_id, CONCAT(u.firstname,' ',u.lastname) AS user, username, email, p.fullname as position, 
        o.fullname as practice, o.id as practice_org_id, o.path, d1.data as user_start_date, 
        d2.data as user_state  
FROM mdl_pos_assignment pa
inner join mdl_pos p on pa.positionid = p.id
inner join mdl_org o on pa.organisationid = o.id
inner join mdl_user u on pa.userid = u.id
left join (
    select data, userid from mdl_user_info_data d 
    inner join mdl_user_info_field f on d.fieldid = f.id
    where shortname = 'startdate') d1 on u.id = d1.userid
left join (
    select data, userid from mdl_user_info_data d 
    inner join mdl_user_info_field f on d.fieldid = f.id
    where shortname = 'state') d2 on u.id = d2.userid
where 1 = 1 and u.deleted = 0 
$and_user
order by $sort $dir";
if ($debug == 1) {
    echo '$query : ' . $q . '';
}
if($download == ''){
//get a page worth of records
$results = $DB->get_records_sql($q, array(), $page * $perpage, $perpage);
}
else{
    $results = $DB->get_records_sql($q);
}
if($download == ''){
    //also get the total number we have of these
    $results_for_count = $DB->get_records_sql($q);
    $result_count = count($results_for_count);

    echo $OUTPUT->heading(get_string('targets_report_heading', 'block_androgogic_training_history'));

    if ($user_id > 0) {
        $subject_user = $DB->get_record('user', array('id' => $user_id));
        echo $OUTPUT->heading(fullname($subject_user));
        echo $OUTPUT->heading(fullname($subject_user->idnumber));
    }

    require_once('targets_report_form.php');
    $mform = new targets_report_form(null, array('sort' => $sort, 'dir' => $dir, 'perpage' => $perpage, 'tab' => $currenttab, 
        'user_id' => $user_id, 'context' => $context, 'org_id' => $org_id, 'dimension_id' => $dimension_id, 'competency_id' => $competency_id,
	'debug'=>$debug, 'startdate'=>$startdate_int, 'enddate'=>$enddate_int, 'is_practice_viewer'=>$is_practice_viewer));
    $mform->display();
    if ($results){
        echo '<table width="100%"><tr><td width="50%">';
        echo $result_count .  " records found" . '<br>';
        echo '</td><td style="text-align:right;">';
        echo '</td></tr></table>';
    }
}

//RESULTS
if (!$results) {
    if($download == '' and !empty($_POST)){
        echo $OUTPUT->heading(get_string('noresults', 'block_androgogic_training_history'));
    }
} else {
    $kas = array();
    $augmented_results = array();
    foreach ($results as $result) {        
        
        //let's get their targets, for which we need the lowest hanging match on the org/pos hierarchies 
        //that matches their primary pos assignment
        $this_user = new stdClass();
        $this_user->id = $result->user_id;
        $user_org_pos = block_androgogic_training_history_get_user_org_pos($this_user);
        if($user_org_pos){
            $org_pos_period = block_androgogic_training_history_get_org_pos_period(
                    $user_org_pos->org_framework_id,$user_org_pos->pos_framework_id,
                    $user_org_pos->pos_sortthread,$user_org_pos->org_sortthread,$result->user_id);
            if($org_pos_period){
                list($period_start_date,$period_end_date) = block_androgogic_training_history_get_cpd_period($this_user, $org_pos_period);
                $result->period_start_date = $period_start_date;
                $result->period_end_date = $period_end_date;

                //let's get their targets
                $targets = block_androgogic_training_history_get_targets($this_user,$org_pos_period,$and_training_history,$period_start_date,$period_end_date);

                $result->cpd_targets = $targets->cpd_targets;           
                $result->cpe_targets = $targets->cpe_targets;            
                $result->total_cpe_target = $targets->total_cpe_target;
                $result->total_cpd_target = $targets->total_cpd_target;
                
                // and points/hours earned
                $points_and_hours = block_androgogic_training_history_get_points_and_hours($result->user_id,$ands,$period_start_date,$period_end_date);
                if($debug==1){
                    var_dump($points_and_hours);
                    var_dump($ands);
                    var_dump($period_start_date);
                    var_dump($period_end_date);
                }
                $result->cpd_knowledge_areas = $points_and_hours->cpd_knowledge_areas;
                $result->cpe_knowledge_areas = $points_and_hours->cpe_knowledge_areas;
                $result->total_completed_points = $points_and_hours->total_completed_points;
                $result->total_completed_hours = $points_and_hours->total_completed_hours;

                //these will become the basis for the columns a bit later on
                $kas = array_merge($kas,$points_and_hours->cpd_knowledge_areas);
        
        
            }
        }
        $augmented_results[] = $result;
    }
    //need to get a unique set of ka ids from the set now, so as to add col headers
    $unique_kas = array();
    foreach($kas as $ka){
        if(!in_array($ka->competency_id, array_keys($unique_kas))){
            $unique_kas[$ka->competency_id] = $ka->fullname;
        }
    }
    //add in the kas from the targets as well
    if(isset($result->cpd_targets)){
        foreach($result->cpd_targets as $cpd_target){
            if(!in_array($cpd_target->competency_id, array_keys($unique_kas))){
                $unique_kas[$cpd_target->competency_id] = $cpd_target->fullname;
            }
        }
    }
    $table = new html_table();
    
    $table->head = array();
    if ($user_id == 0) {
        $table->head[] = $user;
        $table->head[] = $username;
        $table->head[] = $email;
        $table->head[] = $user_state;
        $table->head[] = $user_start_date;
        $table->head[] = $position;
        $table->head[] = $org_path;
    } 
    
    //totals
    $table->head[] = get_string('cpd_target_total','block_androgogic_training_history');
    $table->head[] = get_string('completed_cpd_total','block_androgogic_training_history');
    $table->head[] = get_string('over_under','block_androgogic_training_history');
    $table->head[] = get_string('cpe_target_total','block_androgogic_training_history');
    $table->head[] = get_string('completed_cpe_total','block_androgogic_training_history');
    $table->head[] = get_string('over_under','block_androgogic_training_history');
    
    if($user_id != 0){
        $ka_table = new html_table();
        $ka_table->head[] = get_string('competency','block_androgogic_training_history');
        $ka_table->head[] = get_string('target_points','block_androgogic_training_history');
        $ka_table->head[] = get_string('completed_assessed_points','block_androgogic_training_history');
        $ka_table->head[] = get_string('completed_unassessed_points','block_androgogic_training_history');
        $ka_table->head[] = get_string('total_completed_points','block_androgogic_training_history');
        $ka_table->head[] = get_string('over_under','block_androgogic_training_history');
        $ka_table->head[] = get_string('percent_target_assessed','block_androgogic_training_history');
    }
    else{
        foreach($unique_kas as $unique_ka_key=>$unique_ka_value){
            $table->head[] = get_string('competency','block_androgogic_training_history');
            $table->head[] = get_string('target_points','block_androgogic_training_history');
            $table->head[] = get_string('completed_assessed_points','block_androgogic_training_history');
            $table->head[] = get_string('completed_unassessed_points','block_androgogic_training_history');
            $table->head[] = get_string('total_completed_points','block_androgogic_training_history');
            $table->head[] = get_string('over_under','block_androgogic_training_history');
            $table->head[] = get_string('percent_target_assessed','block_androgogic_training_history');
        }
    }
    
    $total_points = 0;
    foreach ($augmented_results as $result) {
        $defaults = array('total_completed_points','total_completed_hours','total_cpe_target','total_cpd_target');
        foreach ($defaults as $default) {
           if(!isset($result->$default)){
               $result->$default = 0;
           } 
        }
        //init totals values
        $cpd_target_total = 0;
        $completed_cpd_total = $result->total_completed_points;
        $over_under_cpd_totals = 0;
        //$cpe_target_total = 0;
        $completed_cpe_total = $result->total_completed_hours;
        $over_under_cpe_totals = 0;
        $td_array = array(); // this one will hold the whole row, but we have to add to it in a particular order: userdetails, then totals, then ka details
        $ka_array = array(); // this one is going to hold the series of ka values (which are repeated within the row)
        if ($user_id == 0) {
            $td_array[] = html_writer::link($PAGE->url . '&user_id=' . $result->user_id, $result->user);
            $td_array[] = $result->username;
            $td_array[] = $result->email;
            $td_array[] = $result->user_state;
            if($result->user_start_date != null){
                $td_array[] = date('d-m-Y',$result->user_start_date);
            }
            else{
                $td_array[] = '';
            }
            $td_array[] = $result->position;
            $org_hierarchy = new hierarchy();
            $org_hierarchy->shortprefix = 'org';
            $org_path_string = '';
            try{
                $org_parents = $org_hierarchy->get_item_lineage($result->practice_org_id);                
                foreach($org_parents as $op){
                    $org_path_string .= $op->fullname . " > ";
                }
            }
            catch(Exception $e){
                //forget it
            }
            $td_array[] = trim($org_path_string," > ");
        }
        
        foreach($unique_kas as $unique_ka_key=>$unique_ka_value){
            //init
            $target_points = 0;
            $completed_assessed_points = 0;
            $completed_unassessed_points = 0;
            $total_completed_points = 0;
            $over_under = 0;
            $percent_target_assessed = 0;
            //has the user got any values for this one?
            if(isset($result->cpd_knowledge_areas)){
            foreach($result->cpd_knowledge_areas as $user_knowledge_area){
                if($user_knowledge_area->competency_id == $unique_ka_key){
                    if($user_knowledge_area->assessed == 1){
                        $completed_assessed_points += $user_knowledge_area->cpd_points;
                    }
                    else{
                        $completed_unassessed_points += $user_knowledge_area->cpd_points;
                    }
                    $total_completed_points += $user_knowledge_area->cpd_points;                    
                }
            }
            }
            if(isset($result->cpd_targets)){
                foreach($result->cpd_targets as $user_target){
                    if($user_target->competency_id == $unique_ka_key){
                        $target_points += $user_target->cpd_target;
                    }
                }
            }
            if($target_points > 0 and $total_completed_points > 0){
                $percent_target_assessed = $total_completed_points*100/$target_points;
                if($percent_target_assessed > 100){$percent_target_assessed = 100;}                
            }
            else if ($target_points == 0){
                $percent_target_assessed = "N/A";
            }
            $over_under = $total_completed_points - $target_points;            
            
            $ka_array[] = $unique_ka_value;
            $ka_array[] = $target_points;
            $ka_array[] = $completed_assessed_points;
            $ka_array[] = $completed_unassessed_points;
            $ka_array[] = $total_completed_points;
            $ka_array[] = $over_under;
            if ($percent_target_assessed != 'N/A'){
                $ka_array[] = number_format($percent_target_assessed,2);
            }
            else{
                $ka_array[] = $percent_target_assessed;
            }
            if($user_id != 0){
                $ka_table->data[] = $ka_array;
                //reset it
                $ka_array = array();
            }
            
            $over_under_cpd_totals += $over_under;
            
        }
        
        
        $over_under_cpe_totals = $completed_cpe_total - $result->total_cpe_target;
        $over_under_cpd_totals = $completed_cpd_total - $result->total_cpd_target;
        
        //totals
        $td_array[] = number_format($result->total_cpd_target,2);
        $td_array[] = number_format($completed_cpd_total,2);
        $td_array[] = number_format($over_under_cpd_totals,2);
        $td_array[] = number_format($result->total_cpe_target,2);
        $td_array[] = number_format($completed_cpe_total,2);
        $td_array[] = number_format($over_under_cpe_totals,2);
        if($user_id == 0){
            foreach($ka_array as $ka_item){
                $td_array[] = $ka_item;
            }
        }
        $table->data[] = $td_array;
        
    }

}
if($download != ''){
    //export the table to whatever they asked for
    if($user_id != 0){
        block_androgogic_training_history_export_data($download,$table,$tab,$report_name_lang,$ka_table);
    }
    else{
        block_androgogic_training_history_export_data($download,$table,$tab,$report_name_lang);
    }
}
elseif (!empty($table)) {
    echo html_writer::table($table);
    if($user_id != 0){
        echo html_writer::table($ka_table);
    }
    $pagingbar = new paging_bar($result_count, $page, $perpage, $PAGE->url);
    $pagingbar->pagevar = 'page';
    echo $OUTPUT->render($pagingbar);
    block_androgogic_training_history_output_download_links($PAGE->url,'targets_report_heading');
}
?>
