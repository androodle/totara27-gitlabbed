<?php

/** 
 * Androgogic Training History Block: Search form
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     17/06/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 * Provides search form for the object.
 * This is used by search page
 *
 **/

if (!defined('MOODLE_INTERNAL')) {
die('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}
require_once($CFG->libdir . '/formslib.php');
class targets_report_form extends moodleform {
function definition() {
global $DB;
$mform =& $this->_form;
foreach($this->_customdata as $custom_key=>$custom_value){
$$custom_key = $custom_value;
}
$config = get_config('block_androgogic_training_history');
$cpd_framework_id = $config->cpd_framework_id;

$mform->addElement('html','<div>');
if(has_capability('block/androgogic_training_history:admin', $context)){
    // controls: orgs
    $dboptions = $DB->get_records_menu('org',array(),'fullname','id,fullname');
    unset($options);
    $options[0] = 'Any';
    foreach($dboptions as $key=>$value){
    $options[$key] = $value;
    }
    $select = $mform->addElement('select', 'org_id', get_string('practice','block_androgogic_training_history'), $options);
}
// dimensions
$dboptions = $DB->get_records_menu('androgogic_dimensions',array(),'name','id,name');
unset($options);
$options[0] = 'Any';
foreach($dboptions as $key=>$value){
$options[$key] = $value;
}
$select = $mform->addElement('select', 'dimension_id', get_string('dimension','block_androgogic_training_history'), $options);
//knowledge areas
$ka_type = $DB->get_field('comp_type','id',array('fullname'=>'Knowledge area'));
$dboptions = $DB->get_records_menu('comp',array('typeid'=>$ka_type,'frameworkid'=>$cpd_framework_id),'fullname','id,fullname');
unset($options);
$options[0] = 'Any';
foreach($dboptions as $key=>$value){
$options[$key] = $value;
}
$select = $mform->addElement('select', 'competency_id', get_string('competency','block_androgogic_training_history'), $options);

$mform->addElement('date_selector','startdate',get_string('startdate', 'block_androgogic_training_history'), array('optional'=>true));
$mform->addElement('date_selector','enddate',get_string('enddate', 'block_androgogic_training_history'), array('optional'=>true));
 
// if admin they can search across users
if(has_capability('block/androgogic_training_history:admin', $context) or $is_practice_viewer){
    $mform->addElement('text','user_search',get_string('cpd_report_user_search_instructions', 'block_androgogic_training_history'));
    
}
//suspended
$mform->addElement('selectyesno', 'show_suspended_users', get_string('show_suspended_users', 'block_androgogic_training_history'));

$controls = array('org_id','startdate','enddate','dimension_id','comp_type','competency_id','user_search');
foreach($controls as $control){
    if(isset($mform->_elementIndex[$control])){
        try{
            if($control == 'user_search'){
                //$mform->setConstant($control, $$control);
            }
            else{
                if($$control > 0){
                    $mform->setConstant($control, $$control);
                }
            }
        }
        catch(Exception $e){
            //do nothing
        }
    }
}
//hiddens
$mform->addElement('hidden','tab',$tab);
$mform->addElement('hidden','sort',$sort);
$mform->addElement('hidden','dir',$dir);
$mform->addElement('hidden','perpage',$perpage);
$mform->addElement('hidden','user_id',$user_id);
$mform->addElement('hidden', 'debug', $debug);
//button
$mform->addElement('submit','submit','Search');
$mform->addElement('html','</div>');
}
}
