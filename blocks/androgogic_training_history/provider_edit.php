<?php

/** 
 * Androgogic Training History Block: Edit object 
 * 
 * @author      Daniel Morphett <dan@androgogic.com> 
 * @version     07/11/2014 
 * @copyright   2014+ Androgogic Pty Ltd <http://www.androgogic.com> 
 * 
 * Edit one of the providers
 *  
 **/

global $OUTPUT;
require_capability('block/androgogic_training_history:admin', $context);
require_once('provider_edit_form.php');
$id = required_param('id', PARAM_INT);
$q = "select DISTINCT a.*  
from mdl_androgogic_training_history_providers a 
where a.id = $id ";
$provider = $DB->get_record_sql($q);
$mform = new provider_edit_form();
if ($data = $mform->get_data()){
    $data->id = $id;
    $data->modified_by = $USER->id;
    $data->date_modified = date('Y-m-d H:i:s');
    //check for unique
    $is_unique = block_androgogic_training_history_record_unique($data,'provider');
    if (!$is_unique) {
        echo $OUTPUT->notification(get_string('providernotunique', 'block_androgogic_training_history'), 'notifyfailure');
    } else {
        $DB->update_record('androgogic_training_history_providers',$data);
        echo $OUTPUT->notification(get_string('datasubmitted','block_androgogic_training_history'), 'notifysuccess');
    }
}
else{
echo $OUTPUT->heading(get_string('provider_edit', 'block_androgogic_training_history'));
$mform->display();
}

