<?php

require_once('../../config.php');

require_login();

global $DB;

error_reporting(0);

$provider_remaps = $DB->get_records('provider_remap');

foreach ($provider_remaps as $provider_remap) {
    //can we find the bad provider record?
    $bad_provider_record = $DB->get_record('androgogic_training_history_providers', array('id'=>$provider_remap->id));
    if (!$bad_provider_record){
        echo "Could not match the bad (existing) provider record by id for this remap:";
        var_dump($provider_remap);
        echo "<br>";
        echo "<br>";
        continue;
    }
    $good_provider_record = $DB->get_record('androgogic_training_history_providers', array('name'=>$provider_remap->correct_provider));
    if (!$good_provider_record){
        //then put it in
        $good_provider_record = new stdClass();
        $good_provider_record->name = $provider_remap->correct_provider;
        $good_provider_record->created_by = 2;
        $good_provider_record->date_created = date('Y-m-d h:i:s');
        $good_provider_record->id = $DB->insert_record('androgogic_training_history_providers',$good_provider_record);
    }
    if($good_provider_record->id != $bad_provider_record->id){
        //change the refs in training history to the good one
        $sql = "UPDATE mdl_androgogic_training_history set provider_id = $good_provider_record->id where provider_id = $bad_provider_record->id";
        try{
            $DB->execute($sql);
            $DB->delete_records('androgogic_training_history_providers',array('id'=>$bad_provider_record->id));
    //        echo "This remap worked out fine";
    //        var_dump($provider_remap);
        }
        catch(Exception $e){
            var_dump($e);
        }
    }
}

