var minStar = 0;
var originalminStar = 0;
var stillin = false;
YUI().use('node', 'event', 'io-base', 'json-parse', function (Y)
{
	var clickMe = Y.all(".block_course_rating .ratelink");
	var mystar  = Y.all('.block_course_rating .rating_star');
	var avgstar = Y.all('.block_course_rating .average_star');
	var totalrate = Y.one('.block_course_rating .total_rating');
	
	Y.on("domready", function()
	{
        var startStars = Y.all(".block_course_rating .ratelink .starstart");
        minStar = startStars.size();
        originalminStar =  minStar;
      }); 
	
	clickMe.on('click', function (e)
	{
		var link_node = e.target.get('parentNode');
		var link = link_node.get('href');
		// Stop the link from loading the href page, we want it to send via AJAX
		e.preventDefault();
		
		// Handle the response
		function complete(id, o, args)
		{
			var id = id; // Transaction ID.
			var data = Y.JSON.parse(o.responseText); // Response data.
			
			minStar = data["myrating"];
			originalminStar = data["myrating"];
			
			//My rating: light up the stars up until the selected star
			for(var i=1; i<=5; i++)
			{
				if(i <= data["myrating"])
				{
					if(!mystar.item(i).hasClass('starfull'))
					{
						mystar.item(i).addClass('starfull');
					}
				}
				else
				{
					if(mystar.item(i).hasClass('starfull'))
					{
						mystar.item(i).removeClass('starfull');
					}
				}
				
				//Show the average stars if previously unrated
				if(avgstar.item(i-1).hasClass('starhide'))
				{
					avgstar.item(i-1).removeClass('starhide');
				}
				
				//Update the average rating
				if(i <= data["avgrating"])
				{
					if(!avgstar.item(i-1).hasClass('starfull'))
					{
						avgstar.item(i-1).addClass('starfull');
					}
				}
				else
				{
					if(avgstar.item(i-1).hasClass('starfull'))
					{
						avgstar.item(i-1).removeClass('starfull');
					}
				}
			}
			
			//Toggle labels
			if(Y.one('.block_course_rating .no_label') && !Y.one('.block_course_rating .no_label').hasClass('starhide'))
			{
				Y.one('.block_course_rating .no_label').addClass('starhide');
			}
			if(Y.one('.block_course_rating .avg_label').hasClass('starhide'))
			{
				Y.one('.block_course_rating .avg_label').removeClass('starhide');
			}
			
			if(totalrate.hasClass('starhide'))
			{
				totalrate.removeClass('starhide');
			}
			
			if(!Y.one('.block_course_rating .myrate_label').hasClass('myrate_label_strong'))
			{
				Y.one('.block_course_rating .myrate_label').addClass('myrate_label_strong');
			}
			
			totalrate.set('text', "(" + data["countrating"] + ")");
		};

		// Subscribe to event "io:complete"
		Y.on('io:complete', complete, Y);

		// Make an HTTP request to the link.
		var request = Y.io(link);
    });
});
	
YUI().use('event-mouseenter', function (Y)
{
    var hoverMe = Y.all('.block_course_rating .rating_star');
    
    

    hoverMe.on('mouseenter', function (e)
    {
        //Get the ID of the specific star that was hovered over
        var hover_id = e.target.get('id');
        hover = hover_id.split("-");
        
        //Light up the hovered star and all the stars to the left
        for(var i=1; i<=hover[1]; i++)
        {
			if(!this.item(i).hasClass('starfull'))
			{
				this.item(i).addClass('starfull');
			}
			if(i==hover[1])
			{
				minStar = i-1;
			}
		}
		for(var i=hover[1]+1; i<=5; i++)
        {
			if(this.item(i).hasClass('starfull'))
			{
				this.item(i).removeClass('starfull');
			}
		}
		
    });

    hoverMe.on('mouseleave', function (e) 
    {
        //minStar = originalminStar;
        //Light up to min star
        minStar = originalminStar;
        for(var i=0; i<=minStar; i++)
        {
			if(!this.item(i).hasClass('starfull'))
			{
				this.item(i).addClass('starfull');
			}
		}
        for(var i=minStar+1; i<=5; i++)
        {
			if(this.item(i).hasClass('starfull'))
			{
				this.item(i).removeClass('starfull');
			}
		}
    });
});
