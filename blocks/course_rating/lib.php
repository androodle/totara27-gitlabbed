<?php

/**
 * Androgogic Course Rating
 * 
 * @author Eamon Delaney     
 * @version 2014031000    
 * @copyright Eamon Delaney  
 *
 **/

//Standard function for getting the average rating
function get_average_rating($courseid)
{
	global $DB;
	$avg = $DB->get_record_sql("SELECT AVG(rating) AS average_rating FROM {block_course_rating} WHERE courseid = ?", array($courseid));
	return round($avg->average_rating);
}
	
function get_count_rating($courseid)
{
	global $DB;
	return $DB->count_records("block_course_rating", array("courseid" => $courseid));
}
// End of blocks/course_rating/lib.php
