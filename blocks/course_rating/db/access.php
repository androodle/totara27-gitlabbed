<?php

/**
 * Androgogic Moodle Course Rating
 * 
 * @author Eamon Delaney  
 * @version     
 * @copyright   
 *
 **/

defined('MOODLE_INTERNAL') || die(); 

// Site Administration > Users > Permissions > Capability Overview

$capabilities = array(

    'block/course_rating:addinstance' => array(
        'riskbitmask' => RISK_SPAM | RISK_XSS,

        'captype' => 'write',
        'contextlevel' => CONTEXT_BLOCK,
        'archetypes' => array(
            'editingteacher' => CAP_ALLOW,
            'manager' => CAP_ALLOW
        ),

        'clonepermissionsfrom' => 'moodle/site:manageblocks'
    ),

	'block/course_rating:rate' => array (
		'captype' => 'write',
		'contextlevel' => CONTEXT_BLOCK,
	)

);

// End of blocks/linkchecker/db/access.php
