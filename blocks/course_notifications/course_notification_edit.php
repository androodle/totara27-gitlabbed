<?php

/**
 * course_notifications Block: Edit object 
 * 
 * @author      Daniel Morphett <dan@androgogic.com> 
 * @version     03/09/2015 
 * @copyright   2015+ Androgogic Pty Ltd <http://www.androgogic.com> 
 * 
 * Edit one of the course_notifications
 *  
 * */
global $OUTPUT, $courseid;
require_capability('block/course_notifications:edit', $context);
require_once('course_notification_edit_form.php');
$id = required_param('id', PARAM_INT);
$q = "select DISTINCT a.* , mdl_course.fullname as course
from mdl_andro_course_notifications a 
LEFT JOIN mdl_course  on a.course_id = mdl_course.id
where a.id = $id ";
$course_notification = $DB->get_record_sql($q);
$mform = new course_notification_edit_form();
$show_form = true;
if ($data = $mform->get_data()) {
    $data->id = $id;
    $data->modified_by = $USER->id;
    $data->date_modified = date('Y-m-d H:i:s');
    $data->body = format_text($data->body['text'], $data->body['format']);
    if (isset($data->event_arr['hasorhasnt'])) {
        $data->hasorhasnt = $data->event_arr['hasorhasnt'];
    }
    if (isset($data->event_arr['event'])) {
        $data->event = $data->event_arr['event'];
    }
    $DB->update_record('andro_course_notifications', $data);
    if(!empty($data->cmid) || !empty($data->trigger_point) || !empty($data->hour)){
        // then they filled in some kind of schedule for it, and we are receiving that post
        $show_form = false;
    }
}
if($show_form){
    echo $OUTPUT->heading(get_string('course_notification_edit', 'block_course_notifications'));
    $mform->display();
}
else {
    echo $OUTPUT->notification(get_string('datasubmitted', 'block_course_notifications'), 'notifysuccess');
}

