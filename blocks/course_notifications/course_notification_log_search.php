<?php

/**
 * Course Notifications Block: Search 
 * 
 * @author      Daniel Morphett <dan@androgogic.com> 
 * @version     25/05/2015 
 * @copyright   2015+ Androgogic Pty Ltd <http://www.androgogic.com> 
 * 
 * Search course_notification_logs
 * Also provides access to edit and delete functions if user has sufficient permissions
 *  
 * */
//params
global $courseid;
$sort = optional_param('sort', 'mdl_user.firstname', PARAM_RAW);
$dir = optional_param('dir', 'ASC', PARAM_ALPHA);
$page = optional_param('page', 0, PARAM_INT);
$perpage = optional_param('perpage', 20, PARAM_INT);
$search = optional_param('search', '', PARAM_TEXT);
$tab = optional_param('tab', 'course_notification_log_search', PARAM_FILE);
$andro_course_notifications_id = optional_param('andro_course_notifications_id', 0, PARAM_INT);
$user_id = optional_param('user_id', 0, PARAM_INT);
$course_id = optional_param('course_id', 0, PARAM_INT);
$startdate_int = optional_param('startdate_int', '0', PARAM_INT);
$enddate_int = optional_param('enddate_int', '0', PARAM_INT);
$debug = optional_param('debug', '0', PARAM_INT);

// can't deal with start and end date with optional param, as they are arrays
if (isset($_POST['startdate']['enabled'])) {
    //make it into a unix time
    $startdate = mktime(0, 0, 0, $_POST['startdate']['month'], $_POST['startdate']['day'], $_POST['startdate']['year']);
    $startdate_int = $startdate;
}
else if ($startdate_int > 0) {
    //might be coming in on the url via paging
    $startdate = $startdate_int;
}
else {
    $startdate = 0;
}
if (isset($_POST['enddate']['enabled'])) {
    //make it into a unix time
    $enddate = mktime(0, 0, 0, $_POST['enddate']['month'], $_POST['enddate']['day'], $_POST['enddate']['year']) + 1;
    $enddate_int = $enddate;
}
else if ($enddate_int > 0) {
//might be coming in on the url via paging
    $enddate = $enddate_int;
}
else {
    $enddate = 0;
}
// prepare url for paging bar
$PAGE->set_url($PAGE->url, compact('sort', 'dir', 'page', 'perpage', 'search', 'tab', 'andro_course_notifications_id', 'user_id', 'course_id',  'startdate_int', 'enddate_int'));
// prepare columns for results table
$columns = array(
    "course_notification",
    "user",
    "course",
    "time_sent",
    "course_notification_send_result"
);
foreach ($columns as $column) {
    $string[$column] = get_string("$column", 'block_course_notifications');
    if ($sort != $column) {
        $columnicon = '';
        $columndir = 'ASC';
    }
    else {
        $columndir = $dir == 'ASC' ? 'DESC' : 'ASC';
        $columnicon = $dir == 'ASC' ? 'down' : 'up';
    }
    if ($column != 'details') {
        $$column = "<a href='$PAGE->url&dir=$columndir&sort=$column'>$string[$column]</a>";
    }
    else {
        $$column = $string[$column];
    }
}
//figure out the and clause from what has been submitted
$and = '';
if ($search != '') {
    $and .= " and " . $DB->sql_concat('mdl_user.firstname', "' '", 'mdl_user.lastname', "' '", 'mdl_course.fullname') . " like '%$search%'";
}
//are we filtering on andro_course_notifications?
if ($andro_course_notifications_id > 0) {
    $and .= " and mdl_andro_course_notifications.id = $andro_course_notifications_id ";
}
//are we filtering on user?
if ($user_id > 0) {
    $and .= " and mdl_user.id = $user_id ";
}
//are we filtering on course?
if ($course_id > 0) {
    $and .= " and mdl_course.id = $course_id ";
}
if (isset($startdate) and is_integer($startdate) and $startdate > 0) {
    $filter_startdate = date('Y-m-d',$startdate);
    $and .= " and (a.time_sent > '$filter_startdate')";
}
if (isset($enddate) and is_integer($enddate) and $enddate > 0) {
    $filter_enddate = date('Y-m-d',$enddate);
    $and .= " and (a.time_sent < '$filter_enddate')";
}
$q = "select a.* , mdl_andro_course_notifications.name as course_notification, " 
        . $DB->sql_concat('mdl_user.firstname',"' '",'mdl_user.lastname') . " as user, mdl_course.fullname as course 
from mdl_course_notification_log a 
LEFT JOIN mdl_andro_course_notifications  on a.course_notification_id = mdl_andro_course_notifications.id
LEFT JOIN mdl_user  on a.user_id = mdl_user.id
LEFT JOIN mdl_course  on a.course_id = mdl_course.id
where a.course_id = $courseid 
$and 
order by $sort $dir";
if ($debug == 1) {
    echo '$query : ' . $q . '';
}
if ($download == '') {
//get a page worth of records
    $results = $DB->get_records_sql($q, array(), $page * $perpage, $perpage);
}
else {
    $results = $DB->get_records_sql($q);
}
if ($download == '') {
//also get the total number we have of these
    $q = "SELECT COUNT(DISTINCT a.id)
from mdl_course_notification_log a 
LEFT JOIN mdl_andro_course_notifications  on a.course_notification_id = mdl_andro_course_notifications.id
LEFT JOIN mdl_user  on a.user_id = mdl_user.id
LEFT JOIN mdl_course  on a.course_id = mdl_course.id
 where a.course_id = $courseid   $and";

    if ($debug == 1) {
        echo '$query : ' . $q . '<br>';
    }

    $result_count = $DB->get_field_sql($q);

    require_once('course_notification_log_search_form.php');
    $mform = new course_notification_log_search_form(null, array('sort' => $sort, 'dir' => $dir, 'perpage' => $perpage, 'search' => $search, 
        'tab' => $currenttab, 'andro_course_notifications_id' => $andro_course_notifications_id, 'user_id' => $user_id, 'course_id' => $course_id, 
        'startdate' => $startdate, 'startdate_int' => $startdate_int, 'enddate' => $enddate, 'enddate_int' => $enddate_int,
        'debug' => $debug, 'courseid' => $courseid));
    $mform->display();
    echo '<table width="100%"><tr><td width="50%">';
    echo $result_count . ' ' . get_string('course_notification_log_plural', 'block_course_notifications') . " found" . '<br>';
    echo '</td><td style="text-align:right;">';

    echo '</td></tr></table>';
}

//RESULTS
if (!$results) {
    if ($download == '') {
        echo $OUTPUT->heading(get_string('noresults', 'block_course_notifications', $search));
    }
}
else {
    $table = new html_table();
    $table->head = array(
        $course_notification,
        $user,
        $course,
        $time_sent,
        $course_notification_send_result
    );
    $table->width = "95%";
    foreach ($results as $result) {

        $table->data[] = array(
            $result->course_notification,
            $result->user,
            $result->course,
            date('d-m-Y h:i:s', strtotime($result->time_sent)),
            $result->result
        );
    }
}
if (!empty($table)) {
    if ($download != '') {
//export the table to whatever they asked for
        block_course_notifications_export_data($download, $table, $tab);
    }
    else {
        echo html_writer::table($table);
        $pagingbar = new paging_bar($result_count, $page, $perpage, $PAGE->url);
        $pagingbar->pagevar = 'page';
        echo $OUTPUT->render($pagingbar);
        block_course_notifications_output_download_links($PAGE->url, 'course_notification_log_plural');
    }
}

