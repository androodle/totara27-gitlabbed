<?php
/** 
 * Course Notifications Block: Permissions 
 * 
 * @author      Daniel Morphett <dan@androgogic.com> 
 * @version     17/04/2015 
 * @copyright   2015+ Androgogic Pty Ltd <http://www.androgogic.com> 
 * 
 *  
 **/
$capabilities = array(
'block/course_notifications:edit' => array(
'captype' => 'write',
'contextlevel' => CONTEXT_SYSTEM,
'archetypes' => array(
'manager' => CAP_ALLOW,
)
),
'block/course_notifications:delete' => array(
'captype' => 'write',
'contextlevel' => CONTEXT_SYSTEM,
'archetypes' => array(
'manager' => CAP_ALLOW,
)
),
'block/course_notifications:addinstance' => array(
'captype' => 'write',
'contextlevel' => CONTEXT_BLOCK,
'archetypes' => array(
'manager' => CAP_ALLOW,
'editingteacher' => CAP_ALLOW,
)
),
);
// End of blocks/course_notifications/db/access.php
