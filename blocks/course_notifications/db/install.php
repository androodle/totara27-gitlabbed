<?php
/** 
 * Course Notifications Block: Install DB scripts 
 * 
 * @author      Daniel Morphett <dan@androgogic.com> 
 * @version     17/04/2015 
 * @copyright   2015+ Androgogic Pty Ltd <http://www.androgogic.com> 
 * 
 *  
 **/


function xmldb_block_course_notifications_install() {
global $DB;
$dbman = $DB->get_manager();
$result = true;
// create tables for the block
if (!$dbman->table_exists('andro_course_notifications')) {
$table = new xmldb_table('andro_course_notifications');
$table->add_field('id', XMLDB_TYPE_INTEGER , '11', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
$table->add_field('name', XMLDB_TYPE_CHAR , '50', null, XMLDB_NOTNULL, null, null);
$table->add_field('subject', XMLDB_TYPE_CHAR , '50', null, XMLDB_NOTNULL, null, null);
$table->add_field('body', XMLDB_TYPE_TEXT , 'null', null, XMLDB_NOTNULL, null, null);
$table->add_field('enabled', XMLDB_TYPE_INTEGER , '4', null, XMLDB_NOTNULL, null, null);
$table->add_field('course_id', XMLDB_TYPE_INTEGER , '11', null, XMLDB_NOTNULL, null, null);
$table->add_field('cmid', XMLDB_TYPE_INTEGER , '11', null, null, null, null);
$table->add_field('hasorhasnt', XMLDB_TYPE_CHAR , '20', null, null, null, null);
$table->add_field('event', XMLDB_TYPE_CHAR , '30', null, null, null, null);
$table->add_field('event_by', XMLDB_TYPE_INTEGER , '10', null, null, null, null);
$table->add_field('trigger_point', XMLDB_TYPE_CHAR , '30', null, null, null, null);
$table->add_field('frequency', XMLDB_TYPE_CHAR , '20', null, null, null, null);
$table->add_field('days_after', XMLDB_TYPE_INTEGER , '11', null, null, null, null);
$table->add_field('hour', XMLDB_TYPE_INTEGER , '10', null, null, null, null);
$table->add_field('day_of_week', XMLDB_TYPE_INTEGER , '10', null, null, null, null);
$table->add_field('day_of_month', XMLDB_TYPE_INTEGER , '10', null, null, null, null);
$table->add_field('created_by', XMLDB_TYPE_INTEGER , '10', null, XMLDB_NOTNULL, null, null);
$table->add_field('date_created', XMLDB_TYPE_DATETIME , 'null', null, XMLDB_NOTNULL, null, null);
$table->add_field('modified_by', XMLDB_TYPE_INTEGER , '10', null, null, null, null);
$table->add_field('date_modified', XMLDB_TYPE_DATETIME , 'null', null, null, null, null);
$table->add_field('mailout_type', XMLDB_TYPE_CHAR , '20', null, null, null, null);
$table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
$table->add_index('ix_course_id', XMLDB_INDEX_NOTUNIQUE, array('course_id'));
$table->add_index('ix_cmid', XMLDB_INDEX_NOTUNIQUE, array('cmid'));
$dbman->create_table($table);
}
if (!$dbman->table_exists('course_notification_log')) {
$table = new xmldb_table('course_notification_log');
$table->add_field('id', XMLDB_TYPE_INTEGER , '11', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
$table->add_field('course_notification_id', XMLDB_TYPE_INTEGER , '11', null, XMLDB_NOTNULL, null, null);
$table->add_field('user_id', XMLDB_TYPE_INTEGER , '11', null, XMLDB_NOTNULL, null, null);
$table->add_field('course_id', XMLDB_TYPE_INTEGER , '11', null, XMLDB_NOTNULL, null, null);
$table->add_field('time_sent', XMLDB_TYPE_DATETIME , 'null', null, XMLDB_NOTNULL, null, null);
$table->add_field('created_by', XMLDB_TYPE_INTEGER , '10', null, XMLDB_NOTNULL, null, null);
$table->add_field('date_created', XMLDB_TYPE_DATETIME , 'null', null, XMLDB_NOTNULL, null, null);
$table->add_field('modified_by', XMLDB_TYPE_INTEGER , '10', null, null, null, null);
$table->add_field('date_modified', XMLDB_TYPE_DATETIME , 'null', null, null, null, null);
$table->add_field('result', XMLDB_TYPE_TEXT , 'null', null, null, null, null);
$table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
$table->add_index('ix_course_notification_id', XMLDB_INDEX_NOTUNIQUE, array('course_notification_id'));
$table->add_index('ix_user_id', XMLDB_INDEX_NOTUNIQUE, array('user_id'));
$table->add_index('ix_course_id', XMLDB_INDEX_NOTUNIQUE, array('course_id'));
$dbman->create_table($table);
}

return $result;
} 