<?php

/** 
 * course_notifications Block: Delete object 
 * 
 * @author      Daniel Morphett <dan@androgogic.com> 
 * @version     03/09/2015 
 * @copyright   2015+ Androgogic Pty Ltd <http://www.androgogic.com> 
 * 
 * Delete one of the course_notifications
 *  
 **/

require_capability('block/course_notifications:edit', $context);
$id = required_param('id', PARAM_INT);
$DB->delete_records('andro_course_notifications',array('id'=>$id));
$DB->delete_records('course_notification_log',array('course_notification_id'=>$id));
echo $OUTPUT->notification(get_string('itemdeleted','block_course_notifications'), 'notifysuccess');

