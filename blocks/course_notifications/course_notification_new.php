<?php

/** 
 * course_notifications Block: Create object 
 * 
 * @author      Daniel Morphett <dan@androgogic.com> 
 * @version     03/09/2015 
 * @copyright   2015+ Androgogic Pty Ltd <http://www.androgogic.com> 
 * 
 * Create new course_notification
 *  
 **/

global $OUTPUT,$courseid;
$mailout_type = optional_param('mailout_type', '', PARAM_ALPHAEXT);
$save_data = FALSE;
require_capability('block/course_notifications:edit', $context);
require_once('course_notification_edit_form.php');
$mform = new course_notification_edit_form();
if ($data = $mform->get_data()){
    // we don't want to save part 1 of the form on a new cn, so let's make sure we are on part 2
    $part2_elements = array('cmid', 'event', 'event_arr', 'event_by', 'trigger_point', 'frequency', 'days_after', 'hour', 'day_of_week', 'day_of_month');
    foreach ($part2_elements as $p2el) {
        if(!empty($data->$p2el)){
            $save_data = TRUE;
            break;
        }
    }        
    if ($save_data){ 
        $data->created_by = $USER->id;
        $data->date_created = date('Y-m-d H:i:s');
        $data->body = format_text($data->body['text'], $data->body['format']);
        if(isset($data->event_arr['hasorhasnt'])){
            $data->hasorhasnt = $data->event_arr['hasorhasnt'];
        }
        if(isset($data->event_arr['event'])){
            $data->event = $data->event_arr['event'];
        }
        $newid = $DB->insert_record('andro_course_notifications',$data);
        echo $OUTPUT->notification(get_string('datasubmitted','block_course_notifications'), 'notifysuccess');
        echo $OUTPUT->action_link($PAGE->url, 'Create another item');
    }
    else{
    echo $OUTPUT->heading(get_string('course_notification_new', 'block_course_notifications'));
    $mform->display();
    }
}
else{
    echo $OUTPUT->heading(get_string('course_notification_new', 'block_course_notifications'));
    $mform->display();
}

