<?php


/** 
 * Course Notifications Block: Lib 
 * 
 * @author      Daniel Morphett <dan@androgogic.com> 
 * @version     17/04/2015 
 * @copyright   2015+ Androgogic Pty Ltd <http://www.androgogic.com> 
 * 
 *  
 **/

/**************************** STANDARD BLOCK BUSTER FUNCTIONS *************************************************/

function block_course_notifications_cron($source = 'cron') {
    global $CFG, $DB;
    
    mtrace("\n\n");
    mtrace("Running cron for course_notifications" . "\n");
    $time = date("d-m-Y h-i-s", time());
    mtrace($time . '\n');
    mtrace('before send out course_notifications:\n');
    send_out_course_notifications();
    $time = date("d-m-Y h-i-s", time());
    mtrace('Finish time' . $time . '\n');
    
}

function block_course_notifications_save_upload_over_existing($form_data, $block_name, $table_name, $foreign_key_name) {
    global $DB;
    $data = $form_data->get_data();
    
    //change the reference to the itemid in the block table
    $sql = "update mdl_$table_name set $foreign_key_name = " . $data->itemid . " where id = " . $data->objectid;
    $DB->execute($sql);
    if(isset($data->itemid) and $data->itemid > 0){
        //change the component and filearea of the file entry
        $sql = "UPDATE mdl_files
            SET filearea = 'content',component='$block_name'
            WHERE itemid = '$data->itemid'";
        $DB->execute($sql);
    }
}

function block_course_notifications_pluginfile($course, $birecord, $context, $filearea, $args, $forcedownload) {
    global $DB;
    $sql = 'select * from mdl_files 
            where itemid = ' . $args[0] . ' 
                and filename = \'' . $args[1] . '\'
                and contextid = ' . $context->id . ' 
                and filearea = \'' . $filearea . '\'';
    $file = $DB->get_record_sql($sql);
    if (!$file) {
        send_file_not_found();
    } else {
        $fs = get_file_storage();
        $stored_file = $fs->get_file_by_hash($file->pathnamehash);
    }
    $forcedownload = true;

    session_get_instance()->write_close();
    send_stored_file($stored_file, 60 * 60, 0, $forcedownload);
}
function block_course_notifications_fix_phantom_file_id($data) {
    global $DB;
    if(isset($data->file_id) and $data->file_id > 0){
        //have we got a phantom file id?
        $sql = "SELECT * FROM mdl_files
        WHERE itemid = '$data->file_id'";
        $file_exists = $DB->get_records_sql($sql);
        if ($file_exists) {
            //fix file entries
            block_course_notifications_fix_file($data->file_id);
        } else {
            //get rid of the phantom out of the subject table
            $sql = "UPDATE mdl_ap_club_strategies
            SET file_id = NULL
            where id = '$data->id'";
            $DB->execute($sql);
        }
    }
}
function block_course_notifications_fix_file($item_id) {
    global $DB;
    if($item_id > 0){
    $sql = "UPDATE mdl_files
            SET filearea = 'content',component='block_course_notifications'
            WHERE itemid = '$item_id'";
            $DB->execute($sql);
    }
}
function block_course_notifications_export_data($download_format, $table, $filename,$report_name_lang='',$table2=null,$club_name='') {
    $stripped_table = block_course_notifications_strip_tags_from_table($table);
    $function = "block_course_notifications_export_data_" . $download_format;
    if($table2){
        $stripped_table2 = block_course_notifications_strip_tags_from_table($table2);
        $function($stripped_table, $filename,$report_name_lang,$stripped_table2,$club_name);
    }
    else{
        $function($stripped_table, $filename,$report_name_lang,$table2,$club_name);
    }
}

function block_course_notifications_export_data_excel($table, $filename,$report_name_lang,$table2=null,$club_name='') {
    global $CFG;

    require_once("$CFG->libdir/excellib.class.php");

    $filename = clean_filename($filename) . '.xls';

    $workbook = new MoodleExcelWorkbook('-');
    $workbook->send($filename);

    $worksheet = array();

    $worksheet[0] = $workbook->add_worksheet('');
    $col = 0;
    foreach ($table->head as $cell) {
        $worksheet[0]->write(0, $col, $cell);
        $col++;
    }

    $row = 1;
    foreach ($table->data as $row_data) {
        $col = 0;
        foreach ($row_data as $cell) {
            $worksheet[0]->write($row, $col, $cell);
            $col++;
        }
        $row++;
    }
    if($table2){
        //add empty row
        $worksheet[0]->write($row, 0, 0);
        $row++;
        $col = 0;
        foreach ($table2->head as $cell) {
            $worksheet[0]->write($row, $col, $cell);
            $col++;
        }
        $row++;
        foreach ($table2->data as $row_data) {
            $col = 0;
            foreach ($row_data as $cell) {
                $worksheet[0]->write($row, $col, $cell);
                $col++;
            }
            $row++;
        }
    }

    $workbook->close();
    die;
}

function block_course_notifications_export_data_csv($table, $filename,$report_name_lang,$table2=null,$club_name='') {
    global $CFG;

    require_once($CFG->libdir . '/csvlib.class.php');

    $filename = clean_filename($filename) . '.csv';

    $csvexport = new csv_export_writer();
    $csvexport->set_filename($filename);
    $csvexport->add_data($table->head);

    foreach ($table->data as $row_data) {
        $csvexport->add_data($row_data);
    }
    if($table2){
        $empty_row = array();
        $csvexport->add_data($empty_row);
        $csvexport->add_data($table2->head);

        foreach ($table2->data as $row_data) {
            $csvexport->add_data($row_data);
        }
    }
    $csvexport->download_file();
    die;
}

function block_course_notifications_strip_tags_from_table($table) {
    //strip any html out of the table head and rows
    $stripped_table = new stdClass();
    foreach ($table->head as $table_head_cell) {
        $stripped_table->head[] = strip_tags($table_head_cell);
    }
    foreach ($table->data as $row_data) {
        $stripped_row_data = array();
        foreach ($row_data as $cell) {
            $stripped_row_data[] = strip_tags($cell);
        }
        $stripped_table->data[] = $stripped_row_data;
    }
    return $stripped_table;
}

function block_course_notifications_export_data_pdf_portrait($table, $filename, $report_name_lang_entry,$table2=null,$club_name='') {
    block_course_notifications_export_data_pdf($table, $filename, $report_name_lang_entry, true,$table2,$club_name);
}

function block_course_notifications_export_data_pdf_landscape($table, $filename, $report_name_lang_entry,$table2=null,$club_name='') {
    block_course_notifications_export_data_pdf($table, $filename, $report_name_lang_entry, false,$table2,$club_name);
}

function block_course_notifications_export_data_pdf($table, $filename, $report_name_lang_entry, $portrait = true,$table2=null,$club_name='') {
    global $CFG;
    //set up some values - these might go to settings page at some stage
    $margin_footer = 10;
    $margin_bottom = 20;
    $font_size_title = 20;
    $font_size_record = 14;
    $font_size_data = 10;
    $memory_limit = 1024;

    require_once $CFG->libdir . '/pdflib.php';

    // Increasing the execution time to no limit.
    set_time_limit(0);
    $filename = clean_filename($filename . '.pdf');

    // Table.
    $html = '';
    $numfields = count($table->head);

    // Layout options.
    if ($portrait) {
        $pdf = new PDF('P', 'mm', 'A4', true, 'UTF-8');
    } else {
        $pdf = new PDF('L', 'mm', 'A4', true, 'UTF-8');
    }

    $pdf->setTitle($filename);
    $pdf->setPrintHeader(false);
    $pdf->setPrintFooter(true);
    $pdf->SetFooterMargin($margin_footer);
    $pdf->SetAutoPageBreak(true, $margin_bottom);
    $pdf->AddPage();

    // Get current language to set the font properly.
    $language = current_language();
    if (in_array($language, array('zh_cn', 'ja'))) {
        $font = 'droidsansfallback';
    } else if ($language == 'th') {
        $font = 'cordiaupc';
    } else {
        $font = 'freesans';
    }
    // Check if language is RTL.
    if (right_to_left()) {
        $pdf->setRTL(true);
    }
    if($report_name_lang_entry != ''){
        $report_header = get_string($report_name_lang_entry, 'block_course_notifications',$club_name);
        $pdf->SetFont($font, 'B', $font_size_title);
        $pdf->Write(0, format_string($report_header), '', 0, 'L', true, 0, false, false, 0);    
    }
    
    $count = count($table->data);
    $resultstr = $count == 1 ? 'record' : 'records';
    $recordscount = $count . ' ' .$resultstr;
    $pdf->SetFont($font, 'B', $font_size_record);
    if(!$table2){
        $pdf->Write(0, $recordscount, '', 0, 'L', true, 0, false, false, 0);
    }
    $pdf->SetFont($font, '', $font_size_data);

    if (isset($restrictions) && is_array($restrictions) && count($restrictions) > 0) {
        $pdf->Write(0, get_string('reportcontents', 'totara_reportbuilder'), '', 0, 'L', true, 0, false, false, 0);
        foreach ($restrictions as $restriction) {
            $pdf->Write(0, $restriction, '', 0, 'L', true, 0, false, false, 0);
        }
    }

    // Add report caching data.
    if (isset($cache)) {
        $usertz = totara_get_clean_timezone();
        $a = userdate($cache['lastreport'], '', $usertz);
        $lastcache = get_string('report:cachelast', 'totara_reportbuilder', $a);
        $pdf->Write(0, $lastcache, '', 0, 'L', true, 0, false, false, 0);
    }
    //make a space between the header and the table
    $html .= '<p>&nbsp;</p>';
    $html .= '<table border="1" cellpadding="2" cellspacing="0">
                        <thead>
                            <tr style="background-color: #CCC;">';
    foreach ($table->head as $field) {
        $html .= '<th>' . $field . '</th>';
    }
    $html .= '</tr></thead><tbody>';

    foreach ($table->data as $record_data) {
        $html .= '<tr>';
        for ($j = 0; $j < $numfields; $j++) {
            if (isset($record_data[$j])) {
                $cellcontent = html_entity_decode($record_data[$j], ENT_COMPAT, 'UTF-8');
            } else {
                $cellcontent = '';
            }
            $html .= '<td>' . $cellcontent . '</td>';
        }
        $html .= '</tr>';

        // Check memory limit.
        $mramuse = ceil(((memory_get_usage(true) / 1024) / 1024));
        if ($memory_limit <= $mramuse) {
            // Releasing resources.
            $records->close();
            // Notice message.
            print_error('exportpdf_mramlimitexceeded', 'totara_reportbuilder', '', $memory_limit);
        }
    }
    $html .= '</tbody></table>';
    
    if($table2){
        //reset the numfields
        $numfields = count($table2->head);
        //make a space between the tables
        $html .= '<p>&nbsp;</p>';
        //make some more html for table2
        $html .= '<table border="1" cellpadding="2" cellspacing="0">
                    <thead>
                        <tr style="background-color: #CCC;">';
        foreach ($table2->head as $field) {
            $html .= '<th>' . $field . '</th>';
        }
        $html .= '</tr></thead><tbody>';

        foreach ($table2->data as $record_data) {
            $html .= '<tr>';
            for ($j = 0; $j < $numfields; $j++) {
                if (isset($record_data[$j])) {
                    $cellcontent = html_entity_decode($record_data[$j], ENT_COMPAT, 'UTF-8');
                } else {
                    $cellcontent = '';
                }
                $html .= '<td>' . $cellcontent . '</td>';
            }
            $html .= '</tr>';

            // Check memory limit.
            $mramuse = ceil(((memory_get_usage(true) / 1024) / 1024));
            if ($memory_limit <= $mramuse) {
                // Releasing resources.
                $records->close();
                // Notice message.
                print_error('exportpdf_mramlimitexceeded', 'totara_reportbuilder', '', $memory_limit);
            }
        }
        $html .= '</tbody></table>';
    }

    // Closing the pdf.
    $pdf->WriteHTML($html, true, false, false, false, '');

    // Returning the complete pdf.
    //if (!$file) {
        $pdf->Output($filename, 'D');
//    } else {
//        $pdf->Output($file, 'F');
//    }
}
function block_course_notifications_output_download_links($moodle_url, $report_name_lang_entry='') {
    $url = $moodle_url->out(false);
    $report_name_param = "&report_name_lang=$report_name_lang_entry";
    echo 'Download: ';
    $download_report_url = $url . "&download=csv";
    echo html_writer::link($download_report_url, 'CSV');
    echo "&nbsp;";
    $download_report_url = $url . "&download=excel";
    echo html_writer::link($download_report_url, 'Excel');
    echo "&nbsp;";
    $download_report_url = $url . "&download=pdf_portrait" . $report_name_param;
    echo html_writer::link($download_report_url, 'PDF Portrait');
    echo "&nbsp;";
    $download_report_url = $url . "&download=pdf_landscape" . $report_name_param;
    echo html_writer::link($download_report_url, 'PDF Landscape');
}

/**************************** END OF STANDARD BLOCK BUSTER FUNCTIONS *************************************************/


function send_out_course_notifications() {
    global $DB;
    //any course_notifications to send out?
    $course_notifications = $DB->get_records('andro_course_notifications',array('enabled'=>1));
    foreach ($course_notifications as $course_notification) {
        $users_in_course = get_users_in_course($course_notification->course_id);
        if(!$users_in_course){
            continue;
        }
        //does the course_notification have a frequency?
        if(in_array($course_notification->frequency, array('daily','weekly','monthly'))){
            $day_of_month = date('j');
            $day_of_week = date('N');
            $hour = date('G');
            if($course_notification->hour != $hour){
                //short circuit
                continue;
            }
            if($course_notification->frequency == 'weekly' && $course_notification->day_of_week != $day_of_week){
                continue;
            }
            if($course_notification->frequency == 'monthly' && $course_notification->day_of_month != $day_of_month){
                continue;
            }
            foreach ($users_in_course as $user) {
                if(!course_notification_has_been_sent($user, $course_notification,true)){
                    send_course_notification_to_user($course_notification, $user);
                }
            }
            continue;
        }
        //is it about an activity?
        if($course_notification->cmid != 0){
            //if we have an event by date, have we passed it?
            if(!empty($course_notification->event_by)){
                $time = time();
                if($course_notification->event_by > $time){
                    //not yet
                    continue;
                }
            }
            
            $cm = $DB->get_record('course_modules', array('id'=>$course_notification->cmid));
            $modulename = $DB->get_field('modules', 'name', array('id'=>$cm->module), IGNORE_MULTIPLE);
            $activity = $DB->get_record($modulename,array('id'=>$cm->instance));
            //for each user see whether they qualify, and if so whether the course_notification has already been sent. If not, then send it
            foreach ($users_in_course as $user) {
                if(!course_notification_has_been_sent($user, $course_notification)){
                    //have we got any logs for the user and the cmid?
                    // GK WARNING: we are not looking at the log action, because it is a can of worms (configuring user
                    // cannot necessarily guess which action it is going to be and some actions of same meaning differ in 
                    // how they are termed). 
                    $sql = "select * from mdl_log
                        where cmid = {$course_notification->cmid} and userid = {$user->id}
                        ";
                    if(!empty($course_notification->days_after)){
                        // consider days after value
                        $seconds_after = $course_notification->days_after*24*60*60;
                        $unixtime_to_compare = time() - $seconds_after;
                        //it might be days after something happened with the activity
                        // or days after some other trigger point
                        if($course_notification->trigger_point == ''){
                            $sql .= " and time >  $unixtime_to_compare" ; //has to be > for this, as it means days after the cmid action
                        }
                        else{
                            //has to be < for these ones, as the time of the trigger point has to be in the past
                            //handle completions
                            if($course_notification->trigger_point == 'usercompletescourse'){
                                $sql .= "and userid in (select cc.userid from mdl_course_completions cc
                                    left join mdl_course_notification_log el on cc.userid = el.user_id and cc.course = el.course_id 
                                        and el.course_notification_id = $course_notification->id
                                    where el.id is null and cc.timecompleted < $unixtime_to_compare 
                                    and cc.course = $course_notification->course_id)";
                            }
                            //handle enrolments
                            if($course_notification->trigger_point == 'userenrolled'){
                                $sql .= "and userid in (select ue.userid
                                    from mdl_user_enrolments ue
                                    inner join mdl_enrol e on ue.enrolid = e.id
                                    left join mdl_course_notification_log el on ue.userid = el.user_id 
                                    and e.courseid = el.course_id and el.course_notification_id = $course_notification->id
                                    where el.id is null
                                    and ue.timecreated < $unixtime_to_compare
                                    and e.courseid = $course_notification->course_id)";  
                            }
                        }
                    }
                    $results = $DB->get_records_sql($sql);
                    //are we working with positives or negatives?
                    if($results && $course_notification->hasorhasnt == 'hasbeen'){
                        send_course_notification_to_user($course_notification, $user);
                        continue;
                    }
                    if(!$results && $course_notification->hasorhasnt == 'hasntbeen'){
                        send_course_notification_to_user($course_notification, $user);
                        continue;
                    }
                }
            }
            continue;
        }
        //is it based on a trigger point?
        elseif($course_notification->trigger_point != ''){
            //handle completions
            if($course_notification->trigger_point == 'usercompletescourse'){
                $sql = "select cc.* from mdl_course_completions cc
                    left join mdl_course_notification_log el on cc.userid = el.user_id and cc.course = el.course_id and el.course_notification_id = $course_notification->id
                    where el.id is null and cc.timecompleted > 0 and cc.course = $course_notification->course_id";

                $module_completions = $DB->get_records_sql($sql);
                foreach ($module_completions as $mc) {
                    // consider days after value
                    $seconds_after = $course_notification->days_after*24*60*60;
                    if(($mc->timecompleted + $seconds_after) < time()){  //ie has the time elapsed?
                        $user = $DB->get_record('user', array('id'=>$mc->userid));
                        send_course_notification_to_user($course_notification,$user);
                    }
                }
                continue;
            }
            //handle enrolments
            if($course_notification->trigger_point == 'userenrolled'){
                $sql = "select ue.id, ue.userid, ue.timecreated 
                    from mdl_user_enrolments ue
                    inner join mdl_enrol e on ue.enrolid = e.id
                    left join mdl_course_notification_log el on ue.userid = el.user_id and e.courseid = el.course_id and el.course_notification_id = $course_notification->id
                    where el.id is null and e.courseid = $course_notification->course_id";

                $enrolments = $DB->get_records_sql($sql);
                foreach ($enrolments as $en) {
                    // consider days after value
                    $seconds_after = $course_notification->days_after*24*60*60;
                    if(($en->timecreated + $seconds_after) < time()){  //ie has the time elapsed?
                        $user = $DB->get_record('user', array('id'=>$en->userid));
                        send_course_notification_to_user($course_notification,$user);
                    }
                }
                continue;
            }
        }
    }
    
    
}
function send_course_notification_to_users($course_notification,$users) {
   foreach ($users as $user) {
       send_course_notification_to_user($course_notification,$user);
   }
}
function get_users_in_course($course_id){
    global $DB;
    $sql = "select DISTINCT u.id, u.firstname, u.lastname, u.email, u.username 
        from mdl_user_enrolments ue
        inner join mdl_enrol e on ue.enrolid = e.id
        inner join mdl_course c on e.courseid = c.id
        inner join mdl_user u on ue.userid = u.id
        where c.id = $course_id ";

    return $DB->get_records_sql($sql);
}
function course_notification_has_been_sent($user,$course_notification,$today=FALSE) {
    global $DB;
    $sql = "SELECT * FROM mdl_course_notification_log WHERE user_id = $user->id and course_notification_id = $course_notification->id";
    if($today){
        $today_first_moment = date("Y-m-d");
        $sql .= " and time_sent > '$today_first_moment'";
    }
    return $DB->get_record_sql($sql);
}
function send_course_notification_to_user($course_notification,$user) {
    if($course_notification->enabled == "1"){
        $body_text = get_course_notification_body_text($course_notification,$user);
        $from = get_admin();
        $config = get_config('block_course_notifications');
        if(isset($config->course_notification_from_email)){
            $from->email = $config->course_notification_from_email;
        }
        $subject = $course_notification->subject;
        $user->mailformat = 1; //force html - this is what the wysiwyg produces
        $result = email_to_user($user, $from, $subject, $body_text, $body_text);
        log_course_notification($course_notification,$user,$result);
    }
}
function get_course_notification_body_text($course_notification,$user) {
    global $DB,$CFG,$SITE;
    
    $course_notification_body = $course_notification->body;
    $course_notification_body = str_ireplace('[user]', $user->firstname . ' ' . $user->lastname, $course_notification_body);
    $course_notification_body = str_ireplace('[username]', $user->username, $course_notification_body);
    $course_notification_body = str_ireplace('[email]', $user->email, $course_notification_body);
    
    if(stristr($course_notification_body,'[course]')){
        $course = $DB->get_record('course',array('id'=>$course_notification->course_id));
        $course_notification_body = str_ireplace('[course]', "<a href='" . $CFG->wwwroot . "/course/view.php?id=" . $course->id . "'>" .$course->fullname ."</a>", $course_notification_body);
    }
    if(stristr($course_notification_body,'[site_url]')){
        $course_notification_body = str_ireplace('[site_url]', "<a href='" . $CFG->wwwroot . "/index.php" . "'>" .$SITE->fullname ."</a>", $course_notification_body);
    }
    if(stristr($course_notification_body,'[admin]')){
        $admin = get_admin();
        $course_notification_body = str_ireplace('[admin]', $admin->firstname . ' ' . $admin->lastname, $course_notification_body);
    }
    return $course_notification_body;
}
function log_course_notification($course_notification,$user,$result) {
    global $DB,$USER;
    $data = new stdClass();
    $data->course_notification_id = $course_notification->id;
    $data->user_id = $user->id;
    $data->course_id = $course_notification->course_id;
    $data->created_by = $USER->id;
    $data->date_created = date('Y-m-d H:i:s');
    $data->time_sent = date('Y-m-d H:i:s');
    $data->result = $result;
    $newid = $DB->insert_record('course_notification_log',$data);

}
function block_course_notifications_get_activities_for_course($courseid) {
    global $DB;
    $sql = "select cm.*,m.name as mod_name from mdl_course_modules cm
        inner join mdl_modules m on cm.module = m.id 
        where course = $courseid
        and m.name not in ('certificate','label','chat','url')";

    $results = $DB->get_records_sql($sql);
    $options = array();
    foreach ($results as $row) {
        //get the name of the mod 
        $sql = "select * from mdl_" . $row->mod_name . " WHERE id = " . $row->instance;
        $mod = $DB->get_record_sql($sql);
        $options[$row->id] = $mod->name;
    }
    asort($options);
    return $options;
}
function block_course_notifications_add_empty_option($options) {
    $new_array = array();
    $new_array[0] = "Select";
    foreach ($options as $key => $value) {
        $new_array[$key] = $value;
    }
    return $new_array;
}