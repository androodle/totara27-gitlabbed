<?php

/**
 * Course Notifications Block: Index 
 * 
 * @author      Daniel Morphett <dan@androgogic.com> 
 * @version     17/04/2015 
 * @copyright   2015+ Androgogic Pty Ltd <http://www.androgogic.com> 
 * 
 *  
 * */
require_once('../../config.php');
require_once('lib.php');
global $CFG, $USER, $DB, $PAGE, $OUTPUT;
//expected params
$currenttab = optional_param('tab', 'course_notification_search', PARAM_FILE);
$download = optional_param('download', '', PARAM_RAW);
$courseid = required_param('courseid', PARAM_INT);
$context = context_system::instance();
$course = $DB->get_record('course',array('id'=>$courseid));
$PAGE->set_context($context);
$PAGE->set_url("$CFG->wwwroot/blocks/course_notifications/index.php", array('tab' => $currenttab,'courseid'=>$courseid));
$PAGE->set_title(get_string($currenttab, 'block_course_notifications'));
$PAGE->set_heading(get_string('plugintitle', 'block_course_notifications'));
$PAGE->navbar->add($course->shortname, new moodle_url('/course/view.php', array('id'=>$course->id)));
$PAGE->navbar->add(get_string('course_notification_search', 'block_course_notifications'), "$CFG->wwwroot/blocks/course_notifications/index.php?courseid=$courseid", 'misc');
require_login();
if(!has_capability('block/course_notifications:edit', $context)){
   print_error('accessdenied','admin'); 
}

// we may be in the middle of doing a redirect. If so we don't want the header
$redirect = false;
// detect if we are here...
if ($download == '' && !$redirect) {
    echo $OUTPUT->header();
    if (has_capability('block/course_notifications:edit', $context)) {
//show tabs
        include('tabs.php');
    }
}
// include current page
include $currenttab . '.php';
if ($download == '') {
    echo $OUTPUT->footer();
}


// End of blocks/course_notifications/index.php