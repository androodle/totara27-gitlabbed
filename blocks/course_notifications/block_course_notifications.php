<?php
/** 
 * Course Notifications Block: Class 
 * 
 * @author      Daniel Morphett <dan@androgogic.com> 
 * @version     17/04/2015 
 * @copyright   2015+ Androgogic Pty Ltd <http://www.androgogic.com> 
 * 
 *  
 **/
require_once($CFG->dirroot.'/blocks/course_notifications/lib.php');

class block_course_notifications extends block_base {


	function init(){
		$this->title = get_string('plugintitle','block_course_notifications');
                $this->cron = 1;
	} //end function init


	function get_content(){
		global $CFG;
		if ($this->content !== null) {
			return $this->content;
		}
		$this->content = new stdClass;
                $courseid = $this->page->course->id;
                if ($courseid == SITEID) {
                    return $this->content;
                }
                $context = context_system::instance();
                if(has_capability('block/course_notifications:edit', $context)){
                    $this->content->text = '<a href="'.$CFG->wwwroot.'/blocks/course_notifications/index.php?courseid='.$courseid.'">'.get_string('admin','block_course_notifications').'</a><br>';
                    if(has_capability('moodle/site:config', $context)){
                            $this->content->text .= '<a href="'.$CFG->wwwroot.'/admin/settings.php?section=blocksettingcourse_notifications">'.get_string('settings').'</a><br>';
                    }
                }
		return $this->content;


	} //end function get_content

        function cron(){
            mtrace('starting course_notifications cron... ');
            block_course_notifications_cron();
	} //end function cron
        
	function has_config(){
		return true;
	} //end function has_config


} //end class block_course_notifications


// End of blocks/course_notifications/block_course_notifications.php
