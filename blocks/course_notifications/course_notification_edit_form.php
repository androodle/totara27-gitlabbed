<?php

/**
 * course_notifications Block: Edit form 
 * 
 * @author      Daniel Morphett <dan@androgogic.com> 
 * @version     03/09/2015 
 * @copyright   2015+ Androgogic Pty Ltd <http://www.androgogic.com> 
 * 
 * Provides edit form for the object.
 * This is used by both new and edit pages
 *  
 * */
if (!defined('MOODLE_INTERNAL')) {
    die('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}
require_once($CFG->libdir . '/formslib.php');

class course_notification_edit_form extends moodleform {

    protected $course_notification;

    function definition() {
        global $USER, $courseid, $DB, $PAGE;
        $mform = & $this->_form;
        $mailout_type = optional_param('mailout_type', '', PARAM_ALPHAEXT);
        $context = context_system::instance();
        if (isset($_REQUEST['id'])) {
            $q = "select DISTINCT a.* , mdl_course.fullname as course
from mdl_andro_course_notifications a 
LEFT JOIN mdl_course  on a.course_id = mdl_course.id
where a.id = {$_REQUEST['id']} ";
            $course_notification = $DB->get_record_sql($q);
        }
        else {
            $course_notification = $this->_customdata['$course_notification']; // this contains the data of this form
        }
        if($mailout_type == '' and !empty($course_notification->mailout_type)){
            $mailout_type = $course_notification->mailout_type;
        }
        $tab = 'course_notification_new'; // from whence we were called
        if (!empty($course_notification->id)) {
            $tab = 'course_notification_edit';
        }
        $mform->addElement('html', '<div>');
        $mform->addElement('header', 'general', get_string('general', 'form'));
//name
        $mform->addElement('text', 'name', get_string('name', 'block_course_notifications'), array('size' => 50));
        $mform->setType('name', PARAM_RAW);
        $mform->addRule('name', get_string('required'), 'required', null, 'server');
        $mform->addRule('name', 'Maximum 50 characters', 'maxlength', 50, 'client');

//subject
        $mform->addElement('text', 'subject', get_string('subject', 'block_course_notifications'), array('size' => 50));
        $mform->setType('subject', PARAM_RAW);
        $mform->addRule('subject', 'Maximum 50 characters', 'maxlength', 50, 'client');

//body
        $editoroptions = array('maxfiles' => 0, 'maxbytes' => 0, 'trusttext' => false, 'forcehttps' => false);
        $mform->addElement('editor', 'body', get_string('body', 'block_course_notifications'), null, $editoroptions);
        $mform->setType('body', PARAM_CLEANHTML);
        $mform->addRule('body', get_string('required'), 'required', null, 'server');
        $mform->addHelpButton('body', 'body', 'block_course_notifications');

//enabled
        $mform->addElement('selectyesno', 'enabled', get_string('enabled', 'block_course_notifications'));
        $mform->addRule('enabled', get_string('required'), 'required', null, 'server');
        $onclick_event = "onclick='M.core_formchangechecker.set_form_submitted();this.form.submit();'";
        $mform->addElement('radio', 'mailout_type', get_string('mailout_type', 'block_course_notifications'), get_string('recurring', 'block_course_notifications'), 'recurring',$onclick_event);
        $mform->addElement('radio', 'mailout_type', null, get_string('event_based', 'block_course_notifications'), 'event_based',$onclick_event);
        $mform->addElement('radio', 'mailout_type', null, get_string('activity_based', 'block_course_notifications'), 'activity_based',$onclick_event);

        if ($mailout_type == 'activity_based') {
            $mform->addElement('header', 'activity_header', get_string('activity_header', 'block_course_notifications'));
//activities
            $options = block_course_notifications_get_activities_for_course($courseid);
            $options = block_course_notifications_add_empty_option($options);
            $mform->addElement('select', 'cmid', get_string('activity', 'block_course_notifications'), $options);

//event
            $event_control_array = array();
            $options = array(
                'hasbeen' => 'Has been', 'hasntbeen' => 'Hasn\'t been'
            );
            $options = block_course_notifications_add_empty_option($options);
            $event_control_array[] = $mform->createElement('select', 'hasorhasnt', "", $options);
//$event_control_array[] = $mform->createElement('html', "&nbsp;hi&nbsp;");
            $options = array(
                'viewed' => 'viewed', 'completed' => 'completed', 'postedto' => 'posted to', 'attended' => 'attended'
            );
            $options = block_course_notifications_add_empty_option($options);
            $event_control_array[] = $mform->createElement('select', 'event', '', $options);
            $mform->addGroup($event_control_array, 'event_arr', get_string('event', 'block_course_notifications'));

//event_by
            $mform->addElement('date_selector', 'event_by', get_string('event_by', 'block_course_notifications'), array('optional' => true));
            
            //trigger_point
            $options = array('userenrolled' => 'User is enrolled', 'usercompletescourse' => 'User completes course');
            $options = block_course_notifications_add_empty_option($options);
            $mform->addElement('select', 'trigger_point', get_string('trigger_point', 'block_course_notifications'), $options);
//$mform->addRule('trigger_point_id', get_string('required'), 'required', null, 'server');
//days_after
            $mform->addElement('text', 'days_after', get_string('days_after', 'block_course_notifications'), array('size' => 10));
            $mform->setType('days_after', PARAM_INT);
            $mform->addRule('days_after', 'Maximum 10 characters', 'maxlength', 10, 'client');
            $mform->addHelpButton('days_after', 'days_after', 'block_course_notifications');
        
        }
        if ($mailout_type == 'event_based') {

            $mform->addElement('header', 'trigger_point_header', get_string('trigger_point_header', 'block_course_notifications'));

//trigger_point
            $options = array('userenrolled' => 'User is enrolled', 'usercompletescourse' => 'User completes course');
            $options = block_course_notifications_add_empty_option($options);
            $mform->addElement('select', 'trigger_point', get_string('trigger_point', 'block_course_notifications'), $options);
//$mform->addRule('trigger_point_id', get_string('required'), 'required', null, 'server');
//days_after
            $mform->addElement('text', 'days_after', get_string('days_after', 'block_course_notifications'), array('size' => 10));
            $mform->setType('days_after', PARAM_INT);
            $mform->addRule('days_after', 'Maximum 10 characters', 'maxlength', 10, 'client');
        }
        if ($mailout_type == 'recurring') {
            $mform->addElement('header', 'cycle_header', get_string('cycle_header', 'block_course_notifications'));

//frequency
            $options = array('daily' => 'Daily', 'weekly' => 'Weekly', 'monthly' => 'Monthly');
            $options = block_course_notifications_add_empty_option($options);
            $mform->addElement('select', 'frequency', get_string('frequency', 'block_course_notifications'), $options);
//$mform->addRule('frequency', get_string('required'), 'required', null, 'server');
//day of week - if weekly is chosen
            $options = array(
                1 => 'Monday',
                2 => 'Tuesday',
                3 => 'Wednesday',
                4 => 'Thursday',
                5 => 'Friday',
                6 => 'Saturday',
                7 => 'Sunday');
            $mform->addElement('select', 'day_of_week', get_string('day_of_week', 'block_course_notifications'), $options);
            $mform->disabledIf('day_of_week', 'frequency', 'neq', 'weekly');

//day of month - if monthly is chosen
            $options = array();
            for ($index = 1; $index < 32; $index++) {
                $options[$index] = $index;
            }
            $mform->addElement('select', 'day_of_month', get_string('day_of_month', 'block_course_notifications'), $options);
            $mform->disabledIf('day_of_month', 'frequency', 'neq', 'monthly');

//hour
            $options = array();
            for ($index = 0; $index < 24; $index++) {
                $options[$index] = $index;
            }
            $mform->addElement('select', 'hour', get_string('hour', 'block_course_notifications'), $options);
            $mform->disabledIf('hour', 'frequency', 'eq', '0');
        }

//set values if we are in edit mode
        if (!empty($course_notification->id) && isset($_GET['id'])) {
            $mform->setConstant('name', $course_notification->name);
            $mform->setConstant('subject', $course_notification->subject);
            $body['text'] = $course_notification->body;
            $body['format'] = 1;
            $mform->setDefault('body', $body);
            $mform->setConstant('enabled', $course_notification->enabled);

            $optional_elements = array('mailout_type', 'cmid', 'event_by', 'trigger_point', 'frequency', 'days_after', 'hour', 'day_of_week', 'day_of_month');
            foreach ($optional_elements as $optional_element) {
                if (!empty($course_notification->$optional_element)) {
                    $mform->setConstant($optional_element, $course_notification->$optional_element);
                }
            }
//the values in the group are tricky...
            if (!empty($course_notification->hasorhasnt) || !empty($course_notification->event)) {
                if (!empty($course_notification->hasorhasnt)) {
                    $event_arr['hasorhasnt'] = $course_notification->hasorhasnt;
                }
                if (!empty($course_notification->hasorhasnt)) {
                    $event_arr['event'] = $course_notification->event;
                }
                $mform->setConstant('event_arr', $event_arr);
            }
        }
//hiddens
        $mform->addElement('hidden', 'tab', $tab);
        $mform->setType('tab', PARAM_TEXT);
        $mform->addElement('hidden', 'course_id', $courseid);
        $mform->setType('course_id', PARAM_INT);
        $mform->addElement('hidden', 'courseid', $courseid);
        $mform->setType('courseid', PARAM_INT);
        if (isset($_REQUEST['id'])) {
            $mform->addElement('hidden', 'id', $_REQUEST['id']);
            $mform->setType('id', PARAM_INT);
        }
        elseif (isset($id)) {
            $mform->addElement('hidden', 'id', $id);
            $mform->setType('id', PARAM_INT);
        }
        $this->add_action_buttons(false);
        $mform->addElement('html', '</div>');
        $mform->addElement('html', '<p style="height:100px"></p>'); //push down the footer so we can see the submit button
    }

}
