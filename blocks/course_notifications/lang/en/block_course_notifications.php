<?php
/** 
 * course_notifications Block: Language Pack 
 * 
 * @author      Daniel Morphett <dan@androgogic.com> 
 * @version     17/04/2015 
 * @copyright   2015+ Androgogic Pty Ltd <http://www.androgogic.com> 
 * 
 *  
 **/
$string['pluginname'] = 'Course notifications';
$string['plugintitle'] = 'Course notifications';
//spares
$string['admin'] = 'Course notifications';
$string['user'] = 'User';

$string['course'] = 'Course';
$string['name'] = 'Name';
$string['created_by'] = 'Created By';
$string['date_created'] = 'Date Created';
$string['modified_by'] = 'Modified By';
$string['date_modified'] = 'Date Modified';
$string['id'] = 'Id';
//common items
$string['datasubmitted'] = 'The data has been submitted';
$string['itemdeleted'] = 'The item has been deleted';
$string['noresults'] = 'There were no results from your search';
$string['block_course_notifications:edit'] = 'Edit objects within the course_notifications block';
$string['block_course_notifications:delete'] = 'Delete objects within the course_notifications block';

//course_notification items
$string['course_notification'] = 'Course notification';
$string['body'] = 'Body';
$string['enabled'] = 'Enabled';
$string['trigger_point'] = 'Trigger Point';
$string['days_after'] = 'Days after';
$string['body_help'] = 'You can use certain placeholders in the body text, with the intention that at run time these will be replaced with entries from the database. '
        . ' These are: user, username, course and admin. They must be enclosed with square brackets in this way: [user] - ie no spaces, and lowercase. '
        . '<ul><li>[user] will be replaced by the user\'s full name. <li>[username] will be replaced by their username. '
        . '<li>[email] will be replaced by their email.<li>[site_url] will be replaced by a link to the site.'
        . '<li>[course] will be replaced with a link to the course. <li>[admin] will insert the name of the admin user.</ul>';
$string['course_notification_search'] = 'Course notifications';
$string['course_notification_plural'] = 'Course notifications';
$string['course_notification_new'] = 'New course notification';
$string['course_notification_edit'] = 'Edit course notification';
$string['course_notification_search_instructions'] = 'Search by Name or Body';
$string['course_notification_delete'] = 'Delete course notification';
//trigger_point items
$string['trigger_point_search'] = 'Trigger Points';
$string['trigger_point_plural'] = 'Trigger Points';
$string['trigger_point_new'] = 'New Trigger Point';
$string['trigger_point_edit'] = 'Edit Trigger Point';
$string['trigger_point_search_instructions'] = 'Search by Name';
$string['trigger_point_id'] = 'Trigger Point'; 
$string['andro_trigger_points'] = 'Trigger Point';


$string['editthisfile'] = 'Edit this file';
$string['upload'] = 'Upload';

//statuses
$string['notyetstarted'] = "Not yet started";
$string['inprogress'] = "In progress";
$string['complete'] = "Complete";

//course_notification_log items
$string['course_notification_log'] = 'Course notification Log';
$string['user'] = 'User';
$string['time_sent'] = 'Time Sent';
$string['course_notification_log_search'] = 'Course notification Logs';
$string['course_notification_log_plural'] = 'Course notification Logs';
$string['course_notification_log_new'] = 'New course notification Log';
$string['course_notification_log_edit'] = 'Edit course_notification Log';
$string['course_notification_log_id'] = 'Id course_notification Log';
$string['course_notification_log_search_instructions'] = 'Search by Name';
$string['startdate'] = 'Start date';
$string['enddate'] = 'End date';
$string['subject'] = 'Subject';
$string['course_notification_send_result'] = 'Result';
$string['course_notification_from_email'] = 'Course notification "from" email address';
$string['course_notification_from_email_explanation'] = 'Course notifications will come from this email address';


$string['search'] = 'Search';
$string['activity'] = 'Activity';
$string['event'] = 'Action';
$string['event_by'] = 'By';
$string['frequency'] = 'Frequency';
$string['hour'] = "Hour of day";
$string['day_of_week'] = "Day of week";
$string['day_of_month'] = "Day of month";
$string['activity_header'] = 'Activity based mailouts';
$string['trigger_point_header'] = 'Event based mailouts';
$string['cycle_header'] = 'Recurring mailouts';
$string['mailout_type'] = 'Mailout type';
$string['recurring'] = 'Recurring';
$string['event_based'] = 'Event based';
$string['activity_based'] = 'Activity based';
$string['days_after_help'] = 'In the activity context, days after can refer to the number of days after the action, or after the trigger point. For example, the notification is sent if the user has not viewed a PDF, 5 days after being enrolled.';