<?php

/** 
 * course_notifications Block: Search form 
 * 
 * @author      Daniel Morphett <dan@androgogic.com> 
 * @version     03/09/2015 
 * @copyright   2015+ Androgogic Pty Ltd <http://www.androgogic.com> 
 * 
 * Provides search form for the object.
 * This is used by search page
 *  
 **/

if (!defined('MOODLE_INTERNAL')) {
die('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}
require_once($CFG->libdir . '/formslib.php');
class course_notification_search_form extends moodleform {
function definition() {
global $DB;
$mform =& $this->_form;
foreach($this->_customdata as $custom_key=>$custom_value){
$$custom_key = $custom_value;
}
$mform->addElement('html','<div>');
//search controls
$mform->addElement('text','search',get_string('course_notification_search_instructions', 'block_course_notifications'));
$mform->setType('search', PARAM_RAW);
//set some values if they have been passed in
foreach($this->_customdata as $custom_key=>$custom_value){
    if(isset($mform->_elementIndex[$custom_key])){
        $mform->setConstant($custom_key,$custom_value);
    }
}
//hiddens
$mform->addElement('hidden','tab',$tab);
$mform->setType('tab', PARAM_TEXT);
$mform->addElement('hidden','sort',$sort);
$mform->setType('sort', PARAM_TEXT);
$mform->addElement('hidden','dir',$dir);
$mform->setType('dir', PARAM_TEXT);
$mform->addElement('hidden','perpage',$perpage);
$mform->setType('perpage', PARAM_INT);
$mform->addElement('hidden','courseid',$courseid);
$mform->setType('courseid', PARAM_INT);
//button
$mform->addElement('submit','submit','Search');
$mform->addElement('html','</div>');
}
}
