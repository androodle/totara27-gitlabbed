<?php
/** 
 * Course Notifications Block: Tabs 
 * 
 * @author      Daniel Morphett <dan@androgogic.com> 
 * @version     17/04/2015 
 * @copyright   2015+ Androgogic Pty Ltd <http://www.androgogic.com> 
 * 
 *  
 **/
global $courseid;   
// This file to be included so we can assume config.php has already been included.
if (empty($currenttab)) {
error('You cannot call this script in that way');
}
$tabs = array();
$row = array();
$row[] = new tabobject('course_notification_search', $CFG->wwwroot.'/blocks/course_notifications/index.php?tab=course_notification_search&courseid='.$courseid, get_string('course_notification_search','block_course_notifications'));
$row[] = new tabobject('course_notification_log_search', $CFG->wwwroot.'/blocks/course_notifications/index.php?tab=course_notification_log_search&courseid='.$courseid, get_string('course_notification_log_search','block_course_notifications'));

$tabs[] = $row;
// Print out the tabs and continue!
print_tabs($tabs, $currenttab);

// End of blocks/course_notifications/tabs.php