<?php

/**
 * course_notifications Block: Search 
 * 
 * @author      Daniel Morphett <dan@androgogic.com> 
 * @version     03/09/2015 
 * @copyright   2015+ Androgogic Pty Ltd <http://www.androgogic.com> 
 * 
 * Search course_notifications
 * Also provides access to edit and delete functions if user has sufficient permissions
 *  
 * */
//params
global $courseid;
$sort = optional_param('sort', 'name', PARAM_RAW);
$dir = optional_param('dir', 'ASC', PARAM_ALPHA);
$page = optional_param('page', 0, PARAM_INT);
$perpage = optional_param('perpage', 20, PARAM_INT);
$search = optional_param('search', '', PARAM_TEXT);
$tab = optional_param('tab', 'course_notification_search', PARAM_FILE);
$course_id = optional_param('course_id', 0, PARAM_INT);

// prepare url for paging bar
$PAGE->set_url($PAGE->url, compact('sort', 'dir', 'page', 'perpage', 'search', 'tab', 'course_id','courseid'));
// prepare columns for results table
$columns = array(
    "name",
    "subject",
    "body",
    "enabled",
    "trigger_point",
    "frequency",
    
);
foreach ($columns as $column) {
    $string[$column] = get_string("$column", 'block_course_notifications');
    if ($sort != $column) {
        $columnicon = '';
        $columndir = 'ASC';
    }
    else {
        $columndir = $dir == 'ASC' ? 'DESC' : 'ASC';
        $columnicon = $dir == 'ASC' ? 'down' : 'up';
    }
    if ($column != 'details') {
        $$column = "<a href='$PAGE->url&dir=$columndir&sort=$column'>$string[$column]</a>";
    }
    else {
        $$column = $string[$column];
    }
}
//figure out the and clause from what has been submitted
$and = '';
if ($search != '') {
    $and .= " and " . $DB->sql_concat(
"coalesce(a.name,'')",
"coalesce(a.body,'')",
"coalesce(a.subject,'')"
) . " like '%$search%'";
}
$q = "select DISTINCT a.*  
from mdl_andro_course_notifications a
where course_id = $courseid
$and 
order by $sort $dir";
if (isset($_GET['debug'])) {
    echo '$query : ' . $q . '';
}
if ($download == '') {
//get a page worth of records
    $results = $DB->get_records_sql($q, array(), $page * $perpage, $perpage);
}
else {
    $results = $DB->get_records_sql($q);
}
if ($download == '') {
//also get the total number we have of these
    $q = "SELECT COUNT(DISTINCT a.id)
from mdl_andro_course_notifications a 
 where course_id = $courseid  $and";
}
if (isset($_GET['debug'])) {
    echo '$query : ' . $q . '<br>';
}

$result_count = $DB->get_field_sql($q);
if ($download == '') {

    require_once('course_notification_search_form.php');
    $mform = new course_notification_search_form(null, array('sort' => $sort, 'dir' => $dir, 'perpage' => $perpage, 'search' => $search, 
        'tab' => $currenttab, 'course_id' => $course_id,'courseid' => $courseid));
    $mform->display();
    echo '<table width="100%"><tr><td width="50%">';
    echo $result_count . ' ' . get_string('course_notification_plural', 'block_course_notifications') . " found" . '<br>';
    echo '</td><td style="text-align:right;">';
    if (has_capability('block/course_notifications:edit', $context)) {
        $link = "<a href='index.php?tab=course_notification_new&courseid=$courseid'>" . get_string('course_notification_new', 'block_course_notifications') . "</a>";
        echo $link;
    }
    echo '</td></tr></table>';
}

//RESULTS
if (!$results) {
    if ($download == '') {
        echo $OUTPUT->heading(get_string('noresults', 'block_course_notifications', $search));
    }
}
else {
    $table = new html_table();
    $table->head = array(
        $name,
        $subject,
        $body,
        $enabled,
        $trigger_point,
        $frequency,
        'Action'
    );
    $table->width = "95%";
    foreach ($results as $result) {
        $preview_link = '';  
        $edit_link = "";
        $delete_link = "";
        if (has_capability('block/course_notifications:edit', $context)) {
// then we show the edit link
            $edit_link = "<a href='index.php?tab=course_notification_edit&id={$result->id}&courseid=$courseid'>Edit</a> ";
            $preview_link = "<a href='course_notification_preview.php?id={$result->id}&courseid=$courseid' target='_blank'>Preview</a> ";
        }
        if (has_capability('block/course_notifications:delete', $context)) {
// then we show the delete link
            $delete_link = "<a href='index.php?tab=course_notification_delete&id={$result->id}&courseid=$courseid' onclick='return confirm(\"Are you sure you want to delete this item?\")'>Delete</a> ";
        }
        $table->data[] = array(
            $result->name,
            $result->subject,
            $result->body,
            $result->enabled,
            $result->trigger_point,
            $result->frequency,
            $edit_link . $delete_link . $preview_link
        );
    }
}
if (!empty($table)) {
    if ($download != '') {
//export the table to whatever they asked for
        block_course_notifications_export_data($download, $table, $tab);
    }
    else {
        echo html_writer::table($table);
        $pagingbar = new paging_bar($result_count, $page, $perpage, $PAGE->url);
        $pagingbar->pagevar = 'page';
        echo $OUTPUT->render($pagingbar);
        block_course_notifications_output_download_links($PAGE->url, 'course_notification_plural');
    }
}
echo ('<p style="height:100px"></p>');//push down the footer so we can see stuff