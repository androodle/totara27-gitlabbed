<?php

/** 
 * Block Course Notifications Block: Upload a file 
 * 
 * @author      Daniel Morphett <dan@androgogic.com> 
 * @version     11/05/2015 
 * @copyright   2015+ Androgogic Pty Ltd <http://www.androgogic.com> 
 * 
 *  
 **/

require_once(dirname(__FILE__).'/upload_form.php');
$objectid = required_param('objectid', PARAM_INT);
$object = required_param('object', PARAM_ALPHAEXT);
$fileid = required_param('fileid', PARAM_INT);
$block_name = required_param('block_name', PARAM_ALPHAEXT);
$table_name = required_param('table_name', PARAM_ALPHAEXT);
$foreign_key_name = required_param('foreign_key_name', PARAM_ALPHAEXT);
$mform = new block_course_notifications_uploadsingle_form(null, array('objectid'=>$objectid, 'object'=>$object, 'fileid'=>$fileid,'block_name'=>$block_name,'table_name'=>$table_name,'foreign_key_name'=>$foreign_key_name));
$redirect_url = new moodle_url('/blocks/course_notifications/index.php', array('tab'=>$object.'_edit','id'=>$objectid));
if ($mform->is_cancelled()) {
    redirect($redirect_url);
} else if ($mform->get_data()) {
    //save the uploaded file over the top of the existing one
    block_course_notifications_save_upload_over_existing($mform,$block_name,$table_name,$foreign_key_name); 
    //send them back to where they came from
    redirect($redirect_url);
}
echo $OUTPUT->box_start('generalbox');
$mform->display();
echo $OUTPUT->box_end();
