<?php

/** 
 * Course Notifications Block: Search form 
 * 
 * @author      Daniel Morphett <dan@androgogic.com> 
 * @version     25/05/2015 
 * @copyright   2015+ Androgogic Pty Ltd <http://www.androgogic.com> 
 * 
 * Provides search form for the object.
 * This is used by search page
 *  
 **/

if (!defined('MOODLE_INTERNAL')) {
die('Direct access to this script is forbidden.');    ///  It must be included from a Moodle page
}
require_once($CFG->libdir . '/formslib.php');
class course_notification_log_search_form extends moodleform {
function definition() {
global $DB;
$mform =& $this->_form;
foreach($this->_customdata as $custom_key=>$custom_value){
$$custom_key = $custom_value;
}
$mform->addElement('html','<div>');
//search controls
$mform->addElement('text','search',get_string('course_notification_log_search_instructions', 'block_course_notifications'));
$mform->setType('search', PARAM_RAW);
//course_notification_id
$dboptions = $DB->get_records_menu('andro_course_notifications',array(),'name','id,name');
unset($options);
$options[0] = 'Any';
foreach($dboptions as $key=>$value){
$options[$key] = $value;
}
$select = $mform->addElement('select', 'andro_course_notifications_id', get_string('course_notification','block_course_notifications'), $options);
//user_id
$dboptions = $DB->get_records_menu('user',array(),'lastname','id,' . $DB->sql_concat('firstname',"' '",'lastname') .' as fullname');
unset($options);
$options[0] = 'Any';
foreach($dboptions as $key=>$value){
$options[$key] = $value;
}
$select = $mform->addElement('select', 'user_id', get_string('user','block_course_notifications'), $options);
//course_id
$dboptions = $DB->get_records_menu('course',array(),'fullname','id,fullname');
unset($options);
$options[0] = 'Any';
foreach($dboptions as $key=>$value){
$options[$key] = $value;
}
$select = $mform->addElement('select', 'course_id', get_string('course','block_course_notifications'), $options);
//time_sent
$mform->addElement('date_selector','startdate',get_string('startdate','block_course_notifications'), array('optional'=>true));
$mform->addElement('date_selector','enddate',get_string('enddate','block_course_notifications'), array('optional'=>true));
if($startdate > 0){
$mform->setConstant('startdate', $startdate);
}
if($enddate > 0){
$mform->setConstant('enddate', $enddate);
}
//set some values if they have been passed in
foreach($this->_customdata as $custom_key=>$custom_value){
    if(isset($mform->_elementIndex[$custom_key])){
        $mform->setConstant($custom_key,$custom_value);
    }
}
//hiddens
$mform->addElement('hidden','tab',$tab);
$mform->setType('tab', PARAM_TEXT);
$mform->addElement('hidden','sort',$sort);
$mform->setType('sort', PARAM_TEXT);
$mform->addElement('hidden','dir',$dir);
$mform->setType('dir', PARAM_TEXT);
$mform->addElement('hidden','perpage',$perpage);
$mform->setType('perpage', PARAM_INT);
$mform->addElement('hidden', 'debug', $debug);
$mform->setType('debug', PARAM_INT);
$mform->addElement('hidden','courseid',$courseid);
$mform->setType('courseid', PARAM_INT);
//button
$mform->addElement('submit','submit','Search');
$mform->addElement('html','</div>');
}
}
