<?php
/** 
 * Course Notifications Block: Version 
 * 
 * @author      Daniel Morphett <dan@androgogic.com> 
 * @version     17/04/2015 
 * @copyright   2015+ Androgogic Pty Ltd <http://www.androgogic.com> 
 * 
 *  
 **/

$plugin->version = 2015041900;
$plugin->requires = 2010112400; // Indicate we need at least Moodle 2.0
$plugin->component = 'block_course_notifications'; // Full name of the plugin (used for diagnostics)
$plugin->cron      = 60; // Period for cron to check this module (secs)

// End of blocks/course_notifications/version.php