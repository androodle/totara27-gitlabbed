<?php


/** 
 * Course Notifications Block: Settings 
 * 
 * @author      Daniel Morphett <dan@androgogic.com> 
 * @version     17/04/2015 
 * @copyright   2015+ Androgogic Pty Ltd <http://www.androgogic.com> 
 * 
 *  
 **/



defined('MOODLE_INTERNAL') || die(); 



if ($ADMIN->fulltree) {
//example text field (INT). CHANGE MY NAME!
//$settings->add(new admin_setting_configtext('block_course_notifications/run_cron_hours', get_string('cron_hours', 'block_course_notifications'),
//               get_string('cron_hours_explanation', 'block_course_notifications'), '', PARAM_INT));
//course_notification_from_email
$settings->add(new admin_setting_configtext('block_course_notifications/course_notification_from_email', get_string('course_notification_from_email', 'block_course_notifications'),
               get_string('course_notification_from_email_explanation', 'block_course_notifications'), '', PARAM_EMAIL));


}

// End of blocks/course_notifications/settings.php
