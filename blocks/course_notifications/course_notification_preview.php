<?php

/** 
 * course_notifications Block: Preview 
 * 
 * @author      Daniel Morphett <dan@androgogic.com> 
 * @version     03/09/2015 
 * @copyright   2015+ Androgogic Pty Ltd <http://www.androgogic.com> 
 * 
 * Preview course_notification
 *  
 **/
require('../../config.php');
require('lib.php');
global $OUTPUT,$courseid,$DB;
$id = required_param('id', PARAM_INT);
$course_notification = $DB->get_record('andro_course_notifications',array('id'=>$id));
$context = context_system::instance();
require_login();
require_capability('block/course_notifications:edit', $context);

$user = new stdClass();
$user->firstname = "Joe";
$user->lastname = "Blow";
$user->username = "jblow";
$user->email = "jblow@gmail.com";

echo $OUTPUT->header();

echo get_course_notification_body_text($course_notification,$user);

echo $OUTPUT->footer();