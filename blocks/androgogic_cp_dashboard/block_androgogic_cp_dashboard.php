<?php
/** 
 * Androgogic Cp Dashboard Block: Class
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     01/08/2014
 * @copyright   2014+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 *
 **/

class block_androgogic_cp_dashboard extends block_base {

	function init(){
		$this->title = get_string('plugintitle','block_androgogic_cp_dashboard');
	} //end function init

	function get_content(){
		global $CFG,$USER,$DB;
                require_once($CFG->dirroot . '/blocks/androgogic_training_history/lib.php');
                if ($this->content !== null) {
			return $this->content;
		}
                $this->content = new stdClass;
                if(isset($USER->id)){
                
                    //for the targets we need the lowest hanging match on the org/pos hierarchies 
                    //that matches their primary pos assignment
                    $user_org_pos = block_androgogic_training_history_get_user_org_pos($USER);
                    if(!$user_org_pos){
                        // then it's all over
                        $this->content->text = "";
                        return $this->content;
                    }
                    $org_pos_period = block_androgogic_training_history_get_org_pos_period(
                            $user_org_pos->org_framework_id,$user_org_pos->pos_framework_id,
                            $user_org_pos->pos_sortthread,$user_org_pos->org_sortthread, $USER->id);
                    if(!$org_pos_period){
                        // then it's all over
                        $this->content->text = "";
                        return $this->content;
                    }

                    list($period_start_date,$period_end_date) = block_androgogic_training_history_get_cpd_period($USER, $org_pos_period);
                    
                    //let's get their targets
                    $targets = block_androgogic_training_history_get_targets($USER,$org_pos_period,'',$period_start_date,$period_end_date);

                    $total_cpd_target = $targets->total_cpd_target;
                    $total_cpe_target = $targets->total_cpe_target;
                    
                    //and points earned
                    $points_and_hours = block_androgogic_training_history_get_points_and_hours($USER->id,null,$period_start_date,$period_end_date);
        
                    $total_cpd_points_earned = $points_and_hours->total_completed_points;
                    $total_cpe_hours_earned = $points_and_hours->total_completed_hours;
                    
                    if($total_cpd_target > 0){
                        $percentage_cpd_points_earned = $total_cpd_points_earned*100/$total_cpd_target;
                    }
                    else{
                        $percentage_cpd_points_earned = 0;
                    }
                    if($total_cpe_target == 0){
                        $percentage_cpe_hours_earned = 0;
                    }
                    else{
                        $percentage_cpe_hours_earned = $total_cpe_hours_earned*100/$total_cpe_target;
                    }
                    
                    ob_start();
                    $progressbarcpd = new progress_bar('',200);
                    $progressbarcpd->create();
                    $progressbarcpd->update_full($percentage_cpd_points_earned, number_format($total_cpd_points_earned,2));
                    $progressbarcpd_output = ob_get_contents();
                    ob_end_clean();
                    $progressbarcpd_output = str_ireplace('center','left',$progressbarcpd_output);                
                    $progressbarcpd_output = str_ireplace('margin:0 auto;','',$progressbarcpd_output);
                    //$progressbarcpd_output = str_ireplace($total_cpd_points_earned,$total_cpd_target,$progressbarcpd_output);
                    $progressbarcpd_output = str_ireplace('update_progress_bar','update_progress_bar_dashboard',$progressbarcpd_output);

                    ob_start();
                    $progressbarcpe = new progress_bar('',200);
                    $progressbarcpe->create();
                    $progressbarcpe->update_full($percentage_cpe_hours_earned, number_format($total_cpe_hours_earned,2));
                    $progressbarcpe_output = ob_get_contents();
                    ob_end_clean();
                    $progressbarcpe_output = str_ireplace('center','left',$progressbarcpe_output);
                    $progressbarcpe_output = str_ireplace('margin:0 auto;','',$progressbarcpe_output);
                    //$progressbarcpe_output = str_ireplace($total_cpe_hours_earned,$total_cpe_target,$progressbarcpe_output);
                    $progressbarcpe_output = str_ireplace('update_progress_bar','update_progress_bar_dashboard',$progressbarcpe_output);

                    $total_cpd_target_formatted = number_format($total_cpd_target,2);
                    $total_cpe_target_formatted = number_format($total_cpe_target,2);
                    $this->content->text = "<table border='0'>"
                            . "<tr><td colspan='2'>CPD</td></tr><tr><td>$progressbarcpd_output</td><td valign='middle'>&nbsp;$total_cpd_target_formatted</td></tr>"
                            . "<tr><td colspan='2'>CPE</td></tr><tr><td>$progressbarcpe_output</td><td valign='middle'>&nbsp;$total_cpe_target_formatted</td></tr>"
                            . "</table>";
                    $period_statement = "<p>Period</p><p>" . date('d/m/Y',$period_start_date) ." to " . date('d/m/Y',$period_end_date) . "</p>";
                    $this->content->text .= $period_statement;
		
                }
                else{
                    $this->content->text = "";
                }
                return $this->content;

	} //end function get_content

	function has_config(){
            return true;
	} //end function has_config

} //end class block_androgogic_cp_dashboard

// End of blocks/androgogic_cp_dashboard/block_androgogic_cp_dashboard.php