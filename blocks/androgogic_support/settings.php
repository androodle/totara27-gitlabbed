<?php
/** 
 * Androgogic Support Block: Settings
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     06/06/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 *
 **/

defined('MOODLE_INTERNAL') || die(); 

if ($ADMIN->fulltree) {
    //support to email address
    $settings->add(new admin_setting_configtext('support_to_email_address', get_string('support_to_email_address', 'block_androgogic_support'),
               get_string('support_to_email_address_explanation', 'block_androgogic_support'), '', PARAM_EMAIL));
    //support from email address
//    $settings->add(new admin_setting_configtext('support_from_email_address', get_string('support_from_email_address', 'block_androgogic_support'),
//               get_string('support_from_email_address_explanation', 'block_androgogic_support'), '', PARAM_EMAIL));
//    //support noreply email address
//    $settings->add(new admin_setting_configtext('support_replyto_email_address', get_string('support_replyto_email_address', 'block_androgogic_support'),
//               get_string('support_replyto_email_address_explanation', 'block_androgogic_support'), '', PARAM_EMAIL));
    
}

// End of blocks/androgogic_support/settings.php