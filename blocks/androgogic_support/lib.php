<?php
/** 
 * Androgogic Support Block: Lib
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     06/06/2013
 * @copyright   2013+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 *
 **/

 function androgogic_support_path_from_hash($contenthash) {
    $l1 = $contenthash[0].$contenthash[1];
    $l2 = $contenthash[2].$contenthash[3];
    return "filedir/$l1/$l2";
}

// End of blocks/androgogic_support/lib.php