<?php

/**
 * Facetoface Block: Search
 *
 * @author Daniel Morphett
 * @version 11/09/2014
 * @copyright 2014+ Androgogic Pty Ltd
 *
 * Search facetoface_webservice_logs
 * Also provides access to edit and delete functions if user has sufficient permissions
 *
 * */
require_once(dirname(dirname(dirname(__FILE__))) . '/config.php');
require_once($CFG->dirroot . '/mod/facetoface/lib.php');

//params
$sort = optional_param('sort', 'error_message', PARAM_RAW);
$dir = optional_param('dir', 'ASC', PARAM_ALPHA);
$page = optional_param('page', 0, PARAM_INT);
$perpage = optional_param('perpage', 20, PARAM_INT);
$search = optional_param('search', '', PARAM_TEXT);
$user_id = optional_param('user_id', 0, PARAM_INT);
$facetoface_sessions_id = optional_param('facetoface_sessions_id', 0, PARAM_INT);
$s = required_param('s', PARAM_INT);
$download = optional_param('download', '', PARAM_TEXT);

// Load data
if (!$session = facetoface_get_session($s)) {
    print_error('error:incorrectcoursemodulesession', 'facetoface');
}
if (!$facetoface = $DB->get_record('facetoface', array('id' => $session->facetoface))) {
    print_error('error:incorrectfacetofaceid', 'facetoface');
}
if (!$course = $DB->get_record('course', array('id' => $facetoface->course))) {
    print_error('error:coursemisconfigured', 'facetoface');
}
if (!$cm = get_coursemodule_from_instance('facetoface', $facetoface->id, $course->id)) {
    print_error('error:incorrectcoursemodule', 'facetoface');
}
$context = context_course::instance($course->id);
$contextmodule = context_module::instance($cm->id);
require_login();

add_to_log($course->id, 'facetoface', 'view webservice logs', "view.php?id=$cm->id", $facetoface->id, $cm->id);

$PAGE->set_context($contextmodule);

$pagetitle = format_string($facetoface->name);

$PAGE->set_url('/mod/facetoface/attendees.php', array('s' => $s));
$PAGE->set_cm($cm);
$PAGE->set_pagelayout('standard');

$PAGE->set_title($pagetitle);
$PAGE->set_heading($course->fullname);

// prepare url for paging bar
$PAGE->set_url($PAGE->url, compact('sort', 'dir', 'page', 'perpage', 'search', 's'));
// prepare columns for results table
$columns = array(
    "user",
    "session",
    "attendance_status",
    "error_message",
    "result",
    "datecreated"
);
foreach ($columns as $column) {
    $string[$column] = get_string("$column", 'mod_facetoface');
    if ($sort != $column) {
        $columnicon = '';
        $columndir = 'ASC';
    } else {
        $columndir = $dir == 'ASC' ? 'DESC' : 'ASC';
        $columnicon = $dir == 'ASC' ? 'down' : 'up';
    }
    if ($column != 'details') {
        $$column = "<a href='$PAGE->url&amp;dir=$columndir&amp;sort=$column'>$string[$column]</a>";
    } else {
        $$column = $string[$column];
    }
}
//figure out the and clause from what has been submitted
$and = '';
if ($search != '') {
    $and .= " and concat(
ifnull(a.error_message,''),
a.result, mdl_user.firstname, ' ', mdl_user.lastname
) like '%$search%'";
}

//are we filtering on facetoface_sessions?
if ($facetoface_sessions_id > 0) {
    $and .= " and mdl_facetoface_sessions.id = $facetoface_sessions_id ";
}
$q = "select DISTINCT a.* , CONCAT(mdl_user.firstname,' ',mdl_user.lastname) as user, f.name as session
from mdl_facetoface_webservice_log a
LEFT JOIN mdl_user on a.user_id = mdl_user.id
LEFT JOIN mdl_facetoface_sessions on a.session_id = mdl_facetoface_sessions.id
LEFT JOIN mdl_facetoface f on mdl_facetoface_sessions.facetoface = f.id

where 1 = 1
$and
order by $sort $dir";
if (isset($_GET['debug'])) {
    echo '$query : ' . $q . '';
}
//get a page worth of records
$results = $DB->get_records_sql($q, array(), $page * $perpage, $perpage);
//also get the total number we have of these
$q = "SELECT COUNT(DISTINCT a.id)
from mdl_facetoface_webservice_log a
LEFT JOIN mdl_user on a.user_id = mdl_user.id
LEFT JOIN mdl_facetoface_sessions on a.session_id = mdl_facetoface_sessions.id
where 1 = 1 $and";

if (isset($_GET['debug'])) {
    echo '$query : ' . $q . '
';
}
$result_count = $DB->get_field_sql($q);
if ($download == '') {
    echo $OUTPUT->header();
    require_once('webservice_log_search_form.php');
    $mform = new facetoface_webservice_log_search_form(null, array('sort' => $sort, 'dir' => $dir, 'perpage' => $perpage, 'search' => $search, 'user_id' => $user_id, 'facetoface_sessions_id' => $facetoface_sessions_id));
    $mform->display();
    echo '<table width="100%"><tr><td width="50%">';
    echo $result_count . ' ' . get_string('facetoface_webservice_log_plural', 'mod_facetoface') . " found" . '
';
    echo '</td><td style="text-align:right;">';
    
    echo '</td></tr></table>';
}

//RESULTS
if (!$results) {
    if ($download == '') {
        echo $OUTPUT->heading(get_string('noresults', 'mod_facetoface', $search));
    }
} else {
    $table = new html_table();
    $table->head = array(
        $user,
        $session,
        $attendance_status,
        $error_message,
        $result,
        $datecreated,
        'Action'
    );
    $table->align = array("left", "left", "left", "left", "left", "left", "left",);
    $table->width = "95%";
    $table->size = array("14%", "14%", "14%", "14%", "14%", "14%", "14%",);
    foreach ($results as $result) {
        $view_link = ''; 
        $edit_link = "";
        $delete_link = "";
        $status = facetoface_get_status($result->attendance_status);
        $status = ucfirst($status);
        $status = str_ireplace('_', ' ', $status);
        
        $table->data[] = array(
            "$result->user",
            "$result->session",
            $status,
            "$result->error_message",
            "$result->result",
            date('Y-m-d h:i:s',$result->date_created),
            $view_link . $edit_link . $delete_link
        );
    }
}
if (!empty($table)) {
    if ($download != '') {
//export the table to whatever they asked for
        facetoface_export_data($download, $table, "webservice_logs");
    } else {
        
        echo html_writer::table($table);
        $pagingbar = new paging_bar($result_count, $page, $perpage, $PAGE->url);
        $pagingbar->pagevar = 'page';
        echo $OUTPUT->render($pagingbar);
//        echo 'Download: ';
//        $download_report_url = $PAGE->url . "&download=csv";
//        echo html_writer::link($download_report_url, 'csv');
//        echo "&nbsp;";
//        $download_report_url = $PAGE->url . "&download=excel";
//        echo html_writer::link($download_report_url, 'excel');
        echo $OUTPUT->footer($course);
    }
}