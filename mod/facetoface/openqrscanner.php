<?php
/**
 * Facetoface Block: Open QR Scanner
 *
 * @author Michael Dunstan <michael@androgogic.com>
 * @version 16/10/2014
 * @copyright 2014+ Androgogic Pty Ltd
 *
 * Redirect browser to custom url scheme used by the Attendance app.
 *
 **/

require_once('../../config.php');

// Get suitable webservice token for user. Create one if required.
// Implementation copied from login/token.php
function get_webservice_token($serviceshortname) {
    global $CFG, $USER, $DB;

    //Non admin can not authenticate if maintenance mode
    $hassiteconfig = has_capability('moodle/site:config', context_system::instance(), $USER);
    if (!empty($CFG->maintenance_enabled) and !$hassiteconfig) {
        throw new moodle_exception('sitemaintenance', 'admin');
    }

    if (isguestuser($USER)) {
        throw new moodle_exception('noguest');
    }
    if (empty($USER->confirmed)) {
        throw new moodle_exception('usernotconfirmed', 'moodle', '', $USER->username);
    }
    // check credential expiry
    $userauth = get_auth_plugin($USER->auth);
    if (!empty($userauth->config->expiration) and $userauth->config->expiration == 1) {
        $days2expire = $userauth->password_expire($USER->username);
        if (intval($days2expire) < 0 ) {
            throw new moodle_exception('passwordisexpired', 'webservice');
        }
    }

    // let enrol plugins deal with new enrolments if necessary
    enrol_check_plugins($USER);

    // setup user session to check capability
    session_set_user($USER);

    //check if the service exists and is enabled
    $service = $DB->get_record('external_services', array('shortname' => $serviceshortname, 'enabled' => 1));
    if (empty($service)) {
        // will throw exception if no token found
        throw new moodle_exception('servicenotavailable', 'webservice');
    }

    //check if there is any required system capability
    if ($service->requiredcapability and !has_capability($service->requiredcapability, context_system::instance(), $USER)) {
        throw new moodle_exception('missingrequiredcapability', 'webservice', '', $service->requiredcapability);
    }

    //specific checks related to user restricted service
    if ($service->restrictedusers) {
        $authoriseduser = $DB->get_record('external_services_users',
            array('externalserviceid' => $service->id, 'userid' => $USER->id));

        if (empty($authoriseduser)) {
            throw new moodle_exception('usernotallowed', 'webservice', '', $serviceshortname);
        }

        if (!empty($authoriseduser->validuntil) and $authoriseduser->validuntil < time()) {
            throw new moodle_exception('invalidtimedtoken', 'webservice');
        }

        if (!empty($authoriseduser->iprestriction) and !address_in_subnet(getremoteaddr(), $authoriseduser->iprestriction)) {
            throw new moodle_exception('invalidiptoken', 'webservice');
        }
    }

    //Check if a token has already been created for this user and this service
    //Note: this could be an admin created or an user created token.
    //      It does not really matter we take the first one that is valid.
    $tokenssql = "SELECT t.id, t.sid, t.token, t.validuntil, t.iprestriction
              FROM {external_tokens} t
             WHERE t.userid = ? AND t.externalserviceid = ? AND t.tokentype = ?
          ORDER BY t.timecreated ASC";
    $tokens = $DB->get_records_sql($tokenssql, array($USER->id, $service->id, EXTERNAL_TOKEN_PERMANENT));

    //A bit of sanity checks
    foreach ($tokens as $key=>$token) {

        /// Checks related to a specific token. (script execution continue)
        $unsettoken = false;
        //if sid is set then there must be a valid associated session no matter the token type
        if (!empty($token->sid)) {
            $session = session_get_instance();
            if (!$session->session_exists($token->sid)){
                //this token will never be valid anymore, delete it
                $DB->delete_records('external_tokens', array('sid'=>$token->sid));
                $unsettoken = true;
            }
        }

        //remove token if no valid anymore
        //Also delete this wrong token (similar logic to the web service servers
        //    /webservice/lib.php/webservice_server::authenticate_by_token())
        if (!empty($token->validuntil) and $token->validuntil < time()) {
            $DB->delete_records('external_tokens', array('token'=>$token->token, 'tokentype'=> EXTERNAL_TOKEN_PERMANENT));
            $unsettoken = true;
        }

        // remove token if its ip not in whitelist
        if (isset($token->iprestriction) and !address_in_subnet(getremoteaddr(), $token->iprestriction)) {
            $unsettoken = true;
        }

        if ($unsettoken) {
            unset($tokens[$key]);
        }
    }

    // if some valid tokens exist then use the most recent
    if (count($tokens) > 0) {
        $token = array_pop($tokens);
    } else {
        if ( ($serviceshortname == MOODLE_OFFICIAL_MOBILE_SERVICE and has_capability('moodle/webservice:createmobiletoken', get_system_context()))
                //Note: automatically token generation is not available to admin (they must create a token manually)
                or (!is_siteadmin($USER) && has_capability('moodle/webservice:createtoken', get_system_context()))) {
            // if service doesn't exist, dml will throw exception
            $service_record = $DB->get_record('external_services', array('shortname'=>$serviceshortname, 'enabled'=>1), '*', MUST_EXIST);
            // create a new token
            $token = new stdClass;
            $token->token = md5(uniqid(rand(), 1));
            $token->userid = $USER->id;
            $token->tokentype = EXTERNAL_TOKEN_PERMANENT;
            $token->contextid = context_system::instance()->id;
            $token->creatorid = $USER->id;
            $token->timecreated = time();
            $token->externalserviceid = $service_record->id;
            $tokenid = $DB->insert_record('external_tokens', $token);
            add_to_log(SITEID, 'webservice', 'automatically create user token', '' , 'User ID: ' . $USER->id);
            $token->id = $tokenid;
        } else {
            throw new moodle_exception('cannotcreatetoken', 'webservice', '', $serviceshortname);
        }
    }

    // log token access
    $DB->set_field('external_tokens', 'lastaccess', time(), array('id'=>$token->id));

    add_to_log(SITEID, 'webservice', 'sending requested user token', '' , 'User ID: ' . $USER->id);

    return $token->token;
}

// Return a URL of the following form suitable for opening the Attendance app:
//
//    facetoface://?siteurl=http%3A%2F%2Fmoodle&token=1234&sessionid=1
function get_url($session) {
    global $CFG;
    $token = get_webservice_token('moodle_mobile_app');
    return 'facetoface://?siteurl=' . urlencode($CFG->wwwroot) . '&token=' . urlencode($token) . '&sessionid=' . urlencode($session);
}

require_login();
$session = required_param('session', PARAM_INT);
$url = get_url($session);
header('Location: ' . $url, true, 303);
die();
?>
