VET Commons Plugin for Moodle
=============================

The VET Commons plugin is a ready-made content channel, which allows for 
authenticated Moodle users to search and discover and use content. 
By downloading this plugin, authenticated users agree to the conditions in the
LICENCE.txt file alongside this README.

Requirements
------------

The RMC plug-in requires the PHP mcrypt library to be installed in order to access
the content services.

How do I install my plugin?
--------------------------

1. Unzip the folder and copy the mod/rmc folder (and file tree) to Moodle's mod/
directory.

2. On next administrative login, follow the prompts to install the RMC plug-in and
update the Moodle Database.

3. You will be prompted to enter an access token which should have been emailed
to you. If you do not have an access token already, please request one at: 
http://eworks.edu.au/vetcommons.

4. You will be prompted to set an Alfresco content object id. Accept the default
and continue.

The "Ready Made Content" option should now appear when you "Add an activity or
resource" within a course.

For more information on how to install this plugin, please contact eWorks.


How do I upgrade my plugin?
---------------------------

If upgrading from an older version of RMC (2014042802 or earlier) there were two Moodle
supplied files which were altered by RMC. These files should be reverted to the Moodle
supplied originals when installing RMC v3.0 or above, as the changes in these files are
no longer required. The two files are:

course/mod.php
course/modlib.php
