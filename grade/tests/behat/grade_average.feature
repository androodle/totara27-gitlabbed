@core @core_grades
Feature: Average grades are displayed in the gradebook
  In order to check the expected results are displayed
  As an admin
  I need to assign grades and check that they display correctly in the gradebook.

  Background:
    Given the following "courses" exist:
      | fullname | shortname | format |
      | Course 1 | C1 | topics |
    And the following "users" exist:
      | username | firstname | lastname | email |
      | teacher1 | Teacher | 1 | teacher1@example.com |
      | student1 | Student | 1 | student1@example.com |
      | student2 | Student | 2 | student2@example.com |
      | student3 | Student | 3 | student3@example.com |
    And the following "course enrolments" exist:
      | user | course | role |
      | teacher1 | C1 | editingteacher |
      | student1 | C1 | student |
      | student2 | C1 | student |
      | student3 | C1 | student |
    And I log in as "admin"
    And I am on homepage
    And I follow "Course 1"
    # Enable averages
    And I navigate to "Grades" node in "Course administration"
    And I navigate to "Course grade settings" node in "Grade administration"
    And I set the following fields to these values:
      | Show average | Show |
    And I click on "Save changes" "button"
    # Add a manual grade item
    And I navigate to "Full view" node in "Grade administration > Categories and items"
    And I press "Add grade item"
    And I set the following fields to these values:
      | Item name | Manual item 1 |
    And I click on "Save changes" "button"
    # Give all student the same grade for the manual grade item
    And I navigate to "Grader report" node in "Grade administration"
    And I give the grade "50.00" to the user "Student 1" for the grade item "Manual item 1"
    And I give the grade "50.00" to the user "Student 2" for the grade item "Manual item 1"
    And I give the grade "50.00" to the user "Student 3" for the grade item "Manual item 1"
    And I click on "Update" "button"
    And I turn editing mode off

  Scenario: Grade a grade item and ensure the results display correctly in the gradebook
    # Check the admin grade table
    And I navigate to "Grades" node in "Course administration"
    Then I should see "50.00" in the ".avg.lastrow .c1" "css_element"
    Then I should see "50.00" in the ".avg.lastrow .c2" "css_element"
    And I log out

    # Check the user grade table
    And I log in as "student1"
    And I am on homepage
    And I follow "Course 1"
    And I navigate to "Grades" node in "Course administration"
    Then I should see "50.00" in the "//tbody/tr/td[contains(@headers,'grade')]" "xpath_element"
    Then I should see "50.00" in the "//tbody/tr/td[contains(@headers,'average')]" "xpath_element"
    And I log out

  Scenario: Grade a grade item and ensure the results display correctly in the gradebook when user suspended
    # Check the admin grade table
    And I navigate to "Grades" node in "Course administration"
    And I navigate to "Grader report" node in "Grade administration"
    And I turn editing mode on
    And I give the grade "10.00" to the user "Student 1" for the grade item "Manual item 1"
    And I click on "Update" "button"
    And I turn editing mode off
    And I navigate to "Grades" node in "Course administration"
    Then I should see "36.67" in the ".lastrow .c1" "css_element"
    Then I should see "36.67" in the ".lastrow .c2" "css_element"

    # suspend a user
    And I navigate to "Browse list of users" node in "Site administration > Users > Accounts"
    And I follow "Student 3"
    And I follow "Edit profile"
    When I set the field "suspended" to "1"
    And I press "Update profile"

    # recheck grade table
    And I am on homepage
    And I follow "Course 1"
    And I navigate to "Grades" node in "Course administration"
    And I navigate to "Grader report" node in "Grade administration"
    Then I should see "30.00" in the ".lastrow .c1" "css_element"
    Then I should see "30.00" in the ".lastrow .c2" "css_element"

    # Change grade report preferences to show all enrolments. This should include suspended users.
    And I navigate to "Grader report" node in "Grade administration > My report preferences"
    Then I set the field "grade_report_showonlyactiveenrol" to "0"
    And I click on "Save changes" "button"

    # recheck grade table
    And I am on homepage
    And I follow "Course 1"
    And I navigate to "Grades" node in "Course administration"
    And I navigate to "Grader report" node in "Grade administration"
    Then I should see "36.67" in the ".lastrow .c1" "css_element"
    Then I should see "36.67" in the ".lastrow .c2" "css_element"

    And I log out
