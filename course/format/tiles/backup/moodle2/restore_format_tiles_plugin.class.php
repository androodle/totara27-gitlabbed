<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/*
 * @package    format
 * @subpackage tiles
 * @author     Greg Newton, Androgogic <greg.newton@androgogic.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @copyright  2014 Androgogic, Ltd.
 *
 * TODO: Description goes here
 */

/**
 * Restore plugin for the tiles format.
 */
class restore_format_tiles_plugin extends restore_format_plugin {
    /**
     * Returns the paths to be handled by the plugin at course level.
     */
    protected function define_course_plugin_structure() {

        // The course level component does not actually do anything at this
        // stage. Left as a placeholder for the time being. The actual meat of
        // this is in the sections.
        $paths = array();
        $paths[] = new restore_path_element('tiles', $this->get_pathfor('/'));
        return $paths;
    }

    /**
     * Process the course level element. Note: at present this is placeholder only.
     */
    public function process_tiles($data) {
        global $DB;

        $data = (object) $data;

        /* We only process this information if the course we are restoring to
          has 'tiles' format (target format can change depending of restore options). */
        $format = $DB->get_field('course', 'format', array('id' => $this->task->get_courseid()));
        if ($format != 'tiles') {
            return;
        }

        $data->courseid = $this->task->get_courseid();

        // Do nothing.
    }

    /**
     * Returns the paths to be handled by the plugin at section level
     */
    protected function define_section_plugin_structure() {

        $paths = array();
        $paths[] = new restore_path_element('tile_image', $this->get_pathfor('/'));
        return $paths; // And we return the interesting paths.
    }

    /**
     * Process the custom tile image for a section.
     */
    public function process_tile_image($data) {
        global $DB;

        $data = (object) $data;

        // Only add this information if the format is using the tiles format.
        // TODO: Review this. This is here based on examples and is take to be a
        // safety measure, but there may be a better way of doing this.
        $format = $DB->get_field('course', 'format', array('id' => $this->task->get_courseid()));
        if ($format != 'tiles') {
            return;
        }

        $data->courseid = $this->task->get_courseid();
        $data->sectionid = $this->task->get_sectionid();
        // The $data object contains 'filename', 'userid', 'timecreated', 'timemodified'.

        $existing = $DB->get_record('format_tiles_tile_image', array(
                                        'courseid' => $data->courseid,
                                        'sectionid' => $data->sectionid
                                    ));
        if ($existing) {
            $existing->filename = $data->filename;
            $existing->bgrepeat = $data->bgrepeat;
            $existing->userid = $data->userid;
            $existing->timecreated = $data->timecreated;
            $existing->timemodified = $data->timemodified;
            $DB->update_record('format_tiles_tile_image', $existing);
        } else {
            $DB->insert_record('format_tiles_tile_image', $data);
        }
    }

    public function after_restore_section() {
        global $DB;

        $context = context_course::instance($this->task->get_courseid());

        static $haveaddedfiles = false;
        if (!$haveaddedfiles) {
            // This was previously done in an after_restore_course "event" where it makes much more
            // sense, however that was not being triggered for the merge into an existing course
            // restore scenario. Since this should only be called once, I am tracking it with a
            // static. I'm not sure if I can rely on the classs to not be recreated, so I've not
            // added it there.

            $files = $DB->get_records('files', array(
                                          'contextid' => $context->id,
                                          'component' => 'format_tiles',
                                          'filearea' => 'section'
                                      ));
            $fs = get_file_storage();
            foreach ($files as $filerec) {
                $file = $fs->get_file_by_hash($filerec->pathnamehash);
                $file->delete();
            }

            $this->add_related_files('format_tiles', 'section', null);
            $haveaddedfiles = true;
        }

        // I use the section id when identifying images to avoid issues with conflicts
        // and to ensure that the tile image stays "attached" to a section, even if it
        // is moved. Unfortuately this doesn't get updated by the restore process
        // because it's a different context. Hence I need to explicity update it.
        $newsectionid = $this->task->get_sectionid();
        $files = $DB->get_records('files', array(
                                      'contextid' => $context->id,
                                      'component' => 'format_tiles',
                                      'filearea' => 'section')
                                 );
        // Loop through all the files and try to find the one for this section. Unfortunately I
        // can't seem to find anything that gives me the old id for this section (that isn't
        // protected or otherwise blocked) so I am resorting to a brute force search of the files.
        foreach ($files as $file) {
            $newitemid = $this->get_mappingid('course_section', $file->itemid);
            if ($newitemid != $newsectionid) {
                // Don't handle tiles for other sections until we are process the after_restore for
                // that section, as I can't gaurantee the mapping will be ready.
                continue;
            }
            $file->itemid = $newitemid;
            // Also need to update the path hash or it will not be findable again.
            $fullpath = "/$file->contextid/format_tiles/$file->filearea/$file->itemid/$file->filename";
            $file->pathnamehash = sha1($fullpath);
            $DB->update_record('files', $file);
        }
    }

}
