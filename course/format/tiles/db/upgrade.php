<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This file keeps track of upgrades to the html block
 *
 * @since 2.0
 * @package block_tiles
 * @copyright 2014 Androgogic, Ltd.
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/**
 *
 * @param int $oldversion
 * @param object $block
 */
function xmldb_format_tiles_upgrade($oldversion) {
    global $CFG, $DB;

    $dbman = $DB->get_manager();

    // Moodle v2.3.0 release upgrade line
    // Put any upgrade step following this


    // Moodle v2.4.0 release upgrade line
    // Put any upgrade step following this


    // Moodle v2.5.0 release upgrade line.
    // Put any upgrade step following this.

    if ($oldversion < 2014062601) {

        $xml_file = __DIR__.'/install.xml';

        // install new tables
        $dbman->install_one_table_from_xmldb_file($xml_file, 'format_tiles_tile_image', false);

        // Navint savepoint reached.
        upgrade_plugin_savepoint(true, 2014062601, 'format', 'tiles');
    }

    if ($oldversion < 2014080100) {

        $tile_files = $DB->get_records('files', array('component' => 'course', 'filepath' => '/format_tiles/'));

        foreach ($tile_files as $file) {
            $file->component = 'format_tiles';
            $file->filepath = '/';
            $fullpath = "/$file->contextid/format_tiles/$file->filearea/$file->itemid/$file->filename";
            $file->pathnamehash = sha1($fullpath);
            $DB->update_record('files', $file);
        }

    }


    if ($oldversion < 2015100100) {

        // Add bgrepeat field
        $table = new xmldb_table('format_tiles_tile_image');
        $bgrepeatfield = new xmldb_field('bgrepeat', XMLDB_TYPE_INTEGER, '2', true, false, false, null, 'filename');
        if (!$dbman->field_exists($table, $bgrepeatfield)) {
            $dbman->add_field($table, $bgrepeatfield);
        }

        // // Get rid of old 1.9 style config settings
        // $DB->execute("insert  into {config_plugins} (plugin, name, value)
        //               select  'format_tiles' as plugin, substring(name from 14) as newname, value
        //               from    {config}
        //               where name like 'format_tiles_%'");
        //
        // $DB->execute("delete from {config}
        //               where name like 'format_tiles_%'");

        // savepoint reached.
        upgrade_plugin_savepoint(true, 2015100100, 'format', 'tiles');
    }

    return true;
}
