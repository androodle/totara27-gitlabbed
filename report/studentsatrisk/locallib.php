<?php
/**
 * This file contains functions used by the studentsatrisk reports
 *
 *
 * @package    report_studentsatrisk
 * @copyright  Androgogic
 */

defined('MOODLE_INTERNAL') || die;


function report_studentsatrisk_get_total_logintime($user)
{
	global $DB;
	$logs = $DB->get_recordset('log', array("userid" => $user), "time");
	if(!$logs->valid())
	{
		return 0;
	}
	$timeloggedin = 0;
	$oldlog = $oldloginlog = new stdClass;
	$oldlog->action = $oldloginlog->action = "";
	$oldlog->time = $oldloginlog->time = 0;
	$timebetweenviews = 60 * 60 * 3;
	
	foreach($logs as $log)
	{
		
		
		if($log->action != "login")
		{
			//Argh, in some situations a logged in user can trigger a view
			//entry before a login entry. If they have 2 actions
			//seperated by a longer period than they would normally timeout,
			//ignore it	
			if(($log->time - $oldlog->time) > $timebetweenviews)
			{
				continue;
			}	
		}
		
		if($log->action=="login") //Find every login action
		{
			if($oldlog->action != "login" && $oldlog->action != "logout")	//Ensure they have done things
			{
				//if($log->userid==578) print $timeloggedin . " " . ($oldlog->time - $oldloginlog->time) . "<br/>";
				$timeloggedin += ($oldlog->time - $oldloginlog->time);
			}
			$oldloginlog = $log;
		}
		else if($log->action == "logout") //Even more useful if they have logged out
		{
			//Somehow its possible for people to have a logout entry without a login entry
			//There's no data we can collect about this case
			if($oldloginlog->action=="" || $oldlog->action == "logout") 
			{
				continue; 
			}
			$timeloggedin += ($log->time - $oldloginlog->time);
		}
		$oldlog = $log;
	}
	if($oldlog->action != "login" && $oldlog->action != "logout") // For their last session where they didn't logout
	{
		$timeloggedin += ($oldlog->time - $oldloginlog->time);
	}

	$logs->close();
	return $timeloggedin;
}


function report_studentsatrisk_get_module_logintime($user, $course, $module)
{
	global $DB;
	$logs = $DB->get_recordset('log', array("userid" => $user), "time");
	if(!$logs->valid())
	{
		return 0;
	}
	$timeloggedin = 0;
	$oldlog = $modstartlog = new stdClass;
	$oldlog->module = $modstartlog->module = "";
	$oldlog->time = $modstartlog->time = 0;	
	foreach($logs as $log)
	{
		
		if($log->module==$module && $log->course == $course) //If it matches the module we are looking for
		{
			if($oldlog->module != $module || $oldlog->module != $course)	//If its the first module action
			{
				$modstartlog = $log;  //We track from here
			}
		}
		else if($log->module != $module || $log->course != $course) //If not our module
		{
			if($oldlog->module == $module && $oldlog->course == $course)
			{
				if($log->action == "login")
				{
					$timeloggedin += ($oldlog->time - $modstartlog->time + 60); //Add total time (using current log entry cause we assume they read till new action)
				}
				else
				{
					$timeloggedin += ($log->time - $modstartlog->time); //Add total time (using current log entry cause we assume they read till new action)
				}
			}
		}
		$oldlog = $log;
	}
	if($oldlog->module == $module && $oldlog->course == $course) //In case this module was their final action
	{
		//Lets just give them an extra minute, since we don't know how long they will have read for on this one
		$timeloggedin += ($oldlog->time - $modstartlog->time + 60); 
	}

	$logs->close();
	return $timeloggedin;
}

function report_studentsatrisk_build_field_sql($fields)
{
	$fieldsqlarr = array();
	foreach($fields as $n =>$f)
	{
		$fieldsqlarr[] = $f . " AS " . $n;
	}
	return join(", ", $fieldsqlarr);
}

function report_studentsatrisk_build_order_array($order)
{
	asort($order);
	return array_keys($order);
}

function report_studentsatrisk_print_pagination($pageparams = array(), $table, $totalresult)
{		
	global $CFG;
	
	$limit = $CFG->report_studentsatrisk_showrows;
	
	$numpages = floor($totalresult[$table] / $limit);
	
	$label = "page".$table;
	$pagination = "";
	if($pageparams[$label] > 0)
	{
		$urlparams = $pageparams;
		$urlparams[$label] = 0;
		$url = new moodle_url("/report/studentsatrisk/index.php", $urlparams);
		$pagination .= html_writer::link($url, "<<", array("class"=>"pagezero"));
		
		$urlparams = $pageparams;
		$urlparams[$label] = $urlparams[$label]-1;
		$url = new moodle_url("/report/studentsatrisk/index.php", $urlparams);
		$pagination .= html_writer::link($url, "<", array("class"=>"pageback"));	
	}
	if($pageparams[$label] < $numpages)
	{
		$urlparams = $pageparams;
		$urlparams[$label] = $urlparams[$label]+1;
		$url = new moodle_url("/report/studentsatrisk/index.php", $urlparams);
		$pagination .= html_writer::link($url, ">", array("class"=>"pagenext"));
		
		$urlparams = $pageparams;
		$urlparams[$label] = $numpages;
		$url = new moodle_url("/report/studentsatrisk/index.php", $urlparams);
		$pagination .= html_writer::link($url, ">>", array("class"=>"pageend"));	
	}
	
	echo html_writer::tag('div', $pagination, array("class" => "pagination"));
	return TRUE;
}

function report_studentsatrisk_print_html($table_heading, $table_headers, $data, $pageparams, $totalresult)
{
    global $CFG, $OUTPUT;

	foreach($table_headers as $key => $value)
	{
		echo $OUTPUT->heading($table_heading[$key], 2);
		$table = new html_table();
		$table->attributes['class'] = "generaltable";
	
		$row = array();
		$row[0] = new html_table_row();
		
		$switch = array(
			"asc" => "desc",
			"desc" => "asc",
		);
		
		foreach($value as $k => $v)
		{
			$orderstudent = ($key == "student") ? $k : $pageparams["orderstudent"];
			$directionstudent = ($k == $pageparams["orderstudent"]) ? $switch[$pageparams["directionstudent"]] : "asc";
			$ordercourse = ($key == "course") ? $k : $pageparams["ordercourse"];
			$directioncourse = ($k == $pageparams["ordercourse"]) ? $switch[$pageparams["directioncourse"]] : "asc";
			
			$url = new moodle_url("/report/studentsatrisk/index.php", array("orderstudent" => $orderstudent, "ordercourse" => $ordercourse, 
																			"directionstudent" => $directionstudent, "directioncourse" => $directioncourse));
			$h = html_writer::link($url, $v, array("class"=>"tableheader"));
			$row[0]->cells[] = new html_table_cell($h);
		}
		$c = 1;
		
		foreach($data[$key] as $result)
		{
			$row[$c] = new html_table_row();
			foreach($result as $k => $v)
			{
				$row[$c]->cells[] = new html_table_cell($v);
			}
				
			$c++;
		}
		
		$table->data = $row;
		echo html_writer::tag("div", html_writer::table($table), array("class" => "table_$key"));
		report_studentsatrisk_print_pagination($pageparams, $key, $totalresult);
		
	}
	return TRUE;
}    

function report_studentsatrisk_print_xls($table_heading, $table_headers, $data)
{
    global $CFG;

    require_once("$CFG->libdir/excellib.class.php");

    $ldcache = array();
    $tt = getdate(time());
    $today = mktime (0, 0, 0, $tt["mon"], $tt["mday"], $tt["year"]);

    $strftimedatetime = get_string("strftimedatetime");

    //$nroPages = ceil(count($logs)/(EXCELROWS-FIRSTUSEDEXCELROW+1));
    $filename = 'student_retention_report_'.userdate(time(),get_string('backupnameformat', 'langconfig'),99,false);
    $filename .= '.xls';

    $workbook = new MoodleExcelWorkbook('-');
    $workbook->send($filename);

    $worksheet = array();
    
    $count = 0;
    foreach($table_headers as $key=>$header)
    {
		$row = 0;
		$col = 0;		
		$worksheet[$key] = $workbook->add_worksheet($table_heading[$key]);
		$worksheet[$key]->write_string($row, $col, $table_heading[$key]);
		$row = 1;
		foreach($header as $k => $val)
		{
			$worksheet[$key]->write_string($row, $col, $val);
			$col++;
			//foreach($data[$key] as $id=>$v)
			//{
			//	$row++;
			//	$worksheet[$key]->write_string($row, $col, $v[$k]);
			//}
			
		}
		$row = 2;
		foreach($data[$key] as $id=> $v)
		{
			$col = 0;
			foreach($v as $dat)
			{
				$worksheet[$key]->write_string($row, $col, $dat);
				$col++;
			}
			
			$row++;
		}
		
	}

    $workbook->close();
    return true;
}

function report_studentsatrisk_print_csv($table_heading, $table_headers, $data)
{
    global $CFG;

    require_once($CFG->libdir . '/csvlib.class.php');

    $csvexporter = new csv_export_writer('tab');
    
    $tt = getdate(time());
    $today = mktime (0, 0, 0, $tt["mon"], $tt["mday"], $tt["year"]);

    $strftimedatetime = get_string("strftimedatetime");

    $csvexporter->set_filename('student_retention_report', '.csv');
	
	foreach($table_headers as $key => $header)
	{
		$csvexporter->add_data(array($table_heading[$key]));
		$csvexporter->add_data($header);
		
		foreach($data[$key] as $id => $value)
		{
			$csvexporter->add_data($value);
		}
		$csvexporter->add_data(array());
		$csvexporter->add_data(array());
	}
    $csvexporter->download_file();
    return true;
}

