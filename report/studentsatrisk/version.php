<?php
/**
 * Version info
 *
 * This File contains information about the current version of report/studentsatrisk
 *
 * @package    report_studentsatrisk
 * @copyright  Androgogic 2014
 */

defined('MOODLE_INTERNAL') || die;

$plugin->version   = 2014072400;    // The current plugin version (Date: YYYYMMDDXX)
$plugin->requires  = 2013110500;    // Requires this Moodle version
$plugin->cron  = 300;    // Cron of course (5 mins)
$plugin->component = 'report_studentsatrisk';  // Full name of the plugin (used for diagnostics)
