<?php
/**
 * This file contains functions used by the studentsontrack reports
 *
 *
 * @package    report_studentsontrack
 * @copyright  Androgogic
 */

defined('MOODLE_INTERNAL') || die;

function report_studentsontrack_get_total_logins($user, $readers, $datafrom)
{
	
	$logincount = 0;
	if(array_key_exists('logstore_legacy', $readers))
	{
		$logincount += report_studentsontrack_get_login_count_per_log($user, $readers['logstore_legacy'], $datafrom);
	}
	if(array_key_exists('logstore_standard', $readers))
	{
		$logincount += report_studentsontrack_get_login_count_per_log($user, $readers['logstore_standard'], $datafrom);
	}
	if(array_key_exists('logstore_external', $readers))
	{
		$logincount += report_studentsontrack_get_login_count_per_log($user, $readers['logstore_external'], $datafrom);
	}
	
	return $logincount;
}

function report_studentsontrack_get_login_count_per_log($user, $reader, $datafrom)
{
	global $DB;
	
	$sqlparams = array("loggedin", $user, $datafrom);
	return $reader->get_events_select_count("action = ? AND userid = ? AND timecreated > ?", $sqlparams);
}

function report_studentsontrack_get_total_logintime($user, $readers, $datafrom)
{
	$logintime = 0;
	if(array_key_exists('logstore_legacy', $readers))
	{
		$logintime += report_studentsontrack_get_total_logintime_per_log($user, $readers['logstore_legacy'], $datafrom);
	}
	if(array_key_exists('logstore_standard', $readers))
	{
		$logintime += report_studentsontrack_get_total_logintime_per_log($user, $readers['logstore_standard'], $datafrom);
	}
	if(array_key_exists('logstore_external', $readers))
	{
		$logintime += report_studentsontrack_get_total_logintime_per_log($user, $readers['logstore_external'], $datafrom);
	}
	
	return $logintime;
}
	
function report_studentsontrack_get_total_logintime_per_log($user, $reader, $datafrom)
{
	global $DB;
	$sqlparams = array($user, $datafrom);
	
	//$logs = $DB->get_recordset('log', array("userid" => $user), "time");
	$logs = $reader->get_events_select("userid = ? AND timecreated > ?", $sqlparams, 'timecreated', 0, 0);
	$timeloggedin = 0;
	$oldlog = $oldloginlog = new stdClass;
	$oldlog->action = $oldloginlog->action = "";
	$oldlog->timecreated = $oldloginlog->timecreated = 0;
	$timebetweenviews = 60 * 60 * 3;
	
	foreach($logs as $log)
	{
		
		
		if($log->action != "loggedin")
		{
			
			//Argh, in some situations a logged in user can trigger a view
			//entry before a login entry. If they have 2 actions
			//seperated by a longer period than they would normally timeout,
			//ignore it	
			if(($log->timecreated - $oldlog->timecreated) > $timebetweenviews)
			{
				continue;
			}	
		}
		
		if($log->action == "loggedin") //Find every login action
		{
			if($oldlog->action != "loggedin" && $oldlog->action != "loggedout")	//Ensure they have done things
			{
				//if($log->userid==578) print $timeloggedin . " " . ($oldlog->time - $oldloginlog->time) . "<br/>";
				$timeloggedin += ($oldlog->timecreated - $oldloginlog->timecreated);
			}
			$oldloginlog = $log;
		}
		else if($log->action == "loggedout") //Even more useful if they have logged out
		{
			//Somehow its possible for people to have a logout entry without a login entry
			//There's no data we can collect about this case
			if($oldloginlog->action=="" || $oldlog->action == "loggedout") 
			{
				continue; 
			}
			$timeloggedin += ($log->timecreated - $oldloginlog->timecreated);
		}
		$oldlog = $log;
	}
	if($oldlog->action != "loggedin" && $oldlog->action != "loggedout") // For their last session where they didn't logout
	{
		$timeloggedin += ($oldlog->timecreated - $oldloginlog->timecreated);
	}

	return $timeloggedin;
}

function report_studentsontrack_get_courseviews($user, $course, $readers, $datafrom)
{
	$courseviews = array("first" => 0, "last" => 0, "total" => 0);
	
	$sqlparams = array($user, $course, $datafrom, "viewed", "course");
	if(array_key_exists('logstore_legacy', $readers))
	{
		$viewlogs = $readers['logstore_legacy']->get_events_select('userid = ? AND courseid = ? AND timecreated > ? AND action = ? AND module = ?', $sqlparams, 'timecreated', 0, 0);
		if(!empty($viewlogs))
		{
			$first = reset($viewlogs);
			$last = end($viewlogs);
			$courseviews['total'] = count($viewlogs);
			$courseviews['first'] = $first->timecreated;
			$courseviews['last'] = $last->timecreated;
		}
	}
	if(array_key_exists('logstore_standard', $readers))
	{
		$viewlogs = $readers['logstore_standard']->get_events_select('userid = ? AND courseid = ? AND timecreated > ? AND action = ? AND target = ?', $sqlparams, 'timecreated', 0, 0);
		if(!empty($viewlogs))
		{
			$first = reset($viewlogs);
			$last = end($viewlogs);
			$courseviews['total'] += count($viewlogs);
			$courseviews['first'] = ($courseviews['first'] > $first->timecreated || $courseviews['first'] == 0) ? $first->timecreated : $courseviews['first'];
			$courseviews['last'] = ($courseviews['last'] < $last->timecreated) ? $last->timecreated : $courseviews['last'];
		}
	}
	if(array_key_exists('logstore_external', $readers))
	{
		$viewlogs = $readers['logstore_external']->get_events_select('userid = ? AND courseid = ? AND timecreated > ? AND action = ? AND target = ?', $sqlparams, 'timecreated', 0, 0);
		if(!empty($viewlogs))
		{
			$first = reset($viewlogs);
			$last = end($viewlogs);
			$courseviews['total'] += count($viewlogs);
			$courseviews['first'] = ($courseviews['first'] > $first->timecreated || $courseviews['first'] == 0) ? $first->timecreated : $courseviews['first'];
			$courseviews['last'] = ($courseviews['last'] < $last->timecreated) ? $last->timecreated : $courseviews['last'];
		}
	}
	
	return $courseviews;
}

function report_studentsontrack_get_forumviews($user, $course, $readers, $datafrom)
{
	$forumviews = array("last" => 0, "total" => 0);
	
	$sqlparams = array($user, $course, $datafrom, "viewed", "forum");
	if(array_key_exists('logstore_legacy', $readers))
	{
		$viewlogs = $readers['logstore_legacy']->get_events_select('userid = ? AND courseid = ? AND timecreated > ? AND action = ? AND module = ?', $sqlparams, 'timecreated', 0, 0);
		if(!empty($viewlogs))
		{
			$first = reset($viewlogs);
			$last = end($viewlogs);
			$forumviews['total'] = count($viewlogs);
			$forumviews['last'] = $last->timecreated;
		}
	}
	if(array_key_exists('logstore_standard', $readers))
	{
		$viewlogs = $readers['logstore_standard']->get_events_select('userid = ? AND courseid = ? AND timecreated > ? AND action = ? AND objecttable = ?', $sqlparams, 'timecreated', 0, 0);
		if(!empty($viewlogs))
		{
			$first = reset($viewlogs);
			$last = end($viewlogs);
			$forumviews['total'] += count($viewlogs);
			$forumviews['last'] = ($forumviews['last'] < $last->timecreated) ? $last->timecreated : $forumviews['last'];
		}
	}
	if(array_key_exists('logstore_external', $readers))
	{
		$viewlogs = $readers['logstore_external']->get_events_select('userid = ? AND courseid = ? AND timecreated > ? AND action = ? AND objecttable = ?', $sqlparams, 'timecreated', 0, 0);
		if(!empty($viewlogs))
		{
			$first = reset($viewlogs);
			$last = end($viewlogs);
			$forumviews['total'] += count($viewlogs);
			$forumviews['last'] = ($forumviews['last'] < $last->timecreated) ? $last->timecreated : $forumviews['last'];
		}
	}
	
	return $forumviews;
}

function report_studentsontrack_get_forumposts($user, $course, $readers, $datafrom)
{
	$forumposts = array("last" => 0, "total" => 0);
	
	$sqlparams = array($user, $course, $datafrom, "uploaded", "updated", "forum_posts");
	if(array_key_exists('logstore_legacy', $readers))
	{
		$postlogs = $readers['logstore_legacy']->get_events_select('userid = ? AND courseid = ? AND timecreated > ? AND (action=? OR action=?) AND module = ?', $sqlparams, 'timecreated', 0, 0);
		if(!empty($postlogs))
		{
			$first = reset($postlogs);
			$last = end($postlogs);
			$forumposts['total'] = count($postlogs);
			$forumposts['last'] = $last->timecreated;
		}
	}
	if(array_key_exists('logstore_standard', $readers))
	{
		$postlogs = $readers['logstore_standard']->get_events_select('userid = ? AND courseid = ? AND timecreated > ? AND (action=? OR action=?) AND objecttable = ?', $sqlparams, 'timecreated', 0, 0);
		if(!empty($postlogs))
		{
			$first = reset($postlogs);
			$last = end($postlogs);
			$forumposts['total'] += count($postlogs);
			$forumposts['last'] = ($forumposts['last'] < $last->timecreated) ? $last->timecreated : $forumposts['last'];
		}
	}
	if(array_key_exists('logstore_external', $readers))
	{
		$postlogs = $readers['logstore_external']->get_events_select('userid = ? AND courseid = ? AND timecreated > ? AND (action=? OR action=?) AND objecttable = ?', $sqlparams, 'timecreated', 0, 0);
		if(!empty($postlogs))
		{
			$first = reset($postlogs);
			$last = end($postlogs);
			$forumposts['total'] += count($postlogs);
			$forumposts['last'] = ($forumposts['last'] < $last->timecreated) ? $last->timecreated : $forumposts['last'];
		}
	}
	return $forumposts;
}

function report_studentsontrack_get_module_logintime($user, $course, $readers, $datafrom, $module)
{
	$logintime = 0;
	if(array_key_exists('logstore_legacy', $readers))
	{
		$logintime += report_studentsontrack_get_module_logintime_per_log($user, $course, $module, $readers['logstore_legacy'], $datafrom);
	}
	if(array_key_exists('logstore_standard', $readers))
	{
		$logintime += report_studentsontrack_get_module_logintime_per_log($user, $course, "mod_".$module, $readers['logstore_standard'], $datafrom);
	}
	if(array_key_exists('logstore_external', $readers))
	{
		$logintime += rreport_studentsontrack_get_module_logintime_per_log($user, $course, "mod_".$module, $readers['logstore_external'], $datafrom);
	}
	
	return $logintime;
}

function report_studentsontrack_get_module_logintime_per_log($user, $course, $module, $reader, $datafrom)
{
	global $DB;
	$sqlparams = array($user, $datafrom);
	$logs = $reader->get_events_select("userid = ? AND timecreated > ?", $sqlparams, 'timecreated', 0, 0);
	//$logs = $DB->get_recordset('log', array("userid" => $user), "time");
	$timeloggedin = 0;
	$oldlog = $modstartlog = new stdClass;
	$oldlog->component = $modstartlog->component = "";
	$oldlog->timecreated = $modstartlog->timecreated = 0;	
	foreach($logs as $log)
	{
		
		if($log->component==$module && $log->courseid == $course) //If it matches the module we are looking for
		{
			if($oldlog->component != $module || $oldlog->courseid != $course)	//If its the first module action
			{
				$modstartlog = $log;  //We track from here
			}
		}
		else if($log->component != $module || $log->courseid != $course) //If not our module
		{
			if($oldlog->component == $module && $oldlog->courseid == $course)
			{
				if($log->action == "loggedin")
				{
					$timeloggedin += ($oldlog->timecreated - $modstartlog->timecreated + 60); //Add total time (using current log entry cause we assume they read till new action)
				}
				else
				{
					$timeloggedin += ($log->timecreated - $modstartlog->timecreated); //Add total time (using current log entry cause we assume they read till new action)
				}
			}
		}
		$oldlog = $log;
	}
	if($oldlog->component == $module && $oldlog->courseid == $course) //In case this module was their final action
	{
		//Lets just give them an extra minute, since we don't know how long they will have read for on this one
		$timeloggedin += ($oldlog->timecreated - $modstartlog->timecreated + 60); 
	}
	return $timeloggedin;
}

function report_studentsontrack_build_field_sql($fields)
{
	$fieldsqlarr = array();
	foreach($fields as $n =>$f)
	{
		$fieldsqlarr[] = $f . " AS " . $n;
	}
	return join(", ", $fieldsqlarr);
}

function report_studentsontrack_build_order_array($order)
{
	asort($order);
	return array_keys($order);
}

function report_studentsontrack_print_pagination($pageparams = array(), $table, $totalresult)
{		
	global $CFG;
	
	$limit = $CFG->report_studentsontrack_showrows;
	
	$numpages = floor($totalresult[$table] / $limit);
	
	$label = "page".$table;
	$pagination = "";
	if($pageparams[$label] > 0)
	{
		$urlparams = $pageparams;
		$urlparams[$label] = 0;
		$url = new moodle_url("/report/studentsontrack/index.php", $urlparams);
		$pagination .= html_writer::link($url, "<<", array("class"=>"pagezero"));
		
		$urlparams = $pageparams;
		$urlparams[$label] = $urlparams[$label]-1;
		$url = new moodle_url("/report/studentsontrack/index.php", $urlparams);
		$pagination .= html_writer::link($url, "<", array("class"=>"pageback"));	
	}
	if($pageparams[$label] < $numpages)
	{
		$urlparams = $pageparams;
		$urlparams[$label] = $urlparams[$label]+1;
		$url = new moodle_url("/report/studentsontrack/index.php", $urlparams);
		$pagination .= html_writer::link($url, ">", array("class"=>"pagenext"));
		
		$urlparams = $pageparams;
		$urlparams[$label] = $numpages;
		$url = new moodle_url("/report/studentsontrack/index.php", $urlparams);
		$pagination .= html_writer::link($url, ">>", array("class"=>"pageend"));	
	}
	
	echo html_writer::tag('div', $pagination, array("class" => "pagination"));
	return TRUE;
}

function report_studentsontrack_print_html($table_heading, $table_headers, $data, $pageparams, $totalresult)
{
    global $CFG, $OUTPUT;

	foreach($table_headers as $key => $value)
	{
		echo $OUTPUT->heading($table_heading[$key], 2);
		$table = new html_table();
		$table->attributes['class'] = "generaltable";
	
		$row = array();
		$row[0] = new html_table_row();
		
		$switch = array(
			"asc" => "desc",
			"desc" => "asc",
		);
		
		foreach($value as $k => $v)
		{
			$orderstudent = ($key == "student") ? $k : $pageparams["orderstudent"];
			$directionstudent = ($k == $pageparams["orderstudent"]) ? $switch[$pageparams["directionstudent"]] : "asc";
			$ordercourse = ($key == "course") ? $k : $pageparams["ordercourse"];
			$directioncourse = ($k == $pageparams["ordercourse"]) ? $switch[$pageparams["directioncourse"]] : "asc";
			
			$url = new moodle_url("/report/studentsontrack/index.php", array("orderstudent" => $orderstudent, "ordercourse" => $ordercourse, 
																			"directionstudent" => $directionstudent, "directioncourse" => $directioncourse));
			$h = html_writer::link($url, $v, array("class"=>"tableheader"));
			$row[0]->cells[] = new html_table_cell($h);
		}
		$c = 1;
		
		foreach($data[$key] as $result)
		{
			$row[$c] = new html_table_row();
			foreach($result as $k => $v)
			{
				$row[$c]->cells[] = new html_table_cell($v);
			}
				
			$c++;
		}
		
		$table->data = $row;
		echo html_writer::tag("div", html_writer::table($table), array("class" => "table_$key"));
		report_studentsontrack_print_pagination($pageparams, $key, $totalresult);
		
	}
	return TRUE;
}    

function report_studentsontrack_print_csv($table_heading, $table_headers, $data)
{
    global $CFG;

    require_once($CFG->libdir . '/csvlib.class.php');

    $csvexporter = new csv_export_writer('tab');
    
    $tt = getdate(time());
    $today = mktime (0, 0, 0, $tt["mon"], $tt["mday"], $tt["year"]);

    $strftimedatetime = get_string("strftimedatetime");

    $csvexporter->set_filename('student_retention_report', '.csv');
	
	foreach($table_headers as $key => $header)
	{
		$csvexporter->add_data(array($table_heading[$key]));
		$csvexporter->add_data($header);
		
		foreach($data[$key] as $id => $value)
		{
			$csvexporter->add_data($value);
		}
		$csvexporter->add_data(array());
		$csvexporter->add_data(array());
	}
    $csvexporter->download_file();
    return true;
}

