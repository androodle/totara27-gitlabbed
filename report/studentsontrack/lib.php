<?php
/**
 * This file contains functions used by the studentsontrack reports
 *
 *
 * @package    report_studentsontrack
 * @copyright  Androgogic
 */
require_once($CFG->dirroot.'/report/studentsontrack/locallib.php');

defined('MOODLE_INTERNAL') || die;

function report_studentsontrack_task()
{
	global $DB, $CFG;
	
	$studentcountinsert = 0;
	$studentcountupdate = 0;
	
	$skiptimes = array("24h" => 86400, "1w" => 604800);
	
	$skipstudent = $CFG->report_studentsontrack_skipstudent;
	$datatime = get_config(null, 'report_studentsontrack_datatime');
	$lastcronstart = get_config(null, 'report_studentsontrack_lastcronstart');
	$lastcronend = get_config(null, 'report_studentsontrack_lastcronend');
	
	$datafrom = ($datatime > 0 && $datatime < 13) ? strtotime("-$datatime months") : 0;
	
	$skiptime = $skiptimes[$skipstudent];
	//STUDENT TABLE
	$current = time();
	
	if($skipstudent === NULL)
	{
		mtrace("Update settings to allow this to run");
		return TRUE;
	}
	
	//Dont run cron if A) Not enough time has passed since last run B) Its not the
	//correct hour C)Frequency is set to not run or D)Last students on track report hasn't finished running yet
	if($lastcronend < $lastcronstart)
	{
		//mtrace("Last report not yet finished running or has failed to finish");
		//return TRUE;
	}
	
	 if (!set_config('report_studentsontrack_lastcronstart', $current)) {
        mtrace("Error: could not update cron start time for students on track report");
    }
    
    
    $manager = get_log_manager();
    $readers = $manager->get_readers();
    
    //$legacy = isset($readers['logstore_legacy']) ? $readers['logstore_legacy'] : NULL;
    //$standard = isset($readers['logstore_standard']) ? $readers['logstore_standard'] : NULL;
    //$external = isset($readers['logstore_external']) ? $readers['logstore_external'] : NULL;
    
    //$legacy = new \logstore_legacy\log\store;
    //$standard = new \logstore_standard\log\store;
    //$external = new \logstore_database\log\store;
    //$manager = new \tool_log\log\manager;

    //$readers = $manager->get_readers();
    //print_r($readers); return TRUE;
    
    
	$tablesql = "{user} u";
	//$sqlparams = array(0);
	$sqlparams = array(0, $datafrom);
	
	$results = $DB->get_recordset_sql("SELECT * FROM $tablesql WHERE u.deleted = ? AND u.lastlogin >= ?", $sqlparams);
	//$results = $legacy->get_events_select("SELECT * FROM $tablesql WHERE u.deleted = ?", $sqlparams);
	
	if($results->valid())
	{
		
		
		//Code goes here
		foreach($results as $result)
		{
			$report = $DB->get_record("report_studentsontrack_stud", array("userid" => $result->id));
			
			//If the user hasn't logged in since configured time, update user records
			if($report !== FALSE )
			{
				if($result->lastaccess < $current - $skiptime)
				{
					continue;
				}
				$entry_exists = TRUE;
			}
			else
			{
				$entry_exists = FALSE;
				$report = new stdClass;
			}
			
			$report->studid = ($result->idnumber) ? $result->idnumber : "";
			$report->modified = time();
			$report->userid = $result->id;
			$report->firstname = $result->firstname;
			$report->lastname = $result->lastname;
			$report->email = $result->email;
			$report->timeusercreated = $result->timecreated ? $result->timecreated : NULL;
			$report->firstaccess = $result->firstaccess ? $result->firstaccess : NULL;
			$report->lastlogin = $result->lastlogin ? $result->lastlogin : NULL;
			$report->logincount = report_studentsontrack_get_total_logins($result->id, $readers, $datafrom);
			$report->logintime = report_studentsontrack_get_total_logintime($result->id, $readers, $datafrom);
			
			if($entry_exists)
			{
				$DB->update_record("report_studentsontrack_stud", $report);
				$studentcountupdate++;
			}
			else
			{
				$DB->insert_record("report_studentsontrack_stud", $report);
				$studentcountinsert++;
			}
		}
	}
	$results->close();
	
	
	//COURSE TABLE
	
	$coursecountinsert = 0;
	$coursecountupdate = 0;
	
	$fields = array(
		"id" => "ue.id",
		"userid" => "u.id",
		"courseid" => "c.id",
		"studid" => "u.idnumber",
		"lastaccess" => "u.lastaccess",
		"firstname" => "u.firstname",
		"lastname" => "u.lastname",
		"email" => "u.email",
		"code" => "c.idnumber",
		"shortname" => "c.shortname",
		"fullname" => "c.fullname"
	);
	
	
	$fieldsql = report_studentsontrack_build_field_sql($fields);
	
	$tablesql = "{user_enrolments} ue INNER JOIN {user} u ON ue.userid = u.id AND u.lastlogin >= ? INNER JOIN {enrol} e ON ue.enrolid = e.id INNER JOIN {course} c ON e.courseid = c.id";
	$sqlparams = array($datafrom, 0, $datafrom);
	
	$results = $DB->get_recordset_sql("SELECT $fieldsql FROM $tablesql WHERE u.deleted = ? AND (ue.timeend > ? OR ue.timeend = 0)", $sqlparams);
	$current = time();
	if($results->valid())
	{
		foreach($results as $result)
		{
			
			$report = $DB->get_record("report_studentsontrack_crse", array("userid" => $result->userid, "courseid" => $result->courseid));
			
			
			if($report !== FALSE)
			{
				if($result->lastaccess < $current - $skiptime)
				{
					continue;
				}
				$entry_exists = TRUE;
			}
			else
			{
				$report = new stdClass;
				$entry_exists = FALSE;
			}
			
			$completion = $DB->get_record("course_completions", array("userid" => $result->userid, "course" => $result->courseid));
				
			//	print_r(array);
			//$courseviews = $DB->get_records("log", array('userid' => $result->userid, "course" => $result->courseid, "action" => "view", "module" => "course"));
			$courseviews = report_studentsontrack_get_courseviews($result->userid, $result->courseid, $readers, $datafrom);
			
			$countcourseviews = $courseviews['total'];
			$firstcourseview = $courseviews['first'];
			$lastcourseview = $courseviews['last'];
				
			//$forumviews = $DB->get_records("log", array('userid' => $result->userid, "course" => $result->courseid, "action" => "view forum", "module" => "forum"));
			$forumviews = report_studentsontrack_get_forumviews($result->userid, $result->courseid, $readers, $datafrom);
			$countforumviews = $forumviews['total'];
			$lastforumview = $forumviews['last'];
			
			$forumposts = report_studentsontrack_get_forumposts($result->userid, $result->courseid, $readers, $datafrom);
			//$sql = "SELECT * FROM {log} WHERE userid=? AND course=? AND (action=? OR action=?) AND module=?";
			//$forumposts = $DB->get_records_sql($sql, array($result->userid, $result->courseid, "add post", "update post", "forum"));
			
			$countforumposts = $forumposts['total'];
			$lastforumpost = $forumposts['last'];
				
				
			$forumtime = report_studentsontrack_get_module_logintime($result->userid, $result->courseid, $readers, $datafrom, "forum");
			$sql = "SELECT COUNT(*) FROM {course_modules_completion} cmc LEFT JOIN {course_modules} cm ON cm.id = cmc.coursemoduleid LEFT JOIN {modules} m ON m.id = cm.module WHERE cmc.completionstate = ? AND cmc.userid = ? AND cm.course = ? AND (m.name=? OR m.name=? OR m.name=?)";
			$modcount = $DB->count_records_sql($sql, array(TRUE, $result->userid, $result->courseid, "assign", "assignment", "quiz"));
			
			$sqlparams = array('userid' => $result->userid, "course" => $result->courseid, "timefinish" => $datafrom);
			$lastquizattempt = $DB->get_record_sql("SELECT * FROM {quiz_attempts} qa INNER JOIN {quiz} q ON qa.quiz = q.id WHERE qa.userid=? AND q.course = ? AND qa.timefinish > ? ORDER BY qa.timefinish DESC LIMIT 1", $sqlparams);

			$sqlparams = array('userid' => $result->userid, "course" => $result->courseid, "timecreated" => $datafrom);
			$lastassignattempt = $DB->get_record_sql("SELECT * FROM {assign_submission} asu INNER JOIN {assign} a ON asu.assignment = a.id WHERE asu.userid=? AND a.course = ? AND asu.timecreated > ? ORDER BY asu.timecreated DESC LIMIT 1", $sqlparams);
			
			$lastquiztime = empty($lastquizattempt) ? 0 : $lastquizattempt->timefinish;
			$lastassigntime = empty($lastassignattempt) ? 0 : $lastassignattempt->timecreated;
			
			$lastmodattempt = ($lastquiztime > $lastassigntime) ? $lastquiztime : $lastassigntime;	
				
			$quiztime = report_studentsontrack_get_module_logintime($result->userid, $result->courseid, $readers, $datafrom, "quiz");
				
			$assigntime = report_studentsontrack_get_module_logintime($result->userid, $result->courseid, $readers, $datafrom, "assign");
				
			$modtime = $quiztime + $assigntime;
			
			
			$report->studid = ($result->studid) ? $result->studid : "";
			$report->modified = time();
			$report->userid = $result->userid;
			$report->courseid = $result->courseid;
			$report->firstname = $result->firstname;
			$report->lastname = $result->lastname;
			$report->email = $result->email;
			$report->code = $result->code;
			$report->shortname = $result->shortname;
			$report->fullname = $result->fullname;
			$report->completion = ($completion && $completion->timecompleted) ? TRUE : FALSE;
			$report->completiondate = ($completion && $completion->timecompleted) ? $completion->timecompleted : NULL;
			$report->courseviews = $countcourseviews;
			$report->firstviewed = ($countcourseviews > 0) ? $firstcourseview : NULL;
			$report->lastviewed = ($countcourseviews > 0) ? $lastcourseview : NULL;
			$report->numforumviews = $countforumviews;
			$report->dateforumlastview = ($countforumviews > 0) ? $lastforumview : NULL;
			$report->numposts = $countforumposts;
			$report->dateposts = ($countforumposts > 0) ? $lastforumpost : NULL;
			$report->timespentinforums = $forumtime;
			$report->numassess = $modcount;
			$report->datelastassess = ($lastmodattempt > 0) ? $lastmodattempt : NULL;
			$report->timespendasses = $modtime;
			//print_r($report);
			
			if($entry_exists)
			{
				$DB->update_record("report_studentsontrack_crse", $report);
				$coursecountupdate++;
			}
			else
			{
				$DB->insert_record("report_studentsontrack_crse", $report);
				$coursecountinsert++;
			}
		}
	}
	$results->close();
	print "Students updated: $studentcountupdate Students inserted $studentcountinsert Courses updated: $coursecountupdate Courses inserted $coursecountinsert <br/>\r\n";
	if (!set_config('report_studentsontrack_lastcronend', time())) {
        mtrace("Error: could not update cron end time for students on track report");
    }
	
}

