<?php

/**
 * Links and settings
 *
 * Contains settings used by logs report.
 *
 * @package    report_studentsontrack
 * @copyright  Androgogic
 */

defined('MOODLE_INTERNAL') || die;


// just a link to course report
$ADMIN->add('reports', new admin_externalpage('reportstudentsontrack_view', get_string('studentsontrack', 'report_studentsontrack'), "$CFG->wwwroot/report/studentsontrack/index.php", 'report/studentsontrack:view'));

if ($ADMIN->fulltree)
{                                    
                       
    $settings->add(new admin_setting_configtext('report_studentsontrack_showrows', new lang_string('showrows', 'report_studentsontrack'),
        new lang_string('showrowslong', 'report_studentsontrack'), 10, PARAM_INT));

    $loggedinoptions = array("24h" => "24 Hours", "1w" => "Weekly");
	
    $settings->add(new admin_setting_configselect('report_studentsontrack_skipstudent', new lang_string('skipstudent', 'report_studentsontrack'),
                       new lang_string('skipstudentlong', 'report_studentsontrack'), '24h', $loggedinoptions));
	
	$monthoptions = array();
	for($i = 1; $i <=12; $i++)
	{
			$monthoptions[$i] = "$i Months";
	}
	$monthoptions[0] = "All time";
	$settings->add(new admin_setting_configselect('report_studentsontrack_datatime', new lang_string('datatime', 'report_studentsontrack'),
        new lang_string('datatimelong', 'report_studentsontrack'), 3, $monthoptions));

}
