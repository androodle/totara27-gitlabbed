<?php
/**
 * Definition of Forum scheduled tasks.
 *
 * @package   report_studentsontrack
 * @category  task
 * @copyright 2015 Androgogic
 */

defined('MOODLE_INTERNAL') || die();

$tasks = array(
    array(
        'classname' => 'report_studentsontrack\task\cron_task',
        'blocking' => 0,
        'minute' => '0',
        'hour' => '0',
        'day' => '*',
        'month' => '*',
        'dayofweek' => '*'
    )
);
