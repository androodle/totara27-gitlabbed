<?php
/**
 * Version info
 *
 * This File contains information about the current version of report/studentsontrack
 *
 * @package    report_studentsontrack
 * @copyright  Androgogic 2015
 */

defined('MOODLE_INTERNAL') || die;

$plugin->version   = 2015041400;    // The current plugin version (Date: YYYYMMDDXX)
$plugin->requires  = 2014051200;    // Requires this Moodle version
$plugin->component = 'report_studentsontrack';  // Full name of the plugin (used for diagnostics)
