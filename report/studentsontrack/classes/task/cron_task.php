<?php
/**
 * This file contains functions used by the studentsontrack reports
 *
 *
 * @package    report_studentsontrack
 * @copyright  Androgogic
 */
namespace report_studentsontrack\task;

class cron_task extends \core\task\scheduled_task {

    /**
     * Get a descriptive name for this task (shown to admins).
     *
     * @return string
     */
    public function get_name() {
        return get_string('crontask', 'report_studentsontrack');
    }

    /**
     * Run studentsontrack report cron.
     */
    public function execute() {
        global $CFG;
        require_once($CFG->dirroot . '/report/studentsontrack/lib.php');
        report_studentsontrack_task();
    }

}
