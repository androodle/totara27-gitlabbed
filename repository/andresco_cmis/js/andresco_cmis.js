/*-----------------------------------------------------------------------------*
 * Andresco Javascript Library
 *-----------------------------------------------------------------------------*
 *
 * Requires the YUI javascript library which is is loaded by Moodle. Put any
 * common Javascript used by Andresco here.
 * 
 * Andresco is copyright 2014+ by Androgogic Pty Ltd
 * http://www.androgogic.com.au/andresco
 * 
 *-----------------------------------------------------------------------------*/

// Test the connection settings specified.
// Tests include -
// (1) Confirming the ability to get to the Alfresco API location
// (2) Confirming that user details specified can generate an Alfresco ticket

YUI(M.yui.loader).use('node', 'io', 'json-parse', function(Y) { 

	var test_connection_button = Y.one('#id_test_connection');

	test_connection_button.on('click', function() {

		var alfresco_url = Y.one('input#id_alfresco_cmis_url').get('value');
		var connection_username = Y.one('input#id_connection_username').get('value');
		test_alfresco_connection(alfresco_url, connection_username);
	});
        
	var test_alfresco_connection = function(url, user) {
		Y.io('../repository/andresco_cmis/util/test_connection.php', {
			method: 'POST',
			form: {
				id: 'mform1', // serialize and POST all form values
				useDisabled: true
			},
			on: {
				complete: function(id, response) {
										
					outcome = Y.JSON.parse(response.responseText);
					if (outcome.alfresco_url_test == 'Successful') {						
						if (outcome.alfresco_login_test == 'Successful') {
							alert('Successfully connected to URL and logged into Alfresco with user: ' + user);
							save_alfresco_aspects();
						}
						else {
							alert('Failed to login to Alfresco with user: ' + user);
							// Stop at this first error message.
							return;
						}
					}
					else {
						if (outcome.message) {
							alert(outcome.message);
						}
						else {
						alert('Failed to connect to URL: ' + url);			
					}
						
					}
				}
			}
		});
	};

	var save_alfresco_aspects = function() {
		Y.io('../repository/andresco_cmis/util/test_connection.php?save_aspects=1', {
			method: 'POST',
			form: {
				id: 'mform1', // serialize and POST all form values
				useDisabled: true
			},
			on: {
				complete: function(id, response) {
					// User doesn't need to know that save was successful					
				}
	}
		});
	};

});