<?php
/**
 * @copyright 
 * @author excelsoft
 * @since
 *
 * file for validate constraints of list type on alfresco
 *
 * */

require('../../../config.php');
global $CFG;
require_once($CFG->dirroot . '/repository/lib.php');
$rid = $_REQUEST['rid'];
$prop = $_REQUEST['name']; 
$id = $_REQUEST['id'];
$context = context_system::instance();
//get the repository object 
$repo = repository::get_repository_by_id($rid,$context->id);
echo $repo->getPropDatatype($prop, $id);
?>
