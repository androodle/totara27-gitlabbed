<?php

/*-----------------------------------------------------------------------------*
 * Andresco Alfresco Connection Tester
 *-----------------------------------------------------------------------------*
 *
 * Tests connectivity to Alfresco based on specified repository settings.
 * Note this script needs to be called from Alfresco and isn't that useful
 * if called by itself.
 *
 * Andresco is copyright 2014+ by Androgogic Pty Ltd
 * http://www.androgogic.com.au/andresco
 *
 *-----------------------------------------------------------------------------*/

if (empty($_POST)) {
    echo 'This is not the script you are looking for.';
    die();
}

$save_aspects = FALSE;
if (isset($_GET['save_aspects']) && $_GET['save_aspects'] == 1) {
    $save_aspects = TRUE;
}

// The moodle config file is required
require('../../../config.php');
require_once($CFG->dirroot . '/repository/andresco_cmis/lib/cmis_repository_wrapper.php');

if (empty($CFG->passwordsaltmain)) {
    $results['message'] = 'No moodle salt set, Andresco requires a Moodle salt';
    echo json_encode($results);
    return TRUE;
}

// Capture POST variables as regular PHP variables for use in this script
$connection_url = $_POST['alfresco_cmis_url'];
$connection_url = str_replace('s/cmis', 'api', $connection_url);
$connection_username = $_POST['connection_username'];
$connection_password = $_POST['connection_password'];
$connection_password_encrypted = $_POST['connection_password_encrypted'];

// CMIS API always requires authentication so first set that up.
if (isset($connection_username) && isset($connection_password)) {
    if ($connection_password_encrypted == 1) {
        $connection_password = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $CFG->passwordsaltmain, (base64_decode($connection_password)), MCRYPT_MODE_ECB);
        // Remove nulls / EOT from end of decrypted string
        $connection_password = trim($connection_password, "\0\4");                    
    }
}

$auth_options = array();

if (!$save_aspects) {

    // Start with failed test results (which need to pass to be changed)
    $results = array(
        'alfresco_url_test' => 'Unsuccessful',
        'alfresco_login_test' => 'Unsuccessful',
        'message' => ''
    );
    
    if (isset($connection_url)) {    

        // Adjust connection URL to get the login API web script URL
        $login_api_url = str_replace('api', 's/api/login', $connection_url);

        // Create JSON data to post to login API web script
        $login_data = json_encode(array(
            'username' => $connection_username,
            'password' => $connection_password
        ));

        if ($login_result = login_request($connection_url, $connection_username, $connection_password)) {
            $results['alfresco_url_test'] = 'Successful';
            // Do we have a ticket in the results? If so, connection details are good.
            if (isset($login_result->data->ticket)) {
                $auth_options = array('alf_ticket' => $login_result->data->ticket);
                $results['alfresco_login_test'] = 'Successful';
            }
        }        
        
    }

    // Return results
    echo json_encode($results);
    return TRUE;
}
else {
    // Save aspects to file, if we have a valid URL and login. Note this occurs AFTER returning 
    // results as it is a background process. This isn't the ideal place to put it, and perhaps 
    // a better place would be in the cron? However, it gives the administrator a manual way to 
    // refresh aspects in captured by Andresco from Alfresco.

    // NOTE: calling this is VERY slow on most Alfresco installations. So realistically
    // the only time we would want to call it is upon saving a new repository connection. 

    if (!$login_result = login_request($connection_url, $connection_username, $connection_password)) {
        error_log('Andresco Test Connection: cannot save aspects, Alfresco connection failed');
        return FALSE;
    }

    $auth_options = array('alf_ticket' => $login_result->data->ticket);
    $client = new CMISService($_POST['alfresco_cmis_url'], $connection_username, $connection_password, $auth_options);    
    $repo_url = str_replace('/s/cmis', '', $_POST['alfresco_cmis_url']);
    $url = $repo_url . "/s/api/classes?cf=aspect";
    $json_aspect = $client->doGet($url);
    $decode_json = json_decode($json_aspect->body);
        
    foreach ($decode_json as $json_array) {
        if ($json_array->isAspect == 1) {
            $aspect_array[] = str_replace(':', '_', $json_array->name);
        }
    }
    
    foreach ($aspect_array as $arr) {
        try {
            $url = $repo_url . "/s/api/classes/" . $arr;
            $json_aspect = $client->doGet($url);
        } catch (Exception $e) {
            //throw new Exception("unable to process the aspect".$arr);
        }
        $decode_json = json_decode($json_aspect->body);
        $aspect_prop_array['Aspect'] = str_replace('_', ':', $arr);
        foreach ($decode_json->properties as $key => $value) {
            $aspect_prop_array[str_replace('_', ':', $arr)][] = $key;
        }
    }
    
    $final_array = array();
    $prop = array();
    $arr1 = array();
    
    foreach ($aspect_prop_array as $asp => $prop) {
        if (is_array($prop)) {
            foreach ($prop as $key1 => $value1) {
                $arr1['key'] = $asp . '(' . $value1 . ')';
                $arr1['value'] = $asp . '(' . $value1 . ')';
                array_push($final_array, $arr1);
            }
        }
    }
    
    $asp_json_temp = json_encode($final_array);
    $asp_json = str_replace('null,', '', $asp_json_temp);

    //write data to file
    $prop_file = $CFG->dirroot . '/repository/andresco_cmis/temp/alfresco_properties.html';
    if (file_exists($prop_file)) {
        unlink($prop_file);
    }
    $fp = fopen($prop_file, "w");
    fwrite($fp, $asp_json);
    fclose($fp);
}

/**
 * Perform an Alfresco API login request and return the results (JSON format).
 *
 * @param   Connection URL
 * @param   Connection username
 * @param   Connection password (decrypted)
 * @return  JSON results if successful, FALSE if unsuccessful
 **/

function login_request($connection_url, $connection_username, $connection_password) {
      // Adjust connection URL to get the login API web script URL
        $login_api_url = str_replace('api', 's/api/login', $connection_url);

        // Create JSON data to post to login API web script
        $login_data = json_encode(array(
            'username' => $connection_username,
            'password' => $connection_password
        ));

        // Submit request to login API web script (POST with JSON data)
        $c = curl_init();
        curl_setopt($c, CURLOPT_URL, $login_api_url);
        curl_setopt($c, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($c, CURLOPT_POSTFIELDS, $login_data);
        curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($c, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($c, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($c, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($login_data)
        ));    
        if (curl_error($c)) {
            error_log("Andresco test connection: Login cURL error: " . curl_error($c));
            return FALSE;
        }
        else {                
            $login_result = json_decode(curl_exec($c)); 
            curl_close($c);
            // Do we have valid JSON? If not, then we most likely have a URL issue
            if (json_last_error() == JSON_ERROR_NONE) {
                return $login_result;
            }
            else {
                error_log('Andresco test connection: Login JSON error (see andresco_cmis.log).');                
                return FALSE;
            }
        }
}
// End andresco/util/test_connection.php
