<?php

/*-----------------------------------------------------------------------------*
 * Andresco Repository Plugin: Language Pack
 *-----------------------------------------------------------------------------*
 * 
 * Andresco is copyright 2014+ by Androgogic Pty Ltd
 * http://www.androgogic.com.au/andresco
 * 
 *-----------------------------------------------------------------------------*/

require($CFG->dirroot . '/repository/andresco_cmis/version.php');        

// Standard Andresco Strings

$string['alfresco_cmis_url'] = 'Alfresco Repository CMIS URL';
$string['alfrescourltext'] = 'Alfresco API url format is http(s)://server:port/alfresco/s/cmis';
$string['alfresco:view'] = 'View Andresco repository';
$string['configplugin'] = 'Andresco (CMIS) configuration';
$string['notitle'] = 'notitle';
$string['password'] = 'Password';
$string['pluginname_help'] = 'Moodle-Alfresco Integration';
$string['pluginname'] = 'Andresco repository (CMIS)';
$string['soapmustbeenabled'] = 'SOAP extension must be enabled for Andresco plugin';
$string['username'] = 'User name';

// Repository Settings

$string['connection_username'] = 'Connection User';
$string['connection_password'] = 'Connection Password';
$string['connection_password_encrypted'] = 'Password Encrypted?';
$string['connection_password_help'] = 'To change, clear the connection password, re-type and set "Password has been encrypted" to no.';

$string['test_connection'] = 'Test Connection Settings';

$string['starting_node_uuid'] = 'Starting Node Path';
$string['starting_node_uuid_help'] = 'Path of the Node to start at in the Alfresco Repository. (eg. /My Home/Images) 
									  Blank is equivalent to "/".';

$string['content_access_method'] = 'Content Duplication';

$string['uploadfilebtnoptions'] = 'Upload File Button Option';
$string['showuploadfilebtn'] = 'Show the Upload File Button in Andresco toolbar';
$string['hideuploadfilebtn'] = 'Hide the Upload File Button in Andresco toolbar';

$string['createfolderbtnoptions'] = 'Create Folder Button Option';
$string['showcreatefolderbtn'] = 'Show the Create Folder button in Andresco toolbar';
$string['hidecreatefolderbtn'] = 'Hide the Create Folder button in Andresco toolbar';

$string['andresco_auth'] = 'Authentication Script Location';
$string['andresco_auth_help'] = 'Specify the name and relative path to the andresco authentication script that should be appended to the base Alfresco URL (if left blank, "auth.php" is the default)';

$string['copy'] = 'Always copy repository files to Moodle where possible';
$string['link'] = 'Never copy repository files to Moodle';
$string['nevercopy'] = 'Copy repository files to Moodle where no link/reference options exist';

$string['versinoning_strategy'] = 'Versioning strategy';
$string['updatetonew'] = 'All links automatically update to new version';
$string['pointpermanently'] = 'All links point to selected version permanently';
$string['userupdate'] = 'User chooses - default is auto update to new version';
$string['userpoint'] = 'User chooses - default is selected version permanently';

$string['unable_to_access_repository'] = 'Unable to access Alfresco. Please review repository settings and Alfresco status.';
$string['upload_file'] = 'Upload File';
$string['upload_folder'] = 'Create Folder';

// Moodle 2.3+ Upload Form Renderer Language strings (files/renderer.php)
$string['uploadfilename'] = 'Filename';
$string['uploadfoldername'] = 'Foldername';
$string['uploadtitle'] = 'Title';
$string['uploaddescription'] = 'Description';
$string['uploadfolder'] = 'Create folder';
$string['contenttype'] = 'Content Type';

$string['contentversion'] = 'Content Version Preferences';
// Version information

$androgogic_alfresco_link = '<a href="http://www.androgogic.com/alfresco" target="_blank">Androgogic\'s Moodle-Alfresco Integration</a>';
$string['version_information'] = 'This is version 1.11.1.' . $plugin->version . ' of Andresco - ' . $androgogic_alfresco_link;
$string['view_on_share'] = 'View in Repository';

//Error Messaged
$string['token_expire_error'] = 'Alfresco session expired.';
$string['readonly_message'] = 'Alfresco is in readonly mode.';

$string['instancedisabled'] = 'Instance disabled';
$string['instanceenabled'] = 'Instance enabled';
$string['confirmdisable'] = 'Confirm disabled';
$string['confirmenable'] = 'Confirm enabled';
$string['disablerepository'] = 'Disable repository';
$string['enablerepository'] = 'Enable repository';
$string['actionrepository'] = 'Enable/Disable';
$string['upload_new_version'] = 'Upload new version';
$string['search_error'] = 'Please search for a valid keyword';

//Strings for search type
$string['instance_default_search'] = 'Search Type Options';
$string['default_search_help'] = 'Select how multiple keywords are treated for free text searches';
$string['instance_stop_words'] = 'Stop Words';
$string['stop_words_help'] = 'Define words to avoid during search (Example: a, an, are, as, at etc..)';
$string['upload_file_error'] = 'You must select a file to upload';

//String for search properties override
$string['search_properties'] = 'Search Override Properties';
$string['search_properties_help'] = 'Add properties to keyword search eg. abc:aspect1(prop1,prop2,...)|xyz:aspect2(prop1,prop2,...)|...';
$string['upload_cancel'] = 'Cancel';
$string['upload_filename'] = 'Upload File';
//Strings for default labels to show
$string['instance_default_label'] = 'Label Display Default';
$string['default_label_help'] = 'Select whether to display file names, titles or both in browsing and search results';

//Strings for content versioning preferences
$string['content_versioning_preferences'] = 'Content Versioning Preferences';

$string['automatically_update'] = 'All links automatically update to new versions';
$string['version_permanently'] = 'All links point to the selected version permanently';
$string['new_versions_as_default'] = 'User chooses, with link update to new versions as default';
$string['add_latest_version'] = 'User chooses, with link to this version permanently the default';
$string['level_error'] = 'Repository can be browsed upto 3 levels only. Please change settings for repository instance';

//Strings for Alfreso username settings
$string['alfresco_username_default'] = 'Default Alfresco group';
$string['alfresco_username_help_1'] = 'Specify the Alfresco group assigned to a standard Alfresco user when they are created.';

$string['alfresco_username_teacher'] = 'Alfresco group for Editing Teacher';
$string['alfresco_username_help_2'] = 'Specify the Alfresco group assigned to an Editing Teacher when they are created.';

$string['alfresco_username_admin'] = 'Alfresco group for Administrator';
$string['alfresco_username_help_3'] = 'Specify the Alfresco group assigned to a Administrator when they are created.';

//Strings for disable/enable 'view in repository' button setting
$string['show_hide_view_in_repo_label'] = 'View in Repo display setting';
$string['show'] = 'Show';
$string['hide'] = 'Hide';

// BEGIN:: ANDRESCO
// Strings for Andresco Scafold
$string['title_add_ui_scafold'] = 'Dynamic Metadata Mapping Configuration';
$string['ui_scafold_property_name'] = 'Property Name';
$string['ui_scafold_display_label'] = 'Display Name';
$string['ui_scafold_is_constraint'] = 'Use Constraints';
$string['ui_scafold_default_value_expression'] = 'Default Value';
$string['ui_scafold_rendering_type'] = 'Rendering Type';
$string['ui_scafold_is_mandatory'] = 'Mandatory';
$string['ui_scafold_delete'] = 'Delete';
$string['customp_property_configuration'] = 'Custom Property Configuration';
$string['customp_property_setting'] = 'Create/Edit';

// Custom Error messages for Exeption Handling
$string['repository_down'] = "Andresco is unable to access this repository. Something went wrong, Please contact Administrator.";
$string['repository_permission'] = "Sorry, you do not have the permissions in Alfresco to perform that action";

//Display Item Configuration
$string['display_item_config'] = "Display Item Configuration";
$string['display_settings'] = "Display Settings";

$string['search_parameters'] = "Search Parameters";

//search property filter
$string['search_filter_fieldset'] = "Search Filter";

$string['search_path_filter'] = "Search Path Filter";
$string['search_path_help'] = "*you can enter multiple paths here, separated by comma. Please provide full path to the directory in which to restrict the search(eg./Data Dictionary/Example Folder)";

$string['search_aspect_filter'] = "Search Aspect Filter";
$string['search_aspect_help'] = "*you can enter multiple aspects here, separated by comma. Please select relation between aspect filters from the dropdown.";
$string['aspect_conjunction'] = 'Aspect Conjunction';

$string['search_prop_filter'] = "Search Property Filter";
$string['search_prop_help'] = "*you can enter multiple properties here, separated by comma. Please select relation between properties from the dropdown.";
$string['prop_conjunction'] = 'Property Conjunction';

// Exception handling
$stirng['unabletogetadminticket'] = "Andresco was unable to retrieve a valid ticket using the connection username and password, please check these settings and connectivity to Alfresco in general.";

// END:: ANDRESCO
//BEGIN : RMC
$string['cust_name'] = 'Customer Username';
$string['cust_intro_mail'] = 'Customer purchase intro mail';
$string['cust_outro_mail'] = 'Customer purchase outro mail';
$string['cust_form_title'] = 'Add customer';
$string['cust_form_title_update'] = 'Add customer';
$string['missingcustomername'] = 'Customer field missing';
$string['sno_label'] = 'Serial #';
$string['action_label'] = 'Action';
$string['customer_label'] = 'Customer';
$string['delete_success'] = 'Item Deleted.';
$string['lms_label'] = 'LMS';
$string['lms_label_update'] = 'Update LMS';
$string['lms_code_label'] = 'LMS Code';
$string['lms_url_label'] = 'LMS Url';
$string['access_type_label'] = 'Access Type';
$string['missingcodename'] = 'LMS code missing';
$string['missinglmsurl'] = 'LMS url missing';
$string['full_label'] = 'Full';
$string['none_label'] = 'None';
$string['shortname_label'] = 'Course Short Name';
$string['fullname_label'] = 'Course Full Name';
$string['course_url_label'] = 'Course Url';
$string['missingcourseshortname'] = 'Missing course name';
$string['missingcourseurl'] = 'Missing course url';
$string['course_form_title'] = 'Add course details';
$string['course_form_update_title'] = 'Add course details';
$string['publisher_label'] = 'Publisher';
$string['publisher_name_label'] = 'Publisher Name';
$string['publisher_alfresco_username_label'] = 'Publisher Alfresco Username';
$string['fromemail_label'] = 'Publisher From Email Address';
$string['purchase_acchtml_noaccount'] = 'Publisher purchase HTML (no account) purchase order';
$string['purchase_acchtml_account'] = 'Publisher purchase HTML account';
$string['missingpublishername'] = 'Publisher field missing';
$string['missingalfresco_name'] = 'Alfresco username missing';
$string['missingfromemail'] = 'From email address missing';
$string['publisher_form_title'] = 'Add publisher';
$string['publisher_form_title_update'] = 'Update publisher';
$string['purchaseentity_label'] = 'Puchased Entities';
$string['entity_type_label'] = 'Entity Type';
$string['entity_id_label'] = 'Entity ID';
$string['content_type_label'] = 'Content Type';
$string['content_id_label'] = 'Content ID';
$string['access_type_label'] = 'Access Type';
$string['no_unique_users_label'] = 'Number Unique Users';
$string['authorisation_label'] = 'Authorisation Username';
$string['expiry_date_label'] = 'Expiry Date';
$string['missingenttype'] = 'Entity type missing';
$string['missingentid'] = 'Entity id missing';
$string['missingconttype'] = 'Content type missing';
$string['missingcontentid'] = 'Content ID missing';
$string['missingacctype'] = 'Access type missing';
$string['missinguniqueuser'] = 'User number missing';
$string['missingauthorisa'] = 'Authorisation missing';
$string['missingexpdate'] = 'Expiry date missing';
$string['content_label'] = 'Content';
$string['allowed_label'] = 'Allowed';
$string['disallowed_label'] = 'Disallowed';
$string['account_label'] = 'Have Account';
$string['missingaccesstype'] = 'Mandatory field';
$string['mappingerr_label'] = 'Mapping already exists';
$string['map_cust_pub'] = 'Map customer and publisher';
$string['map_cust_pub_update'] = 'Map customer and publisher';
$string['title_cust_pub'] = 'Publisher Customer Mapping';
$string['publisher_label'] = 'Publisher';
$string['competency_label'] = 'Competency';
//END : RMC