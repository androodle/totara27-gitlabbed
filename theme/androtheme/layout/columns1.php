<?php

include(__DIR__.'/includes/header.php'); ?>

<div id="page" class="container-fluid">
    <div id="page-content" class="row-fluid">
        <section id="region-main" class="span12">
            <?php
            echo $OUTPUT->course_content_header();
            echo $OUTPUT->main_content();
            echo $OUTPUT->course_content_footer();
            ?>
        </section>
    </div>
</div>

<?php include(__DIR__.'/includes/footer.php'); ?>