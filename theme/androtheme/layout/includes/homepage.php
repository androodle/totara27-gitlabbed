<div id="region-hero-unit">
    <?php
        echo $OUTPUT->heading(get_string('welcome-text', 'theme_androtheme', $USER->firstname), 1, 'hero-heading');
        echo $OUTPUT->blocks('hero-unit');
    ?>
</div>

<div id="region-two-cols-wrap" class="clearfix">
    <div class="column"><?php echo $OUTPUT->blocks('two-cols-left'); ?></div>
    <div class="column"><?php echo $OUTPUT->blocks('two-cols-right'); ?></div>
</div>

<div id="region-three-cols-wrap" class="clearfix">
    <div class="three-cols-header"><?php echo $OUTPUT->blocks('three-cols-head'); ?></div>
    <div class="column"><?php echo $OUTPUT->blocks('three-cols-left'); ?></div>
    <div class="column"><?php echo $OUTPUT->blocks('three-cols-mid'); ?></div>
    <div class="column"><?php echo $OUTPUT->blocks('three-cols-right'); ?></div>
</div>

<div id="region-four-cols-wrap" class="clearfix">
    <div class="four-cols-header"><?php echo $OUTPUT->blocks('four-cols-head'); ?></div>
    <div class="column"><?php echo $OUTPUT->blocks('four-cols-lefta'); ?></div>
    <div class="column"><?php echo $OUTPUT->blocks('four-cols-leftb'); ?></div>
    <div class="column"><?php echo $OUTPUT->blocks('four-cols-righta'); ?></div>
    <div class="column"><?php echo $OUTPUT->blocks('four-cols-rightb'); ?></div>
</div>