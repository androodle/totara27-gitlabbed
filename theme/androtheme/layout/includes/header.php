<?php

$html = theme_androtheme_get_html_for_settings($OUTPUT, $PAGE);
if (!empty($PAGE->theme->settings->logo)) {
    $logourl = $PAGE->theme->setting_file_url('logo', 'logo');
    $logoalt = get_string('logo', 'theme_androtheme', $SITE->fullname);
} else {
    $logourl = $OUTPUT->pix_url('logo', 'theme');
    $logoalt = get_string('logo', 'theme_androtheme');
}

if (!empty($PAGE->theme->settings->logounit)) {
    $logouniturl = $PAGE->theme->setting_file_url('logounit', 'logounit');
    $logounitalt = get_string('logounit', 'theme_androtheme', $SITE->fullname);
} else {
    $logouniturl = $OUTPUT->pix_url('logounit', 'theme');
    $logounitalt = get_string('logounit', 'theme_androtheme');
}

if (!empty($PAGE->theme->settings->favicon)) {
    $faviconurl = $PAGE->theme->setting_file_url('favicon', 'favicon');
} else {
    $faviconurl = $OUTPUT->favicon();
}

$custommenu = $OUTPUT->custom_menu();
$hascustommenu = !empty($custommenu);

$haslogininfo = empty($PAGE->layout_options['nologininfo']);
$showmenu = empty($PAGE->layout_options['nocustommenu']);
$haslangmenu = (!isset($PAGE->layout_options['langmenu']) || $PAGE->layout_options['langmenu'] );

// To know if to add 'pull-right' and 'desktop-first-column' classes in the layout for LTR.
$left = (!right_to_left()); 

if ($showmenu && !$hascustommenu) {
    // load totara menu
    $menudata = totara_build_menu();
    $totara_core_renderer = $PAGE->get_renderer('totara_core');
    $totaramenu = $totara_core_renderer->print_totara_menu($menudata);
}

echo $OUTPUT->doctype() ?>

<html <?php echo $OUTPUT->htmlattributes(); ?>>
<head>
    <title><?php echo $OUTPUT->page_title(); ?></title>
    <link rel="shortcut icon" href="<?php echo $faviconurl; ?>" />
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <?php echo $OUTPUT->standard_head_html() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<body <?php echo $OUTPUT->body_attributes(); ?>>

<?php echo $OUTPUT->standard_top_of_body_html() ?>

<?php include(__DIR__.'/nav.php'); ?>

<!--Andro sniffer-->
<div>
    <?php
    if ($PAGE->pagelayout == 'login' && file_exists("$CFG->dirroot/local/versionsniffer/locallib.php")) {
        require_once("$CFG->dirroot/local/versionsniffer/locallib.php");
        echo local_versionsniffer_out();
    }
    ?>
</div>