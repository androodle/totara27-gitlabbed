<?php require_once(dirname(__FILE__).'/includes/scripts.php'); ?>

<html <?php echo $OUTPUT->htmlattributes(); ?>>
<head>
    <title><?php echo $OUTPUT->page_title(); ?></title>
    <link rel="shortcut icon" href="<?php echo $faviconurl; ?>" />
    <link href='//fonts.googleapis.com/css?family=Roboto+Slab:700' rel='stylesheet' type='text/css'>
    <?php echo $OUTPUT->standard_head_html() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<body <?php echo $OUTPUT->body_attributes(); ?>>

<!--Andro sniffer-->
<div>
    <?php
    if ($PAGE->pagelayout == 'login' && file_exists("$CFG->dirroot/local/versionsniffer/locallib.php")) {
        require_once("$CFG->dirroot/local/versionsniffer/locallib.php");
        echo local_versionsniffer_out();
    }
    ?>
</div>

<?php echo $OUTPUT->standard_top_of_body_html() ?>

<?php require_once(dirname(__FILE__).'/includes/header.php'); ?>

<div id="page" class="container-fluid">
    <div id="page-content" class="row-fluid">
        <div id="region-bs-main-and-pre" class="span9 <?php echo $left ? "pull-left" : "pull-right" ?>">
            <div class="row-fluid">
                <section id="region-main" class="span8 <?php echo $left ? "pull-right" : "pull-left" ?>">
                    <?php
                    echo $OUTPUT->course_content_header();
                    ?>
                    <!-- CUSTOM HOME START -->
                    <div class="region-content" id="custom-homepage-regions">
                        <?php require_once('includes/homepage.php'); ?>
                    </div>
                    <!-- CUSTOM HOME END -->
                    
                    <?php
                    echo $OUTPUT->main_content();
                    ?>
                    
                    <?php
                    echo $OUTPUT->course_content_footer();
                    ?>
                </section>
                <?php echo $OUTPUT->blocks('side-pre', 'span4' . ($left ? ' desktop-first-column' : '')); ?>
            </div>
        </div>
        <?php echo $OUTPUT->blocks('side-post', 'span3' . (!$left ? ' desktop-first-column pull-left' : '')); ?>
    </div>
</div>    

<?php require_once(dirname(__FILE__).'/includes/footer.php'); ?>

<?php echo $OUTPUT->standard_end_of_body_html() ?>

</body>
</html>
