<?php
$html = theme_androtheme_get_html_for_settings($OUTPUT, $PAGE);
if (!empty($PAGE->theme->settings->logo)) {
    $logourl = $PAGE->theme->setting_file_url('logo', 'logo');
    $logoalt = get_string('logo', 'theme_androtheme', $SITE->fullname);
} else {
    $logourl = $OUTPUT->pix_url('logo', 'theme');
    $logoalt = get_string('logo', 'theme_androtheme');
}

if (!empty($PAGE->theme->settings->logounit)) {
    $logouniturl = $PAGE->theme->setting_file_url('logounit', 'logounit');
    $logounitalt = get_string('logounit', 'theme_androtheme', $SITE->fullname);
} else {
    $logouniturl = $OUTPUT->pix_url('logounit', 'theme');
    $logounitalt = get_string('logounit', 'theme_androtheme');
}

if (!empty($PAGE->theme->settings->favicon)) {
    $faviconurl = $PAGE->theme->setting_file_url('favicon', 'favicon');
} else {
    $faviconurl = $OUTPUT->favicon();
}

$custommenu = $OUTPUT->custom_menu();
$hascustommenu = !empty($custommenu);

$haslogininfo = empty($PAGE->layout_options['nologininfo']);
$showmenu = empty($PAGE->layout_options['nocustommenu']);
$haslangmenu = (!isset($PAGE->layout_options['langmenu']) || $PAGE->layout_options['langmenu'] );

// To know if to add 'pull-right' and 'desktop-first-column' classes in the layout for LTR.
$left = (!right_to_left()); 

if ($showmenu && !$hascustommenu) {
    // load totara menu
    $menudata = totara_build_menu();
    $totara_core_renderer = $PAGE->get_renderer('totara_core');
    $totaramenu = $totara_core_renderer->print_totara_menu($menudata);
}

echo $OUTPUT->doctype() ?>