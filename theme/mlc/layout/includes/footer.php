<?php 
$hasfooter = (!isset($PAGE->layout_options['nofooter']) || !$PAGE->layout_options['nofooter'] );
$hasnavbar = (!isset($PAGE->layout_options['nonavbar']) || !$PAGE->layout_options['nonavbar'] );

if ($hasfooter) { ?>
<footer id="page-footer" class="row-fluid">
    <div id="footnotewrapper" class="container-fluid">
        <div class="footer-text">
            <span class="year">&copy;<?php echo date('Y')?></span>
            <?php echo$html->footnote; ?>
        </div>
        <div class="helplink"><?php echo $OUTPUT->page_doc_link(); ?></div>
    </div> 
</footer>
<?php } ?>