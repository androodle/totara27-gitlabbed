<div id="region-wrap">
    <div id="region-hero-unit">
        <?php echo $OUTPUT->blocks('hero-unit'); ?>
    </div>

    <div id="region-two-cols-wrap" class="clearfix">
        <div class="column">
            <?php echo $OUTPUT->blocks('two-cols-left'); ?>
        </div>
        <div class="column">
            <?php echo $OUTPUT->blocks('two-cols-right'); ?>
        </div>
    </div>
</div>