<?php

$THEME->name = 'austinhealth';
$THEME->doctype = 'html5';
$THEME->parents = array('androtheme', 'bootstrapbase', 'standardtotararesponsive');
$THEME->sheets = array(
    'custom',
    'login',
    'customhome',
    'responsive',
    'ie',
    'print'
);
$THEME->supportscssoptimisation = false;
$THEME->yuicssmodules = array();
$THEME->editor_sheets = array();
$THEME->plugins_exclude_sheets = array();
$THEME->layouts = array(
    // The site home page (CUSTOM)
    'frontpage' => array(
        'file' => 'frontpage.php',
        'regions' => array(
            'full-1col',
            'hero-unit',
            'two-cols-left',
            'two-cols-right',
            'three-cols-head',
            'three-cols-left',
            'three-cols-mid',
            'three-cols-right',
            'four-cols-lefta',
            'four-cols-leftb',
            'four-cols-righta',
            'four-cols-rightb',
            'side-pre',
            'side-post'
        ),
        'defaultregion' => 'side-post'
    ),
    'mydashboard' => array(
        'file' => 'frontpage.php',
        'regions' => array(
            'full-1col',
            'hero-unit',
            'two-cols-left',
            'two-cols-right',
            'three-cols-head',
            'three-cols-left',
            'three-cols-mid',
            'three-cols-right',
            'four-cols-lefta',
            'four-cols-leftb',
            'four-cols-righta',
            'four-cols-rightb',
            'side-pre',
            'side-post'
        ),
        'defaultregion' => 'side-post'
    ),
);

// special class tells Moodle to look for renderers first within the theme and then in all of the other default locations.
$THEME->rendererfactory = 'theme_overridden_renderer_factory';
$THEME->csspostprocess = 'theme_austinhealth_process_css';

$THEME->blockrtlmanipulations = array(
    'side-pre' => 'side-post',
    'side-post' => 'side-pre'
);

$THEME->enable_dock = true;