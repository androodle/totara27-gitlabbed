<?php

$html = theme_austinhealth_get_html_for_settings($OUTPUT, $PAGE);
if (!empty($PAGE->theme->settings->logo)) {
    $logourl = $PAGE->theme->setting_file_url('logo', 'logo');
    $logoalt = get_string('logo', 'theme_austinhealth', $SITE->fullname);
} else {
    $logourl = $OUTPUT->pix_url('logo', 'theme');
    $logoalt = get_string('logo', 'theme_austinhealth');
}

if (!empty($PAGE->theme->settings->logounit)) {
    $logouniturl = $PAGE->theme->setting_file_url('logounit', 'logounit');
    $logounitalt = get_string('logounit', 'theme_austinhealth', $SITE->fullname);
} else {
    $logouniturl = $OUTPUT->pix_url('logounit', 'theme');
    $logounitalt = get_string('logounit', 'theme_austinhealth');
}

if (!empty($PAGE->theme->settings->favicon)) {
    $faviconurl = $PAGE->theme->setting_file_url('favicon', 'favicon');
} else {
    $faviconurl = $OUTPUT->favicon();
}

$custommenu = $OUTPUT->custom_menu();
$hascustommenu = !empty($custommenu);

$haslogininfo = empty($PAGE->layout_options['nologininfo']);
$showmenu = empty($PAGE->layout_options['nocustommenu']);
$haslangmenu = (!isset($PAGE->layout_options['langmenu']) || $PAGE->layout_options['langmenu'] );

// To know if to add 'pull-right' and 'desktop-first-column' classes in the layout for LTR.
$left = (!right_to_left()); 

if ($showmenu && !$hascustommenu) {
    // load totara menu
    $menudata = totara_build_menu();
    $totara_core_renderer = $PAGE->get_renderer('totara_core');
    $totaramenu = $totara_core_renderer->print_totara_menu($menudata);
}

echo $OUTPUT->doctype() ?>

<html <?php echo $OUTPUT->htmlattributes(); ?>>
<head>
    <title><?php echo $OUTPUT->page_title(); ?></title>
    <link rel="shortcut icon" href="<?php echo $faviconurl; ?>" />
	<?php $themeroot = $CFG->httpswwwroot.'/theme/austinhealth';?>
    <link href='//fonts.googleapis.com/css?family=Roboto+Condensed:700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="<?php echo $themeroot; ?>/style/flickerplate.css">

	<?php echo $OUTPUT->standard_head_html() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<script src="<?php echo $themeroot; ?>/js/jquery-v1.10.2.min.js" type="text/javascript"></script>
	<script src="<?php echo $themeroot; ?>/js/modernizr-custom-v2.7.1.min.js" type="text/javascript"></script>
	<script src="<?php echo $themeroot; ?>/js/hammer-v2.0.3.min.js" type="text/javascript"></script>
	<script src="<?php echo $themeroot; ?>/js/flickerplate.min.js" type="text/javascript"></script>
	<script>
    var myslide = jQuery.noConflict();
    myslide(document).ready(function(){
        myslide('.slides').flickerplate({
           auto_flick: true,
           auto_flick_delay: 8,
           flick_animation: 'transform-slide'
       });
    });
    </script>
	
</head>

<body <?php echo $OUTPUT->body_attributes(); ?>>

<?php echo $OUTPUT->standard_top_of_body_html() ?>

<?php include(__DIR__.'/nav.php'); ?>

<!--Andro sniffer-->
<div>
    <?php
    if ($PAGE->pagelayout == 'login' && file_exists("$CFG->dirroot/local/versionsniffer/locallib.php")) {
        require_once("$CFG->dirroot/local/versionsniffer/locallib.php");
        echo local_versionsniffer_out();
    }
    ?>
</div>