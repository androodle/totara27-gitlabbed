<?php

include(__DIR__.'/includes/header.php'); ?>

<div id="page" class="container-fluid">
    <div id="page-content" class="row-fluid">
        <div id="region-bs-main-and-pre" class="span9 <?php echo $left ? "pull-left" : "pull-right" ?>">
            <div class="row-fluid">
                <section id="region-main" class="span8 <?php echo $left ? "pull-right" : "pull-left" ?>">
                    <?php echo $OUTPUT->course_content_header(); ?>
                    <?php echo $OUTPUT->main_content(); ?>
                    <?php echo $OUTPUT->course_content_footer(); ?>
                </section>
                <?php echo $OUTPUT->blocks('side-pre', 'span4' . ($left ? ' desktop-first-column' : '')); ?>
            </div>
        </div>
        <?php echo $OUTPUT->blocks('side-post', 'span3' . (!$left ? ' desktop-first-column pull-left' : '')); ?>
    </div>
</div>    

<?php include(__DIR__.'/includes/footer.php'); ?>