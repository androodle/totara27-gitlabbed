<?php

$string['choosereadme'] = '
<div class="clearfix">
        <div class="theme_screenshot">
                <h2>Austin Health</h2>
                <img src="austinhealth/pix/screenshot.jpg" />
                <h3>Theme Credits</h3>
                <p>Created by Androgogic Pty Ltd</p>
                <h3>Report a bug:</h3>
                <p><a href="https://androgogic.livetime.com/" target="_blank">https://androgogic.livetime.com/</a></p>
        </div>
        <div class="theme_description">
                <h2>About</h2>
                <p>Austin Health theme is a flexible responsive theme</p>
                <h2>Tweaks</h2>
                <p>This theme is built upon the Androtheme.</p>
        </div>
</div>
';

$string['pluginname'] = 'Austin Health';

$string['logo'] = 'Logo';
$string['logodesc'] = 'Please upload your custom logo here.';

$string['favicon'] = 'Favicon';
$string['favicondesc'] = 'Select or upload the image file to be used as the site\'s favicon, the icon must be *.ico format';

$string['logounit'] = 'Logo Unit (optional)';
$string['logounitdesc'] = 'Please upload your logo unit here.';

$string['bannertext'] = 'Additional banner text (optional)';
$string['bannertextdesc'] = 'This text goes to the right hand side of the logo.';

/* Custom CSS */

$string['customcss'] = 'Custom CSS';
$string['customcssdesc'] = 'Any CSS you enter here will be added to every page allowing your to easily customise this theme.';

// Home page regions
$string['region-full-1col'] = 'Front page Full Width Single Column';

$string['footnote'] = 'Footnote';
$string['footnotedesc'] = 'The content from this textarea will be displayed in the footer of every page.';