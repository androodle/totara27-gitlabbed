<?php

defined('MOODLE_INTERNAL') || die;

if ($ADMIN->fulltree) {

        // Logo file setting.
        $name = 'theme_austinhealth/logo';
        $title = get_string('logo','theme_austinhealth');
        $description = get_string('logodesc', 'theme_austinhealth');
        //$default = '';
        $setting = new admin_setting_configstoredfile($name, $title, $description, 'logo');
        $setting->set_updatedcallback('theme_reset_all_caches');
        $settings->add($setting);
        
        // Favicon file setting.
        $name = 'theme_austinhealth/favicon';
        $title = new lang_string('favicon', 'theme_austinhealth');
        $description = new lang_string('favicondesc', 'theme_austinhealth');
        $setting = new admin_setting_configstoredfile($name, $title, $description, 'favicon', 0, array('accepted_types' => '.ico'));
        $setting->set_updatedcallback('theme_reset_all_caches');
        $settings->add($setting);

        // Logo Unit file setting.
        $name = 'theme_austinhealth/logounit';
        $title = get_string('logounit','theme_austinhealth');
        $description = get_string('logounitdesc', 'theme_austinhealth');
        //$default = '';
        $setting = new admin_setting_configstoredfile($name, $title, $description, 'logounit');
        $setting->set_updatedcallback('theme_reset_all_caches');
        $settings->add($setting);
        
        //Banner header text setting
        $name = 'theme_austinhealth/bannertext';
        $title = get_string('bannertext', 'theme_austinhealth');
        $description = get_string('bannertextdesc', 'theme_austinhealth');
        $default = '';
        $setting = new admin_setting_confightmleditor($name, $title, $description, $default);
        $setting->set_updatedcallback('theme_reset_all_caches');
        $settings->add($setting);

        //Footnote setting
        $name = 'theme_austinhealth/footnote';
        $title = get_string('footnote', 'theme_austinhealth');
        $description = get_string('footnotedesc', 'theme_austinhealth');
        $default = '';
        $setting = new admin_setting_confightmleditor($name, $title, $description, $default);
        $setting->set_updatedcallback('theme_reset_all_caches');
        $settings->add($setting);
        
        //Custom CSS file
        $name = 'theme_austinhealth/customcss';
        $title = get_string('customcss', 'theme_austinhealth');
        $description = get_string('customcssdesc', 'theme_austinhealth');
        $default = '';
        $setting = new admin_setting_configtextarea($name, $title, $description, $default);
        $setting->set_updatedcallback('theme_reset_all_caches');
        $settings->add($setting);
}