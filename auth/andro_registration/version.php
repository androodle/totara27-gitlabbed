<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Version details
 * NOTE: Based on "email" by Martin Dougiamas (http://dougiamas.com)
 *
 * @package    auth
 * @subpackage emailadmin
 * @copyright  2012 onwards Felipe Carasso (http://carassonet.org)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 *
 * 2015-07-02  Androgogic version with user deduplication and optional admin confirmation by Keith Buss <kbuss@outlook.com>
 * NOTE: requires Androgogic Sync plugin for user deduplication
 */

defined('MOODLE_INTERNAL') || die;

$plugin->version = 2015092400;      // The current plugin version (Date: YYYYMMDDXX)
$plugin->requires = 2013051400; 	// Requires this Moodle version - requires Totara 2.5 or above for AndroSync
$plugin->component = 'auth_andro_registration';      // Full name of the plugin (used for diagnostics)

