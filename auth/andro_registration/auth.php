<?php

/**
 * @author Felipe Carasso
 * @license http://www.gnu.org/copyleft/gpl.html GNU Public License
 * @package moodle multiauth
 *
 * Authentication Plugin: Email Authentication with admin confirmation
 *
 * Standard authentication function.
 *
 * 2012-12-03  File created based on 'email' package by Martin Dougiamas.
 *
 * 2015-07-02  Androgogic version with user deduplication and optional admin confirmation by Keith Buss <kbuss@outlook.com>
 * NOTE: requires Androgogic Sync plugin for user deduplication
 */

defined('MOODLE_INTERNAL') || die();

require_once($CFG->libdir.'/authlib.php');
require_once($CFG->dirroot.'/user/profile/lib.php');

/**
 * Email authentication plugin.
 */
class auth_plugin_andro_registration extends auth_plugin_base {

    /**
     * Constructor.
     */
    function auth_plugin_andro_registration() {
        $this->authtype = 'andro_registration';
        $this->config = get_config('auth/andro_registration');
    }

    /**
     * Returns true if the username and password work and false if they are
     * wrong or don't exist.
     *
     * @param string $username The username
     * @param string $password The password
     * @return bool Authentication success or failure.
     */
    function user_login ($username, $password) {
        global $CFG, $DB;
        if ($user = $DB->get_record('user', array('username'=>$username, 'mnethostid'=>$CFG->mnet_localhost_id))) {
            return validate_internal_user_password($user, $password);
        }
        return false;
    }

    /**
     * Updates the user's password.
     *
     * called when the user password is updated.
     *
     * @param  object  $user        User table object  (with system magic quotes)
     * @param  string  $newpassword Plaintext password (with system magic quotes)
     * @return boolean result
     *
     */
    function user_update_password($user, $newpassword) {
        $user = get_complete_user_data('id', $user->id);
        return update_internal_user_password($user, $newpassword);
    }

    function can_signup() {
        return true;
    }

    /**
     * Sign up a new user ready for confirmation.
     * Password is passed in plaintext.
     *
     * @param object $user new user object
     * @param boolean $notify print notice with link and terminate
     */
    function user_signup($user, $notify=true) {
        global $CFG, $DB;
        require_once($CFG->dirroot.'/user/profile/lib.php');
        $matchcount = $this->is_duplicate($user);
        if ($matchcount) {
            global $CFG, $PAGE, $OUTPUT;
            $duplicate = get_string('duplicate', 'auth_andro_registration');
            $PAGE->navbar->add($duplicate);
            $PAGE->set_title($duplicate);
            $PAGE->set_heading($PAGE->course->fullname);
            echo $OUTPUT->header();
            notice(get_string('duplicate_message', 'auth_andro_registration'), $CFG->wwwroot.'/login/forgot_password.php');
            return false;
        }
        $user->password = hash_internal_user_password($user->password);
        $user->id = $DB->insert_record('user', $user);

        /// Save any custom profile field information
        profile_save_data($user);

        $user = $DB->get_record('user', array('id'=>$user->id));
        //events_trigger('user_created', $user);
        $usercontext = context_user::instance($user->id);
        $event = \core\event\user_created::create(
            array(
                'objectid' => $user->id,
                'relateduserid' => $user->id,
                'context' => $usercontext
                )
            );
        $event->trigger();

        if (! $this->send_confirmation_email_support($user)) {
            print_error('noemail','auth_andro_registration');
        }

        if ($notify) {
            global $CFG, $PAGE, $OUTPUT;
            $emailconfirm = get_string('emailconfirm');
            $PAGE->navbar->add($emailconfirm);
            $PAGE->set_title($emailconfirm);
            $PAGE->set_heading($PAGE->course->fullname);
            echo $OUTPUT->header();
        	if ($this->config->emailoption == 'user') {
				$message = get_string('emailconfirmsent', 'auth_andro_registration', $user->email);
			} else {
				$message = get_string('confirmpending', 'auth_andro_registration');
			}
			notice($message, "$CFG->wwwroot/index.php");
        } else {
            return true;
        }
    }

    /**
     * Returns true if plugin allows confirming of new users.
     *
     * @return bool
     */
    function can_confirm() {
        return true;
    }

    /**
     * Confirm the new user as registered.
     *
     * @param string $username
     * @param string $confirmsecret
     */
    function user_confirm($username, $confirmsecret) {
        global $DB;
        $user = get_complete_user_data('username', $username);

        if (!empty($user)) {
            if ($user->auth != $this->authtype) {
                error_log("Auth mismatch for user: $user->username, auth=$user->auth");
                return AUTH_CONFIRM_ERROR;

            } else if ($user->secret == $confirmsecret && $user->confirmed) {
                return AUTH_CONFIRM_ALREADY;

            } else if ($user->secret == $confirmsecret) {   // They have provided the secret key to get in
                $DB->set_field('user', 'confirmed', 1, array('id'=>$user->id));
                if ($this->config->emailoption == 'user' && $user->firstaccess == 0) {
                
                    //$DB->set_field('user', 'firstaccess', time(), array('id'=>$user->id));
                    
                    $now = time();
                    $DB->set_field("user", "firstaccess", $now, array("id"=>$user->id));

                    $user->firstaccess = $now;
                    events_trigger('user_firstaccess', $user);
                }
                self::send_confirmation_email_user($user);
                add_to_log(1, 'user', 'confirm', $_SERVER['REQUEST_URI'], $user->id);
                return AUTH_CONFIRM_OK;
            }
        } else {
            error_log("User not found: $username");
            return AUTH_CONFIRM_ERROR;
        }
    }

    function prevent_local_passwords() {
        return false;
    }

    /**
     * Returns true if this authentication plugin is 'internal'.
     *
     * @return bool
     */
    function is_internal() {
        return true;
    }

    /**
     * Returns true if this authentication plugin can change the user's
     * password.
     *
     * @return bool
     */
    function can_change_password() {
        return true;
    }

    /**
     * Returns the URL for changing the user's pw, or empty if the default can
     * be used.
     *
     * @return moodle_url
     */
    function change_password_url() {
        return null; // use default internal method
    }

    /**
     * Returns true if plugin allows resetting of internal password.
     *
     * @return bool
     */
    function can_reset_password() {
        return true;
    }

    /**
     * Prints a form for configuring this authentication plugin.
     *
     * This function is called from admin/auth.php, and outputs a full page with
     * a form for configuring this plugin.
     *
     * @param array $page An object containing all the data for this page.
     */
    function config_form($config, $err, $user_fields) {
        include "config.html";
    }

    /**
     * Processes and stores configuration data for this authentication plugin.
     */
    function process_config($config) {
        // set to defaults if undefined
        if (!isset($config->recaptcha)) {
            $config->recaptcha = false;
        }

        // save settings
        set_config('recaptcha', $config->recaptcha, 'auth/andro_registration');
        set_config('emailoption', $config->emailoption, 'auth/andro_registration');
	    return true;
    }

    /**
     * Returns whether or not the captcha element is enabled, and the admin settings fulfil its requirements.
     * @return bool
     */
    function is_captcha_enabled() {
        global $CFG;
        return !empty($CFG->recaptchapublickey) && !empty($CFG->recaptchaprivatekey) && get_config('auth/andro_registration', 'recaptcha');
    }

    /**
     * Send email to admin with confirmation text and activation link for
     * new user.
     *
     * @param user $user A {@link $USER} object
     * @return bool Returns true if mail was sent OK to *any* admin and false if otherwise.
     */
    function send_confirmation_email_support($user) {
        global $CFG;
    
        $site = get_site();
        $supportuser = core_user::get_support_user();
    
        $data = new stdClass();
        $data->firstname = fullname($user);
        $data->sitename  = format_string($site->fullname);
        $data->admin     = generate_email_signoff();

        $data->userdata = '';
        foreach(((array) $user) as $dataname => $datavalue) {
            if ($dataname <> 'password' and $dataname <> 'secret') {
            	$data->userdata	 .= $dataname . ': ' . $datavalue . PHP_EOL;
            }
        }

        // Add custom fields
        $data->userdata .= $this->list_custom_fields($user);
    
        $subject = get_string('confirmationsubject', 'auth_andro_registration', format_string($site->fullname));
    
        $username = urlencode($user->username);
        $username = str_replace('.', '%2E', $username); // prevent problems with trailing dots
        $data->link  = $CFG->wwwroot .'/auth/andro_registration/confirm.php?data='. $user->secret .'/'. $username;
        $message     = get_string('confirmation', 'auth_andro_registration', $data);
        $messagehtml = text_to_html(get_string('confirmation', 'auth_andro_registration', $data), false, false, true);
    
        $user->mailformat = 1;  // Always send HTML version as well
    
        // default to alladmins
        if (!isset($this->config->emailoption)) {
            $this->config->emailoption = 'alladmins';
        }
        
        // Get users to send the email to
        if ($this->config->emailoption == 'user') {
            $emailusers = array($user);
        } else if ($this->config->emailoption == 'usecapability') {
            $context = context_system::instance();
            $emailusers = get_users_by_capability($context, 'auth/andro_registration:receiveemails');
			if (empty($emailusers)) {
				// fallback to 'alladmins' when no users with capability
				$emailusers = get_admins();
			}
			
        } else if ($this->config->emailoption == 'firstadmin' or $this->config->emailoption == 'alladmins') {
			$emailusers = get_admins();
		} 

		// default to user when everything else fails
		if (empty($emailusers)) {
            $emailusers = array($user);
		}

        //directly email rather than using the messaging system to ensure its not routed to a popup or jabber
        $return = true;
        foreach ($emailusers as $user) {
            if (!email_to_user($user, $supportuser, $subject, $message, $messagehtml)) {
                error_log('Failed to send email to ' . $user->username);
                $return = false;
                break;
            }
            if ($this->config->emailoption == 'firstadmin') {
            	break;
            }
        }

        return $return;
    }

    /**
     * Return an array with custom user properties.
     *
     * @param user $user A {@link $USER} object
     */
    function list_custom_fields($user) {
        global $CFG, $DB;

        $result = '';
        if ($fields = $DB->get_records('user_info_field')) {
            foreach($fields as $field) {
                $fieldobj = new profile_field_base($field->id, $user->id);
                $result .= format_string($fieldobj->field->name.':') . ' ' . $fieldobj->display_data() . PHP_EOL;
            }
        }

        return $result;
    }

    function is_duplicate($user) {
        global $CFG;

        $incfile = $CFG->dirroot.'/local/androgogic_sync/classes/usermatch.class.php';
        if (!file_exists($incfile)) {
            return 0;
        }
        require_once($incfile);

        $checkuser = clone($user);
        if (!empty($checkuser->profile_field_dateofbirth)) {
            $checkuser->profile_field_dateofbirth = userdate($checkuser->profile_field_dateofbirth, '%Y-%m-%d', 99, false);
        }

/*
        Fields that can be passed for matching are:
        idnumber (must contain UID)
        firstname
        lastname
        alternatename (preferred first name, requires Totara v2.6)
        email
        profile_field_dateofbirth (requires user customfield in the format YYYY-MM-DD)
 */

        $UserMatch = new UserMatch();
        $matchcount = $UserMatch->findMatch($checkuser);
        return $matchcount;
    }
    
	function send_confirmation_email_user($user) {
		global $CFG;

		$site = get_site();
		$supportuser = core_user::get_support_user();

		$data = new stdClass();
		$data->firstname = fullname($user);
		$data->sitename  = format_string($site->fullname);
		$data->admin     = generate_email_signoff();

		$subject = get_string('userconfirmationsubject', 'auth_andro_registration', format_string($site->fullname));

		$username = urlencode($user->username);
		$username = str_replace('.', '%2E', $username); // prevent problems with trailing dots
		$data->link  = $CFG->wwwroot;
		$data->username = $username;
		$message     = get_string('userconfirmation', 'auth_andro_registration', $data);
		$messagehtml = text_to_html(get_string('userconfirmation', 'auth_andro_registration', $data), false, false, true);

		$user->mailformat = 1;  // Always send HTML version as well

		//directly email rather than using the messaging system to ensure its not routed to a popup or jabber

		return email_to_user($user, $supportuser, $subject, $message, $messagehtml);
	}
}
