<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Capability definitions.
 *
 * @package   auth_emailadmin
 * @copyright 2014 Androgogic {@link http://www.androgogic.com}
 * @author    Kirill Astashov <kirill.astashov@gmail.com>
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 *
 * 2015-07-02  Androgogic version with user deduplication and optional admin confirmation by Keith Buss <kbuss@outlook.com>
 * NOTE: requires Androgogic Sync plugin for user deduplication
 */

$capabilities = array(
    'auth/andro_registration:receiveemails' => array(
        'captype' => 'read',
        'contextlevel' => CONTEXT_SYSTEM,
        'archetypes' => array(
        )
    ),
);

