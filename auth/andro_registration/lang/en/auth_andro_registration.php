<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Strings for component 'auth_emailadmin', language 'en', branch 'MOODLE_20_STABLE'
 * NOTE: Based on 'email' package by Martin Dougiamas
 *
 * @package   auth_emailadmin
 * @copyright 2012 onwards Felipe Carasso  {@link http://moodle.com}
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 *
 * 2015-07-02  Androgogic version with user deduplication and optional admin confirmation by Keith Buss <kbuss@outlook.com>
 * NOTE: requires Androgogic Sync plugin for user deduplication
 */

$string['pluginname'] = 'Androgogic email-based self-registration';
$string['emailconfirmsent'] = '<p>An email should have been sent to your address at <b>{$a}</b></p>
   <p>It contains instructions to complete your registration.</p>
   <p>If you continue to have difficulty, please contact the site administrator.</p>';

$string['andro_registration:receiveemails'] = 'Receive emails to confirm self-registered users (also check the plugin config page)';

$string['auth_andro_registrationdescription'] = '<p><b>Androgogic email-based self-registration</b> enables a user
    to create their own account via a \'Create new account\' button on the login page. To prevent duplicate accounts in the LMS, a new account will be only be created when no matching account is found. The user will be prompted to reset their password when a matching account is found in the LMS.</p>
    <p>When a new account is created, the site admin (or users assigned the capability) then receive an email containing a secure link to a page where they can confirm the account.
    When the admin confirms the account, a "welcome" email is sent to the user. Future logins authenticate using the username and password stored in the LMS database.</p>
    <p><b>Installation:</b> In addition to enabling this authentication plugin, <b>Androgogic email-based self-registration</b> must also be
    selected in the self registration drop-down menu on the <b>Manage authentication</b> page.</p>';
$string['noemail'] = 'Tried to send you email but failed!
    Please contact your system administrator or Education Operations for further assistance.';
$string['recaptcha'] = 'Adds a visual/audio confirmation form element to the signup page for email
    self-registering users. This protects your site against spammers, see http://www.google.com/recaptcha/learnmore for more details. <br /><em>PHP cURL extension is required.</em>';
$string['recaptcha_key'] = 'Enable reCAPTCHA element';
$string['settings'] = 'Settings';

$string['userconfirmationsubject'] = 'Your {$a} user account';
$string['userconfirmation'] = '

Dear {$a->firstname},

Welcome to the {$a->sitename} site. Your new account has been approved.

Kind regards,
Your LMS administration team

';
$string['confirmation'] = 'Hi LMS Admin,

A new account has been requested at \'{$a->sitename}\' with  the following data:

{$a->userdata}

To confirm the new account, please go to this web address:

{$a->link}

In most mail programs, this should appear as a link which you can click on.  If that does not work, then cut and paste the address into the address line at the top of your web browser window.

You can also confirm accounts from within the LMS by going to
Site Administration -> Users';

$string['confirmationsubject'] = '{$a}: account confirmation';
$string['confirmpending'] = '<p>Your account has been registered and is pending confirmation by the administrator. You should expect to either receive a confirmation or to be contacted for further clarification.</p>';

$string['whoreceivesemails'] = 'Send emails to';
$string['whoreceivesemails_help'] = 'Select who will receive emails to confirm self-registered users. For the capability-based
    setting, in case no users with the capability in question are found, the plugin will fallback to the 1st option,
    i.e. send emails to all site admins found in the DB.';
$string['firstadmin'] = 'First site admin found in the DB';
$string['alladmins'] = 'All site admins';
$string['usecapability'] = 'Use the \'auth/andro_registration:receiveemails\' capability';
$string['user'] = 'Self-registered user';
$string['duplicate'] = 'Account already exists';
$string['duplicate_message'] = 'You have attempted to create a new account. However, an account for you is already on
    this system. If you have forgotten your username or password, please press the continue button below to reset your password or contact your system administrator for further assistance.';
