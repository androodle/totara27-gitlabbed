<?php

/**
 * Androgogic QR code: Install
 *
 * @author      Daniel Morphett <dan@androgogic.com>
 * @version     14/10/2014
 * @copyright   2014+ Androgogic Pty Ltd <http://www.androgogic.com>
 *
 *
 * */
function xmldb_profilefield_file_install() {
    global $CFG, $DB;
    $result = true;

    $sql = "SELECT * FROM mdl_user_info_field WHERE shortname = 'QRcode'";
    $existing = $DB->get_record_sql($sql);
    if (!$existing) {
        $user_info_field = new stdClass();
        $user_info_field->shortname = 'QRcode';
        $user_info_field->name = 'QR code';
        $user_info_field->datatype = 'file';
        $user_info_field->descriptionformat = '';
        $user_info_field->categoryid = 1;
        $user_info_field->sortorder = 1;
        $user_info_field->required = 1;
        $user_info_field->locked = 0;
        $user_info_field->visible = 0;
        $user_info_field->forceunique = 2;
        $user_info_field->signup = 0;
        $user_info_field->defaultdata = '';
        $user_info_field->defaultdataformat = 0;
        $user_info_field->param1 = 1;
        $user_info_field->param2 = 234881024;
        
        $DB->insert_record('user_info_field',$user_info_field);
    }
    
    return $result;
}


    