<?php
require_once('../../../../config.php');
$userid = required_param('userid', PARAM_INT);
$path = required_param('path', PARAM_PATH);
//echo 'QR code';
$user = $DB->get_record('user',array('id'=>$userid));
$sql = "select pa.*,p.fullname as position,o.fullname as organisation from mdl_pos_assignment pa
                inner join mdl_pos p on pa.positionid = p.id
                inner join mdl_org o on pa.organisationid = o.id
                where userid = $userid";
$user_pos = $DB->get_record_sql($sql);

$PAGE->set_pagelayout('popup');

$class='';
$size='200px';
$filename = 'qrcode-'. $userid . '.png';
$url = $CFG->wwwroot . '/pluginfile.php'.$path;
$attributes = array('src'=>$url, 'alt'=>$filename, 'title'=>$filename, 'class'=>$class, 'width'=>$size, 'height'=>$size);

$img = html_writer::empty_tag('img', $attributes);
$table = new html_table();
$row = new html_table_row();
$cell = new html_table_cell();
$cell->text = $img;
$cell->colspan = 2;
$row->cells[] = $cell;
$table->data[] = $row;
$row = new html_table_row();
$row->cells[] = get_string('firstname');
$row->cells[] = $user->firstname;
$table->data[] = $row;
$row = new html_table_row();
$row->cells[] = get_string('lastname');
$row->cells[] = $user->lastname;
$table->data[] = $row;

$row = new html_table_row();
$row->cells[] = get_string('organisation', 'totara_hierarchy');
$row->cells[] = $user_pos->organisation;
$table->data[] = $row;
$row = new html_table_row();
$row->cells[] = get_string('position', 'totara_hierarchy');
$row->cells[] = $user_pos->position;
$table->data[] = $row;

echo $OUTPUT->header();
echo html_writer::table($table);

