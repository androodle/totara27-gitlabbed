<?php
/*

Totara LMS Changelog

Release 2.7.7 (22nd September 2015):
==================================================


Improvements:

    TL-6484        Totara Connect Server

                   Totara Connect makes it possible to connect one or more Totara LMS or
                   Totara Social installations to a master Totara LMS installation.
                   This connection allows for users, and audiences to be synchronised from the
                   master to all connected client sites.
                   Once synchronised users can move between the connected sites with ease
                   thanks to the single sign on system accompanying Totara Connect.

    TL-6599        Changes to program assignment dates now override previous exceptions

                   When the completion date of an assignment in a program or certification is
                   changed, any previous exceptions that the related users had will be
                   removed, the specified date will be applied, and exceptions will be
                   recalculated. As a result, exceptions that were previously resolved using
                   "Dismiss and take no action" might reoccur, but this change is providing a
                   means to re-assign those users which was previously not possible (unless
                   the user was completely removed). This patch also enforces the rule that
                   due dates can only be increased (even if an earlier assignment date is set)
                   - previously it was unintentionally possible to decrease them under certain
                   circumstances.

    TL-6634        Added a new capability for managing user profile fields

                   Added totara/core:manageprofilefields capability to allow managing of user
                   profile fields. By default it is not enabled for anyone

    TL-6939        Added a warning that column aggregation options may not be compatible with reports that use aggregation internally
    TL-6965        Reduced the number of DB queries used when triggering events on update of Face-to-face signups
    TL-7151        Added additional settings to the Custom Totara Responsive theme

                   Custom Totara responsive now has the ability to change the text colour,
                   background colour, background image, background image location, and add a
                   footnote.

    TL-7269        Added help text to timezones and times in Face-to-face sessions
    TL-7272        Improved the layout of docked blocks
    TL-7378        Improved the behaviour of the audience-based visibility section of the edit course form


Bug fixes:

    TL-4527        Corrected PHP syntax error when using Hierarchy bulk actions.
    TL-5822        Added a warning to pre-install environment checks if the max_input_vars setting is too low.
    TL-6195        Fixed duplicate messages being sent to managers by Face-to-face when the user has an invalid email address
    TL-6265        Fixed navigation by month in the Face-to-face calendar block
    TL-6632        Fixed the generation of unique tokens within core libraries

                   There were several cases of uniqid being used to generate unique
                   identifiers or tokens.
                   These calls have now been improved to use a method that ensures a truly
                   unique identifier or token is generated.

    TL-6659        Refactored program assignment code

                   Refactored program assignment code to make it more efficient and easier to
                   maintain. It will also prevent sql problems, which could occur on some
                   systems with some configurations, when assigning large numbers of users to
                   programs and certifications (such as using an audience). Performance for
                   adding and removing users has been improved by about a factor of two, while
                   performance when reprocessing existing user assignments (happens during
                   nightly cron) has been significantly improved (from 3 database queries per
                   user assignment down to zero). This should greatly reduce problems
                   experienced with long nightly cron jobs on large sites.

    TL-6804        Fixed competencies in a learning plan showing linked courses even when the course was hidden
    TL-6940        Fixed permissions handling when using the multiple hierarchy dialog

                   The multi hierarchy dialog extends the standard hierarchy dialog but failed
                   to pass through the fourth parameter. This caused the permissions to be
                   incorrectly checked resulting in a false permissions error.

    TL-6970        Fixed hierarchy page not loading due to MySQL join limit

                   MySQL has a limit of 61 tables in a join. When viewing a hierarchy
                   framework, when 60 or more custom fields were defined (across one or more
                   types), the page was failing to load. The query has been changed to prevent
                   this problem.

    TL-6980        Fixed the "Show only active enrolments" option in the Grader Report
    TL-7023        The "Upcoming Certifications" block will now be hidden when "Enable Certifications" is set to Hide or Disable.
    TL-7035        Fixed inconsistent date fields in Excel exports from the Record of Learning - Certifications report source
    TL-7039        Prevented Face-to-face from sending booking confirmations for past sessions

                   When turning off "Approval required" for a Face-to-face activity a booking
                   notification was being sent for sessions in the past. This is now
                   prevented.

    TL-7045        Enable content restriction options for the Face-to-face interest report source
    TL-7074        Fixed the context for capability checks for the display of the button to create new courses, programs and certifications.

                   Users who had been assigned a role with permissions to create programs,
                   certifications or courses within specific categories would not have the
                   relevant "Create" button within the enhanced catalog. Now if they have
                   permissions to create a program, certification or course within any
                   category, this button will appear.

    TL-7114        Show hidden courses, programs and certifications to enrolled users in the Record of Learning

                   Several problems were fixed relating to course, program and
                   certification visibility, in relation to the normal and audience based
                   visibility settings. In some situations, the normal visibility setting was
                   being used when audience visibility was enabled. As a consequence, hidden
                   assigned courses, programs and certifications will now be visible in the
                   Record of Learning, restoring the behaviour from Totara 2.6. As before this
                   patch, hidden assigned courses will not be accessible, but hidden assigned
                   programs and certifications will be.

    TL-7121        Fixed Programs that are potentially stuck as unavailable

                   In 2.6.10, we removed the "availability" checkbox, so that availability is
                   now controlled via the available from/until date fields. This upgrade
                   catches any programs left as unavailable without availability dates. Any
                   issues found will be output to the screen during the upgrade and saved to
                   the upgrade_logs.

    TL-7164        Fixed pagination on the Record of Learning course, program and certification history pages
    TL-7166        Added course reminders to the course backup and restore functionality
    TL-7186        Fixed the translation of generic error messages within totara dialogs
    TL-7191        Fixed a missing sesskey in ajax requests when creating a filter in report builder reports

                   The sesskey and relevant checks were missing in ajax requests involved in
                   adding some audience filters to the report builder.  These have now been
                   put in place.

    TL-7206        Fixed the default end date for learning plans not defaulting to the end date of the associated learning plan template
    TL-7215        Fixed reportbuilder filters for "Menu of choices" custom fields with values containing a comma
    TL-7220        Fixed the Foreign key checks for Totara Dashboards in the XMLDB editor
    TL-7222        Removed the dashboards link from the navigation node when a user has no dashboards assigned
    TL-7224        Fixed the display of Certificates where the "Print Date" depends on a deleted activity
    TL-7234        Fixed the caching of custom Totara Menu urls with course id parameter
    TL-7235        Fixed an error on repository settings pages for hidden but enabled repositories
    TL-7248        Reverted change causing an inability to see uploaded images in Internet Explorer
    TL-7263        Fixed the restoration of course backups containing invalid audience visibility settings

                   If you backup a course with audience visibility it includes the ids of all
                   selected audiences, previously if you attempted to restore this backup
                   without matching audiences it would fail, now it logs a warning and
                   continues restoring the course. It is important to note if you are moving
                   backups between sites the audience ids might not match the expected
                   audiences.

    TL-7265        Improved the layout of tabs when viewing a SCORM
    TL-7275        Fixed case sensitivity for the search within Hierarchy bulk actions.
    TL-7281        Fixed Face-to-face signup process when approval is required for a session with no date

                   This issue occurred when a user signed up to a Face-to-face session that
                   required approval but did not yet have a date. When the manager approved
                   the signup request they were incorrectly booked into the session instead of
                   waitlisted.

    TL-7283        Fixed the field mapping for Organisation and Position imports using a database source
    TL-7292        Fixed a display issue with the file manager when loaded in an iframe

                   When loading the file manager within an atto editor instance and attempting
                   to upload a new file, the display was inconsistent with other file editors.
                   This patch fixes that issue

    TL-7295        Remove unused function rb_display_certification_duedate from base source
    TL-7296        Fixed the minimum Totara 2.2 version in the UPGRADE.txt file
    TL-7303        Fixed hours_minutes display function in the report builder
    TL-7319        Fixed the display of custom fields in the report builder when using a non-English language
    TL-7323        Added checks for https:// links in the learning plans evidence link functionality
    TL-7328        Fixed checks for the course custom fields create, update, and delete capabilities
    TL-7333        Reset cache for current session if required and do not show a menu item if it is disabled through an "Advanced features" setting
    TL-7351        Fixed icon display when managing courses and categories
    TL-7360        Consistently prevent suspended and deleted users from getting any emails
    TL-7362        Updated INSTALL.txt to reflect support for IE8


Contributions:

    * Andrew Hancox at Synergy Learning - TL-6195
    * Carlos Jurado at Kineo UK - TL-6265
    * Eugene Venter at Catalyst - TL-7166
    * Pavel Tsakalidis at Kineo UK - TL-7164


Release 2.7.6 (18th August 2015):
==================================================


Security issues:

    TL-7157        Added a new workaround for password autocompletion issues in some modern browsers

                   This fix works around an issue whereby some modern browsers would
                   automatically fill any password field in any form with a users stored
                   password for the site.
                   This would only occur after the user had made the decision to save Totara
                   authentication credentials within the browser.
                   Previously implemented directives telling the browser to not automatically
                   fill in the field are now ignored.


Improvements:

    TL-3202        HR Import now correctly enforces required user fields

                   It was previously possible to not include some required user fields when
                   using HR Import.
                   First and last name columns are now automatically required if user creation
                   is allowed, and the email field is automatically required if duplicate
                   emails are not allowed and user creation is allowed.
                   It is possible to exclude the first name, last name and email columns if
                   only the user update and/or delete options are enabled.

    TL-5955        Added new events for the creation and deletion of RPL course and activity completions
    TL-6436        Added an option to reset notifications to default for Face-to-face notifications

                   Sites that have been upgraded through Totara 2.4 may have Face-to-face
                   activities that do not have the complete series of notifications a newly
                   created Face-to-face activity would have.
                   This improvement introduces a means of resetting the default notifications
                   allowing those sites that have Face-to-face activities with missing
                   notifications to reset the default notifications if they wish in order to
                   get them back.

    TL-6807        Added a date format setting for HR Import of users when using an external database source
    TL-6817        Added new capabilities to allow the delegation of language settings control

                   Two new capabilities have been added to allow delegation of control of
                   language settings and language packs.
                   Previously this was controlled by the site config capability.
                   By default only site administrators have these new capabilities.
                   The two new capabilities are:

                   * totara/core:langconfig - Allow access to edit language settings.
                   * tool/langimport:managelanguages - Allow installing and uninstalling of
                   language packs.

    TL-6929        Improved Behat tests that use Totara navigation
    TL-6994        Improved the performance of scheduled reports if the export to file system option is enabled

                   Previously scheduled reports with the export to file system option turned
                   on would be generated twice, once for the recipient of the report and once
                   for the file system.
                   The code now only generates the report once and uses it for both the
                   recipient and the file system.

    TL-7109        Changed audience strings to reflect what start and end dates actually do
    TL-7130        Improved the redirect on saving changes when editing Totara menu items

                   The current behaviour is that you will get redirected back to the index
                   page when you save any settings. This means that you will have to go back
                   to the edit page and then navigate to the "Access" tab in order to set
                   custom access rules after having set the visibility to "Use custom access
                   rules".
                   The new behaviour is to redirect to the "Access" tab when the visibility
                   setting is changed to "Use custom access rules".



Bug fixes:

    TL-5709        Prevent exceptions and changes to due date for completed programs

                   Once a user has completed a program, changes cannot be made to their due
                   date using the Assignments tab. A consequence is that new exceptions cannot
                   occur once a user has completed the program. Similarly for certifications,
                   time due changes will not be possible after the user first certifies (after
                   this point, either they are certified, in which case they are due on their
                   expiry date, or their certification has expired, in which case they are
                   overdue).

    TL-3550        Fixed the incorrect removal of completed Feedback 360 requests if JavaScript is disabled

                   This issue was caused by behaviour inconsistency between JS being enabled
                   and disabled.
                   If JS was enabled when editing a Feedback 360 with completed requests and
                   the user clicked add users the page would be lost and the completed
                   requests would be incorrectly removed.
                   The basic interface now behaves in the same way as the enhanced interface.

    TL-6455        Fixed the formatting of report names used within in the Graphical Reports block

                   Report names that contain special characters are now displayed correctly
                   within the Graphical Reports block.

    TL-6487        Fixed the display of competencies achieved through course completion on the My Team page

                   Previously the number of competencies achieved for each team member was
                   been incorrectly displayed on the My Team page if one or more competencies
                   had been achieved through course completion due to stats not being
                   correctly recorded.
                   This has been fixed and an upgrade step ensures all stats are correct.


    TL-6512        Fixed course reminder escalation messages being sent for all historical course completions
    TL-6575        Fixed report builder column order when exporting to Excel in RTL languages
    TL-6645        Changed Certification Completion report source columns to use certification status rather than program status

                   Several fields in the certification completion report source were changed
                   so that their information is determined by the certification status value
                   rather than the program status. The Status column now shows the same data
                   as the Record of Learning: Certifications Status column. Is Complete was
                   changed to Is Certified and Is Not Complete to Is Not Certified. Those two,
                   as well as Is In Progress and Is Not Started were changed to reflect the
                   correct state of the certification. Columns and filters in existing reports
                   have been converted, but customised column headers need to be updated
                   manually to reflect the change. Users should check any saved searches which
                   use these filters, as they may no longer show the information that is
                   expected.

    TL-6708        Fixed course custom field data being saved after events

                   Course custom field data is now being saved before the course_created and
                   course_updated events, allowing observers of those events to access custom
                   field data.

    TL-6767        Fixed HR Import producing a fatal error if more than 65336 import errors were encountered
    TL-6811        Fixed an undefined variable notice when editing a course
    TL-6908        Fixed the display of stage titles when exporting an Appraisal

                   When an Appraisal had a long stage title and either a short description or
                   no description at all the title would sometimes be cut off in the PDF that
                   was produced by the export.
                   This was affecting the interface and PDF snapshots.

    TL-6955        Fixed Face-to-face Session report source Role columns and filters not working

                   These columns and filters are now selectable and use language strings.

    TL-6964        Improved validation when creating main menu items with custom access rules

                   This changes improves the validation of main menu custom access rules to
                   ensure items cannot be created within invalid rules.

    TL-6983        Fixed non-unique query parameter names in report builder filters

                   Report builder filters were previously attempting to generate unique
                   parameter names by hashing a combination of information about the filter.
                   This could lead to duplicate parameter names being generated on occasion
                   causing an error.
                   This has been fixed to use sequential param names instead ensuring a
                   parameter name is always unique.

    TL-6986        Fixed the Face-to-face Manager approval radio button disappearing when user signup note is disabled
    TL-6989        Fixed conflicting content option aliases for report builder reports
    TL-7046        Fixed client side form validation on forms without a header
    TL-7048        Fixed the sending of certification alerts so that suspended users are excluded
    TL-7083        Fixed a missing include when trying to search for a program or certification
    TL-7095        Removed all uses of the sql_fullname function from within the Site Logs report source
    TL-7099        Fixed over restrictive capability checks when uploading custom certificate images

                   User with the totara/core:modconfig capability may now upload custom
                   certificate images through the certificates module setting.
                   This makes it consistent with the other module settings.

    TL-7102        Fixed the cancel button when editing a course section summary

                   Previously the cancel button when editing a course section summary would
                   not cancel the action but instead complete it.
                   The cancel button now correctly disregards any changes the user has made.

    TL-7110        Fixed program exceptions not being triggered when creating new a assignment

                   Previously if an assignment was added to a program or certification, and a
                   completion date was set, all in one step (without saving in between) then
                   exceptions for those assignments were not being checked.
                   The fix for this issue ensures exceptions are correctly checked and
                   triggered.

    TL-7122        Removed the incorrect commas at the end of lines for when displaying Face-to-face room details
    TL-7142        Added a new capability for the certificate modules "email teachers" setting and updated its language strings

                   Previously the setting was sending notifications to everyone with the
                   mod/certificate:manage capability, which resulted in all site managers
                   receiving the messages.
                   The setting is now called send notifications and it uses a new capability
                   mod/certificate:receivenotification which defaults to only the editing and
                   non-editing trainer roles, if you want your site managers or custom roles
                   to receive these notifications you will have to give them the capability.

    TL-7149        Fixed the display of certification status within report builder filters
    TL-7156        Fixed the display of certification renewal status in the Record of Learning report
    TL-7196        Reset Required Learning menu item cache when programs or certifications are completed

                   When the last Required Learning program or certification was completed, the
                   Required Learning menu item was not being immediately removed. This was
                   causing an error message if it was subsequently selected.



New features:

    TL-6407        Added a new colours custom setting to graphs in report builder

                   It is now possible to specify graph series colours in the custom settings
                   for a report builder report.
                   Colours can now be specified using the following syntax in the custom
                   settings input:

                       colours = #ff0000,#00ff00,#0000ff

                   While it is possible to use any colours the browser supports we strongly
                   recommend only hexadecimal colours are used.


Contributions:

    * Chris Wharton at Catalyst NZ - TL-6964
    * Haitham Gasim at Kineo - TL-6955
    * Jamie at E-learning Experts - TL-5709
    * Jo Jones at Kineo - TL-6767
    * Joby Harding at Mindclick and Russell England at Vision by Deloitte - TL-6708


Release 2.7.5.1 (29th July 2015):
==================================================


Bug fixes:

    TL-6763        Fixed the display of the main menu when an item with children was not visible to the user

                   A problem was identified with the Totara main menu when the current user
                   could not see a top level menu item that had sub menu items that were still
                   visible.
                   Visibility was not being inherited by sub menu items correctly and this
                   resulted in a coding error.
                   This was fixed by ensuring that visibility gets inherited by sub menu
                   items.

    TL-7044        Fixed rules for dynamic audiences based on a text input user profile field having multiple values

                   A bug was discovered with dynamic audiences which had been configured with
                   one or more rules on custom user profile fields with a series of comma
                   separated values.
                   When configured in this way users may be incorrectly added and/or removed
                   from the audience.
                   This could lead to users having access to things that they should not
                   otherwise have access to or for users to lose state and data if they were
                   incorrectly removed.

                   The fix for this includes a large number of new automated tests to ensure
                   that this does not happen again.

    TL-7061        Fixed incorrect triggering of the report_created event

                   The report_created event was being incorrectly triggered when embedded
                   reports were being created.
                   This would occur the first time an embedded report was used.
                   The report_created event is now only ever triggered when the user creates a
                   new custom report.


Release 2.7.5 (21st July 2015):
==================================================


Security issues:

    TL-5289        Missing database record errors no longer contain the database table name
    TL-6469        Fixed missing session key error when setting up scheduled reports

                   This occurred if a user search resulted in more than one page of results
                   and one of the page links was clicked. Session key checking was also added
                   to the audience dialog on this page.

    TL-6823        Improved access control handling in Appraisal and Feedback360 assignments

                   Two scripts in Appraisal and two scripts in Feedback360 were identified as
                   having insufficient access control checks.
                   This has now being remedied and all required access control checks are now
                   being made in the four identified scripts.

    TL-6927        Fixed incorrect synchronisation of suspended users in course meta enrolments
    TL-6930        Fixed incorrect protocol handling in the curl library

                   Prior to this patch use of CURLOPT_PROTOCOLS and CURLOPT_REDIR_PROTOCOLS
                   were limited by the existence of the CURLOPT_PROTOCOLS define.
                   This restriction has been removed as it was no longer necessary.

    TL-7032        Improved the generation of random strings within core

                   It was brought to our attention that in some situations the random string
                   generation used during processes such as resetting of user passwords could
                   be predicted and possible exploits crafted.
                   Prior to this patch random string generation used the PHP built in mt_rand
                   function.
                   After this change we use a variety of methods and fall back to our own
                   unpredictable generation.


Bug fixes:

    TL-5562        Fixed a potential problem when inserting multiple records in a batch

                   This fixes a potential problem when importing a broken CSV file into course
                   completion and a potential minor problem when upgrading multiple custom
                   menu fields in Facetoface module.

    TL-6338        Fixed behaviour of "send all to waitlist" Facetoface setting when manager approval is required

                   Previously when manger approval was required and the send all to waitlist
                   setting was enabled, when a user request was approved they were then
                   booked. This fixes the behaviour so the user is correctly put onto the
                   waitlist when their request is approved.

    TL-6347        Fixed "Dropdown menu" profile custom fields always saving the first option
    TL-6378        Fixed report builder display of columns showing 0, 0% or No when the data is empty

                   If a database column contains no value and a Reportbuilder report is using
                   a number, time, grade, percentage or yes/no display function then the cell
                   in the report will now show "-" or will be empty, rather than showing 0, 0%
                   or No. If a custom report expected to display 0, 0% or No, then it should
                   be changed to return a value of 0 when the data contains null or an empty
                   string, e.g. "CASE WHEN val IS NULL OR val = '' THEN 0 ELSE val END AS
                   val".

    TL-6513        Fixed issue causing Certification expiry periods to double

                   If the Certification Completion Upload tool was used uploading completion
                   records for users who were already assigned to the Certification, an issue
                   could arise where the life of the certification would be incorrectly
                   doubled.

    TL-6527        Fixed events not being called when audiences were unenrolled from courses

                   Some problems relating to Facetoface events were also fixed,
                   including users not being removed from future sessions when they were
                   removed in bulk from courses, and ensuring that users are only removed once
                   their last enrolment was removed.

                   This patch also includes changes to unenrol_user_bulk to prevent sql errors
                   caused by unassigning huge numbers of users at once, and adds tests to ensure
                   that individual and bulk unassigning is working correctly.

    TL-6709        Fixed wrapping of long question titles in Appraisal PDF exports
    TL-6745        Fixed an access control bug preventing a manager's manager from reviewing a learner's goals in appraisals

                   The permissions checks to determine who can view goals didn't allow a
                   manager's manager to view a learner's goals and incorrectly displayed a
                   permissions error when they tried to do so.

    TL-6774        Fixed the display of buttons on the manage courses and category page

                   If a user didn't have the correct capabilities there would be 3 buttons
                   displayed with the text "Add new category" that didn't function correctly
                   due to a permissions issue. These buttons now only show when a user has the
                   correct permissions and function as expected.

    TL-6776        Fixed fatal error when viewing competency records within a learning plan
    TL-6784        Fixed the display of unassigned programs on the record of learning: programs report

                   The record of learning was not displaying programs assigned via learning
                   plans, or completed programs that the user was unassigned from.

    TL-6786        Fixed empty usernames bug in reports for users uploaded with empty name fields
    TL-6797        Fixed the access denied error message for appraisals
    TL-6799        Fixed course creator role capabilities for managing audiences
    TL-6802        Fixed a fatal error with learning plan enrolments when a course is included in multiple plans
    TL-6808        Fixed missing calendar icon when adding a set completion date to an audiences enrolled learning
    TL-6816        Fixed fatal error on cron task when calling function dp_plan_item_updated
    TL-6818        Fixed handling of Facetoface completion records when changing attendance for a user
    TL-6819        Changes in memcached connection settings are now applied immediately

                   Prior to this patch changes to memcached cache store settings were not
                   applied immediately.
                   These settings are now applied immediately after changing memcached cache
                   store settings.
                   Please note you still need to restart memcached server manually if the data
                   storage format changes.

    TL-6833        Fixed a regression where the definition of user profile fields could not be edited

                   Code changes associated with TL-6600 resulted in a regression being
                   introduced that prevented site administrators from being able to edit the
                   definition of a custom user profile field.

    TL-6940        Fixed permission handling when using multiple hierarchy dialog

                   The multi hierarchy dialog extends the standard hierarchy dialog but fails
                   to pass through the fourth parameter. This causes the permissions to be
                   incorrectly checked resulting in a false permissions error.

    TL-6960        Fixed alignment of row headings in course completion report
    TL-6976        Fixed issue where trainers were unable to annotate PDF's submited as part of an assignment
    TL-6979        Fixed Facetoface archive when certification window period equals active period

                   If a facetoface belonged to course which belonged to a certification, and
                   the certification window open period was the same as the active period,
                   then when the course was reset to allow recertification, the facetoface
                   activity was automatically re-triggering completion and recertification.

    TL-6997        Fix prog_get_all_programs incorrectly applying visibility

                   On sites which had switched from normal visibility settings to using
                   audience-based visibility, if a program had previously been set to
                   "hidden", progress was not being updated when users completed courses.

    TL-7028        Fixed handling of incorrectly defined embedded reports

                   This patch fixed a fatal error that would be experienced on the
                   Reportbuilder manage reports screen if the site contained an incorrectly
                   defined embedded report.
                   This is a regression from performance improvements made in the last minor
                   release.


Improvements:

    TL-5736        Course and certification completion import reports can now filter errors

                   A new 'errors' filter has been added to course and certification completion
                   import reports

    TL-6333        Improved robustness of completion and conditional activities in the SCORM module

                   Under cases of heavy learner load, or a misconfigured server, causing
                   errors and communication timeouts the SCORM instant completion could be
                   fragile, which could cause knock-on problems with the opening of any
                   subsequent conditional activities . These changes minimise the consequences
                   of any communication errors within the SCORM process.

    TL-6573        Improved support for RTL languages in reportbuilder graphs
    TL-6820        Improve performance when approving audience ruleset changes
    TL-6829        Added an option to the SCORM activity to ignore mastery score when saving state

                   Prior to this patch when a SCORM package provided a mastery score, and
                   LMSFinish was called, and if a raw score had been determined then the
                   status was being recalculated using the raw score and the mastery score.
                   Any status provided by the SCORM (including "incomplete") was being
                   overridden.
                   Turning this option off (it is on by default, to maintain previous
                   behaviour) will prevent this override.
                   This is only applicable to SCORM 1.2 packages.

    TL-6932        Added a link to the manage extension page in the program extension request emails
    TL-6933        Fixed a regression that prevented managers from approving Facetoface requests without enrolling into the course


Contributions:

    * Sergey Vidusov of Androgogic - TL-6820
    * Russell England at Vision By Deloitte - TL-6932


Release 2.7.4.1 (24th June 2015):
==================================================

Bug fixes:

    TL-6706        Fixed "Set fixed completion" option in Enrolled Learning on an Audience

                   When a program or certification is assigned to an Audience in the Audience
                   Enrolled Learning Tab, and the admin attempts to set a completion time, the
                   "Set fixed completion date" option did not work.

    TL-6814        Fixed unwanted creation of certification completion records caused by a regression in TL-6581.

                   Patch TL-6581 caused certification completion records to be created for
                   user who had been unassigned from certifications. This patch removes those
                   new records and ensures that certification completion records are only
                   created for users who are currently assigned.


Release 2.7.4 (23rd June 2015):
==================================================

Security issues:

    TL-6566        Improved XSS prevention checks when serving untrusted files in IE
    TL-6576        Ensured Audience description is sanitised before display

                   Thanks to Hugh Davenport at Catalyst NZ for reporting and providing a fix
                   for this issue.

    TL-6613        Improved validation of local URLs
    TL-6614        Added a warning when a site is not using HTTPS and secure cookies.
    TL-6617        Added username enumeration warnings to the Security Overview report if self-registration is active or protectusernames is disabled.


Improvements:

    TL-5130        Added suspended user rule to dynamic Audiences

                   It is now possible to include or exclude users from a dynamic audience
                   based on whether or not they are suspended

    TL-6133        Improved performance of the main menu resulting in fewer database queries and file includes on each page view
    TL-6255        Added setting to allow users with inactive enrolments to be shown on course completion reports

                   Normally the course completion and activity completion reports within a
                   course do not show completion information for learners who do not have
                   existing active enrolments, but who may have completed activities in the
                   past when enrolled. Disabling this option on both reports will display all
                   completion data in these reports including for those learners with
                   suspended, expired or removed enrolments.

    TL-6303        Improved PDF export of Appraisals when question content results in a page break.
    TL-6329        Added "Use fixed expiry date" recertification option in Certifications

                   This adds a third option for how the expiry dates on certifications are
                   calculated. Details are provided in the help popups in the 'Certification'
                   tab when editing a certification. This patch also slightly changes the
                   behaviour of 'Use certification expiry date' - if a user's assignment (on
                   the 'Assignments' tab) has a completion due date then this date will be
                   used to calculate the expiry date the first time that the user certifies,
                   rather than just using the date that the user completed the certification.
                   The certification import tool has also been updated to support these
                   changes.

    TL-6358        Added config option to control the display of Hierarchy framework, type and item shortcodes

                   Previously whether Hierarchy shortcodes were displayed was defined in code.
                   This patch adds a new config setting under Advanced Features. If you had
                   previously made a customisation to the code (by setting constant
                   HIERARCHY_DISPLAY_SHORTNAMES in totara/hierarchy/lib.php to true) to enable
                   the display of Hierarchy shortcodes, you will need to re-enable the display
                   of shortcodes using the new configuation setting.

    TL-6452        Improved the performance of the course completion scheduled task
    TL-6523        Allowed users to navigate away from long-running report exports in Reportbuilder

                   Attempting to export a large report and then navigate away to any other
                   page while the export was still processing would result in an error: "Timed
                   out while waiting for session lock. Wait for your current requests to
                   finish and try again later." and then the system could then become unusable
                   for that user. Now the user can navigate away from the export safely (which
                   would cancel the export), or continue navigating the site in a different
                   browser window/tab (while waiting for the export window to complete).

    TL-6544        Changed certification Status strings in certification reports to better reflect the actual statuses

                   "Assigned" was changed to "Not certified"
                   "Completed" was changed to "Certified"
                   "Expired" and "In progress" were unchanged.

    TL-6558        Improved scalability of query in course completion

                   This was causing a database error on some platforms due to an oversized IN
                   query with large data sets.

    TL-6582        Fixed inconsistencies in site manager appearance-related capabilities

                   Previously the appearance related permission for a site manager was not
                   consistent comparing a new install and a permission reset. The
                   totara/core:appearance capabilty is now consistently used across all roles.

    TL-6604        Improved appearance of Learning Plans tables on the My Learning pages for RTL languages
    TL-6626        Added new capability controls for access to activity modules plugin settings
    TL-6639        Updated the default content options for the My Team report to include temporary assignments

                   This change will only affect future installs and My Team reports that are
                   reset to default settings, to apply this change manually you can edit the
                   My Team report and on the content tab tick the "Records for user's
                   temporary reports" option.

    TL-6650        Changed program user assignments to defer large changes to happen on the next cron run

                   Previously, when saving changes to user assignments in a Program or
                   Certification, the new users were assigned when the save button was
                   clicked. This was causing pages to time out when assigning large audiences.
                   Now, the contents of the assignment tab are saved immediately but the users
                   are not assigned to the program until the next cron run occurs. On-screen
                   notifications have been added to indicate if pending assignments are
                   waiting for a cron run.

    TL-6735        Added logging whenever activity completion is unlocked
    TL-6756        Improved information provided by webservices logging


Bug fixes:

    TL-5978        Fixed inconsistent access control checks for Learning Plans

                   The behaviour has now been standardised throughout the code. Granting the
                   totara/plan:manageanyplan capability allows users to create and edit plans for any user.
                   Granting totara/plan:accessplan allows users to see and modify their own plans,
                   and allows staff managers to create and edit the plans of their staff.

    TL-6222        Fixed courses incorrectly being visible in the Courses section of the Navigation block when using audience-based visibility
    TL-6263        Fixed reaggregation of course completion

                   Course completion records would never be reaggregated on the cron run, if
                   the "Completion begins on enrolment" course setting was turned off when
                   course completion criteria were unlocked.

    TL-6319        Fixed rules for dynamic Audiences based on a text input user profile/custom field being empty
    TL-6360        Fixed setting of cancellation custom field value when calling facetoface_user_cancel_submission.
    TL-6372        Fixed course deletion so that deleting a course now removes that course from Programs and Certifications

                   Previously if a course was deleted and it was part of a program or
                   certification, then some actions e.g. setting up recertification would
                   cause an error on cron run. This patch ensures that no new orphaned
                   references will be created and also fixes any that currently exist.

    TL-6374        Fixed Reportbuilder 'last/next X days' date filters

                   The 'Is between today and X days before/after today' filters were
                   internally using a specific date rather than a relative number, resulting
                   in saved searches not working as intended. This filter will now always be
                   relative to the date on which it is used. Existing saved searches have been
                   converted, but it is possible that some may be incorrect (although all were
                   wrong without this patch). We advise that users check that saved searches
                   which contain date filters have the intended values.

                   Note that any users that are logged in and using these filters during the
                   upgrade progress may need to log out and back in to see the correct values.

    TL-6403        Fixed error message when displaying categories that contain only hidden courses
    TL-6419        Removed Temporary manager expiry date from Learner's position page when no temporary manager is assigned
    TL-6438        Fixed parameter validation when using the create/update courses web services
    TL-6440        Fixed create/edit capability permissions for Programs and Certifications
    TL-6466        Fixed dynamic Audience rules based off date/time custom fields

                   If the date/time custom field was set to a date after 2038 the rule
                   comparison broke, we switched the cast2int function to use bigint so the
                   comparison can take place.

    TL-6473        Fixed display of Reportbuilder report graph block for reports where a default sort column is specified
    TL-6508        Fixed unenrolled courses showing in My Current Courses home page block
    TL-6515        Fixed scheduling of HR Import, Reportbuilder export and Reportbuilder caching.

                   HR Import scheduling is now using the system timezone. Scheduled reports
                   are now using timezone of the user that created them.

    TL-6516        Fixed resetting of Certification message logs when the recertification window opens

                   When the window opens it tried to delete message logs for the users manager
                   as well as the user even though the manager records were never created.

    TL-6521        Fixed dynamic Audience date-based rules for first and last login dates
    TL-6539        Fixed Program due messages being sent to users who have current exceptions
    TL-6559        Fixed the Evidence report source showing records for deleted users
    TL-6560        Totara Messaging now consistently uses the support user email as the from address when no from user is provided

                   When sending a message, we now use the support_user email if no user is
                   specified. Send functions will also now support NOREPLY_USER.

    TL-6561        Added additional validation when trying to activate Appraisals containing aggregation questions

                   Stops activation of appraisals containing aggregation questions with no
                   selected aggregations

    TL-6562        Fixed Facetoface session custom fields showing PHP Notice and Warning errors when creating a new session
    TL-6579        Fixed ability to add aggregate rating questions to Appraisals when using a non-English language pack
    TL-6581        Improved handling of and recovery from missing Certification completion records

                   Due to various causes such as page timeouts, it is possible that some
                   certification completion records are not being created. This patch ensures
                   that the records are created when users access their certifications. A
                   check has been added to the certification cron task which will find any
                   users who are missing these records and will create them.

    TL-6587        Fixed HR Import log message if a user cannot be deleted
    TL-6589        Removed invalid CSS declaration

                   There was an @charset declaration in a certifications CSS stylesheet that
                   would cause invalid CSS when theme designer mode is turned off. This has
                   been removed.

    TL-6591        Removed unused CSS declarations

                   There were some unused Mozzilla Firefox CSS declarations that were causing
                   issues with custom CSS in Custom Totara Responsive

    TL-6592        Fixed the display of the completion status for deleted users in Record of Learning reports
    TL-6596        Fixed the unassigning of Audience members from system roles when an Audience is deleted
    TL-6597        Fixed blank rows appearing in the sorting default column on Reportbuilder columns tab
    TL-6598        Fixed Facetoface fullname column always showing 'reserved' in reports
    TL-6600        Fixed error when trying to create a user profile custom field after using the browser back button
    TL-6606        Fixed sending of course Reminder messages

                   When a feedback activity is added to a course, invitation and reminder
                   messages would sometimes not be sent, depending on the "Personal messages
                   between users" message output config settings. These reminder messages have
                   now been converted to standard Totara Alerts.

    TL-6608        Fixed order of icons for RTL languages in the Tasks block
    TL-6619        Fixed the error message when trying to delete an unknown post in the Forum
    TL-6628        Fixed error when trying to close an active Appraisal with no assigned users
    TL-6631        Fixed the line wrapping and display of preformatted text in Labels
    TL-6635        Fixed the formatting of exported columns in the Record of Learning: Certifications report

                   Removes the "overdue" and "X days remaining" warnings displayed on the
                   window opens and expiration date columns for exports of reports based off
                   the Record of Learning: Certifications source.

    TL-6647        Fixed the selection of stages to print when printing Appraisals
    TL-6652        Fixed the display of the 'roles that can view' column on the edit Appraisal page
    TL-6661        Fixed alphabetic ordering of user list when using 'Allocate spaces for team' page in a  Facetoface session, when manager reservations are enabled
    TL-6680        Improved display when adding a random quiz question to a quiz when using RTL languages
    TL-6681        Fixed behaviour of Feedback activity forms when form_change_checker is disabled

                   The form change checker detects if any form elements on the page have been
                   changed since last load. If the form change checker is disabled some of the
                   Feedback activity forms were generating errors.

    TL-6694        Prevented incorrect room booking conflicts from being shown when creating a Facetoface session
    TL-6697        Fixed Facetoface custom rooms on session duplication

                   If you duplicated a Facetoface session with a custom room, the room was not
                   duplicated leaving you with 2 sessions using the same custom room. If you
                   then removed the custom room from one session it was deleted, breaking the
                   other session.

    TL-6705        Fixed incorrect risk flag on Plan Evidence capability

                   totara/plan:editownsiteevidence capability was incorrectly marked as a
                   dataloss risk, which made the Security Overview report say the
                   Authenticated User role was incorrectly defined

    TL-6711        Fixed display of course default section title when using multilang filter on a course using the Demo course format
    TL-6720        Fixed role-based visibility access checks on the frontpage
    TL-6744        Fixed error message when adding linked courses to Learning Plan competencies or objectives


Contributions:

    * Hugh Davenport at Catalyst NZ - TL-6576
    * Pavel Tsakalidis at Kineo UK - TL-6452
    * Rickard Skiold at xtractor - TL-6560
    * Russell England at Vision NV - TL-6360
    * Tom Black at Kineo UK - TL-6516


Release 2.7.3 (19th May 2015):
==================================================

Security issues:
    MoodleHQ       Security fixes from MoodleHQ http://docs.moodle.org/dev/Moodle_2.7.8_release_notes


Improvements:

    TL-2279        Added new global setting to control user deletion behavior
    TL-5311        Added Course Completion History report builder source

                   This report source contains all records from both the current course
                   completions table and the course completions history table.

    TL-6165        Refactored timezone handling functions to improve reliability of all timezone-related functionality
    TL-6197        Added option to suspend course enrolments when users lose access to a Program

                   Previously, when learners were unassigned from a Program or a Program
                   becomes unavailable, any course enrolments in courses within the program
                   would be removed. This improvement now changes the default behaviour from
                   removing enrolments created by the program enrolment plugin, to suspending
                   enrolments.

                   This also adds a configuration setting in Site Admin -> Plugins ->
                   Enrolments -> Program so you can change the behaviour back to the old
                   "unenrol learners from courses" behaviour if you wish.

    TL-6271        Improved Accessibility of scheduled reports in Reportbuilder
    TL-6278        Removed all uses of deprecated function sql_fullname in Facetoface

                   Full name format setting is now used when displaying the User's name

    TL-6295        Showed expected csv format when importing a "database" course activitiy
    TL-6304        Changed default request method in dialogs to POST
    TL-6315        Improved accessibility of admin checkbox lists
    TL-6327        Added ability to specify database server port for HR Import external database source settings
    TL-6331        Changed timezone.txt downloads to use Totara servers
    TL-6334        Renamed Program "start date" to "date assigned"

                   This more accurately reflects the actual information recorded. This patch
                   also recalculates "date assigned" values for certifications where the
                   "start date" was removed (before this patch, "start date" had no meaning
                   for certifications in the recertification phase).

    TL-6348        Removed unneeded code when viewing a Certifications overdue warning
    TL-6350        Added a help description to Badge description to explain its plain text nature
    TL-6359        Improved the performance of Reportbuilder management pages
    TL-6366        Improved  Accessibility of the page title when uninstalling a plugin
    TL-6367        Added accessible text to the hamburger responsive button
    TL-6384        Improved Accessibility of filters in Reportbuilder
    TL-6386        Added hidden label to bulk user actions dropdown
    TL-6387        Added text to the label for the badge search functionality
    TL-6389        Added text to hidden label when editing a course topic
    TL-6391        Improved Accessibility of custom course icons
    TL-6397        Added text to the page title for the Facetoface interest report for Accessability
    TL-6398        Added title to browser sessions page
    TL-6411        Improved display of security information on calendar exports
    TL-6424        Changed Reportbuilder scheduled task default settings so that scheduled reports are sent when scheduled rather than at most once per day

                   Currently when a new Totara site is installed (or upgrade to 2.7) the
                   default schedule for scheduled reports is once a day. This means that any
                   reports scheduled to be sent more frequently do not get sent.

                   This change means that system will check for pending scheduled reports on
                   every cron run so reports will get sent out on schedule.

    TL-6434        Improved performance when loading Program message managers
    TL-6489        Updated the default schedules for Program scheduled tasks

                   This change will update the schedules for all sites currently using the
                   defaults. Site administrators can customise the timing of scheduled tasks
                   on the "Site Admin > server > scheduled tasks" page, any customisations
                   will be unaffected.

API changes:

    TL-6442        Fixed query parameter name conflicts by improving parameter name generation

                   This fix introduced a new method moodle_database::get_unique_param that
                   returns a truly unique param name with very little overhead.
                   The bug fix involves conversion of areas generating their own "unique"
                   param names to this new method.
                   All new code requiring unique generated params should use this method.

Bug fixes:

    TL-5953        Fixed SCORM resizing and title display when using popup "New window" setting
    TL-5977        Fixed upgrade for Facetoface notifications when upgrading from 2.2
    TL-6101        Fixed display of enrolment button for Facetoface session enrolment for users with no manager
    TL-6143        Fixed password import being ignored when undeleting users in HR Import

                   Previously, when undeleting a user, the user's password would always be
                   reset, regardless of whether or not the password column was enabled and a
                   password was specified. Now, password reset only occurs if there is no
                   password specified in the import file.

    TL-6180        Fixed capability checks for category Audiences
    TL-6191        Fixed permissions when adding visible audiences to a program or course

                   Permissions are now being checked on the correct context level so users
                   assigned at the category, program or course contexts with permissions are
                   now able to perform actions. This applies to Audience visibility for
                   courses, programs and certifications and also Audience enrolment for
                   courses.

    TL-6236        Fixed preservation of formatting in HTML emails sent by Appraisals
    TL-6259        Fixed completion import records being processed in the wrong date order

                   This caused a problem if there were multiple completion records for one
                   user in one course being uploaded and the date format used did not sort the
                   same chronologically and alphabetically.

    TL-6279        Removed all uses of deprecated function sql_fullname in Appraisals
    TL-6284        Removed all uses of deprecated sql_fullname() function in Hierarchies
    TL-6285        Removed all uses of deprecated sql_fullname() function in Learning Plans
    TL-6287        Removed all uses of deprecated sql_fullname() function in Reportbuilder
    TL-6305        Fixed Program/Certification alerts and messages to exclude suspended and deleted users
    TL-6321        Removed window.status Javascript changes that have been deprecated by modern browsers
    TL-6322        Fixed unassociated label when viewing role definitions to improve Accessibility
    TL-6326        Fixed inconsistent behaviour of course visibility icons
    TL-6345        Fixed setting of a Certification completion status to 'expired' when renewal expires

                   Previously, these certifications were set back to status 'assigned'. This
                   patch makes no change to the behaviour of certifications, it just ensures
                   that the correct data is recorded in the database.

    TL-6349        Fixed backup and restore of course Audience Visibility settings
    TL-6351        Fixed display of Graphical Reports Block when the report name contains an ampersand
    TL-6354        Fixed incorrect inclusion of deleted users when using recurring Programs
    TL-6361        Fixed immediate synchonrisation of Audience enrolments after modifications in Enrolled learning tab or when editing a course.
    TL-6365        Fixed page title when editing another users profile to improve Accessibility
    TL-6373        Fixed Facetoface notification status incorrectly sending manager copy when notification is disabled

                   If a notification is disabled, the manager and third party email addresses
                   will no longer receive the notification, regardless of the "Manager copy"
                   setting.

    TL-6376        Fixed invalid HTML when viewing a complete Program with an end note
    TL-6399        Fixed Javascript error when adding and removing attendees from a Facetoface session
    TL-6400        Fixed editing of Hierarchy items description field
    TL-6405        Fixed aggregation for Badges issued report source
    TL-6408        Fixed the "time signed up" column on the Facetoface session attendees tab

                   The time signed up column now shows the latest time signed up instead of
                   the first, so if users cancel and signs up again the column will update.

    TL-6409        Fixed progress bar for Programs in Record of Learning
    TL-6418        Fixed deletion of related scheduling and saved search data in Reportbuilder when a report is deleted
    TL-6425        Fixed scheduled runs of HR Import

                   HR Import was running every cron run, now it is running according to the
                   given schedule.

    TL-6437        Fixed usage of complex passwords in HR Import
    TL-6439        Fixed error message when trying to access the course progress page from Record Of Learning after user is unenrolled from course

                   Previously, if a user was unenrolled from a course, the course progress
                   page became inaccessible. Now that unenrolled courses with progress are
                   shown in the Record of Learning, it makes sense to allow users to see what
                   progress they previously made.

    TL-6445        Fixed changes to Facetoface session attendees after a waitlisted session has started
    TL-6449        Fixed schema errors on upgrade from Moodle 2.7.7
    TL-6450        Fixed export of parameteric reports in Reportbuilder

                   Fixed error that blocked export of reports that require specific parameters
                   to work (like appraisal or audience members).

    TL-6457        Fixed checkbox selection/deselection when Program exception "Select issue type" is changed
    TL-6471        Fixed the course enrolment date after unlocking completion criteria
    TL-6472        Fixed Completion History Import if it is using 'Alternatively upload csv files via a directory'
    TL-6490        Fixed activity completion when using manual grading on a Facetoface activity
    TL-6510        Fixed the rule for dynamic Audiences based on a positions multi or menu type custom field values
    TL-6518        Fixed display of the "Evidence Type" column on the Record of Learning
    TL-6520        Fixed the context checks for program deletion capabilities

                   Program deletion was only working if you had the capability at a site
                   level, this fixes it for if you have the correct capabilities at category
                   or program level.

    TL-6543        Fixed query using IN in course completion

                   This was causing a database error due to an oversized query in some
                   databases with large data sets.


Contributions:

    * Andrew Hancox at Synergy - TL-6445
    * Eugene Venter at Catalyst - TL-6345, TL-6348
    * Gavin Nelson at Engage in Learning - TL-6472
    * Jo Jones at Kineo UK - TL-5953, TL-6437
    * Russell England - TL-6520
    * Ted van den Brink at Brightalley - TL-6376


Release 2.7.2 (21st April 2015):
==================================================

Security issues:
    T-13359        Improved param type handling in Reportbuilder ajax scripts

New features:
    T-13624        Added new OpenSesame integration plugin

                   OpenSesame is a publisher of online training courses for business. This new
                   plugin allows you to sign up for OpenSesame Plus subscription and access
                   the large catalogue directly from your Totara site. To start the
                   registration go to "Site administration / Courses / OpenSesame / Sign up
                   for access". After the content packages are downloaded to your site you can
                   either select them when adding SCORM activities or use the Create courses
                   link.

                   Additional information will be made available in the coming days, if you have any
                   queries please contact your partner channel manager.

Improvements:
    T-14222        Improved accessibility of messages tab in Programs
    T-13925        Improved sql case sensitive string comparisons for completion import records.

                   When 'Force' is enabled the system keeps track of the course shortnames
                   being used, each new course is compared in a case sensitive way and if it
                   matches that but doesn't match exactly the system uses the first course
                   name instance. There will be no error messages on the import report about
                   it.

    T-10482        Allowed selection of Facetoface rooms even when date is not known

                   Contributed by Eugene Venter at Catalyst

    T-14210        Added a label when adding users to a program to improve accessibility
    T-14217        Added debug param to all Reportbuilder reports

                   This allows administrators to retrieve additional debugging information if
                   they have problems with reports. Some reports already had this feature, now
                   all of them do.

    T-14254        Added description field to the badge criteria
    T-14293        Added labels to financial year start date setting in Reportbuilder for accessibility
    T-13859        Added functionality to allow administrators to manage Facetoface session reservations

                   If a manager reserved spaces for a their team in a Facetoface session and
                   then they either left or changed role, there was no way to edit/remove the
                   reservations they made.

    T-14263        Improved the display of User full names for Programs
    T-14251        Added option to update cancellation reason fields in Facetoface

                   Before, admins were not able to update cancellation reason fields when
                   removing attendees from a Facetoface session. Now, it can be done in the
                   attendees tab.

    T-13432        Added position and organisation framework columns and filters to all relevant Reportbuilder sources
    T-10833        Added column 'ID number' when exporting Organisations and Positions from Hierarchies

                   Position and Organisation ID numbers are now available when exporting
                   Organisations/Positions via Hierarchy.

                   Also, there is a new Position report source and an Organisation report
                   source that include columns used when importing data via HR Import.

    T-14155        Changed "No time limit" to "No minimum time" in Program course sets

                   There was confusion about the purpose of this setting. Some people thought
                   that "No time limit" meant that it would allow users to spend as long as
                   they wanted to complete a program, but user time allowance is controlled in
                   the assignments tab. Setting "No minimum time" will prevent time allowance
                   exceptions (except where a relative assignment completion date would be in
                   the past, such as "5 days after first login" being set on a user who first
                   logged in one month ago).

    T-13491        Fixed immediate updating of Audience enrolments after changes in dynamic audience rules
    T-14016        Added "Choose" option as the default value for "Menu of choices" type custom fields
    T-10081        Added ability to send Scheduled reports to users, audiences, and external emails
    T-14212        Restored instant completion for course completion criteria

                   Instant course completion was introduced in Totara 2.5 but was mistakenly
                   removed in 2.6.0. This fix restores instant course completion (when the
                   criteria completion depends on other courses) which also improves the
                   performance of the completion cron tasks.

    T-13542        Changed language pack, timezone and help site links to use HTTPS

API Changes:
    T-14151        Removed unused file totara/core/js/completion.report.js.php

                   File has been unused since v1.1

    T-14242        Added finer grained control to Scheduler form element

                   Every X hours and Every X minutes were added to the scheduler form element
                   which is used for scheduled reports, Report Builder cache refresh and HR
                   Import. The frequency that these events occurs is still limited by the
                   frequency of the corresponding scheduled tasks - to take full advantage of
                   this feature, set "Import HR elements from external sources", "Generate
                   scheduled reports" and "Refresh report cache" to run frequently (e.g. every
                   minute). Any customisation using the scheduler form element will also be
                   affected by this enhancement.

    T-14215        Enforced linear date paths when upgrading Totara

                   When upgrading a Totara installation you are now required to follow a
                   linear release date path. This means that you now MUST upgrade to a version
                   of the software released at the same time or after the release date of your
                   current version.

                   For example if you are currently running 2.5.25 which was released on 18th
                   March 2015, you can upgrade to any subsequent version of 2.5, any 2.6 from
                   2.6.18 onwards, or any 2.7 from 2.7.1 onwards.

                   This ensures that sites never end up in a situation where they are missing
                   critical security fixes, and are never in a state where the database
                   structure may not match the code base.

                   The Release Notes forum is a useful source for the date order of version
                   releases https://community.totaralms.com/mod/forum/view.php?id=1834

Bug Fixes:
    T-14311        Fixed the archiving of Facetoface session signups and completion

                   When uploading historic certification completions future facetoface session
                   signups were being archived, now it will only archive signups that
                   completed before the window opens date.

    T-12583        Fixed behaviour of expand/collapse link in the standard course catalog
    T-13550        Fixed Due Date displaying as 1970 on Record of Learning: Certifications

                   This patch fixed a problem where an empty timedue data value was causing a
                   date in 1970 to show in the Due Date field in the Record of Learning:
                   Certifications report source. It also introduces a comprehensive phpunit
                   test for the certification and recertification process.

    T-13853        Fixed handling of position, organisation, and manager when a validation error occurs in email based self registration
    T-13879        Fixed grade filter when checking against RPL grades
    T-14184        Fixed appending of message body after manager prefix in Facetoface 3rd party notifications

                   If Third party email addresses were specified in the Facetoface settings then any
                   third-party copies of emails sent by the system would not contain both of the
                   manager prefix and the body of the message (the content that the learner receives)

    T-14394        Ensured Audience-based course visibility is considered when adding courses to a Learning Plan

                   This is to prevent managers from adding a course to a Learning Plan for one of
                   their direct reports, than the learner does not have visibility of and can
                   therefore never complete.

    T-14235        Fixed Audience rulesets for Course completion and Program completion
    T-14105        Fixed calculation of 'User Courses Started Count' in the 'User' report source

                   This is now calculated based on course completion records, rather than
                   block_totara_stats (which might be missing 'started' records for various
                   reasons, such as migration from Moodle or changing certain completion
                   settings while a course is active). This affects the 'Courses Started'
                   column in the 'My Team' page - courses started should now always be greater
                   than or equal to courses completed.

    T-14344        Fixed wrong ID field in signup and cancellation custom fields when upgrading to 2.7.

                   Thanks to Russell England at Vision NV from contributing to this

    T-14187        Fixed resetting of Hierarchy item types after every HR Import

                   Hierarchy item (e.g. position, organisation etc) types should only be changed
                   if the HR Import source contains the "type" column.

    T-14284        Changed Record of Learning: Courses report source to use enrolment records

                   Previously, it used role assignment records, but roles can be granted
                   without the user being enrolled, and only indicated some level of
                   capability within the course rather than participation. The whole query was
                   also rewritten to improve performance.

                   Thanks to Eugene Venter from Catalyst for this contribution

    T-14338        Fixed user fullname handling in Facetoface when editing attendees
    T-14243        Fixed handling of special characters (e.g. &) when returning selections from dialogs
    T-14366        Fixed Reportbuilder expanded sections when multiple params are used

                   Thanks to Andrew Hancox from Synergy Learning for this contribution

    T-14192        Fixed Assignment Submissions report when it is set to use 'Scale' grade

                   The Submission Grade, Max Grade, Min Grade and Grade Scale values columns
                   were displaying incorrect information.

    T-13890        Fixed date change emails being incorrectly sent when a Facetoface session capacity is changed
    T-13917        Fixed error when adding Score column to Appraisal Details report

                   The report would fail if the Score column was added to an Appraisal
                   containing a Rating (Numeric scale) question.

    T-13886        Fixed PHP Notice messages when adding a Rating (custom scale) question to Appraisals
    T-14265        Fixed display of paragraph formatting dropdown in the Atto text editor
    T-13742        Fixed uploading of Organisation and Position custom fields via HR Import

                   Also added options to the interface for custom fields, found on the source
                   configuration pages for positions and organisations just like they are for
                   users.

    T-13353        Fixed handling of param options in Reportbuilder
    T-13894        Fixed validation and handling of Facetoface notification titles
    T-13993        Fixed error when uploading users with the deleted flag set

                   When uploading users via the upload users function, if a user in the CSV
                   file had the deleted flag set, and was already marked as deleted in the
                   system, an invalid user error was generated and this halted all processing
                   of the CSV file.

    T-14327        Fixed undefined function call when sorting Dashboards

                   Thanks to Eugene Venter at Catalyst NZ for this contribution.

    T-14048        Fixed incorrect Quiz question bank names after previous upgrade

                   Question banks containing subcategories had the subcategory names
                   incorrectly changed by a change in 2.5.20 and 2.6.12. If you are upgrading
                   from a version affected (2.5.20-25, 2.6.12-18) you may want to check if
                   your Quiz question bank categories are affected.

    T-14186        Fixed max length validation for Question input text box
    T-13009        Fixed incorrect creation of multiple custom room records when saving a Facetoface session
    T-14131        Fixed handling of error when importing users via HR Import with the create action disabled

                   Thanks to Andrew Hancox at Synergy Leaning for contributing the solution.

    T-14302        Fixed database host validation in HR Import settings
    T-13846        Fixed the default role setting for the Program Enrolment plugin
    T-14083        Fixed display of 'More info' Facetoface session page

                   When a session was fully booked, learners booked on the session would
                   receive a ""This session is now full" message instead of the actual details
                   of the session.

    T-14220        Fixed page formatting for the certifications content tab
    T-14237        Fixed incorrect display of Required Learning menu item when no programs currently need to be completed
    T-14279        Fixed error when upgrading site to 2.7, with a custom theme that is not using the Totara menu

                   This affects all sites that meet the following criteria:

                   1. Upgrading to 2.7 from earlier versions.
                   1. $CFG->courseprogress has been turned on.
                   2. Using a theme that does not have the Totara menu.

    T-13845        Fixed setting position fields on email-based self authentication
    T-14075        Fixed inability to send a previously declined Learning Plan for re-approval
    T-13839        Fixed the display of Facetoface enrolment plugin feedback on the enhanced catalog
    T-14065        Removed non-functional "deleted user" filter from bulk user action pages


Release 2.7.1 (18th March 2015):
==================================================

Security issues:
    MoodleHQ       Security fixes from MoodleHQ http://docs.moodle.org/dev/Moodle_2.7.7_release_notes
    T-13996        Removed potential CSRF when setting course completion via RPL
    T-14175        Fixed file access in attachments for Record Of Learning Evidence items
                   Thanks to Emmanuel Law from Aura Infosec for reporting this issue.


API Changes:
    T-14084        Renamed $onlyrequiredlearning parameter in prog_get_all_programs and prog_get_required_programs functions
    T-14058        Converted all Learning Plan add_to_log() calls to events

                   Plan logging was migrated to new events system

    T-14104        Added courses which have completion records to the record of learning

                   Previously, if a user had been enrolled into a course, made some progress
                   or completed it, then been unenrolled from the course, the record of course
                   participation disappeared from the user's record of learning. With this
                   patch, courses will still show when a user has been unenrolled, if their
                   course status was In Progress, Complete or Complete by RPL. This change was
                   made to the Record of Learning: Courses report source, so all reports based
                   on this source will be affected.

Improvements:
    T-13950        Improved display of long text strings in the alerts block
    T-13944        Improved contents of Course Progress and Record of Learning Courses report sources

                   The Course Progress now uses Report Builder (meaning that you can sort,
                   change columns, etc) and contains the same records as Record of Learning
                   Active Courses. The Record of Learning report source has been updated to
                   include course records based on course completion data, so if a user was
                   previously enrolled in a course and had a course completion record then
                   that course will show in the Record of Learning reports.

    T-13951        Implemented sql_round dml function to fix MySQL rounding problems
    T-13867        Added "is not empty" option to text filters in Reportbuilder
    T-13939        Added course custom field value to Program and Certification overview

                   When a courseset is using "Some courses" completion logic and a course
                   custom field is being used as part of the course completion criteria the
                   custom field is now displayed along with the course so that learners are
                   aware of such completion criteria.

    T-14039        Removed HTML table from tasks block

                   Changed to better meet Accessibility guidelines

    T-12395        Improved HTML heading on calendar page

                   Changed to better meet Accessibility guidelines

    T-13938        Improved required courses message on Program overview

                   The "Some courses" option was recently added to the courseset completion
                   requirements for a program. The overview page for the program however did
                   not change to reflect accurately the possible range of different courseset
                   completion requirements.

    T-14015        Added warning messages to the scheduled tasks page if cron has not run recently
    T-14007        Improved contrast on show/hide icons in HR Import -> Manage Elements

                   Changed to better meet Accessibility guidelines

    T-14095        Improved labels for all filter inputs on the "Browse list of users" page

                   Changed to better meet Accessibility guidelines

    T-13734        Improved labels of all inputs on Reportbuilder filters

                   Changed to better meet Accessibility guidelines

Bug Fixes:
    T-14057        Fixed saving of default content for course and program custom fields
    T-12991        Fixed Facetoface manager reservations with multiple bookings

                   Before, if a learner had been assigned a reserved place they could not be
                   assigned to any other sessions; even if multiple sign-ups was on.

                   Now, it should allow managers to assign members of their staff to multiple
                   sessions if "Allow multiple sessions signup per user" setting is on, but
                   not allow more than one user assignment per Facetoface activity if that
                   setting is off.

    T-13920        Fixed export to Excel of completion progress column in Record Of Learning - Programs report source
    T-13884        Fixed error in Appraisals dialog box when selecting required learning for review
    T-14114        Fixed program and certification exceptions being regenerated after being resolved

                   This patch also prevents certification exceptions being generated when an
                   assignment date is in the past and the user is in the recertification stage
                   (at which point the assignment date is not relevant, as the due date is
                   controlled by the certification expiry date instead).

    T-14093        Fixed dynamic audiences rules based on Position and Organisation custom fields

                   Thanks to Eugene Venter at Catalyst for contributing to this

    T-13628        Fixed deletion of custom fields if missing from the HR Import CSV file
    T-13926        Fixed copying instance data when copying block instances

                   When users click the "Customise this page" button in My Learning, blocks
                   copied from the default My Learning page to the user's personal My Learning
                   page can also copy instance specific data. This allows Quick Links blocks
                   to correctly copy the default URLs.

    T-14070        Fixed MySQL 5.6 compatibility issues

                   Totara would not install on MySQL 5.6, and also unit tests were failing
                   with "Specified key was too long" errors

    T-14170        Fixed Course categories path issue due to changes in 2.7 features
    T-14120        Fixed sort in all Audience dialog boxes
    T-13622        Fixed an issue with the validation of aspirational positions
    T-14142        Fixed display of Submission Feedback Comment and Last modified date in Assignment Submissions report source
    T-13472        Fixed several problems where scheduled Appraisal messages were not sent at the right times
    T-14135        Fixed paging when adding items to a learning plan
    T-14158        Fixed the display of Facetoface sessions spanning several days on the calendar

                   Thanks to Eugene Venter at Catalyst for contributing to this

    T-14023        Fixed expansion of the Totara menu when using Standard Totara Responsive theme on small screens
    T-14098        Fixed visibility of hidden/disabled Certifications and Programs in Audience Enrolled Learning
    T-13984        Fixed editing/deleting of blocks when viewing a Choice activity module

                   Thanks to Ben Lobo at Kineo for contributing to this

    T-13962        Fixed error on site settings page after upgrading to Totara 2.7 from Moodle 2.7
    T-13964        Fixed issue with sending appraisal messages to unassigned roles
    T-14046        Fixed the redirection behaviour of custom menu management pages
    T-14021        Fixed the default title of the Mentees block
    T-14125        Fixed compatibility of iCal email attachments with some SMTP servers
    T-14074        Fixed theme precedence issues

                   Currently if you set a mobile theme in Site Administration > Appearance >
                   Themes > Theme Selector, the mobile theme will take precedence over any
                   user, course or category themes when viewing Totara on a mobile device.
                   This patch reverses this (so that User, Course and Category themes will
                   take precedence over a mobile theme).

                   If you wish to maintain the current (pre patch) behaviour add the line
                   "$CFG->themeorder = array('device', 'course', 'category', 'session',
                   'user', 'site');" to your config.php file

    T-13661        Fixed joinlist for the Assignment Submissions report source

                   When viewing the 'Assignment submissions' report source, no assignment
                   submissions were displayed unless they were either graded, or the
                   'Submission grade' column was removed from the report.

    T-14101        Fixed uppercase column names in Facetoface enrolment plugin

                   Can cause database query errors on MSSQL installations using case sensitive
                   collations


Release 2.7.0 (2nd March 2015):
==================================================

New features:
    T-10087  Added new editing and configuration interface for the main menu
    T-11033  Added new badge report source
    T-11185  Added new dynamic Appraisals advanced feature
    T-12199  Added new audience based visibility setting for courses, programs and certifications
    T-12325  Added new course completion date filter for the Record Of Learning all courses report
    T-12772  Added new aggregation and grouping column options in Reportbuilder reports
    T-12892  Added new cut off date and minimum capacity options for Facetoface sessions
    T-12900  Added new self approval feature for Facetoface sessions
    T-12902  Added new feature to allow users to register their interest in a Facetoface session they cannot sign up for
    T-13092  Added new Facetoface course enrolment plugin added to Totara
    T-13093  Added direct enrolment into Facetoface sessions from the enhanced catalog interface
    T-13114  Added feature to allow users to select a position when signing up for a Facetoface session
    T-13195  Added new autowaitlist and signup lottery in Facetoface
    T-13393  Added new setting to log user out of all sessions when their password is changed
    T-13407  Added new page to view all of a user's active sessions
    T-13468  Added new site log report source
    T-13502  Added graphical reporting to Reportbuilder
    T-13510  Added new upgrade log report source
    T-13514  Added new feature to reset main menu to defaults
    T-13693  Added completion status columns to the course completion report source
    T-13713  Added 'is completed', 'is in progress', and 'is not yet started' columns to programs and certifications report sources
    T-13714  Added new percentage aggregation option in Reportbuilder
    T-13715  Added new options to control the audience visibility of main menu items
    T-13718  Added custom fields to Facetoface sessions, signups and cancellations
    T-13721  Added ability to use user placeholders in main menu URLs
    T-13732  Added new audience based dashboards
    T-13765  Added new aggregated question types to Appraisals
    T-14081  Added MySQL configuration checks to the environment page

General improvements:
    T-9537   Added the ability to include children when filtering course categories in Reportbuilder
    T-9800   Improved the including of JS required by Reportbuilder filters
    T-11043  Improved handling when appraisee is missing roles involved in an Appraisal
    T-12309  Added link to the secure page layout in the element library
    T-12344  Improved use of forms when adding links to the Quicklinks block
    T-12388  Improved message when no results are returned for a program search
    T-12450  Added ability to control whether Facetoface session cancellation is available to learners
    T-12586  Removed totara-expanded-width class applied via JS
    T-12592  Fixed database schemas between installed sites and upgraded sites
    T-12662  Renamed Totara Sync to HR Import
    T-12712  Improved number of content types that can be created by the Test Site Generator
    T-12927  Added a new option to hide records between two dates in Reportbuilder date filters
    T-12929  Allowed more user profile fields to be used in Facetoface notifications via substitution
    T-13104  Changed Facetoface session duration to be stored in seconds instead of minutes
    T-13221  Added visibility controls to Appraisal redisplay questions
    T-13303  Introduced rb_display_nice_datetime_in_timezone method to complete timezone function suite
    T-13335  Improved main menu JS dependencies
    T-13338  Removed Recaptcha login protection in lieu of the new user lockout support
    T-13363  Upgraded Totara cron tasks to scheduled tasks
    T-13370  Changed Fusion Reportbuilder export to be longer enabled by default
    T-13383  Prevented admin redirection to enrolment page after course creation
    T-13408  Improved flexibility of which courses need to be completed to complete a Program courseset
    T-13526  Added a debug option to embedded reports
    T-13527  Added new Reportbuilder nice_date column format for displaying date information
    T-13575  Converted set_time_limit calls to use core_php_time_limit::raise instead
    T-13604  Improved handling of Totara user events to match Moodle behaviour
    T-13605  Added support for Totara-specific post upgrade steps
    T-13608  Added display of Standard Totara footer on popup pages
    T-13615  Added performance improvements for bulk user enrolments and role assignments
    T-13643  Removed W3C deprecated HTML tags
    T-13690  Added ability to specify password rotation as part of the password policy
    T-13716  Added ability to prevent cancellations within a certain time of a Facetoface session date
    T-13762  Fixed Reportbuilder file exports to now use parent folder permissions.
    T-13789  Removed unused function prog_get_all_users_programs
    T-13838  Added ability to withdraw from pending enrolments within the enhanced catalogue
    T-13980  Increased maximum database table name length
    T-14053  Improved timezone handling across the entire codebase

Accessibility and usability improvements:
    T-11919  Cleaned up nested HTML tables in toolbars
    T-12013  Improved the display of links in the Kiwifruit Responsive theme
    T-12332  Fixed current item in the navbar being incorrectly shown as a link
    T-12333  Added headings to many pages where there was no heading
    T-12334  Improved contrast of help icons
    T-12340  Converted labels with no associated input to more appropriate elements
    T-12350  Added a heading to the delete Learning Plan page
    T-12354  Added a heading to the form used to create a Learning Plan
    T-12356  Improved alt text for the date selector
    T-12358  Improved title attribute on the tab name when editing a Learning Plan
    T-12359  Increased contrast of notification messages for usability
    T-12367  Fixed positioning of the Save and Cancel buttons in hierarchy dialogues
    T-12374  Fixed multiple h1 elements on the my reports page
    T-12380  Added a heading to the delete scheduled report page
    T-12389  Removed incorrect fieldset HTML elements on Program content tab
    T-12390  Added missing title to the launch course column when viewing an assigned Program
    T-12398  Changed display of ical image to text, and increased the contrast, when viewing the calendar
    T-12415  Converted calendar export to use mforms
    T-12418  Added a heading to the file upload dialogue
    T-12423  Fixed keyboard navigation on user positions page
    T-12424  Fixed the labels for start and end date on the user positions pages
    T-12685  Converted add new user link to a button on the browse users page
    T-13011  Removed HTML tables from Message alerts
    T-13016  Cleaned up nested HTML tables in the alerts report
    T-13702  Converted to mforms date selector when creating Learning Plans
    T-13076  Converted to mforms date selector when creating new Audiences
    T-13089  Converted to mforms date selector when creating and editing Programs
    T-13304  Improved display of instant filtering in Reportbuilder
    T-13360  Improved the display markup of the users current session in a Facetoface module
    T-13362  Removed nested HTML tables in Facetoface events
    T-13369  Improved Facetoface block session filter HTML
    T-13515  Improved HTML layout of calendar filters in Facetoface block
    T-13609  Improved HTML layout when viewing messages
    T-13614  Improved HTML layout when viewing course groups
    T-13705  Fixed positioning of cancel and save buttons
    T-13728  Improved heading hierarchy on required learning pages
    T-13741  Improved HTML layout of web services documentation
    T-13791  Removed HTML table when viewing another user's Required Learning
    T-13804  Added a label to all duration based admin settings
    T-13949  Improved the alt text on images in the statistics block
    T-13999  Added missing labels to time selectors in admin settings

Feature details
===============

== Main menu customisations (T-10087, T-13514, T-13715, T-13721) ==
The following improvements have been made to the main menu:
1. It is now possible to customise the main menu by adding and removing your own items.
   Existing items cannot at present be deleted, as they may be relied upon for navigation.
2. The placement (sorting) of all items in the menu can be customised to your liking.
3. The visibility of all menu items can be controlled. You can both mark an item either hidden/visible or you can configure
   rules to determine the visibility at the time the page is displayed for the current user.
4. User placeholders can be used in the URLs of custom menu items. These get replaced at run time with the data of the current user.
5. At any point the customised main menu can be reset, returning it to the default state.

== Dynamic appraisals (T-11185) ==
This feature consists of two main changes:
1. When a user is added or removed from a group (position/organisation/audience) that is assigned to an active appraisal, the
   appraisals assignments will mirror this change, assigning or removing the user from the appraisal instead of the assignments
   being locked on the appraisal's activation.
2. When a user's role (manager/teamlead/appraiser) is changed or deleted mid-appraisal the change will be mirrored in the appraisal
   roles instead of locked on the appraisals activation.

Upgraded sites will have to enable this feature by ticking the "Dynamic Appraisals" checkbox on the "Advanced features" site
administration page. It is enabled by default for new installations.

A more detailed description of the changes can be found here: https://community.totaralms.com/mod/forum/discuss.php?d=9563

== New aggregation and grouping column options in Reportbuilder reports (T-12772) ==
This new feature allows administrators to create new reports that aggregate rows using different functions.
There are also new configurable display options.
Developers need to update 3rd party reports to include data types for each column, otherwise the new display and aggregation options
will not be available in report configuration interface.

== Facetoface course enrolment plugin (T-13092) ==
The plugin is disabled by default. When enabled it can be added to any course as an enrolment instance.
Users not already enrolled in the course may then enrol in a Facetoface session within the course and as part of the process an
enrolment record will be created and they will be given access to the course.

== Facetoface custom fields (T-13718, T-11744) ==
Its now possible to add custom fields to three places in the Facetoface module.
There are significant changes to the session, signup and cancellation screens in order to accommodate these changes.
The following areas now custom fields:
* Sessions - included when adding and editing sessions. Shown on the session signup page.
* Signups - included when the user signs up to a sessions. Shown when viewing the attendees and in the Facetoface session report.
* Cancellations - including when cancelling a signup to a session. Shown when viewing cancellations and in the Facetoface session
  report.

== T-13732  Audience based dashboards ==
Administrators can now create audience based dashboards.
These dashboards operate just like the My Learning page in that a default can be created and then each user in the assigned
audiences can customise their dashboard to suit their needs.
As many dashboards can be created as desired.
The default home page for a Totara site can now be set to a dashboard and users can also be given the option of choosing a particular
dashboard as their home page.

Database schema changes
=======================

New tables:

Bug ID   New table name
-----------------------------
T-10087  totara_navigation
T-11185  appraisal_role_changes
T-12902  facetoface_interest
T-13092  enrol_totara_f2f_pending
T-13502  report_builder_graph
T-13715  totara_navigation_settings
T-13718  facetoface_session_info_field
T-13718  facetoface_session_info_data
T-13718  facetoface_session_info_data_param
T-13718  facetoface_signup_info_field
T-13718  facetoface_signup_info_data
T-13718  facetoface_signup_info_data_param
T-13718  facetoface_cancellation_info_field
T-13718  facetoface_cancellation_info_data
T-13718  facetoface_cancellation_info_data_param

New fields:

Bug ID   Table name                New field name
------------------------------------------------------------
T-11185  appraisal_user_assignment status
T-11185  appraisal_role_assignment timecreated
T-12772  report_builder_columns    transform
T-12772  report_builder_columns    aggregate
T-12772  report_builder_cache      queryhash
T-12892  facetoface_sessions       mincapacity
T-12892  facetoface_sessions       cutoff
T-12900  facetoface_sessions       selfapproval
T-12900  facetoface                selfapprovaltandc
T-12902  facetoface                declareinterest
T-12902  facetoface                interestonlyiffull
T-13195  facetoface_sessions       waitlisteveryone
T-13502  report_builder            timemodified
T-13502  report_builder_saved      timemodified
T-13715  totara_navigation         visibilityold
T-12450  facetoface                allowcancellationsdefault
T-12450  facetoface_sessions       allowcancellations
T-13408  prog_courseset            mincourses
T-13408  prog_courseset            coursesumfield
T-13408  prog_courseset            coursesumfieldtotal
T-13716  facetoface                cancellationscutoffdefault
T-13716  facetoface_sessions       cancellationcutoff

Modified fields:

Bug ID   Table name                Field name
--------------------------------------------------------
T-13718  facetoface_notice_data    data     Changed to text

Dropped tables:

Bug ID   Table name
-------------------------
T-13718  facetoface_session_field
T-13718  facetoface_session_data

Dropped fields:

Bug ID   Table name                Field name
--------------------------------------------------------
T-12772  report_builder_cache      config
T-12772  report_builder_settings   cached value


API changes
===========

== T-9800 Reportbuilder filters can now include the JS they require ==
* Newly introduce rb_filter_type::include_js allows filters to include any JS they require.

== T-11185  New dynamic appraisals advanced feature ==
* totara_assign_core::store_user_assignments - two new optional arguments $newusers (arg 1) and $processor (arg 2), both default to
  null.
* totara_assign_core::get_current_users - new argument $forcegroup (arg 4)
* totara_setup_assigndialogs - one new optional argument (arg 1) The html output of a notice to display on change
* totara_appraisal_renderer::confirm_appraisal_activation - new argument $warnings (arg 1)

== T-12199     New audience based visibility setting for courses, programs and certifications ==
* totara_visibility_where - new optional argument $fieldvisible (arg 3) defaults to course.id.
* rb_base_source::add_course_table_to_joinlist - new optional argument $jointype (arg 4) defaults to LEFT

== T-13104  Facetoface session duration is now stored in seconds instead of minutes ==
* Facetoface_sessions.duration is now stored in seconds instead of in minutes.

== T-13221  Added visibility controls to Appraisal redisplay questions ==
* New method question_base::inherits_permissions; classes extending question_base can now optionally override inherits_permissions
  and return true if the question class should inherit its permissions from another question.
  Initially used for the redisplay question type as it should inherit its permissions from the earlier question it is displaying.

== T-13303  Introduced rb_display_nice_datetime_in_timezone method to complete timezone function suite ==
* new rb_base_source::rb_display_nice_datetime_in_timezone method

== T-13338  Recaptcha login protection has been removed in lieu of the new user lockout support ==
* login_forgot_password_form::captcha_enabled method has been removed. It is no longer used.

== T-13363  Upgraded Totara cron tasks to scheduled tasks ==
The following functions have been removed:
* registration_cron
* totara_core_cron
* block_totara_stats::cron method has been removed.
* reminder_cron
* facetoface_cron
* totara_appraisal_cron
* totara_certification_cron
* tcohort_cron
* totara_cohort_cron
* totara_hierarchy_cron
* totara_message_install
* totara_message_cron
* totara_plan_cron
* totara_program_cron
* totara_reportbuilder_cron

The following files have been removed. If you have cron set up to call any of these you will need to update your cron configuration.
* admin/tool/totara_sync/run_cron.php
* totara/appraisal/cron.php
* totara/appraisal/runcron.php
* totara/certification/cron.php
* totara/cohort/cron.php
* totara/hierarchy/prefix/competency/cron.php
* totara/hierarchy/prefix/goal/cron.php
* totara/message/cron.php
* totara/plan/cron.php
* totara/program/cron.php
* totara/reportbuilder/cron.php
* totara/reportbuilder/runcron.php

== T-13393  New setting to log user out of all sessions when their password is changed ==
* \core\session\manager::kill_user_sessions - new optional argument $keepsid (arg 2) keep the given session id alive. If not
 * provided all sessions belonging to the user will be ended.

== T-13502  Implement graphical reporting in Reportbuilder ==
It is now possible to add graphs to reports. The graphs from each report may be also displayed as page blocks.
Developers need to update 3rd party reports to include date types for each numerical column, otherwise the columns will not be
available when setting up graphs in reportbuidler interface.

== T-13527  Implemented report build date column format ==
This change introduces a new nice date list display class and removes remaining uses of the deprecated rb_display_nice_date function.

== T-13605  added support for Totara specific post upgrade steps ==
It is now possible to define Totara specific post upgrade steps.
These steps should be defined within db/totara_postupgrade.php.
A single method should exist within this file xmldb_pluginname_totara_postupgrade.

== T-13714  New percentage aggregation option in the Reportbuilder ==
* New \totara_reportbuilder\rb\aggregate\percent and \totara_reportbuilder\rb\display\percent classes.
* New boolean datatype for report source columns. Existing columns have been converted where required.
* rb_base_source::rb_display_percent function removed as we have now got dedicated type, aggregate and display for percentages.

== T-13721  User placeholders can now be used in main menu URL's ==
* \totara_core\totara\menu\item::get_url - new optional argument $replaceparams (arg 1) when true (default) params in the URL will
  be replaced with relevant data.

== T-13604  Cleanup Totara user events ==
* \totara_core\event\user_firstlogin was removed, use standard \core\event\user_loggedin
  event instead, in case of first login $USER->firstaccess and $USER->currentlogin are equal.

== T-13604  Cleanup Totara user events ==
* \totara_core\event\user_enrolment was removed, use standard \core\event\user_enrolment_created
  event instead

== T-13711  Non numeric columns are no longer shown as graph data sources in Reportbuilder ==
* rb_base_source::get_used_components - new method that should be overridden to return an array of frankenstyle components used by
  the current source and all parents.
* totara_reportbuilder\rb\aggregate\base::is_graphable - new method that should be overridden by all aggregate classes and should
  return true if the given column can be graphed or false otherwise. By default it runs null and Reportbuilder will guess.
* totara_reportbuilder\rb\display\base::is_graphable - new method that should be overridden by all display classes and should return
  true if the given column can be graphed or false otherwise. By default it runs null and Reportbuilder will guess.
* totara_reportbuilder\rb\transform\base::is_graphable - new method that should be overridden by all transform classes and should
  return true if the given column can be graphed or false otherwise. By default it runs null and Reportbuilder will guess.
* New column option “graphable” can be set to true if the column being defined is graphable.

== T-13789  Remove unused function prog_get_all_users_programs ==
* prog_get_all_users_programs function was removed.

== T-13980  Increase maximum database table name length ==
This change increases the maximum database table name length from 28 to 40.
This facilitates better table naming in Totara.

Other notable changes
=====================

T-12772  Imported SVGGraph 2.16 third party library into Totara
T-13092  New enrol_totara_facetoface plugin added to Totara.
T-13407  New report_usersessions plugin added to Totara.
T-13732  New Totara component totara_dashboard
T-13732  New block_totara_dashboard plugin added to Totara.
T-14081  We strongly advise anyone not already using InnoDB or XtraDB to convert to one of these.
T-14081  InnoDB should be configured to use the Barracuda file format and to use one file per table.

*/
